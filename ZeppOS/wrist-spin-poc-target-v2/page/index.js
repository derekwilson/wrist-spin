import { getText } from '@zos/i18n'
import { createWidget, widget, prop, align, text_style } from '@zos/ui'
import { log as Logger, px } from '@zos/utils'
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import { DEVICE_WIDTH, DEVICE_HEIGHT } from "../utils/config/device";
import * as literals from "../common/literals";

const logger = Logger.getLogger("wrist-spin-poc");
const { messageBuilder } = getApp()._options.globalData;

Page({
  state: {
    scrollList: null,
    currentMatch: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, [])
  },
  onInit() {
    logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onInit`);
    this.getMatchList();
    setInterval(() => {
      logger.debug(`APP - ***interval***`);
      this.getMatchList()
    }, literals.DEFAULT_TIMER_TICK_INTERVAL);
    setWakeUpRelaunch({
      relaunch: true,
    })
    logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onInit - complete`);
  },
  build() {
    //console.log(getText('example'))
    logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - build`);

    const initialList = [
      { selection_game_id: -1, team_1: 'Loading...' }
    ];

    this.state.scrollList = createWidget(widget.SCROLL_LIST, {
      x: px(0),
      y: px(70),
      h: DEVICE_HEIGHT - px(70),
      w: DEVICE_WIDTH,
      item_space: px(5),
      item_config: [
        {
          type_id: 1,
          item_bg_color: 0xD80000,
          item_bg_radius: px(10),
          text_view: [
            { x: 0, y: 0, w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_1', color: 0xffffff, text_size: px(30), action: true },
            { x: 0, y: px(28), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'v', color: 0xffffff },
            { x: 0, y: px(56), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_2', color: 0xffffff, text_size: px(30), action: true },
          ],
          text_view_count: 3,
          item_height: px(96)
        },
        {
          type_id: 2,
          item_bg_color: 0x000000,
          item_bg_radius: px(10),
          text_view: [
            { x: 0, y: 0, w: DEVICE_WIDTH - px(30) * 2, h: px(40), key: 'team_1', color: 0xffffff, text_size: px(20), action: true },
            { x: 0, y: px(40), w: DEVICE_WIDTH - px(30) * 2, h: px(40), key: 'v', color: 0xffffff },
            { x: 0, y: px(80), w: DEVICE_WIDTH - px(30) * 2, h: px(40), key: 'team_2', color: 0xffffff, text_size: px(20), action: true },
          ],
          text_view_count: 3,
          item_height: px(120)
        }
      ],
      item_config_count: 2,
      data_array: initialList,
      data_count: initialList.length,
      item_click_func: (list, index, data_key) => {
        logger.log(`wrist-spin-poc click index=${index} dataKey=${data_key}`);
      },
      data_type_config: [
        {
          start: 0,
          end: initialList.length,
          type_id: 1
        },
      ],
      data_type_config_count: 1,
    })

  },
  onDestroy() {
    logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - destroy`);
    localStorage.setItem(literals.STORAGE_KEY_CURRENT_MATCH, this.state.currentMatch)
  },
  getMatchList() {
    messageBuilder.request({
      method: literals.ACTION_SELECT_REFRESH_LIST,
//        method: literals.ACTION_REFRESH,
      matchId: 1431085,
//        dataProvider: literals.PROVIDER_CRICINFO,
      dataProvider: literals.PROVIDER_CRICINFO_2,
    })
    .then((data) => {
      logger.log("receive data");
      const { result = {} } = data;
      //const text = JSON.stringify(result);
      //logger.log(`data = ${text}`);

      const matches = result.matches;
      logger.log(`data items = ${matches.length})`);

      const mappedData = matches.map((thisMatch) => ({team_1 : thisMatch.team_1, v: "v", team_2 : thisMatch.team_2 }));
      logger.log(`mapped data items = ${mappedData.length})`);
  
      this.updateList(mappedData)
    .catch((res) => { });
    })
  },
  updateList(matchList) {
    const { scrollList, currentMatch } = this.state
    logger.log(`updateList with ${matchList.length} items`);
    scrollList.setProperty(prop.UPDATE_DATA, {
      data_type_config: [
        {
          start: 0,
          end: matchList.length,
          type_id: 1
        },
      ],
      data_type_config_count: 1,
      data_array: matchList,
      data_count: matchList.length,
      on_page: 1
    });
    logger.log(`updateList completed`);
  },

})

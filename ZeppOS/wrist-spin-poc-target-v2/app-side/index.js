import { gettext } from 'i18n'
import { MessageBuilder } from "../shared/message-side";

import * as cricket from "./companion-cricket";
import * as literals from "../common/literals";
import * as logging from "../common/logging";

const messageBuilder = new MessageBuilder();

AppSideService({
  onInit() {
    //console.log(gettext('example'))
    console.log(`onInit: wrist-spin-poc v${literals.APP_VERSION} started`);
    //logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
    logging.set_logging_level(logging.LOGGING_LEVEL_VERBOSE);
    cricket.initialize();
    messageBuilder.listen(() => { });

    messageBuilder.on("request", (ctx) => {
      logging.debug("request");
      const jsonRpc = messageBuilder.buf2Json(ctx.request.payload);
      logging.debug("request " + jsonRpc.method);
      return cricket.perform_action(ctx, jsonRpc.method, jsonRpc.matchId, jsonRpc.dataProvider);
    });
  },

  onRun() {
    logging.debug("onRun");
  },

  onDestroy() {
    logging.debug("onDestroy");
  }
})

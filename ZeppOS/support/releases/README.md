# wrist-spin Release Archive

This is the release archive for wrist-spin

## Changelog

Major changes for each version

# v1.0.3 (Build 15) 17th Oct 2024
- Attempt to restore page layout by using the side button

# v1.0.2 (Build 12) 25th Sep 2024
- Added cache for the match selection page
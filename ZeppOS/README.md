# wrist-spin

A live cricket score notifier for ZeppOS / Amazfit devices

This app displays the score for a selected cricket match. A variety of sizes and detail are available. This is a port of the original Fitbit App

![Screen](support/screenshots/screen1.png)
![Screen](support/screenshots/screen2.png)

## Requirement

- NVM
- Node.js 16.+
- NPM
- Zeus CLI

See instructions here

https://docs.zepp.com/docs/guides/best-practice/Basic-environment-construction/#installing-nodejs

### NVM

Different SDKs need different versions of Node so we need to install NVM - see

https://github.com/coreybutler/nvm-windows

https://github.com/coreybutler/nvm-windows/releases

I have installed - v1.1.12

### Node

Install

```
nvm install --lts
```

My installations

```
nvm ls

  * 20.15.1 (Currently using 64-bit executable)
    10.16.0

node --version
v20.15.1

npm --version
10.7.0
```

### Zeus CLI

Install

```
npm i @zeppos/zeus-cli -g
```

My installation

```
zeus -v
1.5.22 (zpm v3.0.30)
```

## Build

### Simulator

When debugging and developing then use the simulator. Download from here

https://docs.zepp.com/docs/guides/tools/simulator/download/

Then run the simulator and download simulator images for the devices needed see

https://docs.zepp.com/docs/guides/tools/simulator/

Then from the command line do this

```sh
$ git clone https://bitbucket.org/derekwilson/wrist-spin.git
$ cd wrist-spin/ZeppOS/wrist-spin
$ npm install
$ zeus dev
```

### Device

Install the Zepp App on the phone, get a device and pair it with the phone

Enable developer mode in the Zepp app - see here

https://docs.zepp.com/docs/guides/tools/zepp-app/

its just Go to "Profile" => "Settings" => "About" and click the Zepp icon 7 times in a row until a pop-up window appears

Then 

```sh
$ git clone https://bitbucket.org/derekwilson/wrist-spin.git
$ cd wrist-spin/ZeppOS/wrist-spin
$ zeus preview
```

Then "Profile" => "My Device" => "Developer Mode" => "+" => "Scan"

You can also use bridge mode.

## Test

There is a test harness for the cricket javascript parser

Its in a folder call `test`, you will need to install and run a HTTP server using the provided scripts and then navigate to

http://127.0.0.1:8080/test.html

Also to make it work you will need to use Firefox and the CORS Everywhere add-on

https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search


## Release

1. **wrist-spin**. the app with a cricinfo decoder that targets ZeppOS v3, HTML/XML/JSON processing is done on the phone (app-side)

The developer console is here

https://console.zepp.com/#/service/app/audit/list/app

## POC

There are proof of concept apps

1. **wrist-spin-poc-target-v2**. a POC with a cricinfo decoder that targets ZeppOS v2
1. **wrist-spin-poc**. a POC with a cricinfo decoder that targets ZeppOS v3, HTML/XML/JSON processing is done on the phone app-side
1. **wrist-spin-svc**. a POC with a cricinfo decoder that targets ZeppOS v3 using a background service, HTML/XML/JSON processing is done on the watch, either in the app or the service

### Target OS

There are multiple major versions of ZeppOS: v1, v2, v3 and v4. The target for an app will depend on the device you are deploying to. The supported OS versions for devices is here

https://docs.zepp.com/docs/reference/related-resources/device-list/

When targetting v3 the app also uses the ZML library

https://github.com/zepp-health/zml


import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import * as packageInfo from "../libs/package"

import * as Styles from "zosLoader:./style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

Page(
  BasePage({
    state: {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0)
    },
    onCreate(e) {
      logger.debug(`about page - onCreate`);
    },
    onInit() {
      logger.debug(`about page - onInit`);
    },
    build() {
      logger.debug(`about page - build`);
      hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.PAGE_TEXT_BLOCK_STYLE,
        text: `wrist-spin-poc\n${packageInfo.getVersionForDisplay()}\nBuild ${literals.APP_VERSION}\nMatch: ${this.state.currentMatchId}`,
      });
      logger.debug(`about page - packageInfo ${packageInfo.getPackageInfoText()}`);
    },
  })
);

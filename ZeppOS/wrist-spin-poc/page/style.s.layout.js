import hmUI from "@zos/ui";
import { px } from "@zos/utils";
import { DEVICE_HEIGHT, DEVICE_WIDTH } from "../libs/utils";

export const MAIN_TITLE_STYLE = {
  x: px(40),
  y: px(60),
  w: DEVICE_WIDTH - px(40) * 2,
  h: px(100),
  text_size: px(32),
  align_h: hmUI.align.CENTER_H,
  color: 0xffffff,
};

export const PAGE_TEXT_BLOCK_STYLE = {
  x: px(20),
  y: px(70),
  w: DEVICE_WIDTH - px(20) * 2,
  h: DEVICE_HEIGHT - px(70),
  text_size: px(32),
  align_h: hmUI.align.CENTER_H,
  color: 0xffffff,
};

export const MAIN_BUTTON_W = px(250);
export const MAIN_BUTTON_H = px(45);
export const MAIN_BUTTON_X = (DEVICE_WIDTH - MAIN_BUTTON_W) / 2;
export const MAIN_BUTTON_Y = px(70);
export const MAIN_BUTTON_OY = px(50);
export const MAIN_BUTTON = {
  x: MAIN_BUTTON_X,
  y: MAIN_BUTTON_Y,
  w: MAIN_BUTTON_W,
  h: MAIN_BUTTON_H,
  radius: 8,
  text_size: px(25),
  press_color: 0x1976d2,
  normal_color: 0xef5350,
};

export const SELECT_GAME_TEXT = [
    { x: 10, y: 0, w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_1', color: 0xffffff, text_size: px(30), action: true },
    { x: 10, y: px(28), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'v', color: 0xffffff },
    { x: 10, y: px(56), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_2', color: 0xffffff, text_size: px(30), action: true },
];

export const SELECT_GAME_ROW = {
  type_id: 1,
  item_bg_color: 0xD80000,
  item_bg_radius: px(10),
  text_view: SELECT_GAME_TEXT,
  text_view_count: 3,
  item_height: px(96)
}

export const SELECT_GAME_ROW_SELECTED = {
  type_id: 2,
  item_bg_color: 0x00D800,
  item_bg_radius: px(10),
  text_view: SELECT_GAME_TEXT,
  text_view_count: 3,
  item_height: px(96)
}

export const SELECT_GAME_LIST = {
  x: px(0),
  y: px(70),
  h: DEVICE_HEIGHT - px(70),
  w: DEVICE_WIDTH,
  item_space: px(5),
  item_config: [
    SELECT_GAME_ROW,
    SELECT_GAME_ROW_SELECTED
  ],
  item_config_count: 2,
};



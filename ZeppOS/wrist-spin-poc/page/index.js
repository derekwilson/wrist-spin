import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { push } from "@zos/router";

import * as packageInfo from "../libs/package"
import * as Styles from "zosLoader:./style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

Page(
  BasePage({
    state: {},
    onCreate(e) {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onCreate`);
    },
    onInit() {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onInit`);
      hmUI.updateStatusBarTitle(packageInfo.getAppName());
    },
    onShow() {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onShow`);
    },
    build() {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - build`);
      const featureArray = [
        { name: "Display Match", url: "page/display-match" },
        { name: "Select Match", url: "page/select-match" },
        { name: "Settings", url: "page/settings" },
        { name: "About", url: "page/about" },
      ];
      featureArray.forEach((feature, index) => {
        let height = Styles.MAIN_BUTTON_H;
        let ypos = Styles.MAIN_BUTTON_Y + (Styles.MAIN_BUTTON_OY * index);
        if (index > 0) {
          ypos = ypos + Styles.MAIN_BUTTON_H
        }
        hmUI.createWidget(hmUI.widget.BUTTON, {
          ...Styles.MAIN_BUTTON,
          y: ypos,
          h: (index == 0 ? Styles.MAIN_BUTTON_H * 2 : Styles.MAIN_BUTTON_H),
          text_size: (index == 0 ? px(30) : px(25)),
          text: feature.name,
          click_func: function (button) {
            logger.debug(`click - url ${feature.url}`);
            push({
              url: feature.url,
            });
          },
        });
      });
    },
    onHide() {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onHide`);
    },
    onDestroy() {
      logger.debug(`wrist-spin-poc v${literals.APP_VERSION} APP - onDestroy`);
    },
  })
);

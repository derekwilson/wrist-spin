import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { back } from "@zos/router";
import * as listUtils from "../libs/listUtils"

import * as Styles from "zosLoader:./style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);
const SELECTED_IMG = 'selected.png'
const UNSELECTED_IMG = 'unselected.png'

Page(
  BasePage({
    state: {
      scrollList: null,
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2)
    },
    onCreate(e) {
      logger.debug(`data-source page - onCreate`);
    },
    onInit() {
      logger.debug(`data-source page - onInit`);
    },
    build() {
      logger.debug(`data-source page - build`);

      let providerList = [
        { id: literals.PROVIDER_CRICINFO, name: literals.getDescription(literals.PROVIDER_CRICINFO),  img_src: UNSELECTED_IMG},
        { id: literals.PROVIDER_CRICINFO_2, name: literals.getDescription(literals.PROVIDER_CRICINFO_2), img_src: UNSELECTED_IMG},
        { id: literals.PROVIDER_TEST, name: literals.getDescription(literals.PROVIDER_TEST), img_src: UNSELECTED_IMG}
      ];
      let selectedIndex = listUtils.findSelectedIndex(providerList, this.state.providerId);
      if (selectedIndex == -1) {
        // make sure the providerList has this provider in it
        selectedIndex = 1;
        this.state.providerId = literals.PROVIDER_CRICINFO_2;
      }
      logger.log(`current id = ${this.state.providerId} selected index = ${selectedIndex}`);
      providerList[selectedIndex].img_src = SELECTED_IMG;

      const dataTypeConfig = listUtils.getScrollListDataConfig(selectedIndex, providerList.length, 2, 1)

      this.state.scrollList = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...Styles.RADIO_SELECT_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}`);
          if (providerList != null && index < providerList.length) {
            listUtils.selectId(providerList, providerList[index].id, SELECTED_IMG, UNSELECTED_IMG);
            const updatedDataTypeConfig = listUtils.getScrollListDataConfig(index, providerList.length, 2, 1)
            this.state.scrollList.setProperty(hmUI.prop.UPDATE_DATA, {
              data_type_config: updatedDataTypeConfig,
              data_type_config_count: updatedDataTypeConfig.length,
              data_array: providerList,
              data_count: providerList.length,
              on_page: 1
            });

            logger.log(`setting provider id = ${providerList[index].id}`);
            this.state.providerId = providerList[index].id;
            // TODO we might want to reset the match ID if it does not work with the new provider
            back();
          }
        },
        data_array: providerList,
        data_count: providerList.length,
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
      });
    },
    onDestroy() {
      logger.debug(`data-source page - onDestroy`);
      localStorage.setItem(literals.STORAGE_KEY_PROVIDER, this.state.providerId)
    },
  })
);

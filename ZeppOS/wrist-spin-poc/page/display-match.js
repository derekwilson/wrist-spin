import * as hmUI from "@zos/ui";
import * as page from "@zos/page";
import * as appService from "@zos/app-service";
import { queryPermission, requestPermission } from "@zos/app";
import { log as Logger } from "@zos/utils";
import { px } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import * as sensor from '@zos/sensor';
import { connectStatus } from '@zos/ble'
import * as alarmMgr from '@zos/alarm'

import * as utils from "../libs/utils";

import * as Styles from "zosLoader:./style.[pf].layout.js";
import * as literals from "../common/literals";
import * as alerter from "../common/alert";

const logger = Logger.getLogger(literals.LOGGER_NAME);
let uiTextMainScore = null;
let uiTextMainTime = null;
let uiTextMainOvers = null;
//let uiTextMainStrikerName = null;
//let uiTextMainStrikerScore = null;
//let uiTextMainNonStrikerName = null;
//let uiTextMainNonStrikerScore = null;
let uiTextMainMessage = null;
let uiTextMainStatus = null;

let uiTextDetailsBattingTeam = null;
let uiTextDetailsScore = null;
let uiTextDetailsOvers = null;
let uiTextDetailsTime = null;
let uiTextDetailsStrikerName = null;
let uiTextDetailsStrikerScore = null;
let uiTextDetailsNonStrikerName = null;
let uiTextDetailsNonStrikerScore = null;
let uiTextDetailsBowlerStats = null;
let uiTextDetailsMessage = null;
let uiTextDetailsFact = null;
let uiTextDetailsStatus = null;

// control the page layout, but we are hard-coding, no UI
let hideStatusBar = true;
let vertical = true;

const vibrator = new sensor.Vibrator();
const timeSensor = new sensor.Time();
let lastUpdateTime = Date.now();
let matchStatusRequested = false;
let intervalId = 0;
let alarmId = 0;

const PERMISSION_BACKGROUND_SERVICE = "device:os.bg_service";
const PERMISSION_ALARM = "device:os.alarm";
const serviceFile = "app-service/time_service";

function doVibrate() {
  logger.debug(`alert_vibrate`);
  vibrator.stop();
  //vibrator.start({mode: sensor.VIBRATOR_SCENE_DURATION_LONG});
  vibrator.start({mode: sensor.VIBRATOR_SCENE_NOTIFICATION});
}

Page(
  BasePage({
    state: {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0),
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2),
      refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL),
    },
    onCreate(e) {
      logger.debug(`display-match page - onCreate`);
    },
    onInit(param) {
      logger.debug(`display-match page - onInit param ${param} refresh rate ${literals.getDescription(this.state.refreshRate)}`);

      // remove me
      //this.state.currentMatchId = 1385694;

      this.getMatchStatus(this.state.currentMatchId, this.state.providerId);

      //this.configureAlarm();
      //this.checkPermissionAndStartService();
      this.configureTimeSensor();
      //this.configureInterval();
      
      setWakeUpRelaunch({ relaunch: true })
      // apparently we dont need to restore it if we hide it, in fact it screws up the real device if we do
      hmUI.setStatusBarVisible(!hideStatusBar);
  
      logger.debug(`display-match page - onInit - complete`);
    },
    onShow() {
      logger.debug(`display-match page - onShow`);
    },
    onHide() {
      logger.debug(`display-match page - onHide`);
    },
    onDestroy() {
      logger.debug(`display-match page - onDestroy - scrollTop ${page.getScrollTop()}, swiper ${page.getSwiperIndex()}`);
      //logger.debug(`display-match page - onDestroy - killing timer`);
      //clearInterval(intervalId);
      //logger.debug(`display-match page - onDestroy - killing service`);
      //this.stopService();
      logger.debug(`display-match page - onDestroy - complete`);
    },

    isServiceRunning() {
      let services = appService.getAllAppServices();
      let running = false;
      if (services != null) {
        running = services.includes(serviceFile);
      }
      logger.log("service status %s", running);
      return running;
    },
    checkPermissionAndStartService() {
      const [bgPermission] = queryPermission({permissions: [PERMISSION_BACKGROUND_SERVICE]});
      logger.debug(`display-match bg service permission - ${bgPermission}`);
      if (bgPermission === 0) {
        requestPermission({
          permissions: [PERMISSION_BACKGROUND_SERVICE],
          callback([result]) {
            if (result === 2) {
              this.startService();
            }
          },
        });
      } else if (bgPermission === 2) {
        this.startService();
      }
    },
    startService() {
      logger.log(`=== start service: ${serviceFile} ===`);
      const result = appService.start({
        url: serviceFile,
        param: `service=${serviceFile}&action=start&matchId=${this.state.currentMatchId}&providerId=${this.state.providerId}`,
        complete_func: (info) => {
          logger.log(`startService result: ` + JSON.stringify(info));
        },
      });
      logger.log(`=== start service: result ${result} ===`);
    },
    stopService() {
      logger.log(`=== stop service: ${serviceFile} ===`);
      appService.stop({
        url: serviceFile,
        param: `service=${serviceFile}&action=stop`,
        complete_func: (info) => {
          logger.log(`stopService result: ` + JSON.stringify(info));
        },
      });
    },

    checkPermissionAndConfigureAlarm() {
      const [alarmPermission] = queryPermission({permissions: [PERMISSION_ALARM]});
      logger.debug(`display-match alarms permission - ${alarmPermission}`);
      if (alarmPermission === 0) {
        requestPermission({
          permissions: [PERMISSION_ALARM],
          callback([result]) {
            if (result === 2) {
              this.configureAlarm();
            }
          },
        });
      } else if (alarmPermission === 2) {
        this.configureAlarm();
      }
    },
    configureAlarm() {
      const alarms = alarmMgr.getAllAlarms();
      logger.debug(`display-match alarms - ${JSON.stringify(alarms)}`);
      alarms.forEach((thisAlarm, index) => {
        // kill any existing alarms
        alarmMgr.cancel(thisAlarm);
      });
      alarmId = alarmMgr.set( {
        url: "page/display-match",
        delay: 60,
        param: "ping",
      });
      logger.debug(`display-match alarm id - ${alarmId}`);
      logger.debug(`display-match alarms - ${JSON.stringify(alarmMgr.getAllAlarms())}`);
    },

    configureInterval() {
      intervalId = setInterval(() => {
        logger.debug(`***interval***`);
        this.getMatchStatus(this.state.currentMatchId, this.state.providerId);
      }, this.state.refreshRate);
    },

    configureTimeSensor() {
      logger.debug(`display-match page - configureTimeSensor`);
      timeSensor.onPerMinute(() => {
        var millis = Date.now() - lastUpdateTime;
        logger.log(`**time sensor: ${timeSensor.getHours()}:${timeSensor.getMinutes()}:${timeSensor.getSeconds()}, elapsed ${millis}`);

        const formattedTime = utils.formatTime(timeSensor.getHours(), timeSensor.getMinutes(), true);
        this.setTextField(uiTextMainTime, formattedTime);
        this.setTextField(uiTextDetailsTime, formattedTime);

        // these things can get unset by other apps
        setWakeUpRelaunch({ relaunch: true })
        hmUI.setStatusBarVisible(!hideStatusBar);

        if (millis > this.state.refreshRate) {
          logger.debug(`***tick*** connected=${connectStatus()}`);
          lastUpdateTime = Date.now();

          this.getMatchStatus(this.state.currentMatchId, this.state.providerId);
        }

        logger.log(`**time sensor: end`);

        //this.getYourData();

        /*
        if (!this.isServiceRunning()) {
          logger.debug(`service not running - starting`);
          this.startService(this.state.currentMatchId, this.state.providerId);
        }

        if (millis > this.state.refreshRate) {
          logger.debug(`***tick*** connected=${connectStatus()}`);
          lastUpdateTime = Date.now();

          if (matchStatusRequested) {
            logger.debug(`status already requested - suppressing`);
          } else {
            this.getMatchStatus(this.state.currentMatchId, this.state.providerId);
          }
        }
        */
      });
    },

    getYourData() {
      logger.debug(`getYourData`);
      return this.httpRequest({
        method: 'get',
        url: 'https://www.espncricinfo.com/ci/engine/match/1385694.json',
      })
        .then((result) => {
          logger.log('result.status', result.status)
          logger.log('result.statusText', result.statusText)
          logger.log('result.headers', result.headers)
          logger.log('result.body', result.body)
        })
        .catch((error) => {
          logger.error('error=>', error)
        })
    },

    getMatchStatus(matchId, provider) {
      if (matchId == null || matchId < 1) {
        logger.debug(`getMatchStatus, invalid id ${matchId}`);
        hmUI.showToast({ text: `Match not selected` });
        this.renderGameDisplayError("Match not selected");
        return;
      }
      logger.debug(`getMatchStatus, id ${matchId}`);
      this.renderGameDisplayError("Loading...");
      matchStatusRequested = true;
      this.request({
        method: literals.ACTION_REFRESH,
        matchId: matchId,
        dataProvider: provider,
      })
      .then((data) => {
        logger.log("receive data");
        matchStatusRequested = false;
        const { result = {} } = data;
        //const text = JSON.stringify(result);
        //logger.log(`result = ${text}`);
        this.updateDisplay(result)
      })
      .catch((res) => {
        matchStatusRequested = false;
        if (!utils.isEmptyObject(res)) {
          logger.log("catch error");
          const errorText = JSON.stringify(res);
          logger.log(`error = ${errorText}`);
        }
      });
    },

    build() {
      logger.debug(`display-match page - build`);

      // build pages
      let top_gap = px(0);
      if (!hideStatusBar) {
        // leave room for the status bar
        top_gap = px(70);
      }
      let page_count = 2;
      // this is the page size in pixels - the height for vertical scrolling pages or the width for horizontally scrolling
      let page_size = vertical ? utils.DEVICE_HEIGHT - top_gap : utils.DEVICE_WIDTH;

      hmUI.setScrollView(true, page_size, page_count, vertical);
      const pg_indicator = hmUI.createWidget(hmUI.widget.PAGE_INDICATOR, {
        ...Styles.PAGE_INDICATOR,
        y: top_gap,
      });


      for (let pageIndex = 0; pageIndex < page_count; pageIndex++) {
        let page_base_x_pos = vertical ? 0 : page_size * pageIndex;
        let page_base_y_pos = vertical ? px(10) + top_gap + page_size * pageIndex : px(10) + top_gap;

        switch (pageIndex) {
          case 0:
            this.buildPage0(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
          case 1:
            this.buildPage1(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
        }
      };
      logger.debug(`display-match page - build - complete`);
    },
    buildPage0(base_x, base_y, page_height) {
      logger.debug(`display-match page - build main page: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);
      
      let current_line_y_pos = base_y + px(25) - px(Styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.XTRA_BIG_TEXT_H - Styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);

      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextMainTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
        text: formattedNow,
        color: Styles.COLOUR_RED_BRIGHT,
      });

      // lets leave a gap
      current_line_y_pos = current_line_y_pos + px(25);

      current_line_y_pos = current_line_y_pos + px(Styles.XTRA_BIG_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      /*
      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      const score_dimensions = this.getTextDimensions(Styles.MEDIUM_TEXT_SIZE, "888");
      uiTextMainStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextMainStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainNonStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextMainNonStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });
      */

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.SMALL_TEXT_TOP_PADDING);
      uiTextMainMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextMainStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(Styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },
    buildPage1(base_x, base_y, page_height) {
      logger.debug(`display-match page - build page1: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);

      let current_line_y_pos = base_y - px(Styles.SMALL_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBattingTeam = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.SMALL_TEXT_H - Styles.BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.BIG_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.LEFT,
      });
      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextDetailsTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + (utils.DEVICE_WIDTH / 2),
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.RIGHT,
        text: formattedNow,
        color: Styles.COLOUR_RED_BRIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      //const score_dimensions = this.getTextDimensions(Styles.MEDIUM_TEXT_SIZE, "288 (122)");
      const score_dimensions = this.getTextDimensions(Styles.MEDIUM_TEXT_SIZE, "288");
      uiTextDetailsStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsNonStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsNonStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBowlerStats = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.MEDIUM_TEXT_H - Styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(Styles.SMALL_TEXT_H - Styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsFact = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextDetailsStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...Styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(Styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },
    getTextDimensions(size, text) {
      const { width, height } = hmUI.getTextLayout(text, {
        text_size: size,
        text_width: utils.DEVICE_WIDTH
      })
      return { w: width, h: height }
    },

    updateDisplay(matchStatus) {
      logger.log(`updateDisplay ${matchStatus.score}`);
      if (matchStatus.isError) {
        this.renderGameDisplayError(matchStatus.errorMessage);
        return;
      }

      // work out alerts
      alerter.check_alert_wicket_fall_and_stale_data(matchStatus);
      if (alerter.is_wicket_fallen_alert()) {
        doVibrate();
      }

      // when we get a non error return - clear the error
      this.renderGameDisplayError("");
      if (this.state.providerId == literals.PROVIDER_TEST) {
        this.renderGameDisplayError("status");
      }

      if (!hideStatusBar) {
        hmUI.updateStatusBarTitle(matchStatus.team_batting);
      }
      this.setTextField(uiTextMainScore, matchStatus.score);
      this.setTextField(uiTextMainOvers, matchStatus.overs);
      //this.setTextField(uiTextMainStrikerName, matchStatus.striker_name);
      //this.setTextField(uiTextMainStrikerScore, "" + matchStatus.striker_score);
      //this.setTextField(uiTextMainNonStrikerName, matchStatus.nonstriker_name);
      //this.setTextField(uiTextMainNonStrikerScore, "" + matchStatus.nonstriker_score);
      this.setTextField(uiTextMainMessage, matchStatus.lead);

      this.setTextField(uiTextDetailsBattingTeam, matchStatus.team_batting);
      this.setTextField(uiTextDetailsScore, matchStatus.score);
      this.setTextField(uiTextDetailsOvers, matchStatus.overs);
      
      this.setTextField(uiTextDetailsStrikerName, matchStatus.striker_name);
      this.setTextField(uiTextDetailsStrikerScore, "" + matchStatus.striker_score);
      //this.setTextField(uiTextDetailsStrikerScore, matchStatus.striker_stats);

      this.setTextField(uiTextDetailsNonStrikerName, matchStatus.nonstriker_name);
      this.setTextField(uiTextDetailsNonStrikerScore, "" + matchStatus.nonstriker_score);
      //this.setTextField(uiTextDetailsNonStrikerScore, matchStatus.nonstriker_stats);
      
      this.setTextField(uiTextDetailsBowlerStats, `${matchStatus.bowler_name} ${matchStatus.bowler_stats}`)
      
      this.setTextField(uiTextDetailsMessage, matchStatus.lead);
      this.setTextField(uiTextDetailsFact, matchStatus.fact);

      if (alerter.is_stale()) {
          this.renderGameDisplayError(`${alerter.minutes_since_last_update()} mins old`);
      }
      alerter.reset_alerts();
    },
    renderGameDisplayError (message) {
      this.setTextField(uiTextMainStatus, message);
      this.setTextField(uiTextDetailsStatus, message);
    },
    setTextField(field, value) {
      if (field != null) {
        //logger.debug(`setTextField, value |${value}|`);
        field.setProperty(hmUI.prop.TEXT, value);
      }
    },
  })
);

import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import { back } from "@zos/router";
import { getScrollListDataConfig } from "../libs/listUtils"

import * as Styles from "zosLoader:./style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);
let currentMatchList = null;

Page(
  BasePage({
    state: {
      scrollList: null,
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0),
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2)
    },
    onCreate(e) {
      logger.debug(`select-match page - onCreate`);
    },
    onInit() {
      logger.debug(`select-match page - onInit`);
      setWakeUpRelaunch({ relaunch: true })
      this.getMatchList();
      logger.debug(`select-match page - onInit - complete`);
    },
    build() {
      logger.debug(`select-match page - build`);

      const initialList = [
        { selection_game_id: -1, team_1: 'Loading...' }
      ];
  
      this.state.scrollList = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...Styles.SELECT_GAME_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}`);
          if (currentMatchList != null && index < currentMatchList.length) {
            logger.log(`setting current match id = ${currentMatchList[index].selection_game_id}`);
            this.state.currentMatchId = currentMatchList[index].selection_game_id;
            back();
          }
        },
        data_array: initialList,
        data_count: initialList.length,
        data_type_config: [
          {
            start: 0,
            end: initialList.length,
            type_id: 1
          },
        ],
        data_type_config_count: 1,
      });
    },
    onDestroy() {
      logger.debug(`select-match page - onDestroy`);
      localStorage.setItem(literals.STORAGE_KEY_CURRENT_MATCH, this.state.currentMatchId)
    },
    getMatchList() {
      this.request({
        method: literals.ACTION_SELECT_REFRESH_LIST,
        matchId: this.state.currentMatchId,
        dataProvider: this.state.providerId,
      })
      .then((data) => {
        logger.log("receive data");
        const { result = {} } = data;
        const text = JSON.stringify(result);
        logger.log(`data = ${text}`);
  
        if (result != null && result.matches != null) {
          const matches = result.matches;
          logger.log(`data items = ${matches.length})`);
  
          const mappedData = matches.map((thisMatch) => ({
            selection_game_id : thisMatch.selection_game_id,
            team_1 : thisMatch.team_1, 
            v: "v", 
            team_2 : thisMatch.team_2 
          }));
          logger.log(`mapped data items = ${mappedData.length})`);
      
          this.updateList(mappedData)
        } else {
          logger.log(`no matches`);
          const emptyList = [
            { selection_game_id: -1, team_1: 'No matches' }
          ];
          updateList(emptyList)
        }
      })
      .catch((res) => {
        if (!isEmptyObject(res)) {
          logger.log("error");
          const errorText = JSON.stringify(res);
          logger.log(`error = ${errorText}`);
        }
      });
    },
    findSelectedIndex(id, matchList) {
      if (matchList == null || matchList.length === 0) {
        return -1
      }
      for (var index = 0; index<matchList.length; index++) {
        if (matchList[index].selection_game_id == id) {
          return index;
        }
      }
      return -1;
    },
    updateList(matchList) {
      currentMatchList = matchList;
      const { scrollList, currentMatchId } = this.state;
      logger.log(`updateList with ${matchList.length} items`);
      const selectedIndex = this.findSelectedIndex(currentMatchId, matchList);
      logger.log(`current id = ${currentMatchId} selected index = ${selectedIndex}`);
      const dataTypeConfig = getScrollListDataConfig(selectedIndex, matchList.length, 2, 1)
      scrollList.setProperty(hmUI.prop.UPDATE_DATA, {
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
        data_array: matchList,
        data_count: matchList.length,
        on_page: 1
      });
      logger.log(`updateList completed`);
    },
  })
);

export function getScrollListDataConfig(activeIndex, length, activeId, inactiveId) {
  const dataTypeConfig = []
  if (activeIndex === -1) {
    dataTypeConfig.push({
      start: 0,
      end: length,
      type_id: inactiveId
    })
  } else if (activeIndex === 0) {
    dataTypeConfig.push({
      start: 0,
      end: activeIndex,
      type_id: activeId
    })
    dataTypeConfig.push({
      start: activeIndex + 1,
      end: length,
      type_id: inactiveId
    })
  } else {
    dataTypeConfig.push({
      start: 0,
      end: activeIndex - 1,
      type_id: inactiveId
    })
    dataTypeConfig.push({
      start: activeIndex,
      end: activeIndex,
      type_id: activeId
    })
    dataTypeConfig.push({
      start: activeIndex + 1,
      end: length,
      type_id: inactiveId
    })
  }
  return dataTypeConfig
}

export function findSelectedIndex(list, id) {
  if (list == null || list.length === 0) {
    return -1;
  }
  for (var index = 0; index<list.length; index++) {
    if (list[index].id == id) {
      return index;
    }
  }
  return -1;
}

export function selectId(list, id, selectedImg, unselectedImg) {
  if (list == null || list.length === 0) {
    return;
  }
  for (var index = 0; index<list.length; index++) {
    list[index].img_src = list[index].id == id ? selectedImg : unselectedImg;
  }
}


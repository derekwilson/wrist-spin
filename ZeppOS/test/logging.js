export const LOGGING_LEVEL_ERROR = 0;
export const LOGGING_LEVEL_WARNING = 1;
export const LOGGING_LEVEL_INFO = 2;
export const LOGGING_LEVEL_DEBUG = 3;
export const LOGGING_LEVEL_VERBOSE = 4;

// default log level for all three apps: device, companion and settings
export const DEFAULT_LOG_LEVEL = LOGGING_LEVEL_DEBUG;

let currentLogLevel = LOGGING_LEVEL_DEBUG;

export function set_logging_level (level) {
    currentLogLevel = level;
    _writeLogEntry(`currentLogLevel = ${currentLogLevel}`)
}

function _writeLogEntry(message) {
    console.log(message);
}

export function getCurrentLogLevel() {
    return currentLogLevel;
}

export function getCurrentLogLevelLabel() {
    switch (currentLogLevel) {
        case LOGGING_LEVEL_VERBOSE:
            return "Verbose";
        case LOGGING_LEVEL_DEBUG:
            return "Debug";
        case LOGGING_LEVEL_INFO:
            return "Info";
        case LOGGING_LEVEL_WARNING:
            return "Warning";
        case LOGGING_LEVEL_ERROR:
            return "Error";
        default:
            return "Unknown " + currentLogLevel
    }
}

export function error (message) {
    if (currentLogLevel>=LOGGING_LEVEL_ERROR) {
        _writeLogEntry(message);
    }
}

export function warning (message) {
    if (currentLogLevel>=LOGGING_LEVEL_WARNING) {
        _writeLogEntry(message);
    }
}

export function info (message) {
    if (currentLogLevel>=LOGGING_LEVEL_INFO) {
        _writeLogEntry(message);
    }
}

export function debug (message) {
    if (currentLogLevel>=LOGGING_LEVEL_DEBUG) {
        _writeLogEntry(message);
    }
}

export function verbose (message) {
    if (currentLogLevel>=LOGGING_LEVEL_VERBOSE) {
        _writeLogEntry(message);
    }
}



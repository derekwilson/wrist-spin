import * as literals from "./literals.js";

// an error item
export function new_out_error (message) {
    var out = {
        "isError":true,
        "noPhone": false,
        "errorMessage":message
    };
    return out;
}

// app settings
export function new_settings () {
    var out = {
        "alertWicket":true,
        "alertNoPhone":true,
        "alertExit":false,
        "provider":literals.PROVIDER_CRICINFO,
        "currentMatch":0,
        "currentDisplayPanel":1,
    };
    return out;
}

// match score update
export function new_out_match () {
    var out = {
        "team_batting":"",
        "score":"",
        "overs":"",
        "lead":"",
        "striker_name":"",
        "striker_stats":"",
        "striker_score":"",
        "nonstriker_name":"",
        "nonstriker_stats":"",
        "nonstriker_score":"",
        "bowler_name":"",
        "bowler_stats":"",
        "fact":"",
        "action":""
    };
    return out;
}

// an individual match - for selection
export function new_out_match_select () {
    var out = {
        "selection_game_id":"",
        "team_1":"",
        "team_2":"",
        "action":""
    };
    return out;
}

// a list of matches - for selection
export function new_out_match_select_list () {
    var out = {
        "matches": [],
        "action":""
    };
    return out;
}

// an item in the list of matches
export function new_out_match_select_item () {
    var out = {
        "selection_game_id":"",
        "team_1":"",
        "team_2":"",
    };
    return out;
}

// for dev
export function new_out_dev (action) {
    var out = {
        "isError":false,
        "errorMessage":"",
        "game_id":"123",
        "selection_game_id":"456",
        "team_1":"TEAM1",
        "team_2":"TEAM2",
        "team_batting":"BATTING",
        "score":"888/8",
        "overs":"123.4",
        "lead":"Message",
        "striker_name":"Striker",
        "striker_stats":"199 (122)",
        "striker_score":"999",
        "nonstriker_name":"Nonstriker",
        "nonstriker_stats":"199 (122)",
        "nonstriker_score":"999",
        "bowler_name":"Bowler",
        "bowler_stats":"12.3-12-199-8",
        "fact":"Fact",
        "action":action
    };
    return out;
}


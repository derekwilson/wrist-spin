import { BaseSideService } from "@zeppos/zml/base-side";

import * as cricket from "./companion-cricket";
import * as literals from "../common/literals";
import * as logging from "../common/logging";

AppSideService(
  BaseSideService({
    onInit() {
      console.log(`onInit: wrist-spin v${literals.APP_VERSION} started`);
      logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
      //logging.set_logging_level(logging.LOGGING_LEVEL_VERBOSE);
      cricket.initialize();
    },

    onRequest(req, res) {
      console.log(`onRequest: =====>, ${req.method}, ${req.matchId}, ${req.dataProvider},`);
      if (req.method == literals.ACTION_PING) {
        // handle this locally
        res(null, {result: literals.ACTION_PONG});
        return;
      }
      cricket.perform_action(res, req.method, req.matchId, req.dataProvider);
    },

    onRun() {
      console.log(`onRun: wrist-spin-poc v${literals.APP_VERSION} started`);
    },

    onDestroy() {
      console.log(`onDestroy: wrist-spin-poc v${literals.APP_VERSION} started`);
    },
  })
);


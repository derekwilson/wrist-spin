import { BaseApp } from "@zeppos/zml/base-app";

App(
  BaseApp({
    globalData: {
      launch: true
    },
    onCreate(options) {
      console.log("wrist-spin: app on create invoke");
    },

    onDestroy(options) {
      console.log("wrist-spin: app on destroy invoke");
    },
  })
);

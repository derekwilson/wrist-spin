import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { back } from "@zos/router";

import * as listUtils from "../libs/listUtils"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);
const SELECTED_IMG = 'selected.png'
const UNSELECTED_IMG = 'unselected.png'

Page(
  BasePage({
    state: {
      scrollList: null,
      startPage: localStorage.getItem(literals.STORAGE_KEY_START_PAGE, literals.START_HOME)
    },
    onCreate(e) {
      logger.debug(`start-page page - onCreate`);
    },
    onInit() {
      logger.debug(`start-page page - onInit`);
    },
    build() {
      logger.debug(`start-page page - build`);

      let optionsList = [
        { id: literals.START_HOME, name: literals.getDescription(literals.START_HOME),  img_src: UNSELECTED_IMG},
        { id: literals.START_DISPLAY_MATCH, name: literals.getDescription(literals.START_DISPLAY_MATCH), img_src: UNSELECTED_IMG},
        { id: literals.START_SELECT_MATCH, name: literals.getDescription(literals.START_SELECT_MATCH), img_src: UNSELECTED_IMG},
      ];
      let selectedIndex = listUtils.findSelectedIndex(optionsList, this.state.startPage);
      if (selectedIndex == -1) {
        // make sure the optionsList has this option in it
        selectedIndex = 0;
        this.state.startPage = literals.START_HOME;
      }
      logger.log(`current id = ${this.state.startPage} selected index = ${selectedIndex}`);
      optionsList[selectedIndex].img_src = SELECTED_IMG;

      const dataTypeConfig = listUtils.getScrollListDataConfig(selectedIndex, optionsList.length, 2, 1)

      this.state.scrollList = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...styles.RADIO_SELECT_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}`);
          if (optionsList != null && index < optionsList.length) {
            listUtils.selectId(optionsList, optionsList[index].id, SELECTED_IMG, UNSELECTED_IMG);
            const updatedDataTypeConfig = listUtils.getScrollListDataConfig(index, optionsList.length, 2, 1)
            this.state.scrollList.setProperty(hmUI.prop.UPDATE_DATA, {
              data_type_config: updatedDataTypeConfig,
              data_type_config_count: updatedDataTypeConfig.length,
              data_array: optionsList,
              data_count: optionsList.length,
              on_page: 1
            });

            logger.log(`startPage id = ${optionsList[index].id}`);
            this.state.startPage = optionsList[index].id;
            back();
          }
        },
        data_array: optionsList,
        data_count: optionsList.length,
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
      });
    },
    onDestroy() {
      logger.debug(`start-page page - onDestroy`);
      localStorage.setItem(literals.STORAGE_KEY_START_PAGE, this.state.startPage)
    },
  })
);

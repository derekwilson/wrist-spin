import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import * as packageInfo from "../libs/package"
import * as widget from "../libs/widget"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

let aboutTextWidget = null;

Page(
  BasePage({
    state: {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0)
    },
    onCreate(e) {
      logger.debug(`about page - onCreate`);
    },
    onInit() {
      logger.debug(`about page - onInit`);
    },
    build() {
      logger.debug(`about page - build`);
      aboutTextWidget = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.PAGE_TEXT_BLOCK_STYLE,
        text: this.getAboutText(null),
      });
      this.pingAppSide();
      logger.debug(`about page - packageInfo ${packageInfo.getPackageInfoText()}`);
    },
    getAboutText(extraLine) {
      const fixedText = `wrist-spin\n${packageInfo.getVersionForDisplay()}\nBuild ${literals.APP_VERSION}\nMatch: ${this.state.currentMatchId}`
      if (extraLine != null) {
        return `${fixedText}\n${extraLine}`;
      }
      return fixedText;
    },
    pingAppSide() {
      logger.debug(`pingAppSide`);
      this.request({
        method: literals.ACTION_PING,
      })
      .then((data) => {
        logger.log("receive data");
        const { result = {} } = data;
        const text = JSON.stringify(result);
        logger.log(`result = ${text}`);
        widget.setTextWidget(aboutTextWidget, this.getAboutText("app-side OK"));
        //hmUI.showToast({ text: `Got response from app-side` });
      })
      .catch((res) => {
        if (!utils.isEmptyObject(res)) {
          logger.log("catch error");
          const errorText = JSON.stringify(res);
          logger.log(`error = ${errorText}`);
        }
      });
    },
  })
);

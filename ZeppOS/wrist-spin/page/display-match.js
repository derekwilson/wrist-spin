import * as hmUI from "@zos/ui";
import * as page from "@zos/page";
import { log as Logger } from "@zos/utils";
import { px } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import * as sensor from '@zos/sensor';
import { home } from "@zos/router";
import * as interaction from '@zos/interaction'

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";
import * as alerter from "../common/alert";
import * as utils from "../libs/utils";
import * as widget from "../libs/widget"

const logger = Logger.getLogger(literals.LOGGER_NAME);

// watchface page
let uiTextMainScore = null;
let uiTextMainTime = null;
let uiTextMainOvers = null;
let uiTextMainMessage = null;
let uiTextMainStatus = null;

// details page
let uiTextDetailsBattingTeam = null;
let uiTextDetailsScore = null;
let uiTextDetailsOvers = null;
let uiTextDetailsTime = null;
let uiTextDetailsStrikerName = null;
let uiTextDetailsStrikerScore = null;
let uiTextDetailsNonStrikerName = null;
let uiTextDetailsNonStrikerScore = null;
let uiTextDetailsBowlerStats = null;
let uiTextDetailsMessage = null;
let uiTextDetailsFact = null;
let uiTextDetailsStatus = null;

// control the page layout, but we are hard-coding, no UI
const hideStatusBar = true;
const vertical = true;
const page_count = 2;

const vibrator = new sensor.Vibrator();
const timeSensor = new sensor.Time();
let lastUpdateTime = Date.now();

function doVibrate() {
  logger.debug(`alert_vibrate`);
  vibrator.stop();
  //vibrator.start({mode: sensor.VIBRATOR_SCENE_DURATION_LONG});
  vibrator.start({mode: sensor.VIBRATOR_SCENE_NOTIFICATION});
}

Page(
  BasePage({
    state: {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0),
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2),
      refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL),
      initialDisplayPage: localStorage.getItem(literals.STORAGE_KEY_CURRENT_DISPLAY_MATCH_PAGE, 0),
    },
    onCreate(e) {
      logger.debug(`display-match page - onCreate`);
    },
    onInit(param) {
      logger.debug(`display-match page - onInit param ${param} refresh rate ${literals.getDescription(this.state.refreshRate)}`);

      // remove me
      //this.state.currentMatchId = 1385694;

      this.getMatchStatus(this.state.currentMatchId, this.state.providerId);
      this.configureKeyHandler();
      this.configureTimeSensor();
      
      setWakeUpRelaunch({ relaunch: true })
      // apparently we dont need to restore it if we hide it, in fact it screws up the real device if we do
      hmUI.setStatusBarVisible(!hideStatusBar);
  
      logger.debug(`display-match page - onInit - complete`);
    },
    onShow() {
      logger.debug(`display-match page - onShow`);
    },
    onHide() {
      logger.debug(`display-match page - onHide`);
    },
    onDestroy() {
      // the getter is one based the setter is zero based - its mad but we will always work zero based
      const swiperIndex = page.getSwiperIndex() - 1;
      logger.debug(`display-match page - onDestroy - scrollTop ${page.getScrollTop()}, swiper ${swiperIndex}`);
      if (swiperIndex >= 0 && swiperIndex < page_count) {
        logger.debug(`display-match page - onDestroy - initial page == ${swiperIndex}`);
        localStorage.setItem(literals.STORAGE_KEY_CURRENT_DISPLAY_MATCH_PAGE, swiperIndex)
      }
      interaction.offKey();
      logger.debug(`display-match page - onDestroy - complete`);
    },

    configureKeyHandler() {
      interaction.onKey({
        callback: (key, action) => {

          /* 
          *  key name values 

              KEY_BACK = 8 
              KEY_SELECT = 13 
              KEY_HOME = 36 
              KEY_UP = 38
              KEY_DOWN = 40 
          * 
          * key action values
          * 
              KEY_SHORTCUT = 93 
              KEY_EVENT_CLICK = 1 
              KEY_EVENT_LONG_PRESS = 2 
              KEY_EVENT_DOUBLE_CLICK = 4 
              KEY_EVENT_RELEASE = 8 
              KEY_EVENT_PRESS = 16 
          */

          logger.debug(`display-match page - onKey - key: ${key}, action: ${action}`);

          if (key === interaction.KEY_HOME) {
            // this is the side button on the Bip5
            logger.debug(`display-match page - onKey - KEY_HOME`);
            switch (action) {
              case interaction.KEY_EVENT_CLICK:
                logger.debug(`display-match page - onKey - KEY_HOME - click`);
                // we use this button to restore the screen after a notification has been displayed
                this.restorePageSetup();
                // suppress the default action as it would exit
                return true;
              case interaction.KEY_EVENT_LONG_PRESS:
                logger.debug(`display-match page - onKey - KEY_HOME - long click`);
                // as we suppress the default action - allow exit on long press
                home();
                return true;
              }
          }
  
          return false;
        }
      })
    },

    restorePageSetup() {
        // these things can get unset by other apps
        setWakeUpRelaunch({ relaunch: true })
        hmUI.setStatusBarVisible(!hideStatusBar);
    },

    configureTimeSensor() {
      logger.debug(`display-match page - configureTimeSensor`);
      timeSensor.onPerMinute(() => {
        var millis = Date.now() - lastUpdateTime;
        logger.log(`**time sensor: ${timeSensor.getHours()}:${timeSensor.getMinutes()}:${timeSensor.getSeconds()}, elapsed ${millis}`);

        const formattedTime = utils.formatTime(timeSensor.getHours(), timeSensor.getMinutes(), true);
        widget.setTextWidget(uiTextMainTime, formattedTime);
        widget.setTextWidget(uiTextDetailsTime, formattedTime);

        this.restorePageSetup();

        // this can often be a couple milliseconds short of a minute so allow 100ms slack
        if (millis > (this.state.refreshRate - 100)) {
          logger.debug(`***tick***`);
          lastUpdateTime = Date.now();

          this.getMatchStatus(this.state.currentMatchId, this.state.providerId);
        }

        logger.log(`**time sensor: end`);
      });
    },

    getMatchStatus(matchId, provider) {
      if (matchId == null || matchId < 1) {
        logger.debug(`getMatchStatus, invalid id ${matchId}`);
        hmUI.showToast({ text: `Match not selected` });
        this.renderGameDisplayError("Match not selected");
        return;
      }
      logger.debug(`getMatchStatus, id ${matchId}`);
      this.renderGameDisplayError("Loading...");
      this.request({
        method: literals.ACTION_REFRESH,
        matchId: matchId,
        dataProvider: provider,
      })
      .then((data) => {
        logger.log("receive data");
        const { result = {} } = data;
        //const text = JSON.stringify(result);
        //logger.log(`result = ${text}`);
        this.updateDisplay(result)
      })
      .catch((res) => {
        if (!utils.isEmptyObject(res)) {
          logger.log("catch error");
          const errorText = JSON.stringify(res);
          logger.log(`error = ${errorText}`);
        }
      });
    },

    build() {
      logger.debug(`display-match page - build`);

      // build pages
      let top_gap = px(0);
      if (!hideStatusBar) {
        // leave room for the status bar
        top_gap = px(70);
      }
      // this is the page size in pixels - the height for vertical scrolling pages or the width for horizontally scrolling
      let page_size = vertical ? utils.DEVICE_HEIGHT - top_gap : utils.DEVICE_WIDTH;

      hmUI.setScrollView(true, page_size, page_count, vertical);
      const pg_indicator = hmUI.createWidget(hmUI.widget.PAGE_INDICATOR, {
        ...styles.PAGE_INDICATOR,
        y: top_gap,
      });

      for (let pageIndex = 0; pageIndex < page_count; pageIndex++) {
        let page_base_x_pos = vertical ? 0 : page_size * pageIndex;
        let page_base_y_pos = vertical ? px(10) + top_gap + page_size * pageIndex : px(10) + top_gap;

        switch (pageIndex) {
          case 0:
            this.buildPage0(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
          case 1:
            this.buildPage1(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
        }
      };

      // the resume is called every time the page is woken
      hmUI.createWidget(hmUI.widget.WIDGET_DELEGATE, {
        resume_call: function () {
          logger.debug(`display-match page delegate resume call`);
          logger.debug(`display-match page delegate resume call - end`);
        },
        pause_call: function () {
          logger.debug(`display-match page delegate pause call`);
          logger.debug(`display-match page delegate pause call - end`);
        },
      });

      if (this.state.initialDisplayPage >= 0 && this.state.initialDisplayPage < page_count) {
        logger.debug(`display-match page - build - initial page swiper ${this.state.initialDisplayPage}`);
        page.swipeToIndex({
          index: this.state.initialDisplayPage,
          animation: page.SCROLL_ANIMATION_NONE,
        })
      }

      logger.debug(`display-match page - build - complete`);
    },
    buildPage0(base_x, base_y, page_height) {
      logger.debug(`display-match page - build main page: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);
      
      let current_line_y_pos = base_y + px(25) - px(styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.XTRA_BIG_TEXT_H - styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);

      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextMainTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
        text: formattedNow,
        color: styles.COLOUR_RED_BRIGHT,
      });

      // lets leave a gap
      current_line_y_pos = current_line_y_pos + px(25);

      current_line_y_pos = current_line_y_pos + px(styles.XTRA_BIG_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextMainMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextMainStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },
    buildPage1(base_x, base_y, page_height) {
      logger.debug(`display-match page - build page1: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);

      let current_line_y_pos = base_y - px(styles.SMALL_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBattingTeam = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.SMALL_TEXT_H - styles.BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.BIG_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.LEFT,
      });
      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextDetailsTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + (utils.DEVICE_WIDTH / 2),
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.RIGHT,
        text: formattedNow,
        color: styles.COLOUR_RED_BRIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      const score_dimensions = widget.getTextDimensions(styles.MEDIUM_TEXT_SIZE, utils.DEVICE_WIDTH, "288");
      uiTextDetailsStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsNonStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsNonStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBowlerStats = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.SMALL_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsFact = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextDetailsStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },

    updateDisplay(matchStatus) {
      logger.log(`updateDisplay ${matchStatus.score}`);
      if (matchStatus.isError) {
        this.renderGameDisplayError(matchStatus.errorMessage);
        return;
      }

      // work out alerts
      alerter.check_alert_wicket_fall_and_stale_data(matchStatus);
      if (alerter.is_wicket_fallen_alert()) {
        doVibrate();
      }

      // when we get a non error return - clear the error
      this.renderGameDisplayError("");
      if (this.state.providerId == literals.PROVIDER_TEST) {
        this.renderGameDisplayError("status");
      }

      if (!hideStatusBar) {
        hmUI.updateStatusBarTitle(matchStatus.team_batting);
      }
      widget.setTextWidget(uiTextMainScore, matchStatus.score);
      widget.setTextWidget(uiTextMainOvers, matchStatus.overs);
      widget.setTextWidget(uiTextMainMessage, matchStatus.lead);

      widget.setTextWidget(uiTextDetailsBattingTeam, matchStatus.team_batting);
      widget.setTextWidget(uiTextDetailsScore, matchStatus.score);
      widget.setTextWidget(uiTextDetailsOvers, matchStatus.overs);
      
      widget.setTextWidget(uiTextDetailsStrikerName, matchStatus.striker_name);
      widget.setTextWidget(uiTextDetailsStrikerScore, "" + matchStatus.striker_score);

      widget.setTextWidget(uiTextDetailsNonStrikerName, matchStatus.nonstriker_name);
      widget.setTextWidget(uiTextDetailsNonStrikerScore, "" + matchStatus.nonstriker_score);
      
      widget.setTextWidget(uiTextDetailsBowlerStats, `${matchStatus.bowler_name} ${matchStatus.bowler_stats}`)
      
      widget.setTextWidget(uiTextDetailsMessage, matchStatus.lead);
      widget.setTextWidget(uiTextDetailsFact, matchStatus.fact);

      if (alerter.is_stale()) {
          this.renderGameDisplayError(`${alerter.minutes_since_last_update()} mins old`);
      }
      alerter.reset_alerts();
    },
    renderGameDisplayError (message) {
      widget.setTextWidget(uiTextMainStatus, message);
      widget.setTextWidget(uiTextDetailsStatus, message);
    },
  })
);

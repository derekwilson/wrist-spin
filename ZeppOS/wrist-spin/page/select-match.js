import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import { back } from "@zos/router";
import { px } from "@zos/utils";

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

import * as utils from "../libs/utils";
import * as listUtils from "../libs/listUtils"
import * as widget from "../libs/widget"

const logger = Logger.getLogger(literals.LOGGER_NAME);
let currentMatchList = null;

let statusTextWidget = null;
let generatedTextWidget = null;
let scrollListWidget = null;

Page(
  BasePage({
    state: {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0),
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2)
    },
    onCreate(e) {
      logger.debug(`select-match page - onCreate`);
    },
    onInit() {
      logger.debug(`select-match page - onInit`);
      setWakeUpRelaunch({ relaunch: true })
      this.getMatchList();
      logger.debug(`select-match page - onInit - complete`);
    },
    build() {
      logger.debug(`select-match page - build`);

      const initialList = [
        { id: -1, team_1: 'Loading...' }
      ];
      let storageObject = this.getMatchListFromCache();
      if (storageObject != null) {
        currentMatchList = storageObject.matchListData;
      } else {
        storageObject = {
          generatedDate: Date.now(),
          matchListData: initialList,
        }
      }
  
      statusTextWidget = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: px(0),
        y: px(60),
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.LEFT,
        text: "Loading...",
      });
      generatedTextWidget = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: utils.DEVICE_WIDTH / 2,
        y: px(60),
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.RIGHT,
        text: utils.formatDateTimeStr(storageObject.generatedDate, false),
      });
      
      const selectedIndex = listUtils.findSelectedIndex(storageObject.matchListData, this.state.currentMatchId);
      logger.log(`current id = ${this.state.currentMatchId} selected index = ${selectedIndex}`);
      const dataTypeConfig = listUtils.getScrollListDataConfig(selectedIndex, storageObject.matchListData.length, 2, 1)

      scrollListWidget = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...styles.SELECT_GAME_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}, current match list length = ${currentMatchList.length}`);
          if (currentMatchList != null && index < currentMatchList.length) {
            // set the id in the state
            logger.log(`setting current match id = ${currentMatchList[index].id}`);
            this.state.currentMatchId = currentMatchList[index].id;
            // refresh the list to give some feedback, no need to do the cache
            this.updateList(currentMatchList, false);
            back();
          }
        },
        data_array: storageObject.matchListData,
        data_count: storageObject.matchListData.length,
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
      });
      logger.debug(`select-match page - build - end`);
    },
    onDestroy() {
      logger.debug(`select-match page - onDestroy`);
      // persist the state
      localStorage.setItem(literals.STORAGE_KEY_CURRENT_MATCH, this.state.currentMatchId)
    },

    getMatchList() {
      this.request({
        method: literals.ACTION_SELECT_REFRESH_LIST,
        matchId: this.state.currentMatchId,
        dataProvider: this.state.providerId,
      })
      .then((data) => {
        logger.log("receive data");
        const { result = {} } = data;
        const text = JSON.stringify(result);
        logger.log(`data = ${text}`);
  
        if (result != null && result.matches != null) {
          const matches = result.matches;
          logger.log(`data items = ${matches.length})`);
  
          const mappedData = matches.map((thisMatch) => ({
            id : thisMatch.selection_game_id,
            team_1 : thisMatch.team_1, 
            v: "v", 
            team_2 : thisMatch.team_2 
          }));
          logger.log(`mapped data items = ${mappedData.length})`);
      
          this.updateList(mappedData, true)
        } else {
          logger.log(`no matches`);
          const emptyList = [
            { id: -1, team_1: 'No matches' }
          ];
          this.updateList(emptyList, true)
        }
      })
      .catch((res) => {
        if (!isEmptyObject(res)) {
          logger.log("error");
          const errorText = JSON.stringify(res);
          logger.log(`error = ${errorText}`);
        }
      });
    },
    updateList(matchList, refreshCache) {
      if (refreshCache) {
        this.cacheMatchList(matchList);
      }
      widget.setTextWidget(statusTextWidget, "");
      widget.setTextWidget(generatedTextWidget, utils.formatDateTime(new Date(), false));
      currentMatchList = matchList;
      logger.log(`updateList with ${matchList.length} items`);
      const selectedIndex = listUtils.findSelectedIndex(matchList, this.state.currentMatchId);
      logger.log(`current id = ${this.state.currentMatchId} selected index = ${selectedIndex}`);
      const dataTypeConfig = listUtils.getScrollListDataConfig(selectedIndex, matchList.length, 2, 1)
      scrollListWidget.setProperty(hmUI.prop.UPDATE_DATA, {
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
        data_array: matchList,
        data_count: matchList.length,
        on_page: 1
      });
      logger.log(`updateList completed`);
    },
    cacheMatchList(matchList) {
      logger.log("cacheMatchList");
      lastUpdateTime = Date.now();
      const storageObject = {
        generatedDate: lastUpdateTime,
        matchListData: matchList,
      };
      localStorage.setItem(literals.STORAGE_KEY_MATCH_SELECTION_CACHE, JSON.stringify(storageObject));
      logger.log("cacheMatchList - end");
    },
    getMatchListFromCache() {
      logger.log("getMatchListFromCache");
      // read data from storage
      const storageObjectStr = localStorage.getItem(literals.STORAGE_KEY_MATCH_SELECTION_CACHE, "{}");
      //logger.log(`storageObjectStr = ${storageObjectStr}`);
      if (storageObjectStr != null) {
        try {
          const storageObject = JSON.parse(storageObjectStr);
          logger.debug(`generated = ${storageObject.generatedDate}, ${utils.formatDateTimeStr(storageObject.generatedDate, false)}`);
          if (storageObject.generatedDate == null || storageObject.matchListData == null) {
            return null;
          } else {
            return storageObject;
          }
        } catch (error) {
          logger.log(`ERR: ${error.message}`);
          return null;
        }
      }
    },
  })
);

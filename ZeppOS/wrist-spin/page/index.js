import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { push } from "@zos/router";
import { localStorage } from '@zos/storage'

import * as packageInfo from "../libs/package"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

Page(
  BasePage({
    state: {
      startPage: localStorage.getItem(literals.STORAGE_KEY_START_PAGE, literals.START_HOME),
    },
    onCreate(e) {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onCreate`);
    },
    onInit() {
      let isLaunching = getApp().globalData.launch;
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onInit - ${this.state.startPage}, isLaunching ${isLaunching}`);
      getApp().globalData.launch = false;
      // in case we have mucked about with the title
      hmUI.updateStatusBarTitle(packageInfo.getAppName());
      if (isLaunching) {
        switch (this.state.startPage) {
          case literals.START_DISPLAY_MATCH:
            logger.debug(`auto launch display match`);
            push({url: "page/display-match",});
            break;
          case literals.START_SELECT_MATCH:
            logger.debug(`auto launch select match`);
            push({url: "page/select-match",});
            break;
        }
      }
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onInit - end`);
    },
    onShow() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onShow`);
    },
    build() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - build`);
      const featureArray = [
        { name: "Display Match", url: "page/display-match" },
        { name: "Select Match", url: "page/select-match" },
        { name: "Settings", url: "page/settings" },
        { name: "About", url: "page/about" },
      ];

      const viewContainer = hmUI.createWidget(hmUI.widget.VIEW_CONTAINER, {
        ...styles.BUTTON_VIEW_CONTAINER,
      });

      featureArray.forEach((feature, index) => {
        viewContainer.createWidget(hmUI.widget.BUTTON, {
          ...styles.MAIN_BUTTON,
          y: (index == 0 ? 0 : (styles.MAIN_BUTTON_Y * index) + styles.MAIN_BUTTON_H),
          h: (index == 0 ? styles.MAIN_BUTTON_H * 2 : styles.MAIN_BUTTON_H),
          text_size: (index == 0 ? px(30) : px(25)),
          text: feature.name,
          click_func: function (button) {
            logger.debug(`click - url ${feature.url}`);
            push({url: feature.url,});
          },
        });
      });
    },
    onHide() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onHide`);
    },
    onDestroy() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onDestroy`);
    },
  })
);



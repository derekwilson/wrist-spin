import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { back } from "@zos/router";

import * as listUtils from "../libs/listUtils"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);
const SELECTED_IMG = 'selected.png'
const UNSELECTED_IMG = 'unselected.png'

Page(
  BasePage({
    state: {
      scrollList: null,
      refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL)
    },
    onCreate(e) {
      logger.debug(`refresh-rate page - onCreate`);
    },
    onInit() {
      logger.debug(`refresh-rate page - onInit`);
    },
    build() {
      logger.debug(`refresh-rate page - build`);

      let optionsList = [
        { id: literals.FAST_TIMER_TICK_INTERVAL, name: literals.getDescription(literals.FAST_TIMER_TICK_INTERVAL),  img_src: UNSELECTED_IMG},
        { id: literals.DEFAULT_TIMER_TICK_INTERVAL, name: literals.getDescription(literals.DEFAULT_TIMER_TICK_INTERVAL), img_src: UNSELECTED_IMG},
        { id: literals.SLOW_TIMER_TICK_INTERVAL, name: literals.getDescription(literals.SLOW_TIMER_TICK_INTERVAL), img_src: UNSELECTED_IMG},
        { id: literals.VERY_SLOW_TIMER_TICK_INTERVAL, name: literals.getDescription(literals.VERY_SLOW_TIMER_TICK_INTERVAL), img_src: UNSELECTED_IMG},
      ];
      let selectedIndex = listUtils.findSelectedIndex(optionsList, this.state.refreshRate);
      if (selectedIndex == -1) {
        // make sure the optionsList has this option in it
        selectedIndex = 1;
        this.state.refreshRate = literals.DEFAULT_TIMER_TICK_INTERVAL;
      }
      logger.log(`current id = ${this.state.refreshRate} selected index = ${selectedIndex}`);
      optionsList[selectedIndex].img_src = SELECTED_IMG;

      const dataTypeConfig = listUtils.getScrollListDataConfig(selectedIndex, optionsList.length, 2, 1)

      this.state.scrollList = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...styles.RADIO_SELECT_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}`);
          if (optionsList != null && index < optionsList.length) {
            listUtils.selectId(optionsList, optionsList[index].id, SELECTED_IMG, UNSELECTED_IMG);
            const updatedDataTypeConfig = listUtils.getScrollListDataConfig(index, optionsList.length, 2, 1)
            this.state.scrollList.setProperty(hmUI.prop.UPDATE_DATA, {
              data_type_config: updatedDataTypeConfig,
              data_type_config_count: updatedDataTypeConfig.length,
              data_array: optionsList,
              data_count: optionsList.length,
              on_page: 1
            });

            logger.log(`refreshRate id = ${optionsList[index].id}`);
            this.state.refreshRate = optionsList[index].id;
            back();
          }
        },
        data_array: optionsList,
        data_count: optionsList.length,
        data_type_config: dataTypeConfig,
        data_type_config_count: dataTypeConfig.length,
      });
    },
    onDestroy() {
      logger.debug(`refresh-rate page - onDestroy`);
      localStorage.setItem(literals.STORAGE_KEY_REFRESH_RATE, this.state.refreshRate)
    },
  })
);

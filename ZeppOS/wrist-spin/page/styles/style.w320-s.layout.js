import hmUI from "@zos/ui";
import { px } from "@zos/utils";
import { DEVICE_HEIGHT, DEVICE_WIDTH } from "../../libs/utils";

//
// colours
//

export const COLOUR_BLACK = 0x000000;
export const COLOUR_WHITE = 0xffffff;

export const COLOUR_RED_BRIGHT = 0xFA1E43;
export const COLOUR_RED_MUTED = 0xef5350;

export const COLOUR_GREEN_BRIGHT = 0x00D800;
export const COLOUR_GREEN_MUTED = 0x00AD5F;

export const COLOUR_BLUE = 0x1976d2;
export const COLOUR_DARK_BLUE = 0x125A9E;

//
// general styles
//

export const MAIN_TITLE_STYLE = {
  x: px(40),
  y: px(60),
  w: DEVICE_WIDTH - px(40) * 2,
  h: px(100),
  text_size: px(32),
  align_h: hmUI.align.CENTER_H,
  color: 0xffffff,
};

export const PAGE_TEXT_BLOCK_STYLE = {
  x: px(20),
  y: px(70),
  w: DEVICE_WIDTH - px(20) * 2,
  h: DEVICE_HEIGHT - px(70),
  text_size: px(32),
  align_h: hmUI.align.CENTER_H,
  color: 0xffffff,
};

export const MAIN_BUTTON_W = px(250);
export const MAIN_BUTTON_H = px(45);
export const MAIN_BUTTON_X = (DEVICE_WIDTH - MAIN_BUTTON_W) / 2;
export const MAIN_BUTTON_Y = px(50);
export const MAIN_BUTTON = {
  x: MAIN_BUTTON_X,
  y: MAIN_BUTTON_Y,
  w: MAIN_BUTTON_W,
  h: MAIN_BUTTON_H,
  radius: 8,
  text_size: px(25),
  press_color: COLOUR_BLUE,
  normal_color: COLOUR_RED_BRIGHT,
};

//
// select match
//

export const SELECT_GAME_TEXT = [
    { x: 10, y: 0, w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_1', color: 0xffffff, text_size: px(30), action: true },
    { x: 10, y: px(28), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'v', color: 0xffffff },
    { x: 10, y: px(56), w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'team_2', color: 0xffffff, text_size: px(30), action: true },
];

export const SELECT_GAME_ROW = {
  type_id: 1,
  item_bg_color: COLOUR_RED_BRIGHT,
  item_bg_radius: px(10),
  text_view: SELECT_GAME_TEXT,
  text_view_count: 3,
  item_height: px(96)
}

export const SELECT_GAME_ROW_SELECTED = {
  type_id: 2,
  item_bg_color: COLOUR_BLUE,
  item_bg_radius: px(10),
  text_view: SELECT_GAME_TEXT,
  text_view_count: 3,
  item_height: px(96)
}

export const SELECT_GAME_LIST = {
  x: px(0),
  y: px(90),
  h: DEVICE_HEIGHT - px(70),
  w: DEVICE_WIDTH,
  item_space: px(5),
  item_config: [
    SELECT_GAME_ROW,
    SELECT_GAME_ROW_SELECTED
  ],
  item_config_count: 2,
};

//
// display match
//

export const PAGE_INDICATOR = {
  x: 0,
  y: px(5),
  w: DEVICE_WIDTH,
  h: px(100),
  align_h: hmUI.align.CENTER_H,
  h_space: 8,
  select_src: 'page_select.png',
  unselect_src: 'page_unselect.png',
};

export const DISPLAY_BASE_TEXT = {
  w: DEVICE_WIDTH,
  h: px(30),
  text_size: px(25),
  color: 0xffffff,
  align_h: hmUI.align.CENTER_H,
  line_space: 0,
};


export const XTRA_BIG_TEXT_SIZE = 120;
export const XTRA_BIG_TEXT_H = 165;
export const XTRA_BIG_TEXT_TOP_PADDING = 50;

export const BIG_TEXT_SIZE = 80;
export const BIG_TEXT_H = 110;
export const BIG_TEXT_TOP_PADDING = 30;

export const MEDIUM_TEXT_SIZE = 40;
export const MEDIUM_TEXT_H = 64;
export const MEDIUM_TEXT_TOP_PADDING = 20;

export const SMALL_TEXT_SIZE = 20;
export const SMALL_TEXT_H = 40;
export const SMALL_TEXT_TOP_PADDING = 10;

export const DISPLAY_XTRA_BIG_TEXT = {
  ...DISPLAY_BASE_TEXT,
  h: px(XTRA_BIG_TEXT_H),
  text_size: px(XTRA_BIG_TEXT_SIZE),
};

export const DISPLAY_BIG_TEXT = {
  ...DISPLAY_BASE_TEXT,
  h: px(BIG_TEXT_H),
  text_size: px(BIG_TEXT_SIZE),
};

export const DISPLAY_MEDIUM_TEXT = {
  ...DISPLAY_BASE_TEXT,
  h: px(MEDIUM_TEXT_H),
  text_size: px(MEDIUM_TEXT_SIZE),
};

export const DISPLAY_SMALL_TEXT = {
  ...DISPLAY_BASE_TEXT,
  h: px(SMALL_TEXT_H),
  text_size: px(SMALL_TEXT_SIZE),
};

// 
// button list
//

export const BUTTON_VIEW_CONTAINER = {
  x: px(0),
  y: px(70),
  w: DEVICE_WIDTH,
  h: DEVICE_HEIGHT - px(70),
};

export const LIST_BUTTON_W = px(250);
export const LIST_BUTTON_H = px(60);
export const LIST_BUTTON_X = (DEVICE_WIDTH - LIST_BUTTON_W) / 2;
export const LIST_BUTTON_Y = px(70);
export const LIST_BUTTON = {
  x: LIST_BUTTON_X,
  y: LIST_BUTTON_Y,
  w: LIST_BUTTON_W,
  h: LIST_BUTTON_H,
  radius: 8,
  text_size: px(20),
  press_color: COLOUR_BLUE,
  normal_color: COLOUR_RED_BRIGHT,
};


//
// text select list
//

export const SIMPLE_SELECT_TEXT = [
  { x: 10, y: 0, w: DEVICE_WIDTH - px(10) * 2, h: px(40), key: 'name', color: COLOUR_WHITE, text_size: px(30), action: true },
];

export const SIMPLE_SELECT_ROW = {
type_id: 1,
item_bg_color: COLOUR_RED_BRIGHT,
item_bg_radius: px(10),
text_view: SIMPLE_SELECT_TEXT,
text_view_count: 1,
item_height: px(40)
}

export const SIMPLE_SELECT_ROW_SELECTED = {
type_id: 2,
item_bg_color: COLOUR_BLUE,
item_bg_radius: px(10),
text_view: SIMPLE_SELECT_TEXT,
text_view_count: 1,
item_height: px(40)
}

export const SIMPLE_SELECT_LIST = {
x: px(0),
y: px(70),
h: DEVICE_HEIGHT - px(70),
w: DEVICE_WIDTH,
item_space: px(5),
item_config: [
  SIMPLE_SELECT_ROW,
  SIMPLE_SELECT_ROW_SELECTED
],
item_config_count: 2,
};

//
// radio select list
//

export const RADIO_SELECT_TEXT = [
  { x: 10, y: 0, w: DEVICE_WIDTH - px(10 + 68), h: px(68), key: 'name', color: COLOUR_WHITE, text_size: px(30), action: true },
];
export const RADIO_SELECT_IMAGE = [
  { x: DEVICE_WIDTH - px(66), y: 2, w: px(64), h: px(64), key: 'img_src', color: COLOUR_WHITE, action: true },
];

export const RADIO_SELECT_ROW = {
type_id: 1,
item_bg_color: COLOUR_BLACK,
item_bg_radius: px(10),
image_view: RADIO_SELECT_IMAGE,
image_view_count: 1,
text_view: RADIO_SELECT_TEXT,
text_view_count: 1,
item_height: px(68)
}

export const RADIO_SELECT_ROW_SELECTED = {
type_id: 2,
item_bg_color: COLOUR_DARK_BLUE,
item_bg_radius: px(10),
image_view: RADIO_SELECT_IMAGE,
image_view_count: 1,
text_view: RADIO_SELECT_TEXT,
text_view_count: 1,
item_height: px(68)
}

export const RADIO_SELECT_LIST = {
  x: px(0),
  y: px(70),
  h: DEVICE_HEIGHT - px(70),
  w: DEVICE_WIDTH,
  item_space: px(5),
  item_config: [
    RADIO_SELECT_ROW,
    RADIO_SELECT_ROW_SELECTED
  ],
  item_config_count: 2,
};
  
  

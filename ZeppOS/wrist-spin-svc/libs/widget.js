import * as hmUI from "@zos/ui";

//
// set the text content
//
export function setTextWidget(widget, value) {
    if (widget != null) {
        widget.setProperty(hmUI.prop.MORE, { text: value, });
    }
}

//
// change the width of a control
//
export function setWidgetWidth(widget, width) {
    if (widget == null) {
        return;
    }
    // widget.getProperty(hmUI.prop.MORE) does not work
    const current_x = widget.getProperty(hmUI.prop.X);
    const current_y = widget.getProperty(hmUI.prop.Y);
    const current_h = widget.getProperty(hmUI.prop.H);

    // pants, but apparently you have to
    // see https://docs.zepp.com/docs/reference/device-app-api/newAPI/ui/widget/FILL_RECT/
    widget.setProperty(hmUI.prop.MORE, {
        x: current_x,
        y: current_y,
        w: width,
        h: current_h,
    })
}


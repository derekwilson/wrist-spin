import { getPackageInfo } from '@zos/app'

export function getVersionForDisplay() {
    const packageInfo = getPackageInfo();
    return `v${packageInfo.version} (${packageInfo.versionCode})`
}

export function getAppId() {
    const packageInfo = getPackageInfo();
    return `${packageInfo.appId}`
}

export function getAppName() {
    const packageInfo = getPackageInfo();
    return `${packageInfo.name}`
}

export function getPackageInfoText() {
    const packageInfo = getPackageInfo();
    return JSON.stringify(packageInfo);
}

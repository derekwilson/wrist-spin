import * as haptics from '@zos/sensor'
import { log as Logger } from "@zos/utils";

import * as literals from "../common/literals";

const MINUTES_FOR_STALE = 5;

const logger = Logger.getLogger(literals.LOGGER_NAME);
const vibrator = new haptics.Vibrator();
let lastScore = null;
let lastOvers = null;
let lastUpdateTime = Date.now();
let noPhoneHasBeenAlerted = true;       // dont start alerting until we have started up

export function doVibrate() {
    logger.debug(`alert_vibrate`);
    vibrator.stop();
    //vibrator.start({mode: haptics.VIBRATOR_SCENE_DURATION_LONG});
    vibrator.start({mode: haptics.VIBRATOR_SCENE_NOTIFICATION});
}

export function alert_app_exit() {
    doVibrate();
}

export function alert_no_phone() {
    logger.debug(`alert_no_phone`);
    if (noPhoneHasBeenAlerted) {
        // we have already done the alert
        logger.debug(`suppressed`);
        return;
    }
    noPhoneHasBeenAlerted = true;
    doVibrate();
}

export function reset_alert_no_phone() {
    //logger.debug(`reset_alert_no_phone`);
    noPhoneHasBeenAlerted = false;
}

function alert_wicket_fallen() {
    doVibrate();
}

export function is_stale() {
    var howOld = minutes_since_last_update();
    logger.debug(`is_stale mins ${howOld}`);
    if (howOld >= MINUTES_FOR_STALE) {
        return true;
    }
    return false;
}

function reset_last_update_time() {
    logger.debug("reset last update time");
    lastUpdateTime = Date.now();
}

export function minutes_since_last_update() {
    if (lastUpdateTime == null) {
        return 0;
    }
    var millis = Date.now() - lastUpdateTime;
    if (millis < 1) {
        return 0;
    }
    var seconds = Math.floor(millis/1000);
    if (seconds < 1) {
        return 0;
    }
    return Math.floor(seconds / 60);
}

function get_wickets(score) {
    var numbers = score.split('/');
    if (numbers.length < 2) {
        return null;
    }
    return numbers[1];
}

function has_a_wicket_fallen(newScore) {
    if (lastScore == null) {
        logger.debug("has_a_wicket_fallen: initial state");
        return false;
    }
    if (newScore == null) {
        logger.debug("has_a_wicket_fallen: no new score");
        return false;
    }
    if (lastScore !== newScore) {
        reset_last_update_time();
    }
    var lastWickets = get_wickets(lastScore);
    var newWickets = get_wickets(newScore);
    if (lastWickets == null) {
        logger.debug("has_a_wicket_fallen: no last wickets");
        return false;
    }
    if (newWickets == null) {
        if (lastWickets != null) {
            logger.debug("has_a_wicket_fallen: last wicket or end of innings");
            return true;
        }
        logger.debug("has_a_wicket_fallen: no new wickets");
        return false;
    }
    logger.debug(`has_a_wicket_fallen: ${lastWickets}, ${newWickets}`);
    if (lastWickets !== newWickets) {
        return true;
    }
    return false;
}

function has_overs_changed(newOvers) {
    if (lastOvers == null) {
        logger.debug("has_overs_changed: initial state");
        return false;
    }
    if (newOvers == null) {
        logger.debug("has_overs_changed: no new overs");
        return false;
    }
    logger.debug(`has_overs_changed: ${lastOvers}, ${newOvers}`);
    if (lastOvers !== newOvers) {
        return true;
    }
    return false;
}

export function check_alert_wicket_fall_and_stale_data(data, alertOnWicket) {
    if (has_a_wicket_fallen(data.score)) {
        if (alertOnWicket) {
            alert_wicket_fallen();
        }
    }
    if (has_overs_changed(data.overs)) {
        reset_last_update_time();
    }
    lastScore = data.score;
    lastOvers = data.overs;
}
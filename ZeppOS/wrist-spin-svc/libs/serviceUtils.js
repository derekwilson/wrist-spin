import * as appService from "@zos/app-service";
import { queryPermission, requestPermission } from "@zos/app";

const PERMISSION_BACKGROUND_SERVICE = "device:os.bg_service";

export const backgroundServiceUrl = "app-service/background_service";

export function isServiceRunning(serviceFile) {
    let services = appService.getAllAppServices();
    //console.log(`getAllAppServices - ${services}`);
    let running = false;
    if (services != null) {
        running = services.includes(serviceFile);
    }
    return running;
}

export function checkPermissionAndStartService(serviceFile, param, callback) {
    const [bgPermission] = queryPermission({ permissions: [PERMISSION_BACKGROUND_SERVICE] });
    if (bgPermission === 0) {
        requestPermission({
            permissions: [PERMISSION_BACKGROUND_SERVICE],
            callback([result]) {
                if (result === 2) {
                    return startService(serviceFile, param, callback);
                }
            },
        });
    } else if (bgPermission === 2) {
        return startService(serviceFile, param, callback);
    }
}

export function startService(serviceFile, param, callback) {
    //console.log(`=== start service: ${serviceFile} ===`);
    const result = appService.start({
        url: serviceFile,
        param: `service=${serviceFile}&action=start&param=${param}`,
        complete_func: (info) => {
            callback(info);
        },
    });
    if (result != 0) {
        console.log(`=== start service: ${serviceFile}, result ${result} ===`);
    }
    return result;
}

export function stopService(serviceFile, callback) {
    appService.stop({
        url: serviceFile,
        param: `service=${serviceFile}&action=stop`,
        complete_func: (info) => {
            callback(info);
        },
    });
}


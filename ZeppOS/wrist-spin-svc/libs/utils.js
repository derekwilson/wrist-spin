import { getDeviceInfo } from "@zos/device";

export const { height: DEVICE_HEIGHT, width: DEVICE_WIDTH } = getDeviceInfo();

export function parseQuery(queryString) {
  if (!queryString) {
    return {};
  }
  const query = {};
  const pairs = (
    queryString[0] === "?" ? queryString.substr(1) : queryString
  ).split("&");
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split("=");
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
  }
  return query;
}

export function isEmptyObject(obj) {
  if (obj == null) {
    return true;
  }
  for (const prop in obj) {
    if (Object.hasOwn(obj, prop)) {
      return false;
    }
  }
  return true;
}

export function formatDateTimeStr(dateTimeStr, twelveHourFormat) {
  const date = new Date(dateTimeStr);
  return formatDateTime(date, twelveHourFormat)
}

export function formatDateTime(dateTime, twelveHourFormat) {
  return formatTime(dateTime.getHours(), dateTime.getMinutes(), twelveHourFormat)
}

export function formatTime(hours, mins, twelveHourFormat) {
  return `${formatTimeHours(hours, twelveHourFormat)}:${formatTimeMinutes(mins)}`
}

export function formatTimeHours(hours, twelveHourFormat) {
  if (twelveHourFormat && hours > 12) {
    hours = hours - 12
  }
  if (twelveHourFormat && hours == 0) {
    hours = 12;
  }
  return hours
}

export function formatTimeMinutes(mins) {
  return `${mins.toString().padStart(2, '0')}`
}


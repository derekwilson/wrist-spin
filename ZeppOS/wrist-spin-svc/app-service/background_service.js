import { log as Logger } from "@zos/utils";
import * as sensor from '@zos/sensor';
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { parseQuery,  isEmptyObject} from "../libs/utils";
import * as alarmMgr from '@zos/alarm'

import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as cricket from "../common/cricket";
import * as alerter from "../common/alert";
import * as serviceUtils from "../libs/serviceUtils"

const logger = Logger.getLogger(literals.LOGGER_NAME_SERVICE);
const vibrator = new sensor.Vibrator();
const time_sensor = new sensor.Time();
let lastUpdateTime = Date.now();
let intervalId = 0;
let alarmId = 0;

function getDataFromAppSide(vm) {
  console.log(`service - getDataFromAppSide`);

  const start_time = new Date().getTime();
  vm.request({
    method: "TEST-SVC",
    matchId: "ID",
    dataProvider: "PROVIDER",
  })
  .then((result) => {
    console.log("service - getDataFromAppSide receive data");

    const resp = JSON.stringify(result);
    console.log(`service - response: ${resp}`);

    const end_time = new Date().getTime();
    const duration = end_time - start_time;

    console.log(`service - Duration: ${duration}ms`);

    // add duration into response
    // making sure BG service works
    // even if the app-side component fails
    let storageObject = {
      duration: duration,
      status: 200,
      statusText: "OK",
      headers: {
        date: new Date()
      },
      response: resp
    };

    console.log(`service - getDataFromAppSide - setting response ${JSON.stringify(storageObject)}`);

    //storage.setKey("response", JSON.stringify(storageObject));

    console.log(`service - getDataFromAppSide - complete`);
  })
  .catch((res) => {
  });
}

function getMatchStatusFromAppSide(vm, state) {
  if (state.currentMatchId == null || state.currentMatchId < 1) {
    logger.debug(`service - getMatchStatus, invalid id ${state.currentMatchId}`);
    return;
  }
  logger.debug(`service - getMatchStatus, id ${state.currentMatchId}`);
  vm.request({
    method: literals.ACTION_REFRESH,
    matchId: state.currentMatchId,
    dataProvider: state.providerId,
  })
  .then((data) => {
    logger.log("service - receive data");
    const { result = {} } = data;
    const storageObject = {
      generatedDate: new Date(),
      statusData: result
    };
    console.log(`service - setting storage ${JSON.stringify(storageObject)}`);
    //storage.setKey(literals.STORAGE_KEY_MATCH_STATUS, JSON.stringify(storageObject));
    localStorage.setItem(literals.STORAGE_KEY_MATCH_STATUS, JSON.stringify(storageObject));
    logger.log("service - receive data - end");
  })
  .catch((res) => {
    if (!utils.isEmptyObject(res)) {
      logger.log("service - catch error");
      const errorText = JSON.stringify(res);
      logger.log(`service - error = ${errorText}`);
    }
  });
}

//const end_point = "https://jsonplaceholder.typicode.com/todos/1";
//const end_point = 'https://www.espncricinfo.com/ci/engine/match/index/live.html';
//const end_point = 'https://static.espncricinfo.com/rss/livescores.xml';
//const end_point = 'https://www.espncricinfo.com/ci/engine/match/1431085.json';
const end_point = 'https://www.espncricinfo.com/ci/engine/match/1445834.json';

function getDataFromCricinfo(vm) {
  logger.log("Sending request");

  vm.httpRequest({
    method: 'GET',
    url: end_point,
  })
    .then((response) => {
      logger.log("service - receive data");
      //const resultTxt = JSON.stringify(response);
      //logger.log(`service - response ${resultTxt}`);
      const result = typeof response.body === 'string' ? JSON.parse(response.body) : response.body
      //const { result = {} } = response.body;
      const storageObject = {
        generatedDate: new Date(),
        statusData: result
      };
      logger.log(`service - setting storage ${JSON.stringify(storageObject)}`);
      //storage.setKey(literals.STORAGE_KEY_MATCH_STATUS, JSON.stringify(storageObject));
      //localStorage.setItem(literals.STORAGE_KEY_MATCH_STATUS, JSON.stringify(storageObject));
      logger.log("service - receive data - end");
    })
    .catch((error) => {
      if (!utils.isEmptyObject(error)) {
        logger.log("service - catch error");
        const errorText = JSON.stringify(error);
        logger.log(`service - error = ${errorText}`);
      }
    });
}

function processMatchStatus(param) {
  logger.log(`service - processMatchStatus`);
  if (param.result) {
    logger.log(`service - processMatchStatus - ${param.result.state}, ${param.result.score}`);

    // work out alerts
    alerter.check_alert_wicket_fall_and_stale_data(param.result);
    if (alerter.is_wicket_fallen_alert()) {
      doVibrate();
    }

    // update the storage so that the new data van be rendered
    lastUpdateTime = Date.now();
    const storageObject = {
      generatedDate: lastUpdateTime,
      statusData: param.result,
      staleMessage: alerter.is_stale() ? `${alerter.minutes_since_last_update()} mins old` : "",
    };
    localStorage.setItem(literals.STORAGE_KEY_MATCH_STATUS, JSON.stringify(storageObject));

    alerter.reset_alerts();
  }
  logger.log(`service - processMatchStatus - end`);
}

function getMatchStatus(vm, state) {
  if (state.currentMatchId == null || state.currentMatchId < 1) {
    logger.debug(`service - getMatchStatus, invalid id ${state.currentMatchId}`);
    return;
  }
  logger.debug(`service - getMatchStatus, id ${state.currentMatchId}`);
  cricket.perform_action(vm, processMatchStatus, literals.ACTION_REFRESH, state.currentMatchId, state.providerId);
  logger.debug(`service - getMatchStatus, id ${state.currentMatchId} - ended`);
}

function getState() {
  try {
    return {
      currentMatchId: localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0),
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2),
      refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL),
    }
  } catch (error) {
    logging.debug(`service - getState error ${error}`);
    return {
      currentMatchId: 1428695,
      providerId: literals.PROVIDER_CRICINFO_2,
      refreshRate: literals.DEFAULT_TIMER_TICK_INTERVAL,
    }
  }
}

function doVibrate() {
  logger.debug(`alert_vibrate`);
  vibrator.stop();
  //vibrator.start({mode: sensor.VIBRATOR_SCENE_DURATION_LONG});
  vibrator.start({mode: sensor.VIBRATOR_SCENE_NOTIFICATION});
}

AppService(
  BasePage({
    onInit(param) {
      logger.log(`bg service v${literals.APP_VERSION} service onInit - param ${param}`);
      //logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
      logging.set_logging_level(logging.LOGGING_LEVEL_VERBOSE);
      cricket.initialize();

      const state = getState();
      getMatchStatus(this, state);

      time_sensor.onPerMinute(() => {
        logger.log("bg service TICK");
        var millis = Date.now() - lastUpdateTime;
        const state = getState();
        logger.log(`bg service state: matchId ${state.currentMatchId}, provider ${state.providerId}, refresh rate ${literals.getDescription(state.refreshRate)}, elapsed ${millis}`);

        if (millis > (state.refreshRate - 100)) {
          logger.debug(`***tick***`);
          getMatchStatus(this, state);
        }
        logger.log("bg service TICK - end");
      });

      //this.configureTimeSensor(this, state);
      //this.configureInterval(state);
      //this.configureAlarm(state);
      logger.log(`bg service v${literals.APP_VERSION} service onInit - complete`);
    },
    
    configureTimeSensor(vm, state) {
      time_sensor.onPerMinute(() => {
        logger.log("bg service TICK");
        var millis = Date.now() - lastUpdateTime;
        logger.log(`bg service state: matchId ${state.currentMatchId}, provider ${state.providerId}, refresh rate ${literals.getDescription(state.refreshRate)}, elapsed ${millis}`);

        if (millis > (state.refreshRate - 100)) {
          logger.debug(`***tick***`);
          getMatchStatus(vm, state);
        }
        logger.log("bg service TICK - end");
      });
    },

    configureInterval(state) {
      logger.log(`bg service interval: matchId ${state.currentMatchId}, provider ${state.providerId}, refresh rate ${literals.getDescription(state.refreshRate)}`);
      intervalId = setInterval(() => {
        logger.debug(`***interval***`);
        this.getMatchStatus(state.currentMatchId, state.providerId);
      }, state.refreshRate);
    },

    killAllAlarms() {
      const alarms = alarmMgr.getAllAlarms();
      logger.debug(`service alarms to kill - ${JSON.stringify(alarms)}`);
      alarms.forEach((thisAlarm, index) => {
        // kill any existing alarms
        alarmMgr.cancel(thisAlarm);
      });
    },
    configureAlarm(state) {
      this.killAllAlarms();
      alarmId = alarmMgr.set( {
        url: serviceUtils.backgroundServiceUrl,
        delay: 60,
        param: "param=ping",
      });
      logger.debug(`display-match alarm id - ${alarmId}`);
      logger.debug(`display-match alarms - ${JSON.stringify(alarmMgr.getAllAlarms())}`);
    },

    onDestroy() {
      logger.log(`bg service: v${literals.APP_VERSION} service onDestroy`);
      //clearInterval(intervalId);
      //this.killAllAlarms();
    },
  })
);

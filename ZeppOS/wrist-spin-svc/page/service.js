import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import * as widget from "../libs/widget"
import * as serviceUtils from "../libs/serviceUtils"

import { DEVICE_HEIGHT, DEVICE_WIDTH } from "../libs/utils";
import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

let textWidget = null;

function serviceStarted(info) {
  logger.log(`serviceStatusChanged result: ` + JSON.stringify(info));
  widget.setTextWidget(textWidget, getStatusText());
}

function serviceStopped(info) {
  logger.log(`serviceStatusChanged result: ` + JSON.stringify(info));
  setTimeout(() => {
    logger.debug(`***timeout***`);
    widget.setTextWidget(textWidget, getStatusText());
  }, 500);
}

function getStatusText() {
  return `Service is\n${serviceUtils.isServiceRunning(serviceUtils.backgroundServiceUrl) ? "running" : "not running"}`;
}

Page(
  BasePage({
    state: {},
    onCreate(e) {
      logger.debug(`service page - onCreate`);
    },
    onInit() {
      logger.debug(`service page - onInit`);
    },
    build() {
      logger.debug(`service page - build`);
      textWidget = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.PAGE_TEXT_BLOCK_STYLE,
        h: DEVICE_HEIGHT / 2,
        text: getStatusText(),
      });

      hmUI.createWidget(hmUI.widget.BUTTON, {
        ...styles.LIST_BUTTON,
        y: DEVICE_HEIGHT - styles.LIST_BUTTON_Y * 2 + 10,
        text: "Start Service",
        click_func: function (button) {
          serviceUtils.checkPermissionAndStartService(serviceUtils.backgroundServiceUrl, "test-param", serviceStarted)
        },
      });
      hmUI.createWidget(hmUI.widget.BUTTON, {
        ...styles.LIST_BUTTON,
        y: DEVICE_HEIGHT - styles.LIST_BUTTON_Y + 5,
        text: "Stop Service",
        click_func: function (button) {
          serviceUtils.stopService(serviceUtils.backgroundServiceUrl, serviceStopped)
        },
      });
      logger.debug(`service page - build end`);
    },
  })
);

import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import { back } from "@zos/router";
import { getScrollListDataConfig } from "../libs/listUtils"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";
import * as logging from "../common/logging";
import * as cricket from "../common/cricket";

const logger = Logger.getLogger(literals.LOGGER_NAME);

let currentMatchId = -1;
let currentMatchList = null;
let scrollListWidget = null;

function findSelectedIndex(id, matchList) {
  if (matchList == null || matchList.length === 0) {
    return -1
  }
  for (var index = 0; index<matchList.length; index++) {
    if (matchList[index].selection_game_id == id) {
      return index;
    }
  }
  return -1;
}

function updateList(matchList) {
  currentMatchList = matchList;
  logger.log(`updateList with ${matchList.length} items`);
  const selectedIndex = findSelectedIndex(currentMatchId, matchList);
  logger.log(`current id = ${currentMatchId} selected index = ${selectedIndex}`);
  const dataTypeConfig = getScrollListDataConfig(selectedIndex, matchList.length, 2, 1)
  scrollListWidget.setProperty(hmUI.prop.UPDATE_DATA, {
    data_type_config: dataTypeConfig,
    data_type_config_count: dataTypeConfig.length,
    data_array: matchList,
    data_count: matchList.length,
    on_page: 1
  });
  logger.log(`updateList completed`);
}

function processMatchList(param) {
  logger.log(`processMatchList`);
  const text = JSON.stringify(param);
  logger.log(`param = ${text}`);

  if (param.result && param.result.matches) {
    const matches = param.result.matches;
    logger.log(`data items = ${matches.length})`);

    const mappedData = matches.map((thisMatch) => ({
      selection_game_id : thisMatch.selection_game_id,
      team_1 : thisMatch.team_1, 
      v: "v", 
      team_2 : thisMatch.team_2 
    }));
    logger.log(`mapped data items = ${mappedData.length})`);

    updateList(mappedData)
  } else {
    logger.log(`no matches`);
    const emptyList = [
      { selection_game_id: -1, team_1: 'No matches' }
    ];
    updateList(emptyList)
  }
  logger.log(`processMatchList - end`);
}

function getMatchList(vm) {
  logger.debug(`getMatchList, id ${currentMatchId}`);
  cricket.perform_action(vm, processMatchList, literals.ACTION_SELECT_REFRESH_LIST, currentMatchId, vm.state.providerId);
  logger.debug(`getMatchList, id ${currentMatchId} - ended`);
}

Page(
  BasePage({
    state: {
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2)
    },
    onCreate(e) {
      logger.debug(`select-match page - onCreate`);
    },
    onInit() {
      logger.debug(`select-match page - onInit`);
      setWakeUpRelaunch({ relaunch: true })
      currentMatchId = localStorage.getItem(literals.STORAGE_KEY_CURRENT_MATCH, 0);
      logging.set_logging_level(logging.DEFAULT_LOG_LEVEL);
      //logging.set_logging_level(logging.LOGGING_LEVEL_VERBOSE);
      cricket.initialize();
      getMatchList(this);
      logger.debug(`select-match page - onInit - complete`);
    },
    build() {
      logger.debug(`select-match page - build`);

      const initialList = [
        { selection_game_id: -1, team_1: 'Loading...' }
      ];
  
      scrollListWidget = hmUI.createWidget(hmUI.widget.SCROLL_LIST, {
        ...styles.SELECT_GAME_LIST,
        item_click_func: (list, index, data_key) => {
          logger.log(`index=${index} dataKey=${data_key}`);
          if (currentMatchList != null && index < currentMatchList.length) {
            logger.log(`setting current match id = ${currentMatchList[index].selection_game_id}`);
            currentMatchId = currentMatchList[index].selection_game_id;
            back();
          }
        },
        data_array: initialList,
        data_count: initialList.length,
        data_type_config: [
          {
            start: 0,
            end: initialList.length,
            type_id: 1
          },
        ],
        data_type_config_count: 1,
      });
    },
    onDestroy() {
      logger.debug(`select-match page - onDestroy`);
      localStorage.setItem(literals.STORAGE_KEY_CURRENT_MATCH, currentMatchId)
    },
  })
);

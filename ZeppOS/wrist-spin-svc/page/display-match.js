import * as hmUI from "@zos/ui";
import * as page from "@zos/page";
import { log as Logger } from "@zos/utils";
import { px } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { setWakeUpRelaunch } from '@zos/display'
import { Time as TimeSensor } from "@zos/sensor";

import * as utils from "../libs/utils";
import * as serviceUtils from "../libs/serviceUtils"
import * as widget from "../libs/widget"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);
let uiTextMainScore = null;
let uiTextMainTime = null;
let uiTextMainOvers = null;
let uiTextMainMessage = null;
let uiTextMainStatus = null;

let uiTextDetailsBattingTeam = null;
let uiTextDetailsScore = null;
let uiTextDetailsOvers = null;
let uiTextDetailsTime = null;
let uiTextDetailsStrikerName = null;
let uiTextDetailsStrikerScore = null;
let uiTextDetailsNonStrikerName = null;
let uiTextDetailsNonStrikerScore = null;
let uiTextDetailsBowlerStats = null;
let uiTextDetailsMessage = null;
let uiTextDetailsFact = null;
let uiTextDetailsStatus = null;

// control the page layout, but we are hard-coding, no UI
let hideStatusBar = true;
let vertical = true;

const timeSensor = new TimeSensor();

function renderMatchStatus() {
  logger.log("renderMatchStatus");
  // read data from storage
  const matchStatusStr = localStorage.getItem(literals.STORAGE_KEY_MATCH_STATUS, "{}");
  logger.log(`status = ${matchStatusStr}`);
  if (matchStatusStr != null) {
    try {
      const data = JSON.parse(matchStatusStr);
      logger.debug(`generated = ${data.generatedDate}`);
      if (data.statusData == null) {
        renderGameDisplayError("Empty match data");
      } else {
        updateDisplay(data.statusData, data.generatedDate, data.staleMessage);
      }
    } catch (error) {
      logger.log(`ERR: ${error.message}`);
      renderGameDisplayError(error.message);
    }
  }
}

function updateDisplay(matchStatus, generatedDate, staleMessage) {
  logger.log(`updateDisplay ${matchStatus.score}`);
  if (matchStatus.isError) {
    renderGameDisplayError(matchStatus.errorMessage);
    return;
  }
  if (matchStatus.score == null) {
    renderGameDisplayError("Invalid match format");
    return;
  }

  // when we get a non error return - clear the error
  renderGameDisplayError(`generated: ${utils.formatDateTimeStr(generatedDate, false)}`);
  const isServiceRunning = serviceUtils.isServiceRunning(serviceUtils.backgroundServiceUrl);
  if (!isServiceRunning) {
    renderGameDisplayError("no service");
  }

  if (!hideStatusBar) {
    hmUI.updateStatusBarTitle(matchStatus.team_batting);
  }

  widget.setTextWidget(uiTextMainScore, matchStatus.score);
  widget.setTextWidget(uiTextMainOvers, matchStatus.overs);
  widget.setTextWidget(uiTextMainMessage, matchStatus.lead);

  widget.setTextWidget(uiTextDetailsBattingTeam, matchStatus.team_batting);
  widget.setTextWidget(uiTextDetailsScore, matchStatus.score);
  widget.setTextWidget(uiTextDetailsOvers, matchStatus.overs);
  
  widget.setTextWidget(uiTextDetailsStrikerName, matchStatus.striker_name);
  widget.setTextWidget(uiTextDetailsStrikerScore, "" + matchStatus.striker_score);

  widget.setTextWidget(uiTextDetailsNonStrikerName, matchStatus.nonstriker_name);
  widget.setTextWidget(uiTextDetailsNonStrikerScore, "" + matchStatus.nonstriker_score);
  
  widget.setTextWidget(uiTextDetailsBowlerStats, `${matchStatus.bowler_name} ${matchStatus.bowler_stats}`)
  
  widget.setTextWidget(uiTextDetailsMessage, matchStatus.lead);
  widget.setTextWidget(uiTextDetailsFact, matchStatus.fact);

  if (staleMessage != null && staleMessage.length > 0) {
    //renderGameDisplayError(staleMessage);
  }
}

function renderGameDisplayError (message) {
  widget.setTextWidget(uiTextMainStatus, message);
  widget.setTextWidget(uiTextDetailsStatus, message);
}

Page(
  BasePage({
    state: {
      providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2),
      refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL),
    },
    onCreate(e) {
      logger.debug(`display-match page - onCreate`);
    },
    onInit() {
      logger.debug(`display-match page - onInit refresh rate ${literals.getDescription(this.state.refreshRate)}`);

      this.configureTimeSensor();
      
      setWakeUpRelaunch({ relaunch: true })
      // apparently we dont need to restore it if we hide it, in fact it screws up the real device if we do
      hmUI.setStatusBarVisible(!hideStatusBar);
  
      logger.debug(`display-match page - onInit - complete`);
    },
    onShow() {
      logger.debug(`display-match page - onShow`);
    },
    onHide() {
      logger.debug(`display-match page - onHide`);
    },
    onDestroy() {
      logger.debug(`display-match page - onDestroy - scrollTop ${page.getScrollTop()}, swiper ${page.getSwiperIndex()}`);
      logger.debug(`display-match page - onDestroy - complete`);
    },

    configureTimeSensor() {
      logger.debug(`display-match page - configureTimeSensor`);
      timeSensor.onPerMinute(() => {
        logger.log(`**time sensor: ${timeSensor.getHours()}:${timeSensor.getMinutes()}:${timeSensor.getSeconds()}`);

        // these things can get unset by other apps
        setWakeUpRelaunch({ relaunch: true })
        hmUI.setStatusBarVisible(!hideStatusBar);

        const formattedTime = utils.formatTime(timeSensor.getHours(), timeSensor.getMinutes(), true);
        widget.setTextWidget(uiTextMainTime, formattedTime);
        widget.setTextWidget(uiTextDetailsTime, formattedTime);

        renderMatchStatus();

        logger.log(`**time sensor: end`);
      });
    },

    build() {
      logger.debug(`display-match page - build`);

      // build pages
      let top_gap = px(0);
      if (!hideStatusBar) {
        // leave room for the status bar
        top_gap = px(70);
      }
      let page_count = 2;
      // this is the page size in pixels - the height for vertical scrolling pages or the width for horizontally scrolling
      let page_size = vertical ? utils.DEVICE_HEIGHT - top_gap : utils.DEVICE_WIDTH;

      hmUI.setScrollView(true, page_size, page_count, vertical);
      const pg_indicator = hmUI.createWidget(hmUI.widget.PAGE_INDICATOR, {
        ...styles.PAGE_INDICATOR,
        y: top_gap,
      });


      for (let pageIndex = 0; pageIndex < page_count; pageIndex++) {
        let page_base_x_pos = vertical ? 0 : page_size * pageIndex;
        let page_base_y_pos = vertical ? px(10) + top_gap + page_size * pageIndex : px(10) + top_gap;

        switch (pageIndex) {
          case 0:
            this.buildPage0(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
          case 1:
            this.buildPage1(page_base_x_pos, page_base_y_pos, utils.DEVICE_HEIGHT - top_gap);
            break;
        }
      };

      // the resume is called every time the page is woken
      hmUI.createWidget(hmUI.widget.WIDGET_DELEGATE, {
        resume_call: function () {
          logger.debug(`display-match page delegate resume call`);
          renderMatchStatus();
          logger.debug(`display-match page delegate resume call - end`);
        },
      });

      logger.debug(`display-match page - build - complete`);
      renderMatchStatus();
    },
    buildPage0(base_x, base_y, page_height) {
      logger.debug(`display-match page - build main page: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);
      
      let current_line_y_pos = base_y + px(25) - px(styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.XTRA_BIG_TEXT_H - styles.XTRA_BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);

      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextMainTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_XTRA_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
        text: formattedNow,
        color: styles.COLOUR_RED_BRIGHT,
      });

      // lets leave a gap
      current_line_y_pos = current_line_y_pos + px(25);

      current_line_y_pos = current_line_y_pos + px(styles.XTRA_BIG_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextMainOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextMainMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextMainStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },
    buildPage1(base_x, base_y, page_height) {
      logger.debug(`display-match page - build page1: baseX ${base_x}, baseY ${base_y}, pageH ${page_height}`);

      let current_line_y_pos = base_y - px(styles.SMALL_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBattingTeam = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.SMALL_TEXT_H - styles.BIG_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_BIG_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.BIG_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsOvers = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.LEFT,
      });
      const now = new Date();
      const formattedNow = utils.formatTime(now.getHours(), now.getMinutes(), true);
      uiTextDetailsTime = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + (utils.DEVICE_WIDTH / 2),
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH / 2,
        align_h: hmUI.align.RIGHT,
        text: formattedNow,
        color: styles.COLOUR_RED_BRIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      const score_dimensions = this.getTextDimensions(styles.MEDIUM_TEXT_SIZE, "288");
      uiTextDetailsStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsNonStrikerName = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
        w: utils.DEVICE_WIDTH - score_dimensions.w,
        align_h: hmUI.align.LEFT,
      });
      uiTextDetailsNonStrikerScore = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x + utils.DEVICE_WIDTH - score_dimensions.w,
        y: current_line_y_pos,
        w: score_dimensions.w,
        align_h: hmUI.align.RIGHT,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.MEDIUM_TEXT_TOP_PADDING);
      logger.debug(`display-match page - current_line_y_pos ${current_line_y_pos}`);
      uiTextDetailsBowlerStats = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_MEDIUM_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.MEDIUM_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsMessage = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      current_line_y_pos = current_line_y_pos + px(styles.SMALL_TEXT_H - styles.SMALL_TEXT_TOP_PADDING);
      uiTextDetailsFact = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x,
        y: current_line_y_pos,
      });

      uiTextDetailsStatus = hmUI.createWidget(hmUI.widget.TEXT, {
        ...styles.DISPLAY_SMALL_TEXT,
        x: base_x + px(30),
        y: base_y + page_height - px(styles.SMALL_TEXT_H),
        w: utils.DEVICE_WIDTH - px(60),
        text: 'Loading...',
      });
    },
    getTextDimensions(size, text) {
      const { width, height } = hmUI.getTextLayout(text, {
        text_size: size,
        text_width: utils.DEVICE_WIDTH
      })
      return { w: width, h: height }
    },
  })
);

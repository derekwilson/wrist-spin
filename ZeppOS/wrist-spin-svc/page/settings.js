import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { localStorage } from '@zos/storage'
import { push } from "@zos/router";

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

Page(
  BasePage({
    state: {
        providerId: localStorage.getItem(literals.STORAGE_KEY_PROVIDER, literals.PROVIDER_CRICINFO_2),
        refreshRate: localStorage.getItem(literals.STORAGE_KEY_REFRESH_RATE, literals.DEFAULT_TIMER_TICK_INTERVAL)
    },
    onCreate(e) {
      logger.debug(`settings page - onCreate`);
    },
    onInit() {
      logger.debug(`settings page - onInit`);
    },
    build() {
      logger.debug(`settings page - build`);

      const settingsArray = [
        { name: "Data Source\n" + literals.getDescription(this.state.providerId), url: "page/data-source" },
        { name: "Refresh Rate\n" + literals.getDescription(this.state.refreshRate), url: "page/refresh-rate" },
      ];

      const viewContainer = hmUI.createWidget(hmUI.widget.VIEW_CONTAINER, {
        ...styles.BUTTON_VIEW_CONTAINER,
      });

      settingsArray.forEach((feature, index) => {
        viewContainer.createWidget(hmUI.widget.BUTTON, {
          ...styles.LIST_BUTTON,
          y: styles.LIST_BUTTON_Y * index,
          text: feature.name,
          click_func: function (button) {
            logger.debug(`click - url ${feature.url}`);
            push({
              url: feature.url,
            });
          },
        });
      });

    },
    onDestroy() {
      logger.debug(`settings page - onDestroy`);
    },
  })
);

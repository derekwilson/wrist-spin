import * as hmUI from "@zos/ui";
import { log as Logger } from "@zos/utils";
import { BasePage } from "@zeppos/zml/base-page";
import { push } from "@zos/router";

import * as packageInfo from "../libs/package"
import * as serviceUtils from "../libs/serviceUtils"

import * as styles from "zosLoader:./styles/style.[pf].layout.js";
import * as literals from "../common/literals";

const logger = Logger.getLogger(literals.LOGGER_NAME);

Page(
  BasePage({
    state: {},
    onCreate(e) {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onCreate`);
    },
    onInit() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onInit`);
      // in case we have mucked about with the title
      hmUI.updateStatusBarTitle(packageInfo.getAppName());
    },
    onShow() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onShow`);
    },
    build() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - build`);
      const featureArray = [
        { name: "Display Match", url: "page/display-match" },
        { name: "Select Match", url: "page/select-match" },
        { name: "Settings", url: "page/settings" },
        { name: "Service", url: "page/service" },
        { name: "About", url: "page/about" },
      ];

      const viewContainer = hmUI.createWidget(hmUI.widget.VIEW_CONTAINER, {
        ...styles.BUTTON_VIEW_CONTAINER,
      });

      const isServiceRunning = serviceUtils.isServiceRunning(serviceUtils.backgroundServiceUrl);

      featureArray.forEach((feature, index) => {
        viewContainer.createWidget(hmUI.widget.BUTTON, {
          ...styles.MAIN_BUTTON,
          y: (index == 0 ? 0 : (styles.MAIN_BUTTON_Y * index) + styles.MAIN_BUTTON_H),
          h: (index == 0 ? styles.MAIN_BUTTON_H * 2 : styles.MAIN_BUTTON_H),
          normal_color: (feature.name == "Service" ? (isServiceRunning ? styles.COLOUR_GREEN_MUTED : styles.COLOUR_RED_BRIGHT) : styles.COLOUR_RED_BRIGHT ),
          text_size: (index == 0 ? px(30) : px(25)),
          text: feature.name,
          click_func: function (button) {
            logger.debug(`click - url ${feature.url}`);
            push({
              url: feature.url,
            });
          },
        });
      });
    },
    onHide() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onHide`);
    },
    onDestroy() {
      logger.debug(`wrist-spin v${literals.APP_VERSION} APP - onDestroy`);
    },
  })
);

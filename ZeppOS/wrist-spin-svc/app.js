import { BaseApp } from "@zeppos/zml/base-app";

App(
  BaseApp({
    globalData: {},
    onCreate(options) {
      console.log('wrist-spin-svc: app on create invoke')
    },

    onDestroy(options) {
      console.log('wrist-spin-svc: app on destroy invoke')
    },
  })
);

import { BaseSideService } from "@zeppos/zml/base-side";

import * as literals from "../common/literals";

// app side component has to be declared in order for ZML to work
AppSideService(
  BaseSideService({
    onInit() {
      console.log(`onInit: wrist-spin v${literals.APP_VERSION} started`);
    },

    onRun() {
      console.log(`onRun: wrist-spin v${literals.APP_VERSION} started`);
    },

    onDestroy() {
      console.log(`onDestroy: wrist-spin v${literals.APP_VERSION} started`);
    },
  })
);




// app version
export const APP_VERSION = "78";

// refresh rates
export const FAST_TIMER_TICK_INTERVAL = 60 * 1000;
export const DEFAULT_TIMER_TICK_INTERVAL = 2 * 60 * 1000;
export const SLOW_TIMER_TICK_INTERVAL = 5 * 60 * 1000;
export const VERY_SLOW_TIMER_TICK_INTERVAL = 15 * 60 * 1000;

// action sent from device to the phone to check comms
export const ACTION_PING = "PING";
// expected result from a ping
export const ACTION_PONG = "PONG";
// action sent from JS to phone when the JS is ready
export const ACTION_READY = "READY";
// refresh the scores
export const ACTION_REFRESH = "REFRESH";
// select the given match id and get the scores
export const ACTION_SELECT_MATCH = "SELECT-MATCH";
// get the previous active match
export const ACTION_SELECT_PREVIOUS = "SELECT-PREVIOUS";
// get the next active match
export const ACTION_SELECT_NEXT = "SELECT-NEXT";
// get the current match teams
export const ACTION_SELECT_REFRESH = "SELECT-REFRESH";
// get all the current active matches
export const ACTION_SELECT_REFRESH_LIST = "SELECT-REFRESH-LIST";

// match states
export const STATE_CURRENT = "STATE-CURRENT";
export const STATE_DORMANT = "STATE-DORMANT";
export const STATE_COMPLETE = "STATE-COMPLETE";

// data source providers
export const PROVIDER_TEST = "TEST-PROVIDER";
export const PROVIDER_CRICINFO = "CRICINFO";
export const PROVIDER_CRICINFO_2 = "CRICINFO2";
export const PROVIDER_CRICSCORE = "CRICSCORE";
export const PROVIDER_CRICAPI = "CRICAPI2";
export const CRICAPI_KEY = "oaxpTMPlXKSYrv9G9x7LM4gjxut1";

// ZeppOS Specific
export const LOGGER_NAME = "wrist-spin";
export const LOGGER_NAME_SERVICE = "wrist-spin-service";

// local storage
export const STORAGE_KEY_CURRENT_MATCH = "current-match";
export const STORAGE_KEY_PROVIDER = "provider";
export const STORAGE_KEY_REFRESH_RATE = "refresh-rate";

// easy storage
export const STORAGE_KEY_MATCH_STATUS = "match-status";



export function getDescription(literal) {
    switch (literal) {
        case PROVIDER_TEST:
            return "Test";
        case PROVIDER_CRICINFO:
            return "Cricinfo XML";
        case PROVIDER_CRICINFO_2:
            return "Cricinfo HTML";
        case PROVIDER_CRICSCORE:
            return "Cricscore";
        case PROVIDER_CRICAPI:
            return "Cricapi";

        case FAST_TIMER_TICK_INTERVAL:
            return "1 Min";
        case DEFAULT_TIMER_TICK_INTERVAL:
            return "2 Mins";
        case SLOW_TIMER_TICK_INTERVAL:
            return "5 Mins";
        case VERY_SLOW_TIMER_TICK_INTERVAL:
            return "15 Mins";
    }
    return "Unknown"
}
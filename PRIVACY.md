# wrist-spin Privacy Policy #

wrist-spin is an application written by Derek Wilson.

This policy describes the data that is collected by Derek Wilson.


## What information is collected? ##

None. No personally identifiable information is collected by the app. 


## How is this information processed? ##

No information is collected.



package net.derekwilson.wrist_spin_cricket_logic_poc;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;

import org.junit.Test;
import org.mockito.Mock;

public class TestEnvironmentIsGood extends MockingAnnotationSetup {

    @Mock
    ILoggingSink mockLogger;

    @Test
    public void mockito_can_verify() {
        // arrange

        // act
        mockLogger.debug("TEST");

        // assert
        verify(mockLogger).debug("TEST");
    }
}



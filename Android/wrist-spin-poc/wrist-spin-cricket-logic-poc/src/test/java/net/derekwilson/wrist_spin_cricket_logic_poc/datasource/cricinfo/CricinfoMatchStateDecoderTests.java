package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.BaseDataSourceTest;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class CricinfoMatchStateDecoderTests extends BaseDataSourceTest {

    private CricinfoMatchStateDecoder decoder;

    @Before
    public void setUp() {
        decoder = new CricinfoMatchStateDecoder(mockLogger);
    }

    @Test
    public void decodeMatchState_gets_complete_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1391800_complete.json");

        // act
        MatchState state = decoder.decodeMatchState("1391800", rawJson);

        // assert
        assertThat(state.getId(), is("1391800"));
        assertThat(state.getCurrentState().name(), is("Complete"));
        assertThat(state.getTeam1().getAbbreviation(), is("WA"));
        assertThat(state.getTeam2().getAbbreviation(), is("TAS"));
        assertThat(state.getScoreDisplay(), is("WA v TAS"));
        assertThat(state.getStrikerName(), is("West Aust won by"));
        assertThat(state.getNonStrikerName(), is("377 runs"));
    }

    @Test
    public void decodeMatchState_gets_current_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1391800_current.json");

        // act
        MatchState state = decoder.decodeMatchState("1391800", rawJson);

        // assert
        assertThat(state.getId(), is("1391800"));
        assertThat(state.getCurrentState().name(), is("Current"));

        assertThat(state.getTeam1().getAbbreviation(), is("WA"));
        assertThat(state.getTeam2().getAbbreviation(), is("TAS"));

        assertThat(state.getScoreDisplay(), is("105/0"));
        assertThat(state.getScore().getDisplay(), is("105/0"));
        assertThat(state.getOvers().getDisplay(), is("35.4"));
        assertThat(state.getFact(), is("35.4 FOUR Short"));
        assertThat(state.getLead(), is("First innings"));
        assertThat(state.getBattingTeamAbbreviation(), is("WA"));
        assertThat(state.getStrikerName(), is("Short"));
        assertThat(state.getStrikerScore(), is("45"));
        assertThat(state.getStrikerStats(), is("45 (124)"));
        assertThat(state.getNonStrikerName(), is("Whiteman"));
        assertThat(state.getNonStrikerScore(), is("58"));
        assertThat(state.getNonStrikerStats(), is("58 (91)"));
        assertThat(state.getBowlerName(), is("Carlisle"));
        assertThat(state.getBowlerStats(), is("7.4-2-27-0"));
    }

    @Test
    public void decodeMatchState_gets_current_test_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1419830_current_test.json");

        // act
        MatchState state = decoder.decodeMatchState("1419830", rawJson);

        // assert
        assertThat(state.getId(), is("1419830"));
        assertThat(state.getCurrentState().name(), is("Current"));

        assertThat(state.getTeam1().getAbbreviation(), is("BAN"));
        assertThat(state.getTeam2().getAbbreviation(), is("SL"));

        assertThat(state.getScoreDisplay(), is("47/5"));
        assertThat(state.getScoreDisplayWithOvers(), is("47/5 13.0"));
        assertThat(state.getScore().getDisplay(), is("47/5"));
        assertThat(state.getOvers().getDisplay(), is("13.0"));
        assertThat(state.getFact(), is("Stumps"));
        assertThat(state.getLead(), is("Target 511"));
        assertThat(state.getBattingTeamAbbreviation(), is("BAN"));
        assertThat(state.getStrikerName(), is("Mominul"));
        assertThat(state.getStrikerScore(), is("7"));
        assertThat(state.getStrikerStats(), is("7 (29)"));
        assertThat(state.getNonStrikerName(), is("Taijul"));
        assertThat(state.getNonStrikerScore(), is("6"));
        assertThat(state.getNonStrikerStats(), is("6 (14)"));
        assertThat(state.getBowlerName(), is("Fernando"));
        assertThat(state.getBowlerStats(), is("7.0-3-13-3"));
    }

    @Test
    public void decodeMatchState_gets_current_odi_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1388205_current_odi.json");

        // act
        MatchState state = decoder.decodeMatchState("1388205", rawJson);

        // assert
        assertThat(state.getId(), is("1388205"));
        assertThat(state.getCurrentState().name(), is("Current"));
        assertThat(state.getTeam1().getAbbreviation(), is("NZ-W"));
        assertThat(state.getTeam2().getAbbreviation(), is("ENG-W"));

        assertThat(state.getScoreDisplay(), is("47/2"));
        assertThat(state.getScoreDisplayWithOvers(), is("47/2 6.1"));
        assertThat(state.getScore().getDisplay(), is("47/2"));
        assertThat(state.getOvers().getDisplay(), is("6.1"));
        assertThat(state.getFact(), is("5.5 FOUR Green"));
        assertThat(state.getLead(), is("Need 131 from 83"));
        assertThat(state.getBattingTeamAbbreviation(), is("NZ-W"));
        assertThat(state.getStrikerName(), is("Green"));
        assertThat(state.getStrikerScore(), is("6"));
        assertThat(state.getStrikerStats(), is("6 (6)"));
        assertThat(state.getNonStrikerName(), is("Bezuidenhout"));
        assertThat(state.getNonStrikerScore(), is("9"));
        assertThat(state.getNonStrikerStats(), is("9 (9)"));
        assertThat(state.getBowlerName(), is("Ecclestone"));
        assertThat(state.getBowlerStats(), is("0.1-0-0-0"));
    }

    @Test
    public void decodeMatchState_gets_current_odi_wicket_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1388205_current_odi_wicket.json");

        // act
        MatchState state = decoder.decodeMatchState("1388205", rawJson);

        // assert
        assertThat(state.getId(), is("1388205"));
        assertThat(state.getCurrentState().name(), is("Current"));
        assertThat(state.getTeam1().getAbbreviation(), is("NZ-W"));
        assertThat(state.getTeam2().getAbbreviation(), is("ENG-W"));

        assertThat(state.getScoreDisplay(), is("47/2"));
        assertThat(state.getScoreDisplayWithOvers(), is("47/2 6.1"));
        assertThat(state.getScore().getDisplay(), is("47/2"));
        assertThat(state.getOvers().getDisplay(), is("6.1"));
        assertThat(state.getFact(), is("4.3 AC Kerr c Dunkley b Dean 21 (17b 3x4 0x6) SR: 123.52"));
        assertThat(state.getLead(), is("Need 131 from 83"));
        assertThat(state.getBattingTeamAbbreviation(), is("NZ-W"));
        assertThat(state.getStrikerName(), is("Green"));
        assertThat(state.getStrikerScore(), is("6"));
        assertThat(state.getStrikerStats(), is("6 (6)"));
        assertThat(state.getNonStrikerName(), is("Bezuidenhout"));
        assertThat(state.getNonStrikerScore(), is("9"));
        assertThat(state.getNonStrikerStats(), is("9 (9)"));
        assertThat(state.getBowlerName(), is("Ecclestone"));
        assertThat(state.getBowlerStats(), is("0.1-0-0-0"));
    }
    @Test
    public void decodeMatchState_gets_dormant_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1426056_dormant.json");

        // act
        MatchState state = decoder.decodeMatchState("1426056", rawJson);

        // assert
        assertThat(state.getId(), is("1426056"));
        assertThat(state.getCurrentState().name(), is("Dormant"));
        assertThat(state.getTeam1().getAbbreviation(), is("NEP"));
        assertThat(state.getTeam2().getAbbreviation(), is("IRE-A"));
        assertThat(state.getScoreDisplay(), is("NEP v IRE-A"));
        assertThat(state.getLead(), is("Match starts in 5:10"));
        assertThat(state.getFact(), is(""));
        assertThat(state.getStrikerName(), is(""));
        assertThat(state.getNonStrikerName(), is(""));
    }

    @Test
    public void decodeMatchState_gets_dormant_after_toss_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1388205_dormant.json");

        // act
        MatchState state = decoder.decodeMatchState("1388205", rawJson);

        // assert
        assertThat(state.getId(), is("1388205"));
        assertThat(state.getCurrentState().name(), is("Dormant"));
        assertThat(state.getTeam1().getAbbreviation(), is("NZ-W"));
        assertThat(state.getTeam2().getAbbreviation(), is("ENG-W"));
        assertThat(state.getScoreDisplay(), is("NZ-W v ENG-W"));
        assertThat(state.getLead(), is("Match starts in 0:07"));
        assertThat(state.getFact(), is(""));
        assertThat(state.getStrikerName(), is("NZ Women won toss, "));
        assertThat(state.getNonStrikerName(), is("will field"));
    }

    @Test
    public void decodeMatchState_gets_not_covered_live_state() throws IOException {
        // arrange
        String rawJson = getResourcesFile("/testdata/cricinfo/match_1391925_not_covered_live.json");

        // act
        MatchState state = decoder.decodeMatchState("1391925", rawJson);

        // assert
        assertThat(state.getId(), is("1391925"));
        assertThat(state.getCurrentState().name(), is("Current"));

        assertThat(state.getTeam1().getAbbreviation(), is("ND"));
        assertThat(state.getTeam2().getAbbreviation(), is("WELL"));

        assertThat(state.getScoreDisplay(), is("282/9"));
        assertThat(state.getScoreDisplayWithOvers(), is("282/9 94.0"));
        assertThat(state.getScore().getDisplay(), is("282/9"));
        assertThat(state.getOvers().getDisplay(), is("94.0"));
        assertThat(state.getFact(), is("Stumps"));
        assertThat(state.getLead(), is("First innings"));
        assertThat(state.getBattingTeamAbbreviation(), is("WELL"));
        assertThat(state.getStrikerName(), is("Blundell"));
        assertThat(state.getStrikerScore(), is("64"));
        assertThat(state.getStrikerStats(), is("64 (115)"));
        assertThat(state.getNonStrikerName(), is("Sears"));
        assertThat(state.getNonStrikerScore(), is("11"));
        assertThat(state.getNonStrikerStats(), is("11 (51)"));
        assertThat(state.getBowlerName(), is("JG Walker"));
        assertThat(state.getBowlerStats(), is("28.0-1-95-4"));
    }
}

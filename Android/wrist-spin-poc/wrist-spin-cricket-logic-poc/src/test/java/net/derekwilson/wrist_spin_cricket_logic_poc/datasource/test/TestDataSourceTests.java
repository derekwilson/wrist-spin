package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;

import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.BaseDataSourceTest;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;

import org.junit.Before;
import org.junit.Test;

public class TestDataSourceTests extends BaseDataSourceTest {

    TestDataSource dataSource;

    @Before
    public void setUp() {
        dataSource = new TestDataSource(mockLogger);
    }

    @Test
    public void getMatchState_gets_id() {
        // arrange

        // act
        MatchState state = dataSource.getMatchState("123");

        // assert
        assertThat(state.getId(), is("123"));
    }

    @Test
    public void getMatchState_gets_score() {
        // arrange

        // act
        MatchState state = dataSource.getMatchState("123");

        // assert
        assertThat(state.getScore().getDisplay(), is("888/8"));
    }

    @Test
    public void getMatchState_gets_overs() {
        // arrange

        // act
        MatchState state = dataSource.getMatchState("123");

        // assert
        assertThat(state.getOvers().getDisplay(), is("123.4"));
    }

    @Test
    public void getMatchState_logs() {
        // arrange

        // act
        dataSource.getMatchState("123");

        // assert
        verify(mockLogger).debug("TestDataSource.getMatchState 123");
    }
}

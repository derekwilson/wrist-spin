package net.derekwilson.wrist_spin_cricket_logic_poc.utility;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;

import net.derekwilson.wrist_spin_cricket_logic_poc.MockingAnnotationSetup;

import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.URL;

public class DataDownloaderTests extends MockingAnnotationSetup {

    // test requires network connection
    @Ignore
    @Test
    public void getJsonStringFromUrl_gets() throws IOException {
        // arrange
        URL source = new URL("https://www.espncricinfo.com/ci/engine/match/1391800.json");

        // act
        String rawJson = DataDownloader.getStringFromUrl(source);

        // assert
        JSONObject result = new JSONObject(rawJson);
        assertNotNull(result.getJSONObject("match"));
    }

    // test requires network connection
    @Ignore
    @Test
    public void getXmlStringFromUrl_gets() throws Exception {
        // arrange
        URL source = new URL("https://static.espncricinfo.com/rss/livescores.xml");

        // act
        String rawXml = DataDownloader.getStringFromUrl(source);

        // assert
        Document doc = XmlHelper.loadXMLFromString(rawXml);
        NodeList nl = XmlHelper.getNodeList(doc, "//rss/channel/item");
        assertThat(nl.getLength(), greaterThan(0));
    }
}

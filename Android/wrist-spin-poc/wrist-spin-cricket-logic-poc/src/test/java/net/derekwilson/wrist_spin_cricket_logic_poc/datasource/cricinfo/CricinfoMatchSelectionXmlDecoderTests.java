package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.BaseDataSourceTest;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class CricinfoMatchSelectionXmlDecoderTests extends BaseDataSourceTest {

    private CricinfoMatchSelectionXmlDecoder decoder;

    @Before
    public void setUp() {
        decoder = new CricinfoMatchSelectionXmlDecoder(mockLogger);
    }

    @Test
    public void decodeMatchSelection_gets() throws Exception {
        // arrange
        String rawXml = getResourcesFile("/testdata/cricinfo/match_selection.xml");

        // act
        List<MatchSelection> matches = decoder.decodeMatchSelectionList(rawXml, "");

        // assert
        assertThat(matches.size(), is(4));
        assertThat(matches.get(0).getId(), is("1388205"));
        assertThat(matches.get(0).getTeam1(), is("New Zealand Women"));
        assertThat(matches.get(0).getScore1(), is("276/10 & 156/4 *"));
        assertThat(matches.get(0).getTeam2(), is("England Women"));
        assertThat(matches.get(0).getScore2(), is("60/3"));

        assertThat(matches.get(1).getId(), is("1425063"));
        assertThat(matches.get(1).getTeam1(), is("Bangladesh Women"));
        assertThat(matches.get(1).getTeam2(), is("Australia Women"));

        assertThat(matches.get(2).getId(), is("1422126"));
        assertThat(matches.get(2).getTeam1(), is("Sunrisers Hyderabad"));
        assertThat(matches.get(2).getTeam2(), is("Mumbai Indians"));

        assertThat(matches.get(3).getId(), is("1398261"));
        assertThat(matches.get(3).getTeam1(), is("South Africa Women"));
        assertThat(matches.get(3).getTeam2(), is("Sri Lanka Women"));
    }
}


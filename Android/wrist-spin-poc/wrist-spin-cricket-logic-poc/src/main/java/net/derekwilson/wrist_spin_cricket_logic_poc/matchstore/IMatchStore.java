package net.derekwilson.wrist_spin_cricket_logic_poc.matchstore;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public interface IMatchStore {
    /**
     * @return true if there are no matches in the store
     */
    boolean isEmpty();

    /**
     * remove all matches from the store
     */
    void reset();

    void addMatch(MatchSelection match);

    void addMatches(List<MatchSelection> matches);

    void replaceAllMatches(@NotNull List<MatchSelectionAndState> matches);

    void removeMatch(String id);

    List<String> getAllIdsInStore();

    @Nullable MatchSelection getMatchSelection(String id);

    @Nullable MatchState getMatchState(String id);

    /**
     * @param id the ID to update, because the state might be null
     * @param state the state of the game, null if we cannot get the state
     * this method will also set the alarm state based on the change in state
     */
    void updateMatchState(String id, @Nullable MatchState state);
}

package net.derekwilson.wrist_spin_cricket_logic_poc.model;

public interface IMatchStateDataDecoder {
    MatchState decodeMatchState(String id, String data);
}



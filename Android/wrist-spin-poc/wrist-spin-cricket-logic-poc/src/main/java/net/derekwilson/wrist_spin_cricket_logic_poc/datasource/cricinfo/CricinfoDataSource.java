package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ICricketDataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.IMatchSelectionDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.IMatchStateDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;
import net.derekwilson.wrist_spin_cricket_logic_poc.utility.DataDownloader;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class CricinfoDataSource implements ICricketDataSource {
    private static final String MATCH_SELECTION = "https://static.espncricinfo.com/rss/livescores.xml";
    private static final String MATCH_STATE_TEMPLATE = "https://www.espncricinfo.com/ci/engine/match/%s.json";

    private IMatchSelectionDataDecoder selectionDecoder;
    private IMatchStateDataDecoder stateDecoder;
    private ILoggingSink loggingSink;

    private CricinfoDataSource(ILoggingSink loggingSink, IMatchSelectionDataDecoder selectionDecoder, IMatchStateDataDecoder decoder) {
        this.loggingSink = loggingSink;
        this.stateDecoder = decoder;
        this.selectionDecoder = selectionDecoder;
    }

    public static @NotNull ICricketDataSource createDataSource(ILoggingSink loggingSink) {
        return new CricinfoDataSource(
                loggingSink,
                new CricinfoMatchSelectionXmlDecoder(loggingSink),
                new CricinfoMatchStateDecoder(loggingSink)
        );
    }

    @Override
    public List<MatchSelection> getMatchSelection() {
        try {
            loggingSink.debug("CricinfoDataSource.getMatchSelection");
            String rawXml = getRawMatchSelectionData();
            return selectionDecoder.decodeMatchSelectionList(rawXml, MATCH_SELECTION);
        } catch (Exception ex) {
            loggingSink.error("error unpacking match selection ", ex);
        }
        return null;
    }

    private String getRawMatchSelectionData() throws IOException {
        loggingSink.debug(String.format("CricinfoDataSource.getRawMatchSelectionData %s", MATCH_SELECTION));
        return DataDownloader.getStringFromUrl(new URL(MATCH_SELECTION));
    }

    @Override
    public MatchState getMatchState(String id) {
        try {
            loggingSink.debug(String.format("CricinfoDataSource.getMatchState %s", id));
            String rawJson = getRawMatchStateData(id);
            return stateDecoder.decodeMatchState(id, rawJson);
        } catch (Exception ex) {
            loggingSink.error("error unpacking selection state ", ex);
        }
        return null;
    }

    private String getRawMatchStateData(String id) throws IOException {
        String urlStr = String.format(MATCH_STATE_TEMPLATE, id);
        loggingSink.debug(String.format("CricinfoDataSource.getRawMatchStateData %s", urlStr));
        return DataDownloader.getStringFromUrl(new URL(urlStr));
    }
}

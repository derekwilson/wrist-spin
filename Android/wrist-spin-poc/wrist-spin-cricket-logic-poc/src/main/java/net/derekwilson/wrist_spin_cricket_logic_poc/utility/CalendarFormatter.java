package net.derekwilson.wrist_spin_cricket_logic_poc.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarFormatter {
    private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("EEE d, MMM, yyyy HH:mm:ss", Locale.getDefault());;
    private static final SimpleDateFormat SORTABLE_DATE_TIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());;
    private static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());;
    private static final SimpleDateFormat SHORT_TIME_FORMATTER = new SimpleDateFormat("HH:mm", Locale.getDefault());;

    public static String convertCalendarToDateTimeString(Calendar date) {
        return DATE_TIME_FORMATTER.format(date.getTime());
    }

    public static String convertCalendarToSortableDate(Calendar date) {
        return SORTABLE_DATE_TIME_FORMATTER.format(date.getTime());
    }

    public static String convertCalendarToTimeString(Calendar date) {
        return TIME_FORMATTER.format(date.getTime());
    }

    public static String convertCalendarToShortTimeString(Calendar date) {
        return SHORT_TIME_FORMATTER.format(date.getTime());
    }

}

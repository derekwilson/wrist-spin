package net.derekwilson.wrist_spin_cricket_logic_poc.model;

import java.util.Calendar;

public class Alerts {
    private static final long MINUTES_FOR_VERY_STALE = 60;
    private static final long MINUTES_FOR_STALE = 5;

    private boolean wicket = false;
    private boolean team50 = false;
    private boolean team100 = false;
    private Calendar lastBallBowled = Calendar.getInstance();

    public boolean isWicket() {
        return wicket;
    }

    public void setWicket(boolean wicket) {
        this.wicket = wicket;
    }

    public boolean isTeam50() {
        return team50;
    }

    public void setTeam50(boolean team50) {
        this.team50 = team50;
    }

    public boolean isTeam100() {
        return team100;
    }

    public void setTeam100(boolean team100) {
        this.team100 = team100;
    }

    public Calendar getLastBallBowled() {
        return lastBallBowled;
    }

    public void setLastBallBowled(Calendar lastBallBowled) {
        this.lastBallBowled = lastBallBowled;
    }

    public long getMinutesSinceLastBallBowled(Calendar now) {
        if (now == null || lastBallBowled == null) {
            return 0;
        }
        long diff = now.getTimeInMillis() - lastBallBowled.getTimeInMillis();
        if (diff < 0) {
            return 0;
        }
        return diff / (60 * 1000);
    }

    public boolean isStale() {
        return getMinutesSinceLastBallBowled(Calendar.getInstance()) >= MINUTES_FOR_STALE;
    }

    public String getDisplayStale() {
        long mins = getMinutesSinceLastBallBowled(Calendar.getInstance());
        if (mins >= MINUTES_FOR_VERY_STALE) {
            return "> 60 mins old";
        }
        if (mins >= MINUTES_FOR_STALE) {
            return String.format("%d mins old", mins);
        }
        return "";
    }
}

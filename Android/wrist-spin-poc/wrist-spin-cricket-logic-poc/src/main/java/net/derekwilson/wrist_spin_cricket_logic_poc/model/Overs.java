package net.derekwilson.wrist_spin_cricket_logic_poc.model;

public class Overs {
    private int complete;
    private int balls;

    public Overs(int complete, int balls) {
        this.complete = complete;
        this.balls = balls;
    }

    public int getComplete() {
        return complete;
    }

    public void setComplete(int complete) {
        this.complete = complete;
    }

    public int getBalls() {
        return balls;
    }

    public void setBalls(int balls) {
        this.balls = balls;
    }

    public String getDisplay() {
        if (complete == 0 && balls == 0) {
            return "";
        }
        return String.format("%s.%s", complete, balls);
    }

    public void parse(String str) {
        complete = 0;
        balls = 0;
        if (str == null || "".equals(str)) {
            return;
        }
        String[] parts = str.split("\\.");
        if (parts.length > 0) {
            complete = Integer.parseInt(parts[0]);
        }
        if (parts.length > 1) {
            balls = Integer.parseInt(parts[1]);
        }
    }
}

package net.derekwilson.wrist_spin_cricket_logic_poc.utility;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HtmlHelper {
    public static Document loadHtmlFromString(String html, String baseUrl) {
        return Jsoup.parse(html, baseUrl);
    }
}

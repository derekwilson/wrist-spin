package net.derekwilson.wrist_spin_cricket_logic_poc.model;

import java.util.List;

public interface IMatchSelectionDataDecoder {
    List<MatchSelection> decodeMatchSelectionList(String data, String baseUrl) throws Exception;
}

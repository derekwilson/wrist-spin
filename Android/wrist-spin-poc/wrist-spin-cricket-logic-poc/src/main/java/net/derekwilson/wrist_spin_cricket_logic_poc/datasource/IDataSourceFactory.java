package net.derekwilson.wrist_spin_cricket_logic_poc.datasource;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.DataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ICricketDataSource;

import org.jetbrains.annotations.NotNull;

public interface IDataSourceFactory {
    @NotNull ICricketDataSource getDataSource(DataSource source);
}

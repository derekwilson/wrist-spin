package net.derekwilson.wrist_spin_cricket_logic_poc.model;

import org.jetbrains.annotations.NotNull;

///
/// logging sink that just discards all messages
///
public class NullLoggingSink implements ILoggingSink {

    @Override
    public void debug(String message) {
        // do nothing
    }

    @Override
    public void error(String message, @NotNull Throwable t) {
        // do nothing
    }
}

package net.derekwilson.wrist_spin_cricket_logic_poc.matchstore;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class MatchStore implements IMatchStore {
    private static final int NO_INDEX = -1;

    private final ILoggingSink loggingSink;
    private final ArrayList<MatchSelectionAndState> matchStore = new ArrayList<>();

    public MatchStore(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    public static @NotNull IMatchStore createStore(ILoggingSink loggingSink) {
        // we do it like this so we can add any module private dependencies here
        return new MatchStore(loggingSink);
    }

    @Override
    public synchronized boolean isEmpty() {
        return matchStore.isEmpty();
    }

    @Override
    public synchronized void reset() {
        matchStore.clear();
    }

    @Override
    public synchronized void addMatch(MatchSelection match) {
        if (match == null) {
            return;
        }
        loggingSink.debug(String.format("MatchStore.addMatch %s", match.getId()));
        matchStore.add(new MatchSelectionAndState(match, null));
    }

    @Override
    public void addMatches(List<MatchSelection> matches) {
        if (matches == null) {
            return;
        }
        for (MatchSelection match : matches) {
            addMatch(match);
        }
    }

    @Override
    public void replaceAllMatches(@NotNull List<MatchSelectionAndState> matches) {
        reset();
        for (MatchSelectionAndState match : matches) {
            matchStore.add(match);
        }
    }

    @Override
    public synchronized void removeMatch(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            matchStore.remove(index);
        }
    }

    @Override
    public synchronized List<String> getAllIdsInStore() {
        List<String> ids = new ArrayList<>();
        for (MatchSelectionAndState thisMatch : matchStore) {
            if (thisMatch.getSelection() != null) {
                ids.add(thisMatch.getSelection().getId());
            }
        }
        return ids;
    }

    @Override
    public synchronized @Nullable MatchSelection getMatchSelection(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            return matchStore.get(index).getSelection();
        }
        return null;
    }

    @Override
    public @Nullable MatchState getMatchState(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            return matchStore.get(index).getState();
        }
        return null;
    }

    @Override
    public void updateMatchState(String id, @Nullable MatchState newState) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            loggingSink.debug(String.format("MatchStore.UpdateMatchState %s", id));
            MatchSelectionAndState matchInStore = matchStore.get(index);
            if (newState != null) {
                newState.setAlertsFromPreviousState(matchInStore.getState(), loggingSink);
            }
            matchInStore.setState(newState);
        } else {
            loggingSink.debug(String.format("MatchStore.UpdateMatchState %s not found", id));
        }
    }

    private int findIdInStore(String id) {
        if (id == null) {
            return NO_INDEX;
        }
        for (int index = 0, size = matchStore.size(); index < size; index++) {
            MatchSelection selection = matchStore.get(index).getSelection();
            if (selection != null && id.equals(selection.getId())) {
                return index;
            }
        }
        return NO_INDEX;
    }
}

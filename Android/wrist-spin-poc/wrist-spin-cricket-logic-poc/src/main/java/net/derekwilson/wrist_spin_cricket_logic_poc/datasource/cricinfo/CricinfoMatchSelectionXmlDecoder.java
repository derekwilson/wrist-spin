package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.IMatchSelectionDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic_poc.utility.XmlHelper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class CricinfoMatchSelectionXmlDecoder implements IMatchSelectionDataDecoder {
    private ILoggingSink loggingSink;

    public CricinfoMatchSelectionXmlDecoder(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    @Override
    public List<MatchSelection> decodeMatchSelectionList(String data, String baseUrl) throws Exception {
        Document doc = XmlHelper.loadXMLFromString(data);
        NodeList nl = XmlHelper.getNodeList(doc, "//rss/channel/item");
        if (nl != null) {
            loggingSink.debug(String.format("CricinfoMatchSelectionXmlDecoder.decodeMatchSelectionList nodes= %s", nl.getLength()));
            List<MatchSelection> returnValue = new ArrayList<>();
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                String title = XmlHelper.getChildNode(e, "title");
                String guid = XmlHelper.getChildNode(e, "guid");

                MatchSelection thisMatch = new MatchSelection(getIdFromGuid(guid));
                if (!"".equals(thisMatch.getId())) {
                    String[] parts = title.split(" v ");
                    if (parts.length > 0) {
                        thisMatch.setTeam1(getTeamFromTitle(parts[0].trim()));
                        thisMatch.setScore1(getScoreFromTitle(parts[0].trim()));
                    }
                    if (parts.length > 1) {
                        thisMatch.setTeam2(getTeamFromTitle(parts[1].trim()));
                        thisMatch.setScore2(getScoreFromTitle(parts[1].trim()));
                    }
                    returnValue.add(thisMatch);
                }
            }
            return returnValue;
        }
        return null;
    }

    private String getTeamFromTitle(String title) {
        if (title == null || title.length() < 1) {
            return "";
        }
        int index = findStartOfScore(title);
        if (index < 0) {
            return title;
        }
        return title.substring(0,index).trim();
    }

    private String getScoreFromTitle(String title) {
        if (title == null || title.length() < 1) {
            return "";
        }
        int index = findStartOfScore(title);
        if (index < 0) {
            return "";
        }
        return title.substring(index).trim();
    }

    private int findStartOfScore(String title) {
        final String SCORE_CHARS = "0123456789 */&";

        if (title == null || title.length() < 1) {
            return -1;
        }
        for (int index = title.length()-1; index>=-1; index--) {
            if (index == -1) {
                return -1;
            }
            boolean isScoreChar = SCORE_CHARS.indexOf(title.charAt(index)) != -1;
            if (!isScoreChar) {
                if (index == title.length()-1) {
                    // there are no score chars in the string
                    return -1;
                }
                // we have found the first char that is not part of the score
                return index+1;
            }
        };
        return -1;
    }

    private String getIdFromGuid(String guid) {
        String[] parts = guid.split("/");
        if (parts.length > 0) {
            String last = parts[parts.length - 1];
            String[] parts2 = last.split("\\.");
            if (parts2.length > 0) {
                return parts2[0];
            }
        }
        return "";
    }
}

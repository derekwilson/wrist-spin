package net.derekwilson.wrist_spin_cricket_logic_poc.datasource.test;

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ICricketDataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.Overs;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.Score;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

///
/// canned in test data for testing
///
public class TestDataSource implements ICricketDataSource {
    private ILoggingSink loggingSink;

    public TestDataSource(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    @Override
    public List<MatchSelection> getMatchSelection() {
        loggingSink.debug("TestDataSource.getMatchSelection");
        List<MatchSelection> matches = new ArrayList<>();

        MatchSelection match1 = new MatchSelection("1", "Team1", "Team2");
        matches.add(match1);

        MatchSelection match2 = new MatchSelection("2", "Dormant", "Match");
        matches.add(match2);

        MatchSelection match3 = new MatchSelection("3", "Complete", "Match");
        matches.add(match3);

        MatchSelection match4 = new MatchSelection("4", "Team3", "Team4");
        matches.add(match4);

        return matches;
    }

    @Override
    public MatchState getMatchState(String id) {
        loggingSink.debug(String.format("TestDataSource.getMatchState %s", id));
        Score score = new Score(888,8);
        Overs overs = new Overs(123,4);
        MatchState state = new MatchState(score,overs);
        state.setId(id);
        state.setCurrentState(MatchState.CurrentState.Current);
        state.getTeam1().setAbbreviation("TEAM1");
        state.getTeam2().setAbbreviation("TEAM2");
        state.setBattingTeamAbbreviation("TEAM1");
        state.setLead("Message");
        state.setStrikerName("Striker");
        state.setStrikerScore("999");
        state.setStrikerStats("199 (122)");
        state.setNonStrikerName("Nonstriker");
        state.setNonStrikerScore("999");
        state.setNonStrikerStats("199 (122)");
        state.setBowlerName("Bowler");
        state.setBowlerStats("12.3-12-199-8");
        state.setFact("Fact");

        switch (id) {
            case "2":   // dormant
                state.setCurrentState(MatchState.CurrentState.Dormant);
                state.setLead("Match starts in 0:07");
                state.setStrikerName("TEAM1 won toss, ");
                state.setStrikerScore("");
                state.setStrikerStats("");
                state.setNonStrikerName("will field");
                state.setNonStrikerScore("");
                state.setNonStrikerStats("");
                break;
            case "3":   // complete
                state.setCurrentState(MatchState.CurrentState.Complete);
                state.setLead("Match starts in 0:07");
                state.setStrikerName("TEAM1 won by");
                state.setStrikerScore("");
                state.setStrikerStats("");
                state.setNonStrikerName("377 runs");
                state.setNonStrikerScore("");
                state.setNonStrikerStats("");
                break;
            case "4":   // other current
                state.setScore(new Score(105,0));
                state.setOvers(new Overs(35,4));
                state.getTeam1().setAbbreviation("WA");
                state.getTeam2().setAbbreviation("TAS");
                state.setBattingTeamAbbreviation("WA");
        }
        return state;
    }
}

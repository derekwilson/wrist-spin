package net.derekwilson.wrist_spin_cricket_logic_poc.model;

public class Player {
    private String id = "";
    private String cardShortName = "";
    private String popularName = "";
    private String name = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardShortName() {
        return cardShortName;
    }

    public void setCardShortName(String cardShortName) {
        this.cardShortName = cardShortName;
    }

    public String getPopularName() {
        return popularName;
    }

    public void setPopularName(String popularName) {
        this.popularName = popularName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String shorten(String name) {
        if (name == null || "".equals(name)) {
            return name;
        }
        String[] bits = name.split(" ");
        for (int index = 0; index < bits.length-1; index++)
            bits[index] = bits[index].substring(0,1);
        return String.join(".", bits);
    }

    public String getDisplay() {
        String returnValue = cardShortName;
        if (returnValue.length() > 10 && popularName.length() > 0) {
            returnValue = popularName;
        }
        if (returnValue.length() > 10) {
            returnValue = shorten(returnValue);
        }
        return returnValue;
    }
}

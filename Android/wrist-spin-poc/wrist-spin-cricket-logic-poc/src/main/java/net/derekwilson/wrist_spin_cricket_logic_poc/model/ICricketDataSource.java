package net.derekwilson.wrist_spin_cricket_logic_poc.model;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ICricketDataSource {
    @Nullable List<MatchSelection> getMatchSelection();

    @Nullable MatchState getMatchState(String id);
}


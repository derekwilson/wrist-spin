package net.derekwilson.wrist_spin_cricket_logic_poc.model;

public class MatchSelection {
    private String id = "";
    private String team1 = "";
    private String score1 = "";
    private String team2 = "";
    private String score2 = "";
    private String status = "";

    public MatchSelection(String id) {
        this.id = id;
    }

    public MatchSelection(String id, String team1, String team2) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getDisplay() {
        return String.format("%s v %s", team1, team2);
    }

    public String getScore1Display() {
        return getScoreDisplay(score1);
    }

    public String getScore2Display() {
        return getScoreDisplay(score2);
    }

    private String getScoreDisplay(String score) {
        if (score == null || "".equals(score)) {
            return "";
        }
        return String.format("(%s)", score);
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status1) {
        this.status = status1;
    }
}

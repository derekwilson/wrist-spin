package net.derekwilson.wrist_spin_cricket_logic_poc.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class DataDownloader {
    public static String getStringFromUrl(URL url) throws IOException {
        InputStream input = null;
        InputStreamReader isr = null;
        BufferedReader reader = null;
        StringBuilder json = new StringBuilder();
        try {
            input = url.openStream();
            isr = new InputStreamReader(input);
            reader = new BufferedReader(isr);
            int c;
            while ((c = reader.read()) != -1) {
                json.append((char) c);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (input != null) {
                input.close();
            }
        }
        return json.toString();
    }
}

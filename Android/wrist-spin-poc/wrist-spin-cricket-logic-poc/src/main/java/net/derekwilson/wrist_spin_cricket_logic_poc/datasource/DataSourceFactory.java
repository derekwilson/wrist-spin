package net.derekwilson.wrist_spin_cricket_logic_poc.datasource;

import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo.Cricinfo2DataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.cricinfo.CricinfoDataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.test.TestDataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.DataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ICricketDataSource;
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink;

import org.jetbrains.annotations.NotNull;

public class DataSourceFactory implements IDataSourceFactory {
    private ILoggingSink loggingSink;

    private ICricketDataSource testDataSource;
    private ICricketDataSource cricinfoDataSource;
    private ICricketDataSource cricinfo2DataSource;

    private DataSourceFactory(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;

        this.testDataSource = new TestDataSource(loggingSink);
        this.cricinfoDataSource = CricinfoDataSource.createDataSource(loggingSink);
        this.cricinfo2DataSource = Cricinfo2DataSource.createDataSource(loggingSink);
    }

    public static @NotNull IDataSourceFactory createFactory(ILoggingSink loggingSink) {
        return new DataSourceFactory(loggingSink);
    }

    @Override
    public @NotNull ICricketDataSource getDataSource(DataSource source) {
        switch (source) {
            case Cricinfo:
                return cricinfoDataSource;
            case Cricinfo2:
                return cricinfo2DataSource;
            case Test:
                return testDataSource;
            default:
                return testDataSource;
        }
    }
}

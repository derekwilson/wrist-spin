package net.derekwilson.wrist_spin_cricket_logic_poc.model;

import net.derekwilson.wrist_spin_cricket_logic_poc.utility.CalendarFormatter;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class MatchState {

    public enum CurrentState {
        Current,
        Dormant,
        Complete
    }

    private String id = "";
    private CurrentState currentState = CurrentState.Complete;
    private Score score = new Score(0,0);
    private Overs overs = new Overs(0,0);
    private Team team1 = new Team();
    private Team team2 = new Team();
    private String fact = "";
    private String lead = "";
    private String battingTeamAbbreviation = "";
    private String strikerName = "";
    private String strikerScore = "";
    private String strikerStats = "";
    private String nonStrikerName = "";
    private String nonStrikerScore = "";
    private String nonStrikerStats = "";
    private String bowlerName = "";
    private String bowlerStats = "";
    private Alerts alerts = new Alerts();
    private Calendar generated = Calendar.getInstance();

    public MatchState(
            Score score,
            Overs overs
    ) {
        this.score = score;
        this.overs = overs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CurrentState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(CurrentState currentState) {
        this.currentState = currentState;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Overs getOvers() {
        return overs;
    }

    public void setOvers(Overs overs) {
        this.overs = overs;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public String getLead() {
        return lead;
    }

    public void setLead(String lead) {
        this.lead = lead;
    }

    public String getStrikerName() {
        return strikerName;
    }

    public void setStrikerName(String strikerName) {
        this.strikerName = strikerName;
    }

    public String getNonStrikerName() {
        return nonStrikerName;
    }

    public void setNonStrikerName(String nonStrikerName) {
        this.nonStrikerName = nonStrikerName;
    }

    public String getBowlerName() {
        return bowlerName;
    }

    public void setBowlerName(String bowlerName) {
        this.bowlerName = bowlerName;
    }

    public String getBattingTeamAbbreviation() {
        return battingTeamAbbreviation;
    }

    public void setBattingTeamAbbreviation(String battingTeamAbbreviation) {
        this.battingTeamAbbreviation = battingTeamAbbreviation;
    }

    public String getStrikerScore() {
        return strikerScore;
    }

    public void setStrikerScore(String strikerScore) {
        this.strikerScore = strikerScore;
    }

    public String getStrikerStats() {
        return strikerStats;
    }

    public void setStrikerStats(String strikerStats) {
        this.strikerStats = strikerStats;
    }

    public String getNonStrikerScore() {
        return nonStrikerScore;
    }

    public void setNonStrikerScore(String nonStrikerScore) {
        this.nonStrikerScore = nonStrikerScore;
    }

    public String getNonStrikerStats() {
        return nonStrikerStats;
    }

    public void setNonStrikerStats(String nonStrikerStats) {
        this.nonStrikerStats = nonStrikerStats;
    }

    public String getBowlerStats() {
        return bowlerStats;
    }

    public void setBowlerStats(String bowlerStats) {
        this.bowlerStats = bowlerStats;
    }

    public Alerts getAlerts() {
        return alerts;
    }

    public void setAlerts(Alerts alerts) {
        this.alerts = alerts;
    }

    public Calendar getGenerated() {
        return generated;
    }

    public void setGenerated(Calendar generated) {
        this.generated = generated;
    }

    @NotNull
    public String getScoreSpeech(Boolean includeBattingTeamName) {
        if (currentState == CurrentState.Current) {
            String teamName = "";
            if (includeBattingTeamName) {
                teamName = String.format("%s, ", battingTeamAbbreviation);
            }
            switch (score.getWickets()) {
                case 0:
                    return String.format("%s%s without loss", teamName, score.getRuns());
                case 10:
                    return String.format("%s%s all out", teamName, score.getRuns());
                default:
                    return String.format("%s%s for %s", teamName, score.getRuns(), score.getWickets());
            }
        }
        return "";
    }

    public String getScoreDisplay() {
        if (currentState == CurrentState.Current) {
            return score.getDisplay();
        }
        // the game is not in play
        return getMatchTitle();
    }

    public String getMatchTitle() {
        return String.format("%s v %s", team1.getAbbreviation(), team2.getAbbreviation());
    }

    public String getScoreDisplayWithOvers() {
        if (currentState == CurrentState.Current) {
            return String.format("%s %s", score.getDisplay(), overs.getDisplay());
        }
        // the game is not in play
        return getMatchTitle();
    }

    public String getStaleDisplay() {
        if (currentState == CurrentState.Current) {
            return alerts.getDisplayStale();
        }
        // the game is not in play
        return "";
    }

    public String getStaleDisplayWithGeneratedTime() {
        if (currentState == CurrentState.Current) {
            String staleStr = alerts.getDisplayStale();
            String generatedStr = CalendarFormatter.convertCalendarToShortTimeString(generated);
            if ("".equals(staleStr)) {
                return generatedStr;
            } else {
                return String.format("%s, %s", generatedStr, staleStr);
            }
        }
        // the game is not in play
        return "";
    }

    public void resetAlerts() {
        alerts.setWicket(false);
        alerts.setTeam50(false);
        alerts.setTeam100(false);
    }

    public void setAlertsFromPreviousState(MatchState previousState, ILoggingSink loggingSink) {
        alerts.setWicket(shouldAlertForWickets(previousState, loggingSink));
        alerts.setTeam50(shouldAlertForScoreThreshold(previousState, loggingSink, 50, previousState != null && previousState.alerts.isTeam50()));
        alerts.setTeam100(shouldAlertForScoreThreshold(previousState, loggingSink, 100, previousState != null && previousState.alerts.isTeam100()));
        alerts.setLastBallBowled(updateLastBallBowled(previousState, loggingSink));
    }

    private Calendar updateLastBallBowled(MatchState previousState, ILoggingSink loggingSink) {
        Calendar newValue = Calendar.getInstance();
        if (previousState == null) {
            loggingSink.debug("setAlertsFromPreviousState.updateLastBallBowled - previous null");
            return newValue;
        }
        if (overs.getBalls() != previousState.overs.getBalls() && overs.getComplete() != previousState.overs.getComplete()) {
            loggingSink.debug("setAlertsFromPreviousState.updateLastBallBowled - new ball bowled");
            return newValue;
        }
        loggingSink.debug("setAlertsFromPreviousState.updateLastBallBowled - retained last value");
        return previousState.alerts.getLastBallBowled();
    }

    private boolean shouldAlertForWickets(MatchState previousState, ILoggingSink loggingSink) {
        if (previousState == null) {
            // ignore uninitialised state
            loggingSink.debug("setAlertsFromPreviousState.shouldAlertForWickets - previous null");
            return false;
        }
        if (previousState.alerts.isWicket()) {
            // persist existing state
            loggingSink.debug("setAlertsFromPreviousState.shouldAlertForWickets - previous state had wicket");
            return true;
        }
        loggingSink.debug(String.format("setAlertsFromPreviousState.shouldAlertForWickets %s<-%s", score.getWickets(), previousState.score.getWickets()));
        return score.getWickets() != previousState.score.getWickets();
    }

    private boolean shouldAlertForScoreThreshold(MatchState previousState, ILoggingSink loggingSink, int boundary, Boolean previousAlert) {
        if (previousState == null) {
            // ignore uninitialised state
            loggingSink.debug(String.format("setAlertsFromPreviousState.shouldAlertForScoreThreshold %s - previous null", boundary));
            return false;
        }
        if (previousAlert) {
            // persist existing state
            loggingSink.debug(String.format("setAlertsFromPreviousState.shouldAlertForScoreThreshold %s - previous state had alert", boundary));
            return true;
        }
        loggingSink.debug(String.format("setAlertsFromPreviousState.shouldAlertForScoreThreshold %s<-%s", score.getRuns(), previousState.score.getRuns()));
        if (score.getRuns() <= previousState.score.getRuns()) {
            return false;
        }
        for (int i = previousState.score.getRuns()+1; i <= score.getRuns(); i++) {
            if (i % boundary == 0) {
                loggingSink.debug(String.format("setAlertsFromPreviousState.shouldAlertForScoreThreshold %s - setting alert", boundary));
                return true;
            }
        }
        return false;
    }

    public static MatchState emptyState() {
        Score score = new Score(0,0);
        Overs overs = new Overs(0,0);
        MatchState state = new MatchState(score,overs);
        state.currentState = CurrentState.Complete;
        return state;
    }
}

package net.derekwilson.wrist_spin_poc.utility

import android.content.Context
import javax.inject.Inject

interface IResourceProvider {
    fun getString(id: Int): String
}

class AndroidResourceProvider
@Inject constructor(
    internal var context: Context
) : IResourceProvider
{
    override fun getString(id: Int): String {
        return context.getString(id)
    }
}
package net.derekwilson.wrist_spin_poc.di

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.lockscreen.LockScreenService

@Subcomponent
interface ILockScreenServiceSubcomponent : AndroidInjector<LockScreenService> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<LockScreenService>
}

@Module(subcomponents = [
    ILockScreenServiceSubcomponent::class
])
abstract class LockScreenServiceModule {
    @Binds
    @IntoMap
    @ClassKey(LockScreenService::class)
    abstract fun bindInjectorFactory(builder: ILockScreenServiceSubcomponent.Factory): AndroidInjector.Factory<*>
}

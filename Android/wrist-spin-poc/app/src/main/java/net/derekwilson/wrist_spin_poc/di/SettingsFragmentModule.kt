package net.derekwilson.wrist_spin_poc.di

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.settings.SettingsFragment

@Subcomponent
interface ISettingsFragmentSubcomponent : AndroidInjector<SettingsFragment> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<SettingsFragment>
}

@Module(subcomponents = [
    ISettingsFragmentSubcomponent::class
])
abstract class SettingsFragmentModule {
    @Binds
    @IntoMap
    @ClassKey(SettingsFragment::class)
    abstract fun bindInjectorFactory(builder: ISettingsFragmentSubcomponent.Factory): AndroidInjector.Factory<*>
}


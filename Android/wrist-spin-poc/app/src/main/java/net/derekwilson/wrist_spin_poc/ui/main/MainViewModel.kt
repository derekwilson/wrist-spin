package net.derekwilson.wrist_spin_poc.ui.main

import android.app.ActivityManager
import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import net.derekwilson.wrist_spin_cricket_logic_poc.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic_poc.matchstore.MatchSelectionAndState
import net.derekwilson.wrist_spin_poc.BuildConfig
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.scoreticker.ScoreTickerService
import net.derekwilson.wrist_spin_poc.utility.IAndroidEnvironmentInformationProvider
import net.derekwilson.wrist_spin_poc.utility.ICrashReporter
import net.derekwilson.wrist_spin_poc.utility.IPermissionChecker
import net.derekwilson.wrist_spin_poc.utility.IResourceProvider
import net.derekwilson.wrist_spin_poc.utility.ISpeechHelper
import net.derekwilson.wrist_spin_poc.utility.SingleLiveEvent
import java.util.Collections
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    app: Application,
    private val loggerFactory: ILoggerFactory,
    private val resourceProvider: IResourceProvider,
    private val applicationContext: Context,
    private val activityManager: ActivityManager,
    private val matchStore: IMatchStore,
    private val speechHelper: ISpeechHelper,
    private val crashReporter: ICrashReporter,
    private val androidEnvironmentInformationProvider: IAndroidEnvironmentInformationProvider,
    private val permissionChecker: IPermissionChecker,
)
    : AndroidViewModel(app), DefaultLifecycleObserver {

    // observables
    data class Observables(
        val displayMessage: MutableLiveData<String> = SingleLiveEvent(),
        val displayText: MutableLiveData<String> = SingleLiveEvent(),
        val scoreTickerRunningState: MutableLiveData<Boolean> = SingleLiveEvent(),
        val displayScoreTickerState: MutableLiveData<Pair<Boolean, String>> = SingleLiveEvent(),
        val activeMatchesLoaded: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val activeMatchMoved: SingleLiveEvent<Pair<Int, Int>> = SingleLiveEvent(),
        val activeMatchRemoved: SingleLiveEvent<Int> = SingleLiveEvent(),
        val activeMatchTitle: MutableLiveData<String> = SingleLiveEvent(),
        val navigateToSettings: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val requestPushNotificationPermission: SingleLiveEvent<Void?> = SingleLiveEvent(),
    )
    val observables = Observables()

    init {
        speechHelper.initialise()
/*
        if (matchStore.isEmpty) {
            matchStore.addMatches(listOf(MatchSelection("1426061", "NEP-A x", "IRE-A x")))
        }
 */
    }

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onCreate")
        requestNotificationPermission()
    }

    override fun onResume(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onResume")
        updateDisplayText()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onDestroy")
        speechHelper.shutdown()
    }

    private fun requestNotificationPermission() {
        if (!permissionChecker.hasPushNotificationPermission()) {
            loggerFactory.logger.debug("MainViewModel.requestPushNotificationPermission")
            observables.requestPushNotificationPermission.call()
        }
    }

    fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("MainViewModel: actionSelected ${itemId}")
        when (itemId) {
            R.id.action_settings -> {
                observables.navigateToSettings.call()
                return true
            }
        }
        return false
    }

    // yes I know its deprecated - see
    // https://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android
    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        loggerFactory.logger.debug("MainViewModel.isMyServiceRunning ${serviceClass.name}")
        for (service in activityManager.getRunningServices(Int.MAX_VALUE)) {
            loggerFactory.logger.debug("->MainViewModel.isMyServiceRunning ${service.service.className}")
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    public fun updateDisplayText() {
        val versionFmt = resourceProvider.getString(R.string.settings_version_summary_fmt)
        val version = String.format(versionFmt, BuildConfig.VERSION_NAME, BuildConfig.GIT_HASH)
        val scoreTickerService = isMyServiceRunning(ScoreTickerService::class.java)
        val isSpeechAvailable = androidEnvironmentInformationProvider.isSpeechAvailable()
        val isDnd = androidEnvironmentInformationProvider.isDnd()
        observables.displayText.value =
            "Version: ${version}, SpeechAvailable: ${isSpeechAvailable}  DND: ${isDnd}, ScoreTicker Service: ${scoreTickerService}"

        updateScoreTickerStateDisplay(scoreTickerService)
    }

    private fun updateScoreTickerStateDisplay(scoreTickerServiceRunning: Boolean) {
        observables.displayScoreTickerState.value = Pair(
            scoreTickerServiceRunning,
            if (scoreTickerServiceRunning) resourceProvider.getString(R.string.row_title_score_ticker_on) else resourceProvider.getString(R.string.row_title_score_ticker_off)
        )
    }

    fun toggleScoreTickerService() {
        val serviceRunning = isMyServiceRunning(ScoreTickerService::class.java)
        if (!serviceRunning && !permissionChecker.hasPushNotificationPermission()) {
            // the app will work without this permission but we should alert the user that the score notification
            loggerFactory.logger.warn("MainViewModel: notifications disabled")
            observables.displayMessage.value = resourceProvider.getString(R.string.notification_permission_denied)
        }
        observables.scoreTickerRunningState.value = !serviceRunning
    }

    fun sayScores() {
        if (!androidEnvironmentInformationProvider.isSpeechAvailable()) {
            return
        }
        val matchIds = matchStore.getAllIdsInStore()
        if (matchIds.size < 1) {
            return
        }
        val state = matchStore.getMatchState(matchIds.get(0))
        state?.let {
            speechHelper.speak(it.getScoreSpeech(true))
        }
    }

    fun clearMatches() {
        matchStore.reset()
        loadActiveMatches()
    }

    private fun setActiveMatchTitle() {
        if (itemCount < 1) {
            observables.activeMatchTitle.value = resourceProvider.getString(R.string.active_matches_title)
            return
        }
        val titleFmt = resourceProvider.getString(R.string.active_matches_title_fmt)
        val title = String.format(titleFmt, itemCount.toString())
        observables.activeMatchTitle.value = title
    }

    fun loadActiveMatches() {
        activeMatches.clear()
        val matchIds = matchStore.getAllIdsInStore()
        for (id in matchIds) {
            loggerFactory.logger.debug("MainViewModel.loadActiveMatches ID= ${id}")
            val match = matchStore.getMatchSelection(id)
            val state = matchStore.getMatchState(id)
            match.let {
                loggerFactory.logger.debug("MainViewModel.loadActiveMatches Adding ID= ${id}")
                activeMatches.add(MatchSelectionAndState(it, state))
            }
        }
        loggerFactory.logger.debug("MainViewModel.loadActiveMatches size= ${activeMatches.size}")
        observables.activeMatchesLoaded.call()
        setActiveMatchTitle()
    }

    fun getMatchId(position: Int): Long {
        return activeMatches[position].selection.id.toLong()
    }

    fun getMatchLabel(position: Int): String {
        return "${activeMatches[position].selection.team1} v ${activeMatches[position].selection.team2}"
    }

    fun matchMoved(fromPosition: Int, toPosition: Int) {
        loggerFactory.logger.debug("MainViewModel.matchMoved ${fromPosition} -> ${toPosition}")

        for (thisMatch in activeMatches) {
            loggerFactory.logger.debug("MainViewModel.matchMoved before ${thisMatch.selection.display}")
        }
        Collections.swap(activeMatches, fromPosition, toPosition)
        for (thisMatch in activeMatches) {
            loggerFactory.logger.debug("MainViewModel.matchMoved after ${thisMatch.selection.display}")
        }
        // we also need to update the store
        matchStore.replaceAllMatches(activeMatches)

        observables.activeMatchMoved.value = Pair(fromPosition, toPosition)
    }

    fun matchOptionSelected(id: Long, position: Int) {
        loggerFactory.logger.debug("MainViewModel.matchOptionSelected ${id}, ${position}")
        // remove from the store (we look the id up in the active list
        matchStore.removeMatch(activeMatches[position].selection.id)
        // remove from the list in the recycler
        activeMatches.removeAt(position)
        observables.activeMatchRemoved.value = position
        setActiveMatchTitle()
    }

    val itemCount: Int
        get() {
            return activeMatches.size
        }

    private var activeMatches: ArrayList<MatchSelectionAndState> = ArrayList()
}

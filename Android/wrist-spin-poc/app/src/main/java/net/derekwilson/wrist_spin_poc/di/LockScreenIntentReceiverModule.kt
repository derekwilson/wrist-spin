package net.derekwilson.wrist_spin_poc.di

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.lockscreen.LockScreenIntentReceiver

@Subcomponent
interface ILockScreenIntentReceiverSubcomponent : AndroidInjector<LockScreenIntentReceiver> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<LockScreenIntentReceiver>
}

@Module(subcomponents = [
    ILockScreenIntentReceiverSubcomponent::class]
)
abstract class LockScreenIntentReceiverModule {
    @Binds
    @IntoMap
    @ClassKey(LockScreenIntentReceiver::class)
    abstract fun bindInjectorFactory(builder: ILockScreenIntentReceiverSubcomponent.Factory): AndroidInjector.Factory<*>
}


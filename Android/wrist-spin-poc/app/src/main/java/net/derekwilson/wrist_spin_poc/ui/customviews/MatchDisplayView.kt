package net.derekwilson.wrist_spin_poc.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import net.derekwilson.wrist_spin_poc.R

class MatchDisplayView : LinearLayout {

    private lateinit var titleTxt: TextView
    private lateinit var subTitleTxt: TextView

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    var title: String? = null
        set(value) {
            field = value
            titleTxt.text = value
        }

    var subtitle: String? = null
        set(value) {
            field = value
            subTitleTxt.text = value
        }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        val view = inflateView(context)
        titleTxt = findViewById<TextView>(R.id.match_display_title_txt);
        subTitleTxt = findViewById<TextView>(R.id.match_display_sub_txt);

        loadAttributes(attrs, defStyle)
    }

    private fun loadAttributes(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.MatchDisplayView, defStyle, 0)

        title = a.getString(R.styleable.MatchDisplayView_title)
        subtitle = a.getString(R.styleable.MatchDisplayView_subtitle)

        a.recycle()
    }

    private fun inflateView(context: Context): View {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.view_match_display, this, true)
    }
}


package net.derekwilson.wrist_spin_poc.di

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import net.derekwilson.wrist_spin_poc.AndroidApplication
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ScoreTickerServiceModule::class,
    ScoreTickerIntentReceiverModule::class,
    LockScreenServiceModule::class,
    LockScreenIntentReceiverModule::class,
    MainActivityModule::class,
    SettingsFragmentModule::class,
    LockScreenActivityModule::class,
    MatchPickerActivityModule::class,
))
interface IApplicationComponent {
    fun inject(application: AndroidApplication)
}



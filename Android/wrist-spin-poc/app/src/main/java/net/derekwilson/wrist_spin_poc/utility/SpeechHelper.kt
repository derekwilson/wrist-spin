package net.derekwilson.wrist_spin_poc.utility

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import javax.inject.Inject


interface ISpeechHelper {
    fun isInitialised(): Boolean
    fun initialise()
    fun shutdown()
    fun speak(str: String)
}

class SpeechHelper @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val applicationContext: Context,
    private val androidEnvironmentInformationProvider: IAndroidEnvironmentInformationProvider
) : ISpeechHelper {

    private var initialised: Boolean = false
    private var tts: TextToSpeech? = null

    override fun isInitialised(): Boolean {
        return initialised
    }

    override fun initialise() {
        tts = TextToSpeech(applicationContext, OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                loggerFactory.logger.debug("TTS Engine initialized")
                onTTSInitialized()
            } else {
                loggerFactory.logger.debug("Internal engine init error.")
            }
        })
    }

    private fun onTTSInitialized() {
        initialised = true
        // useful for debugging
        //logVoices()
    }

    override fun shutdown() {
        tts?.let {
            it.stop()
            it.shutdown()
        }
        tts = null
        initialised = false
    }

    fun logVoices() {
        if (tts == null || !initialised) {
            loggerFactory.logger.debug("Speech engine not initialised")
            return
        }
        tts?.let {
            it.stop()
            for (v in it.voices) {
                loggerFactory.logger.debug("Voice: ${v.name}, ${v.locale.displayName}, ${v.isNetworkConnectionRequired}, ${v.quality}")
            }
        }
    }

    override fun speak(str: String) {
        if (str.length < 1) {
            loggerFactory.logger.debug("SpeechHelper.speak - empty string")
            return
        }
        if (tts == null || !initialised) {
            loggerFactory.logger.debug("SpeechHelper.speak - Speech engine not initialised")
            return
        }
        if (androidEnvironmentInformationProvider.isDnd()) {
            loggerFactory.logger.debug("SpeechHelper.speak - DND")
            return
        }
        tts?.let {
            it.speak(str, TextToSpeech.QUEUE_FLUSH, null, null)
        }
    }
}
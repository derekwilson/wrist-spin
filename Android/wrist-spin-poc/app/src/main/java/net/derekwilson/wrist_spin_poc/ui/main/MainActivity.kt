package net.derekwilson.wrist_spin_poc.ui.main

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.switchmaterial.SwitchMaterial
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.customviews.EmptyRecyclerView
import net.derekwilson.wrist_spin_poc.ui.matchpicker.MatchPickerActivity
import net.derekwilson.wrist_spin_poc.ui.scoreticker.ScoreTickerService
import net.derekwilson.wrist_spin_poc.ui.settings.SettingsActivity
import net.derekwilson.wrist_spin_poc.ui.utility.PermissionRequester
import net.derekwilson.wrist_spin_poc.utility.IPermissionChecker
import javax.inject.Inject


class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    @Inject
    lateinit var permissionChecker: IPermissionChecker

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ActiveMatchRecyclerItemAdapter

    private lateinit var mainText: TextView
    private lateinit var scoreTickerRowContainer: ConstraintLayout
    private lateinit var scoreTickerRowSubLabel: TextView
    private lateinit var scoreTickerRowCheck: SwitchMaterial
    private lateinit var sayScoreButton: Button
    private lateinit var activeMatchTitle: TextView
    private lateinit var rvActiveMatchItems: EmptyRecyclerView
    private lateinit var noDataView: LinearLayout
    private lateinit var fabReset: FloatingActionButton
    private lateinit var fabAdd: FloatingActionButton

    private fun bindControls() {
        mainText = findViewById(R.id.main_text)
        scoreTickerRowContainer = findViewById(R.id.score_ticker_row_container)
        scoreTickerRowSubLabel = findViewById(R.id.score_ticker_row_sub_label)
        scoreTickerRowCheck = findViewById(R.id.score_ticker_row_check)
        sayScoreButton = findViewById(R.id.btnSayScore)
        activeMatchTitle = findViewById(R.id.txtActiveMatchListTitle)
        rvActiveMatchItems = findViewById(R.id.rvActiveMatches)
        noDataView = findViewById(R.id.layNoDataActiveMatches)
        fabReset = findViewById(R.id.fab_main_reset);
        fabAdd = findViewById(R.id.fab_main_add);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("MainActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindControls()

        rvActiveMatchItems.setLayoutManager(LinearLayoutManager(this))
        rvActiveMatchItems.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rvActiveMatchItems.setEmptyView(noDataView)

        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]

        adapter = ActiveMatchRecyclerItemAdapter(this, viewModel, loggerFactory)
        rvActiveMatchItems.setAdapter(adapter)
        val moveCallback = ActiveMatchRecyclerViewMoveCallback(viewModel, loggerFactory)
        val itemTouchHelper = ItemTouchHelper(moveCallback)
        itemTouchHelper.attachToRecyclerView(rvActiveMatchItems)

        lifecycle.addObserver(viewModel)

        setupViewModelObserver()
        setupBindings()
    }

    override fun onResume() {
        loggerFactory.logger.debug("MainActivity.onResume()")
        super.onResume()
    }

    override fun onPause() {
        loggerFactory.logger.debug("MainActivity.onPause()")
        super.onPause()
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("MainActivity.onDestroy()")
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionRequester.MY_PERMISSIONS_REQUEST_NOTIFICATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                    if (permissionChecker.hasPushNotificationPermission()) {
                        loggerFactory.logger.debug("MainActivity notification permission granted")
                    }
                } else {
                    // permission denied
                    showMessage(getString(R.string.notification_permission_denied))
                    loggerFactory.logger.debug("MainActivity notification permission denied")
                }
                return
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (viewModel.actionSelected(item.itemId)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBindings() {
        scoreTickerRowContainer.setOnClickListener {
            loggerFactory.logger.debug("MainActivity.setOnClickListener() - score switch toggle")
            viewModel.toggleScoreTickerService()
        }
        sayScoreButton.setOnClickListener {
            loggerFactory.logger.debug("MainActivity.setOnClickListener() - say score")
            viewModel.sayScores()
        }
        fabReset.setOnClickListener {
            loggerFactory.logger.debug("MainActivity.setOnClickListener() - fab reset")
            viewModel.clearMatches()
            viewModel.updateDisplayText()
        }
        fabAdd.setOnClickListener {
            loggerFactory.logger.debug("MainActivity.setOnClickListener() - fab add")
            val intent = Intent(this, MatchPickerActivity::class.java)
            matchPickerLauncher.launch(intent)
        }
    }

    private val matchPickerLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            loggerFactory.logger.debug("Match Picker Activity Result result ${it.resultCode}")
            if (it.resultCode == Activity.RESULT_OK) {
                loggerFactory.logger.debug("MainActivity - new match selected")
                viewModel.loadActiveMatches()
            }
        }

    private fun setupViewModelObserver() {
        viewModel.observables.displayMessage.observe(this, Observer {
            showMessage(it)
        })
        viewModel.observables.displayText.observe(this, Observer {
            displayText(it)
        })
        viewModel.observables.displayScoreTickerState.observe(this, Observer {
            displayScoreTickerState(it)
        })
        viewModel.observables.scoreTickerRunningState.observe(this, Observer {
            scoreTickerRunningState(it)
        })
        viewModel.observables.activeMatchTitle.observe(this, Observer {
            activeMatchTitle(it)
        })
        viewModel.observables.activeMatchesLoaded.observe(this, Observer {
            activeMatchesLoaded()
        })
        viewModel.observables.activeMatchMoved.observe(this, Observer {
            activeMatchMoved(it)
        })
        viewModel.observables.activeMatchRemoved.observe(this, Observer {
            activeMatchRemoved(it)
        })
        viewModel.observables.navigateToSettings.observe(this, Observer {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        })
        viewModel.observables.requestPushNotificationPermission.observe(this, Observer {
            PermissionRequester.requestPushNotificationPermission(this)
        })
    }

    private fun activeMatchTitle(title: String) {
        activeMatchTitle.text = title
    }

    private fun activeMatchRemoved(position: Int) {
        loggerFactory.logger.debug("MainActivity.activeMatchRemoved() ${position}")
        adapter.notifyItemRemoved(position)
        // see - https://stackoverflow.com/questions/28189371/using-notifyitemremoved-or-notifydatasetchanged-with-recyclerview-in-android
        adapter.notifyItemRangeChanged(position, viewModel.itemCount)
    }

    private fun activeMatchMoved(parameters: Pair<Int, Int>) {
        val (fromPosition, toPosition) = parameters
        loggerFactory.logger.debug("MainActivity.activeMatchMoved() ${fromPosition} -> ${toPosition} ")
        adapter.notifyItemMoved(fromPosition, toPosition)
        // this also worked
        //adapter.notifyItemRangeChanged(if (fromPosition<toPosition) fromPosition else toPosition, viewModel.itemCount)
        // but this is better
        // see - https://stackoverflow.com/questions/32354917/recyclerview-corrupts-view-using-notifyitemmoved
        adapter.notifyItemChanged(fromPosition)
        adapter.notifyItemChanged(toPosition)
    }

    private fun activeMatchesLoaded() {
        loggerFactory.logger.debug("MainActivity.activeMatchesLoaded()")
        adapter.notifyDataSetChanged()
    }

    private fun scoreTickerRunningState(running: Boolean) {
        loggerFactory.logger.debug("MainActivity.scoreTickerRunningState() - ${running}")
        if (running) {
            startService(Intent(this, ScoreTickerService::class.java))
        } else {
            stopService(Intent(this, ScoreTickerService::class.java))
        }
        viewModel.updateDisplayText()
    }

    private fun displayScoreTickerState(parameters: Pair<Boolean, String>) {
        val (isRunning, label) = parameters
        scoreTickerRowCheck.isChecked = isRunning
        scoreTickerRowSubLabel.text = label
    }

    private fun displayText(text: String) {
        mainText.text = text
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
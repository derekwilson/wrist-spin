package net.derekwilson.wrist_spin_poc.ui.scoreticker

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.lockscreen.LockScreenIntentReceiver
import net.derekwilson.wrist_spin_poc.ui.main.MainActivity
import net.derekwilson.wrist_spin_poc.utility.IPreferencesProvider
import net.derekwilson.wrist_spin_poc.utility.IResourceProvider
import javax.inject.Inject

class ScoreTickerService : Service() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var notificationManager: NotificationManager
    @Inject
    lateinit var alarmManager: AlarmManager
    @Inject
    lateinit var resourceProvider: IResourceProvider
    @Inject
    lateinit var preferencesProvider: IPreferencesProvider

    private var broadcastReceiver: BroadcastReceiver? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("ScoreTickerService: onCreate")
        super.onCreate()
    }

    // Unregister receiver
    override fun onDestroy() {
        loggerFactory.logger.debug("ScoreTickerService: onDestroy")
        super.onDestroy()
        stopAlarm()
        unRegisterLockScreenReceiver()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForeground()
        startAlarm()
        registerLockScreenReceiver()
        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "wrist-spin-poc-score-ticker-service", NotificationManager.IMPORTANCE_LOW)

            // Configure the notification channel.
            notificationChannel.description = "wrist-spin-poc-score-ticker-service"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    // Run service in foreground so it is less likely to be killed by system
    private fun startForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        builder
            .setContentTitle(getResources().getString(R.string.app_name))
            .setTicker(getResources().getString(R.string.app_name))
            .setContentText("wrist-spin-poc score ticker service")
            .setSmallIcon(R.drawable.ic_app_foreground)
            .setContentIntent(getNotificationIntent())
            .setOngoing(true)

        loggerFactory.logger.debug("ScoreTickerService: startForeground")
        // lets try and tell the OS we need to stay
        startForeground(FOREGROUND_NOTIFICATION_ID, builder.build())
    }

    private fun getNotificationIntent(): PendingIntent {
        val intent = Intent(this, MainActivity::class.java)
            .setAction(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    private fun startAlarm() {
        var intent = ScoreTickerIntentReceiver.getAlarmOperation(this)
        intent.send()   // the intent receiver will setup the next alarm
    }

    private fun stopAlarm() {
        loggerFactory.logger.debug("ScoreTickerService: stopAlarm")
        ScoreTickerIntentReceiver.stopAlarm(this, alarmManager)
    }

    private fun registerLockScreenReceiver() {
        val lockScreenActive = preferencesProvider.getPreferenceBoolean(
            resourceProvider.getString(R.string.settings_lock_screen_key),
            false
        )
        if (!lockScreenActive) {
            loggerFactory.logger.debug("ScoreTickerService: registerLockScreenReceiver - lock screen not active")
            return;
        }

        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        broadcastReceiver = LockScreenIntentReceiver()
        registerReceiver(broadcastReceiver, filter)
        loggerFactory.logger.debug("ScoreTickerService: registerLockScreenReceiver - lock screen registered")
    }

    private fun unRegisterLockScreenReceiver() {
        broadcastReceiver?.let {
            loggerFactory.logger.debug("ScoreTickerService: unRegisterLockScreenReceiver - lock screen unregistered")
            unregisterReceiver(it)
            broadcastReceiver = null
        }
    }

    companion object {
        // we cannot modify a channel once its created - so we need to increment this ID or reinstall
        private const val NOTIFICATION_CHANNEL_ID = "wrist-spin-poc-score-ticker-service-channel-id-01"
        private const val FOREGROUND_NOTIFICATION_ID = 200
    }

}



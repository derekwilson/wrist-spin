package net.derekwilson.wrist_spin_poc.utility

import com.google.firebase.crashlytics.FirebaseCrashlytics
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import javax.inject.Inject

interface ICrashReporter {
    fun testReporting()
    fun logNonFatalException(e: Throwable)
}

class CrashlyticsReporter @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val environmentInformationProvider: IAndroidEnvironmentInformationProvider,
) : ICrashReporter {

    // to see crashes goto
    // https://console.firebase.google.com/project/wrist-spin/crashlytics

    init {
        loggerFactory.logger.debug("CrashlyticsReporter: init setting custom keys")
        val crashlytics = FirebaseCrashlytics.getInstance()
        crashlytics.setCustomKey("isWsa", environmentInformationProvider.isWsa() /* boolean value */)
        crashlytics.setCustomKey("isKindle", environmentInformationProvider.isKindle() /* boolean value */)
        crashlytics.setCustomKey("isSpeechAvailable", environmentInformationProvider.isSpeechAvailable() /* boolean value */)
        crashlytics.setCustomKey("isDnd", environmentInformationProvider.isDnd() /* boolean value */)
        crashlytics.setCustomKey("uiMode", environmentInformationProvider.uiMode() /* string value */)
        crashlytics.setCustomKey("fontScaling", environmentInformationProvider.fontScaling() /* double value */)
    }

    override fun logNonFatalException(e: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(e)
    }

    override fun testReporting() {
        throw RuntimeException("Test Crash Reporting");
    }
}


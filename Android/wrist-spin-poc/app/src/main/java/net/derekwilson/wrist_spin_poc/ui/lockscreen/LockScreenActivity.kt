package net.derekwilson.wrist_spin_poc.ui.lockscreen

import android.app.Activity
import android.app.KeyguardManager
import android.os.Bundle
import android.view.WindowManager
import android.widget.LinearLayout
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_cricket_logic_poc.matchstore.IMatchStore
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.customviews.MatchDisplayView
import javax.inject.Inject

class LockScreenActivity : Activity() {

    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var matchStore: IMatchStore
    @Inject
    lateinit var keyGuardManager: KeyguardManager

    private lateinit var lockscreenContainer: LinearLayout

    private fun bindControls() {
        lockscreenContainer = findViewById(R.id.lockscreen_container)
    }

    private fun setupBindings() {
        lockscreenContainer.setOnClickListener {
            loggerFactory.logger.debug("LockScreenActivity.setOnClickListener() - container")
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("LockScreenActivity.onCreate()")

        setupWindow()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lockscreen)
        bindControls()
        setupBindings()
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("LockScreenActivity.onDestroy()")
        super.onDestroy()
    }

    override fun onResume() {
        loggerFactory.logger.debug("LockScreenActivity.onResume()")
        super.onResume()
        setupLockText()
        if (!keyGuardManager.isKeyguardLocked) {
            // user unlocked the device - they dont want a lock screen
            loggerFactory.logger.debug("LockScreenActivity.onResume() - device unlocked")
            finish()
        }
    }

    override fun onPause() {
        loggerFactory.logger.debug("LockScreenActivity.onPause()")
        super.onPause()
        sendAppToBack()
    }

    private fun setupWindow() {
        loggerFactory.logger.debug("LockScreenActivity.setupWindow()")
        this.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                    or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
        )
        loggerFactory.logger.debug("LockScreenActivity.setupWindow() - flags added")
    }

    override fun onAttachedToWindow() {
        loggerFactory.logger.debug("LockScreenActivity.onAttachedToWindow()")
        super.onAttachedToWindow()
    }

    private fun sendAppToBack() {
        loggerFactory.logger.debug("LockScreenActivity.sendAppToBack()")
        moveTaskToBack(true)
    }

    private fun setupNoData() {
        val display = MatchDisplayView(this)
        display.title = getString(R.string.match_display_no_data)
        display.subtitle = getString(R.string.app_name)
        lockscreenContainer.addView(display)
    }

    private fun setupLockText() {
        lockscreenContainer.removeAllViews()
        var addedAMatch = false
        for (id in matchStore.getAllIdsInStore()) {
            val state = matchStore.getMatchState(id)
            state?.let {
                var title = it.scoreDisplay
                val stringBuilder = StringBuilder()
                loggerFactory.logger.debug("LockScreenActivity.setupLockText - ${it.matchTitle}")
                stringBuilder.clear()
                if (it.battingTeamAbbreviation.isNotBlank()) {
                    stringBuilder.append(it.battingTeamAbbreviation)
                    stringBuilder.append("\n")
                }
                if (it.overs.display.isNotBlank()) {
                    stringBuilder.append(it.overs.display)
                    stringBuilder.append("\n")
                }
                if (it.strikerName.isNotBlank()) {
                    stringBuilder.append(it.strikerName)
                    if (it.strikerStats.isNotBlank()) {
                        stringBuilder.append(" ")
                        stringBuilder.append(it.strikerStats)
                    }
                    stringBuilder.append("\n")
                }
                if (it.nonStrikerName.isNotBlank()) {
                    stringBuilder.append(it.nonStrikerName)
                    if (it.nonStrikerStats.isNotBlank()) {
                        stringBuilder.append(" ")
                        stringBuilder.append(it.nonStrikerStats)
                    }
                    stringBuilder.append("\n")
                }
                if (it.lead.isNotBlank()) {
                    stringBuilder.append(it.lead)
                    stringBuilder.append("\n")
                }
                if (it.fact.isNotBlank()) {
                    stringBuilder.append(it.fact)
                    stringBuilder.append("\n")
                }
                stringBuilder.append(it.staleDisplay)
                val display = MatchDisplayView(this)
                display.title = title
                display.subtitle = stringBuilder.toString()
                lockscreenContainer.addView(display)
                addedAMatch = true
            }
        }
        if (!addedAMatch) {
            // show something
            setupNoData()
        }
    }
}
package net.derekwilson.wrist_spin_poc.ui.lockscreen

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.main.MainActivity
import javax.inject.Inject

class LockScreenService : Service() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var notificationManager: NotificationManager

    private var broadcastReceiver: BroadcastReceiver? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("LockScreenService: onCreate")
        super.onCreate()
    }

    // Register for Lockscreen event intents
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        broadcastReceiver = LockScreenIntentReceiver()
        registerReceiver(broadcastReceiver, filter)
        startForeground()
        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "wrist-spin poc-lock-screen", NotificationManager.IMPORTANCE_LOW)

            // Configure the notification channel.
            notificationChannel.description = "wrist-spin poc-lock-screen"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    // Run service in foreground so it is less likely to be killed by system
    private fun startForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        builder
            .setContentTitle(getResources().getString(R.string.app_name))
            .setTicker(getResources().getString(R.string.app_name))
            .setContentText("wrist-spin-poc lock screen active")
            .setSmallIcon(R.drawable.ic_app_foreground)
            .setContentIntent(getNotificationIntent())
            .setOngoing(true)
        startForeground(FOREGROUND_NOTIFICATION_ID, builder.build())
    }

    private fun getNotificationIntent(): PendingIntent {
        val intent = Intent(this, MainActivity::class.java)
            .setAction(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    // Unregister receiver
    override fun onDestroy() {
        super.onDestroy()
        loggerFactory.logger.debug("LockScreenService: onDestroy")
        unregisterReceiver(broadcastReceiver)
    }

    companion object {
        // we cannot modify a channel once its created - so we need to increment this ID or reinstall
        private const val NOTIFICATION_CHANNEL_ID = "wrist-spin-poc-lock-screen-channel-id-01"
        private const val FOREGROUND_NOTIFICATION_ID = 100
    }

}

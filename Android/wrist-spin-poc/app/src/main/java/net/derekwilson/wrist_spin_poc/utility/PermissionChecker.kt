package net.derekwilson.wrist_spin_poc.utility

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import javax.inject.Inject

interface IPermissionChecker {
    fun hasPushNotificationPermission(): Boolean
}

class PermissionChecker
@Inject constructor(
    internal var context: Context,
) : IPermissionChecker
{

    override fun hasPushNotificationPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return hasPermissionBeenGranted(Manifest.permission.POST_NOTIFICATIONS)
        }
        // earlier versions didnt need to ask
        return true
    }

    private fun hasPermissionBeenGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }
}


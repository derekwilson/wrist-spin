package net.derekwilson.wrist_spin_poc.ui.scoreticker

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import dagger.android.AndroidInjection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.derekwilson.wrist_spin_cricket_logic_poc.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchState
import net.derekwilson.wrist_spin_cricket_logic_poc.utility.CalendarFormatter
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.main.MainActivity
import net.derekwilson.wrist_spin_poc.utility.ICoroutineDispatcherProvider
import net.derekwilson.wrist_spin_poc.utility.ICrashReporter
import net.derekwilson.wrist_spin_poc.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin_poc.utility.IPreferencesProvider
import net.derekwilson.wrist_spin_poc.utility.IResourceProvider
import net.derekwilson.wrist_spin_poc.utility.ISpeechHelper
import net.derekwilson.wrist_spin_poc.utility.ISystemTime
import javax.inject.Inject

class ScoreTickerIntentReceiver : BroadcastReceiver() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var resourceProvider: IResourceProvider
    @Inject
    lateinit var preferencesProvider: IPreferencesProvider
    @Inject
    lateinit var notificationManager: NotificationManager
    @Inject
    lateinit var alarmManager: AlarmManager
    @Inject
    lateinit var powerManager: PowerManager
    @Inject
    lateinit var systemTime: ISystemTime
    @Inject
    lateinit var cricketDataSourceProvider: ICricketDataSourceProvider
    @Inject
    lateinit var matchStore: IMatchStore
    @Inject
    lateinit var coroutineDispatcherProvider: ICoroutineDispatcherProvider
    @Inject
    lateinit var speechHelper: ISpeechHelper
    @Inject
    lateinit var crashReporter: ICrashReporter

    private val job = Job()
    private var scope:CoroutineScope? = null

    // Handle actions and display Lockscreen
    override fun onReceive(context: Context, intent: Intent) {
        // we only have 10 seconds if the device was asleep
        AndroidInjection.inject(this, context)
        val lock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TICKER_WAKELOCK_TAG)
        lock?.let { powerLock ->
            powerLock.acquire(TICKER_WAKELOCK_TIMEOUT)
            loggerFactory.logger.debug("ScoreTickerIntentReceiver: onReceive ${intent.action}")
            // we need to do repeating alarms ourselves
            startAlarm(context, alarmManager, systemTime, loggerFactory, preferencesProvider, resourceProvider)

            if (scope == null) {
                scope = CoroutineScope(coroutineDispatcherProvider.getCoroutineDispatcher() + job)
            }
            scope?.launch {
                try {
                    getMatchStateAndShowNotifications(context)
                } catch (ex: Exception) {
                    loggerFactory.logger.error("ScoreTickerService: ", ex)
                    crashReporter.logNonFatalException(ex)
                } finally {
                    loggerFactory.logger.debug("ScoreTickerIntentReceiver: release lock")
                    powerLock.release()
                }
            }
        }
    }

    private fun getMatchStateAndShowNotifications(context: Context) {
        val dataSource = cricketDataSourceProvider.getDataSource()
        val ids = matchStore.allIdsInStore
        val multipleMatches = ids.size > 1
        for (id in ids) {
            val state = dataSource.getMatchState(id)
            state?.let {
                matchStore.updateMatchState(id, it)
                showTicker(context, it, multipleMatches)
                if (isSpeechNeeded(it)) {
                    speechHelper.speak(it.getScoreSpeech(multipleMatches))
                }
                it.resetAlerts()
            } ?: run {
                loggerFactory.logger.debug("ScoreTickerIntentReceiver:getMatchStateAndShowNotifications - null state")
            }
        }
    }

    private fun isSoundNeeded(state: MatchState): Boolean {
        if (state.alerts.isWicket && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_alert_wicket_key), false)) {
            return true
        }
        if (state.alerts.isTeam50 && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_alert_team50_key), false)) {
            return true
        }
        if (state.alerts.isTeam100 && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_alert_team100_key), false)) {
            return true
        }
        return false
    }

    private fun isSpeechNeeded(state: MatchState): Boolean {
        if (state.alerts.isWicket && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_speak_wicket_key), false)) {
            return true
        }
        if (state.alerts.isTeam50 && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_speak_team50_key), false)) {
            return true
        }
        if (state.alerts.isTeam100 && preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_speak_team100_key), false)) {
            return true
        }
        return false
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createTickerChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(TICKER_NOTIFICATION_CHANNEL_ID, "wrist-spin-poc-score-ticker", NotificationManager.IMPORTANCE_HIGH)

            // Configure the notification channel.
            notificationChannel.description = "wrist-spin-poc-score-ticker"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun showTicker(context: Context, state: MatchState, multipleMatches: Boolean) {
        var silentAlert = !isSoundNeeded(state)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createTickerChannel()
        }
        val builder = NotificationCompat.Builder(context, TICKER_NOTIFICATION_CHANNEL_ID)

        // icon from - https://freesvg.org/cricket-bat
        builder
            .setTicker(getNotificationContentTitle(state))
            .setSubText(getNotificationSubTitle(state))
            .setContentTitle(getNotificationContentTitle(state))
            .setStyle(NotificationCompat.BigTextStyle().bigText(getNotificationContentText(state)))
            .setContentText(getNotificationContentText(state))
            .setSmallIcon(R.drawable.ic_cricket_bat)
            .setColor(ContextCompat.getColor(context, R.color.red_light))
            .setColorized(true)
            .setContentIntent(getNotificationIntent(context))
            .setOngoing(false)
            .setSilent(silentAlert)

        val id = getNotificationId(state)
        loggerFactory.logger.debug("ScoreTickerService:showTicker ${id}, ${CalendarFormatter.convertCalendarToTimeString(systemTime.getCurrentTime())}, silent = ${silentAlert}")
        notificationManager.notify(id, builder.build())
    }

    private fun getNotificationId(state: MatchState): Int {
        val id = state.id.toIntOrNull()
        id?.let {
            return it
        } ?: run {
            return TICKER_NOTIFICATION_ID
        }
    }

    private fun getNotificationSubTitle(state: MatchState): String {
        return "${state.matchTitle}";
    }

    private fun getNotificationContentTitle(state: MatchState): String {
        return "${state.scoreDisplayWithOvers}";
    }

    private fun getNotificationContentText(state: MatchState): String {
        val stringBuilder = StringBuilder()
        if (state.strikerName.isNotBlank()) {
            stringBuilder.append(state.strikerName)
            if (state.strikerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.strikerStats)
            }
        }
        if (state.nonStrikerName.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append(", ")
                stringBuilder.append(state.nonStrikerName)
            }
            if (state.nonStrikerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.nonStrikerStats)
            }
        }
        if (state.bowlerName.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            stringBuilder.append(state.bowlerName)
            if (state.bowlerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.bowlerStats)
            }
        }
        if (state.lead.isNotBlank() || state.fact.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            if (state.lead.isNotBlank()) {
                stringBuilder.append(state.lead)
                if (state.fact.isNotBlank()) {
                    if (state.lead.isNotBlank()) {
                        stringBuilder.append(", ")
                    }
                    stringBuilder.append(state.fact)
                }
            }
        }
        if (state.staleDisplayWithGeneratedTime.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            stringBuilder.append(state.staleDisplayWithGeneratedTime)
        }
        return stringBuilder.toString()

        //return "${state.strikerName} ${state.strikerStats}, ${state.nonStrikerName} ${state.nonStrikerStats}\n${state.bowlerName} ${state.bowlerStats}\n${state.lead}, ${state.fact}\n${state.staleDisplayWithGeneratedTime}"
    }

    private fun getNotificationIntent(context: Context): PendingIntent {
        val intent = Intent(context, MainActivity::class.java)
            .setAction(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    companion object {
        private const val TICKER_WAKELOCK_TAG = "wrist-spin-dbg::wakelock"
        private const val TICKER_WAKELOCK_TIMEOUT = 10 * 60 * 1000L         // we have 10 mins to do our stuff

        private const val TICKER_NOTIFICATION_CHANNEL_ID = "wrist-spin-poc-score-ticker-channel-id-02"
        private const val TICKER_NOTIFICATION_ID = 1000

        private const val MILLISECONDS_IN_A_MINUTE = 1 * 60 * 1000L

        public fun startAlarm(
            context: Context,
            alarmManager: AlarmManager,
            systemTime: ISystemTime,
            loggerFactory: ILoggerFactory,
            preferencesProvider: IPreferencesProvider,
            resourceProvider: IResourceProvider
        ) {
            val intervalInMins = preferencesProvider.getPreferencesNumber(
                resourceProvider.getString(R.string.settings_ticker_update_mins_key),
                resourceProvider.getString(R.string.settings_ticker_update_mins_default)
            )
            val interval = intervalInMins * MILLISECONDS_IN_A_MINUTE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                loggerFactory.logger.debug("ScoreTickerService: setAndAllowWhileIdle, interval = ${intervalInMins}, ${interval}")
                alarmManager.setAndAllowWhileIdle(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    systemTime.getBootElapsedMillis() + interval,
                    getAlarmOperation(context)
                )
            } else {
                loggerFactory.logger.debug("ScoreTickerService: set, interval = ${intervalInMins}, ${interval}")
                alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    systemTime.getBootElapsedMillis() + interval,
                    getAlarmOperation(context)
                )
            }
        }

        public fun stopAlarm(context: Context, alarmManager: AlarmManager) {
            alarmManager.cancel(getAlarmOperation(context))
        }

        public fun getAlarmOperation(context: Context): PendingIntent {
            val intent = Intent(context, ScoreTickerIntentReceiver::class.java)
            return PendingIntent.getBroadcast(
                context,
                TICKER_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        }
    }
}


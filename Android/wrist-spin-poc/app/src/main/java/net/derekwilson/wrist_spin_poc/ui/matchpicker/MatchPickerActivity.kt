package net.derekwilson.wrist_spin_poc.ui.matchpicker

import android.app.Activity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.ui.customviews.EmptyRecyclerView
import net.derekwilson.wrist_spin_poc.ui.customviews.ProgressSpinnerView
import net.derekwilson.wrist_spin_poc.ui.utility.ProgressViewHelper
import javax.inject.Inject


class MatchPickerActivity: AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    private lateinit var viewModel: MatchPickerViewModel
    private lateinit var adapter: MatchPickerRecyclerItemAdapter

    private lateinit var rvMatchItems: EmptyRecyclerView
    private lateinit var noDataView: LinearLayout
    private lateinit var progressSpinner: ProgressSpinnerView

    private fun bindControls() {
        rvMatchItems = findViewById(R.id.rvMatches)
        noDataView = findViewById(R.id.layNoDataMatches)
        progressSpinner = findViewById(R.id.progressBarMatchPicker)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("MatchPickerActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_matchpicker)
        bindControls()

        rvMatchItems.setLayoutManager(LinearLayoutManager(this))
        rvMatchItems.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rvMatchItems.setEmptyView(noDataView)

        viewModel = ViewModelProvider(this, viewModelFactory)[MatchPickerViewModel::class.java]

        adapter = MatchPickerRecyclerItemAdapter(this, viewModel, loggerFactory)
        rvMatchItems.setAdapter(adapter)

        lifecycle.addObserver(viewModel)

        setupViewModelObserver()
    }

    override fun onResume() {
        loggerFactory.logger.debug("MatchPickerActivity.onResume()")
        super.onResume()
    }

    override fun onPause() {
        loggerFactory.logger.debug("MatchPickerActivity.onPause()")
        super.onPause()
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("MatchPickerActivity.onDestroy()")
        lifecycle.removeObserver(viewModel)
        super.onDestroy()
    }

    private fun setupViewModelObserver() {
        viewModel.observables.displayMessage.observe(this, Observer {
            showMessage(it)
        })
        viewModel.observables.matchesLoaded.observe(this) {
            matchesLoaded()
        }
        viewModel.observables.startProgress.observe(this) {
            startProgress()
        }
        viewModel.observables.completeProgress.observe(this) {
            completeProgress()
        }
        viewModel.observables.exit.observe(this) {
            exitWithResult(it)
        }
    }

    private fun exitWithResult(id: String?) {
        setResult(Activity.RESULT_OK)
        // TODO - actually return the ID
        finish()
    }

    private fun startProgress() {
        ProgressViewHelper.startProgress(progressSpinner, window)
    }

    private fun completeProgress() {
        ProgressViewHelper.completeProgress(progressSpinner, window)
    }

    private fun matchesLoaded() {
        adapter?.notifyDataSetChanged()
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
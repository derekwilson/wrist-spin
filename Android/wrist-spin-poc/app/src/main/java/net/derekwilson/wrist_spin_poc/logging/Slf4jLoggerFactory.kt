package net.derekwilson.wrist_spin_poc.logging

import net.derekwilson.wrist_spin_poc.AndroidApplication
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class Slf4jLoggerFactory @Inject constructor() : ILoggerFactory
{
    override val logger: Logger
        get() = LoggerFactory.getLogger(AndroidApplication::class.java)
}

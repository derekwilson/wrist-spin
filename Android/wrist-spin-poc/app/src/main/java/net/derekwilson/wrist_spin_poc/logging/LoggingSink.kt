package net.derekwilson.wrist_spin_poc.logging

import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink
import net.derekwilson.wrist_spin_poc.utility.ICrashReporter
import javax.inject.Inject

// acts as a bridge from the lobrary to our logs
class LoggingSink @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val reporter: ICrashReporter,
) : ILoggingSink {

    override fun debug(message: String?) {
        loggerFactory.logger.debug(message)
    }

    override fun error(message: String?, t: Throwable) {
        loggerFactory.logger.error(message, t)
        reporter.logNonFatalException(t)
    }
}
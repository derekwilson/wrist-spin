package net.derekwilson.wrist_spin_poc.utility

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import javax.inject.Inject

interface IPreferencesProvider {
    fun setDefaultValues(settingsId: Int)
    fun sanitiseNumber(defaultNumber: String, numberAsString: String?): Int
    fun getPreferenceString(key: String, defaultValue: String): String
    fun getPreferencesNumber(key: String, defaultValue: String): Int
    fun setPreferenceString(key: String, value: String)
    fun getPreferenceFloat(key: String, defaultValue: Float): Float
    fun setPreferenceFloat(key: String, value: Float)
    fun getPreferenceBoolean(key: String, defaultValue: Boolean): Boolean
    fun setPreferenceBoolean(key: String, value: Boolean)
    fun <T : Enum<T>> getPreferenceEnum(key: String, enumClass: Class<T>, defaultValue: T): T
}

class AndroidDefaultSharedPreferencesProvider
@Inject constructor(
    internal var context: Context,
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
) : IPreferencesProvider {
    private fun getPreferences(): SharedPreferences {
        // by default the SettingsFragment uses the default shared preferences so we need to use them here as well
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    override fun setDefaultValues(settingsId: Int) {
        // by default the SettingsFragment uses the default shared preferences so we need to use them here as well
        PreferenceManager.setDefaultValues(context, settingsId, false)
    }

    override fun sanitiseNumber(defaultNumber: String, numberAsString: String?): Int {
        var number = defaultNumber.toInt()
        try {
            if (numberAsString != null) {
                number = numberAsString.toInt()
            }
        } catch (ex: Exception) {
            crashReporter.logNonFatalException(ex)
            loggerFactory.logger.error("AndroidDefaultSharedPreferencesProvider.sanitiseNumber()", ex)
        }
        return number
    }

    override fun getPreferenceString(key: String, defaultValue: String): String {
        return getPreferences().getString(key, defaultValue) ?: defaultValue
    }

    override fun getPreferencesNumber(key: String, defaultValue: String): Int {
        val numberAsString = getPreferenceString(key, defaultValue)
        return sanitiseNumber(defaultValue, numberAsString)
    }

    override fun setPreferenceString(key: String, value: String) {
        getPreferences().edit()
            .putString(key, value)
            .apply()
    }

    override fun getPreferenceFloat(key: String, defaultValue: Float): Float {
        return getPreferences().getFloat(key, defaultValue)
    }

    override fun setPreferenceFloat(key: String, value: Float) {
        getPreferences().edit()
            .putFloat(key, value)
            .apply()
    }

    override fun getPreferenceBoolean(key: String, defaultValue: Boolean): Boolean {
        return getPreferences().getBoolean(key, defaultValue)
    }

    override fun setPreferenceBoolean(key: String, value: Boolean) {
        getPreferences().edit()
            .putBoolean(key, value)
            .apply()
    }

    override fun <T : Enum<T>> getPreferenceEnum(key: String, enumClass: Class<T>, defaultValue: T): T {
        val stringValue = getPreferenceString(key, defaultValue.toString())
        loggerFactory.logger.debug("AndroidDefaultSharedPreferencesProvider: getting user setting ${key}, ${stringValue}")
        // We'd like to do this, but we can't
        //return T.valueOf<T>(enumClass, stringValue)
        return java.lang.Enum.valueOf(enumClass, stringValue)
    }
}



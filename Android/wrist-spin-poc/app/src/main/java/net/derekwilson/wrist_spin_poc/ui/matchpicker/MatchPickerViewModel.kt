package net.derekwilson.wrist_spin_poc.ui.matchpicker

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.derekwilson.wrist_spin_cricket_logic_poc.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic_poc.model.MatchSelection
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import net.derekwilson.wrist_spin_poc.utility.ICoroutineDispatcherProvider
import net.derekwilson.wrist_spin_poc.utility.ICrashReporter
import net.derekwilson.wrist_spin_poc.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin_poc.utility.IResourceProvider
import net.derekwilson.wrist_spin_poc.utility.SingleLiveEvent
import javax.inject.Inject

class MatchPickerViewModel
@Inject constructor(
    app: Application,
    private val loggerFactory: ILoggerFactory,
    private val resourceProvider: IResourceProvider,
    private val applicationContext: Context,
    private val matchStore: IMatchStore,
    private val cricketDataSourceProvider: ICricketDataSourceProvider,
    private val coroutineDispatcherProvider: ICoroutineDispatcherProvider,
    private val crashReporter: ICrashReporter,
)
    : AndroidViewModel(app), DefaultLifecycleObserver {

    data class Observables(
        val displayMessage: MutableLiveData<String> = SingleLiveEvent(),
        val matchesLoaded: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val startProgress: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val completeProgress: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val exit: MutableLiveData<String> = SingleLiveEvent(),
    )
    val observables = Observables()

    private val job = Job()
    private var scope: CoroutineScope = CoroutineScope(coroutineDispatcherProvider.getCoroutineDispatcher() + job)

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MatchPickerViewModel.onCreate")
    }

    override fun onResume(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MatchPickerViewModel.onResume")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MatchPickerViewModel.onDestroy")
    }

    private var matches: List<MatchSelection> = emptyList()
    val itemCount: Int
        get() {
            return matches.size
        }

    fun loadMatches() {
        loggerFactory.logger.debug("MatchPickerViewModel.loadMatches")
        observables.startProgress.call()
        scope.launch {
            try {
                val dataSource = cricketDataSourceProvider.getDataSource()
                val loadedMatches = dataSource.getMatchSelection();
                if (loadedMatches != null) {
                    for (match in loadedMatches) {
                        loggerFactory.logger.debug("loadMatches:match ${match.id}, ${match.display}")
                    }
                    matches = loadedMatches
                    withContext (coroutineDispatcherProvider.getMainContext())
                    {
                        observables.matchesLoaded.call()
                        observables.completeProgress.call()
                    }
                }
            } catch (ex: Exception) {
                loggerFactory.logger.error("loadMatches: ", ex)
                crashReporter.logNonFatalException(ex)
            }
        }
    }

    fun getMatchId(position: Int): Long {
        return matches[position].id.toLong()
    }

    fun getMatchLabel(position: Int): String {
        return "${matches[position].team1}${addScore(matches[position].score1)}\nv\n${matches[position].team2}${addScore(matches[position].score2)}"
    }

    private fun addScore(score: String): String {
        if (score == "") {
            return ""
        }
        return " (${score})"
    }

    fun getMatchSubLabel(position: Int): String {
        return matches[position].status
    }

    fun matchSelected(id: Long, position: Int) {
        loggerFactory.logger.debug("MatchPickerViewModel:matchSelected ${id}, ${position}")
        matchStore.addMatch(matches[position])
        observables.exit.value = id.toString()
    }

}
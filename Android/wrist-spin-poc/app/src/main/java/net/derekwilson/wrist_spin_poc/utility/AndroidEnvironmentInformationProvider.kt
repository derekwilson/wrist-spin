package net.derekwilson.wrist_spin_poc.utility

import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings.Global
import android.speech.tts.TextToSpeech
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import javax.inject.Inject

interface IAndroidEnvironmentInformationProvider {
    fun isKindle(): Boolean
    fun isWsa(): Boolean
    fun isSpeechAvailable(): Boolean
    fun isDnd(): Boolean
    fun fontScaling(): Float
    fun uiMode(): String
}

class AndroidEnvironmentInformationProvider @Inject constructor(
    private val applicationContext: Context,
    private val resourceProvider: IResourceProvider,
    private val loggerFactory: ILoggerFactory,
    private val notificationManager: NotificationManager,
    private val contentResolver: ContentResolver,
) : IAndroidEnvironmentInformationProvider {
    override fun isKindle(): Boolean {
        return Build.MANUFACTURER.equals("Amazon", true)
                && (Build.MODEL.equals("Kindle Fire", true)
                || Build.MODEL.startsWith("KF", true));
    }

    override fun isWsa(): Boolean {
        return Build.MANUFACTURER.equals("Microsoft Corporation", true)
                && (Build.MODEL.equals("Subsystem for Android(TM)", true));
    }

    private var ttsIsInstalled:Boolean? = null

    override fun isSpeechAvailable(): Boolean {
        ttsIsInstalled?.let {
            // use cached value
            return it
        }

        val ttsIntent = Intent()
        ttsIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA)
        val pm: PackageManager = applicationContext.getPackageManager()
        val list = pm.queryIntentActivities(ttsIntent, PackageManager.GET_META_DATA)

        ttsIsInstalled = false

        for (i in list.indices) {
            val resolveInfoUnderScrutiny = list[i]
            val engineName = resolveInfoUnderScrutiny.activityInfo.applicationInfo.packageName
            loggerFactory.logger.debug("Engine $engineName is installed")
            var version = "null"
            try {
                version = pm.getPackageInfo(
                    engineName,
                    PackageManager.GET_META_DATA
                ).versionName
            } catch (e: Exception) {
                loggerFactory.logger.error("Error getting TTS engine version: ", e)
            }
            loggerFactory.logger.debug("TTS engine version $version is installed")
            ttsIsInstalled = true
        }
        return ttsIsInstalled!!
    }

    override fun isDnd(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val dnd = notificationManager.currentInterruptionFilter != NotificationManager.INTERRUPTION_FILTER_ALL
            //loggerFactory.logger.debug("isDnd =  $dnd")
            return dnd
        } else {
            // see https://stackoverflow.com/questions/31387137/android-detect-do-not-disturb-status
            val dnd = Global.getInt(contentResolver, "zen_mode") != 0
            loggerFactory.logger.debug("old isDnd =  $dnd")
            return dnd
        }
    }

    override fun fontScaling(): Float {
        try
        {
            return applicationContext.resources.configuration.fontScale
        } catch (ex: Exception)
        {
            loggerFactory.logger.error("AndroidEnvironmentInformationProvider: fontScaling - error getting scalling", ex)
            return 0.0F
        }
    }

    override fun uiMode(): String {
        // override in the "night" folder
        return resourceProvider.getString(R.string.ui_mode)
    }
}


package net.derekwilson.wrist_spin_poc.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import net.derekwilson.wrist_spin_poc.R
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory

class ActiveMatchRecyclerItemAdapter(
    private val context: Context,
    private val viewModel: MainViewModel,
    private val loggerFactory: ILoggerFactory,
) : RecyclerView.Adapter<ActiveMatchRecyclerItemAdapter.RecyclerViewHolder>()
{
    init {
        setHasStableIds(true)
        viewModel.loadActiveMatches()
    }

    override fun getItemCount(): Int {
        return viewModel.itemCount
    }

    override fun getItemId(position: Int): Long {
        return viewModel.getMatchId(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_active_match, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val id = viewModel.getMatchId(position)
        val label = viewModel.getMatchLabel(position)
        holder.bind(label)
        holder.optionButton.setOnClickListener {
            viewModel.matchOptionSelected(id, position)
        }
    }

    inner class RecyclerViewHolder(private var view: View) : RecyclerView.ViewHolder(view) {

        internal var rowLabelContainer: View = view.findViewById(R.id.active_match_row_label_container)
        private var txtLabel: TextView = view.findViewById(R.id.active_match_row_label)
        internal var optionButton: ImageView = view.findViewById(R.id.active_match_row_option)

        fun bind(label: String) {
            txtLabel.text = label
        }
    }
}


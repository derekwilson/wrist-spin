package net.derekwilson.wrist_spin_poc.utility

import net.derekwilson.wrist_spin_cricket_logic_poc.datasource.IDataSourceFactory
import net.derekwilson.wrist_spin_cricket_logic_poc.model.DataSource
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ICricketDataSource
import net.derekwilson.wrist_spin_cricket_logic_poc.model.ILoggingSink
import net.derekwilson.wrist_spin_poc.R
import javax.inject.Inject

interface ICricketDataSourceProvider {
    fun getDataSource(): ICricketDataSource
}

class CricketDataSourceProvider @Inject constructor(
    private val loggingSink: ILoggingSink,
    private val preferencesProvider: IPreferencesProvider,
    private val resourceProvider:IResourceProvider,
    private val dataSourceFactory: IDataSourceFactory,
) : ICricketDataSourceProvider {

    override fun getDataSource(): ICricketDataSource {
        // TODO - consider caching the datasource and then providing a mechanism to reset
        val dataSourceFromPreferences: DataSource = preferencesProvider.getPreferenceEnum(
            resourceProvider.getString(R.string.settings_datasource_key),
            DataSource::class.java,
            DataSource.Cricinfo2
        )

        return dataSourceFactory.getDataSource(dataSourceFromPreferences)
    }
}
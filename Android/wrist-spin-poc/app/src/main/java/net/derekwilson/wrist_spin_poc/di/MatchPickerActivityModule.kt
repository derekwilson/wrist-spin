package net.derekwilson.wrist_spin_poc.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.matchpicker.MatchPickerActivity
import net.derekwilson.wrist_spin_poc.ui.matchpicker.MatchPickerViewModel

@Subcomponent
interface IMatchPickerActivitySubcomponent : AndroidInjector<MatchPickerActivity> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<MatchPickerActivity>
}

@Module(subcomponents = [
    IMatchPickerActivitySubcomponent::class
])
abstract class MatchPickerActivityModule {
    @Binds
    @IntoMap
    @ClassKey(MatchPickerActivity::class)
    abstract fun bindActivityInjectorFactory(builder: IMatchPickerActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ViewModelKey(MatchPickerViewModel::class)
    abstract fun bindActivityViewModel(model: MatchPickerViewModel): ViewModel
}




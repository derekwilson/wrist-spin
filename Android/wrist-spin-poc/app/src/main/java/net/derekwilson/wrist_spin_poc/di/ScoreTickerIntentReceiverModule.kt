package net.derekwilson.wrist_spin_poc.di

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.scoreticker.ScoreTickerIntentReceiver


@Subcomponent
interface IScoreTickerIntentReceiverSubcomponent : AndroidInjector<ScoreTickerIntentReceiver> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<ScoreTickerIntentReceiver>
}

@Module(subcomponents = [
    IScoreTickerIntentReceiverSubcomponent::class]
)
abstract class ScoreTickerIntentReceiverModule {
    @Binds
    @IntoMap
    @ClassKey(ScoreTickerIntentReceiver::class)
    abstract fun bindInjectorFactory(builder: IScoreTickerIntentReceiverSubcomponent.Factory): AndroidInjector.Factory<*>
}


package net.derekwilson.wrist_spin_poc.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin_poc.ui.lockscreen.LockScreenActivity

@Subcomponent
interface ILockScreenActivitySubcomponent : AndroidInjector<LockScreenActivity> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<LockScreenActivity>
}

@Module(subcomponents = [
    ILockScreenActivitySubcomponent::class
])
abstract class LockScreenActivityModule {
    @Binds
    @IntoMap
    @ClassKey(LockScreenActivity::class)
    abstract fun bindActivityInjectorFactory(builder: ILockScreenActivitySubcomponent.Factory): AndroidInjector.Factory<*>
}



package net.derekwilson.wrist_spin_poc.ui.utility

import android.content.Context
import android.view.Window
import android.view.WindowManager
import net.derekwilson.wrist_spin_poc.ui.customviews.ProgressSpinnerView

object ProgressViewHelper {
    // specify a dynamic message to display in the progress view and use a stepped progress
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, messageId: Int, context: Context, max: Int) {
        progressBar.message = context.getString(messageId)
        progressBar.max = max
        startProgress(progressBar, window, false)
    }

    // specify a dynamic message to display in the progress view
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, messageId: Int, context: Context) {
        progressBar.message = context.getString(messageId)
        startProgress(progressBar, window)
    }

    // use the message specified in the layout XML and use a stepped progress
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, max: Int) {
        progressBar.max = max
        startProgress(progressBar, window, false)
    }

    // use the message specified in the layout XML
    fun startProgress(progressBar: ProgressSpinnerView, window: Window, indeterminateProgress: Boolean = true) {
        progressBar.progress = 0
        progressBar.slideDown(indeterminateProgress)

        // disable the UI
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun completeProgress(progressBar: ProgressSpinnerView, window: Window) {
        progressBar.slideUp()

        // enable the UI
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}


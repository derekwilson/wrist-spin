package net.derekwilson.wrist_spin_poc.ui.lockscreen

import android.app.ActivityOptions
import android.app.ActivityOptions.MODE_BACKGROUND_ACTIVITY_START_ALLOWED
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin_poc.logging.ILoggerFactory
import javax.inject.Inject

class LockScreenIntentReceiver : BroadcastReceiver() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory

    // Handle actions and display Lockscreen
    override fun onReceive(context: Context, intent: Intent) {
        AndroidInjection.inject(this, context)
        loggerFactory.logger.debug("LockScreenIntentReceiver: onReceive ${intent.action}")

        if (intent.action == Intent.ACTION_SCREEN_OFF ||
            intent.action == Intent.ACTION_SCREEN_ON ||
            intent.action == Intent.ACTION_BOOT_COMPLETED) {
            start_lockscreen(context)
            //startLockScreen(context)
        }
    }

    // Display lock screen
    private fun start_lockscreen(context: Context) {
        loggerFactory.logger.debug("LockScreenIntentReceiver: start_lockscreen")
        val mIntent = Intent(context, LockScreenActivity::class.java)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(mIntent)
    }

    private fun startLockScreenDelayed(context: Context) {
        context?.let {
            loggerFactory.logger.debug("LockScreenIntentReceiver: delayed start UI")
            val intent = PendingIntent.getActivity(
                it, 0, Intent(
                    it, LockScreenActivity::class.java
                ),
                PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
            val mgr = it.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            mgr[AlarmManager.RTC, System.currentTimeMillis() + 500] = intent
        }
    }

    private fun startLockScreen(context: Context) {
        context?.let {
            loggerFactory.logger.debug("LockScreenIntentReceiver: startLockScreen")
            val intent = PendingIntent.getActivity(
                it, 0, Intent(
                    it, LockScreenActivity::class.java
                ),
                PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                loggerFactory.logger.debug("LockScreenIntentReceiver: start UI the hard way")
                val options = ActivityOptions.makeBasic()
                options.pendingIntentBackgroundActivityStartMode = MODE_BACKGROUND_ACTIVITY_START_ALLOWED
                intent.send(options.toBundle())
            } else {
                loggerFactory.logger.debug("LockScreenIntentReceiver: start UI")
                intent.send()
            }
        }
    }
}

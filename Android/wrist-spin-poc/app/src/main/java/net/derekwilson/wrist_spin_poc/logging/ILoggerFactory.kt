package net.derekwilson.wrist_spin_poc.logging

import org.slf4j.Logger

interface ILoggerFactory {
    val logger: Logger
}
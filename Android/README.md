# wrist-spin for Android

A live cricket score notifier for Android devices

## What is wrist-spin?

![Screen](Support/screen.png)

wrist-spin is a simple, quick and easy to use app keep track of cricket matches current score and state.

## Benefits

1. Keep track of multiple matches
1. Notifications display the current score
1. Adjust the update refresh rate
1. Custom lock screen can optionally display the scores
1. Chimes and speech can be used to show score milestones or wickets falling

## wrist-spin-poc

Proof of concept app to test out different techniques, not intended for release

## wrist-spin

The production app for Android

## Support

Assets for delivering the app to stores

## LocalOnly

This folder contains the files needed to sign a release APK/AAB. This folder is not checked into revision control


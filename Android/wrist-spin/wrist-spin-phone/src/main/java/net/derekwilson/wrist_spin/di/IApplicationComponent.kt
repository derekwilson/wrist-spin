package net.derekwilson.wrist_spin.di

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import net.derekwilson.wrist_spin.AndroidApplication
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    MainActivityModule::class,
    SettingsFragmentModule::class,
    OpenSourceLicensesActivityModule::class,
    MatchPickerActivityModule::class,
    ScoreTickerServiceModule::class,
    ScoreTickerIntentReceiverModule::class,
    LockScreenIntentReceiverModule::class,
    LockScreenActivityModule::class,
))
interface IApplicationComponent {
    fun inject(application: AndroidApplication)
}




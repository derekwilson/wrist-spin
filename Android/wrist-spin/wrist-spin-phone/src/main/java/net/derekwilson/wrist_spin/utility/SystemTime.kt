package net.derekwilson.wrist_spin.utility

import android.content.Context
import android.os.SystemClock
import java.util.Calendar
import javax.inject.Inject

interface ISystemTime {
    fun getCurrentTime(): Calendar
    fun getBootElapsedMillis(): Long
}

class SystemTime
@Inject constructor(
    internal var context: Context
) : ISystemTime {
    override fun getCurrentTime(): Calendar {
        return Calendar.getInstance()
    }

    override fun getBootElapsedMillis(): Long {
        return SystemClock.elapsedRealtime()
    }
}


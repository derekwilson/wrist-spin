package net.derekwilson.wrist_spin.utility

import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin_cricket_logic.datasource.DataSource
import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSource
import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSourceFactory
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import javax.inject.Inject

interface ICricketDataSourceProvider {
    fun getDataSource(): ICricketDataSource
}

class CricketDataSourceProvider @Inject constructor(
    private val loggingSink: ILoggingSink,
    private val preferencesProvider: IPreferencesProvider,
    private val resourceProvider:IResourceProvider,
    private val dataSourceFactory: ICricketDataSourceFactory,
) : ICricketDataSourceProvider {

    override fun getDataSource(): ICricketDataSource {
        // TODO - consider caching the datasource and then providing a mechanism to reset
        val dataSourceFromPreferences: DataSource = preferencesProvider.getPreferenceEnum(
            resourceProvider.getString(R.string.settings_datasource_key),
            DataSource::class.java,
            DataSource.Cricinfo2
        )

        return dataSourceFactory.getDataSource(dataSourceFromPreferences)
    }
}

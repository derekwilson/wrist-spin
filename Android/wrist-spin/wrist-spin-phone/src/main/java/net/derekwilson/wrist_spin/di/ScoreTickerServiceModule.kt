package net.derekwilson.wrist_spin.di

import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin.ui.scoreticker.IScoreTicker
import net.derekwilson.wrist_spin.ui.scoreticker.ScoreTicker
import net.derekwilson.wrist_spin.ui.scoreticker.ScoreTickerService

@Subcomponent
interface IScoreTickerServiceSubcomponent : AndroidInjector<ScoreTickerService> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<ScoreTickerService>
}

@Module(subcomponents = [
    IScoreTickerServiceSubcomponent::class
])
abstract class ScoreTickerServiceModule {
    @Binds
    @IntoMap
    @ClassKey(ScoreTickerService::class)
    abstract fun bindInjectorFactory(builder: IScoreTickerServiceSubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    abstract fun bindTicker(ticker: ScoreTicker): IScoreTicker
}




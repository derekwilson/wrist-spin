package net.derekwilson.wrist_spin.ui.lockscreen

import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin_cricket_logic.matchstore.IMatchStore
import javax.inject.Inject

interface IMatchStateDisplayTextProvider {
    fun getSelectedMatchesDisplayTextFromStore(): List<Pair<String, String>>
}

class MatchStateDisplayTextProvider @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val resourceProvider: IResourceProvider,
    private val matchStore: IMatchStore,
    private val analyticsEngine: IAnalyticsEngine,
) : IMatchStateDisplayTextProvider
{
    override fun getSelectedMatchesDisplayTextFromStore(): List<Pair<String, String>> {
        analyticsEngine.showLockScreenEvent(matchStore.allIdsInStore.size)
        val textForAllMatches = ArrayList<Pair<String, String>>()
        for (id in matchStore.allIdsInStore) {
            val state = matchStore.getMatchState(id)
            state?.let {
                var title = it.scoreDisplay
                val stringBuilder = StringBuilder()
                loggerFactory.logger.debug("LockScreenActivity.setupLockText - ${it.matchTitle}")
                stringBuilder.clear()
                if (it.battingTeamAbbreviation.isNotBlank()) {
                    stringBuilder.append(it.battingTeamAbbreviation)
                    stringBuilder.append("\n")
                }
                if (it.overs.display.isNotBlank()) {
                    stringBuilder.append(it.overs.display)
                    stringBuilder.append("\n")
                }
                if (it.strikerName.isNotBlank()) {
                    stringBuilder.append(it.strikerName)
                    if (it.strikerStats.isNotBlank()) {
                        stringBuilder.append(" ")
                        stringBuilder.append(it.strikerStats)
                    }
                    stringBuilder.append("\n")
                }
                if (it.nonStrikerName.isNotBlank()) {
                    stringBuilder.append(it.nonStrikerName)
                    if (it.nonStrikerStats.isNotBlank()) {
                        stringBuilder.append(" ")
                        stringBuilder.append(it.nonStrikerStats)
                    }
                    stringBuilder.append("\n")
                }
                if (it.lead.isNotBlank()) {
                    stringBuilder.append(it.lead)
                    stringBuilder.append("\n")
                }
                if (it.fact.isNotBlank()) {
                    stringBuilder.append(it.fact)
                    stringBuilder.append("\n")
                }
                stringBuilder.append(it.staleDisplay)
                textForAllMatches.add(Pair(title, stringBuilder.toString()))
            }
        }
        if (textForAllMatches.isEmpty()) {
            // show something
            val title = resourceProvider.getString(R.string.match_display_no_data)
            val subtitle = resourceProvider.getString(R.string.app_name)
            return listOf(Pair(title, subtitle))
        } else {
            return textForAllMatches
        }
    }
}
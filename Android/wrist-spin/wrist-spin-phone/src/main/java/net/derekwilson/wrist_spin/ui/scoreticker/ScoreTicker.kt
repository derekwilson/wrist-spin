package net.derekwilson.wrist_spin.ui.scoreticker

import android.content.BroadcastReceiver
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.lockscreen.LockScreenIntentReceiver
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin.utility.SingleLiveEvent
import javax.inject.Inject

interface IScoreTicker {
    abstract val observables: ScoreTicker.Observables

    fun shutdown()
    fun startup()
}

class ScoreTicker @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val preferencesProvider: IPreferencesProvider,
    private val resourceProvider: IResourceProvider,
    private val analyticsEngine: IAnalyticsEngine,
) : IScoreTicker
{
    private var broadcastReceiver: BroadcastReceiver? = null

    data class Observables(
        val showServiceNotification: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val registerLockScreen: MutableLiveData<Pair<BroadcastReceiver?, List<String>>> = SingleLiveEvent(),
        val unRegisterLockScreen: MutableLiveData<BroadcastReceiver> = SingleLiveEvent(),
        val triggerMatchUpdates: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val cancelAlarm: SingleLiveEvent<Void?> = SingleLiveEvent(),
    )
    override val observables = Observables()

    override fun startup() {
        observables.showServiceNotification.call()
        triggerUpdates()
        val isLockScreenActive = registerLockScreenReceiver()
        analyticsEngine.startServiceEvent(isLockScreenActive)
    }

    override fun shutdown() {
        stopAlarm()
        unRegisterLockScreenReceiver()
        analyticsEngine.stopServiceEvent()
    }

    private fun triggerUpdates() {
        loggerFactory.logger.debug("ScoreTicker: triggerUpdates")
        observables.triggerMatchUpdates.call()
    }

    private fun stopAlarm() {
        loggerFactory.logger.debug("ScoreTicker: stopAlarm")
        observables.cancelAlarm.call()
    }

    private fun registerLockScreenReceiver(): Boolean {
        val lockScreenActive = preferencesProvider.getPreferenceBoolean(
            resourceProvider.getString(R.string.settings_lock_screen_key),
            false
        )
        if (!lockScreenActive) {
            loggerFactory.logger.debug("ScoreTicker: registerLockScreenReceiver - lock screen not active")
            return false
        }

        broadcastReceiver = LockScreenIntentReceiver()
        val actionFilters = listOf(Intent.ACTION_SCREEN_ON, Intent.ACTION_SCREEN_OFF)
        val params = Pair(broadcastReceiver, actionFilters)
        observables.registerLockScreen.value = params
        loggerFactory.logger.debug("ScoreTicker: registerLockScreenReceiver - lock screen registered")
        return true
    }

    private fun unRegisterLockScreenReceiver() {
        broadcastReceiver?.let {
            loggerFactory.logger.debug("ScoreTicker: unRegisterLockScreenReceiver - lock screen unregistered")
            observables.unRegisterLockScreen.value = it
            broadcastReceiver = null
        }
    }
}
package net.derekwilson.wrist_spin.ui.lockscreen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import javax.inject.Inject

class LockScreenIntentReceiver : BroadcastReceiver() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory

    // Handle actions and display Lockscreen
    override fun onReceive(context: Context, intent: Intent) {
        AndroidInjection.inject(this, context)
        loggerFactory.logger.debug("LockScreenIntentReceiver: onReceive ${intent.action}")

        if (intent.action == Intent.ACTION_SCREEN_OFF ||
            intent.action == Intent.ACTION_SCREEN_ON ||
            intent.action == Intent.ACTION_BOOT_COMPLETED) {
            startLockscreen(context)
        }
    }

    // Display lock screen
    private fun startLockscreen(context: Context) {
        loggerFactory.logger.debug("LockScreenIntentReceiver: startLockscreen")
        val mIntent = Intent(context, LockScreenActivity::class.java)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(mIntent)
    }
}
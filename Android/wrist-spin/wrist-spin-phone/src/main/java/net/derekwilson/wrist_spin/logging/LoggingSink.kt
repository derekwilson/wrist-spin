package net.derekwilson.wrist_spin.logging

import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import javax.inject.Inject

// acts as a bridge from the library to our logs
class LoggingSink @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val reporter: ICrashReporter,
) : ILoggingSink {

    override fun debug(message: String?) {
        loggerFactory.logger.debug(message)
    }

    override fun error(message: String?, t: Throwable) {
        loggerFactory.logger.error(message, t)
        reporter.logNonFatalException(t)
    }
}

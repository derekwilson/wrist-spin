package net.derekwilson.wrist_spin.utility

import android.content.Context
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import javax.inject.Inject

interface IFileSystemHelper {
    fun getAssetsFolderFiles(foldername: String): List<String>?
    fun getAssetsFileContents(filename: String, addLineEndings: Boolean): String
}

class FileSystemHelper @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
    private val context: Context
) : IFileSystemHelper
{

    override fun getAssetsFolderFiles(foldername: String): List<String>? {
        var allFiles: MutableList<String> = mutableListOf()
        try {
            val files = context.assets.list(foldername) ?: return null
            for (file in files) {
                val filename = "$foldername/$file"
                val subFolderFiles = getAssetsFolderFiles(filename)
                if (subFolderFiles != null && subFolderFiles.size > 0) {
                    // the file is a folder, add its files
                    allFiles.addAll(subFolderFiles)
                } else {
                    // the file is a file
                    allFiles.add(filename)
                }
            }
        } catch (e: Throwable) {
            loggerFactory.logger.error("Error getting files in folder {}", foldername, e)
            crashReporter.logNonFatalException(e)
        }
        return allFiles
    }

    override fun getAssetsFileContents(filename: String, addLineEndings: Boolean): String {
        var inputStream: InputStream? = null
        var reader: BufferedReader? = null
        val buf = StringBuffer()
        try {
            inputStream = context.assets.open(filename)
            loggerFactory.logger.debug("Opened {} Available {}", filename, inputStream.available())
            reader = BufferedReader(InputStreamReader(inputStream))

            var str: String?
            while (reader.readLine().let {str = it; it != null }) {
                buf.append(str);
                if (addLineEndings) {
                    buf.append("\n")
                }
            }
        } catch (e: Throwable) {
            loggerFactory.logger.error("Error reading {}", filename, e)
            crashReporter.logNonFatalException(e)
        } finally {
            try {
                reader?.close()
                inputStream?.close()
            } catch (e: Throwable) {
                loggerFactory.logger.error("Error closing {}", filename, e)
                crashReporter.logNonFatalException(e)
            }
        }
        return buf.toString()
    }
}
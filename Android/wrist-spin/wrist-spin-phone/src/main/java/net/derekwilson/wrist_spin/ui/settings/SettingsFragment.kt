package net.derekwilson.wrist_spin.ui.settings

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.InputType
import android.view.inputmethod.EditorInfo
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import dagger.android.support.AndroidSupportInjection
import net.derekwilson.wrist_spin.BuildConfig
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.IPermissionChecker
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import javax.inject.Inject

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    @Inject
    lateinit var crashReporter: ICrashReporter

    @Inject
    lateinit var preferencesProvider: IPreferencesProvider

    @Inject
    lateinit var permissionsChecker: IPermissionChecker

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        loggerFactory.logger.debug("SettingsFragment.onCreate()")
        super.onCreate(savedInstanceState)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
        setupNumberPreference(R.string.settings_ticker_update_mins_key, R.string.settings_ticker_update_mins_default, R.string.settings_ticker_update_mins_summary_fmt)
        updateAllListPreferenceSummaries()
        updateVersion()
        updateOsl()
        updatePrivacy()
        updateLockScreenPermissionsSummary()
        updateLockScreenPermissionsClick()
    }

    private fun setupNumberPreference(keyId: Int, defaultId: Int, summaryFormatId: Int) {
        val preference = findPreference<EditTextPreference>(getString(keyId))
        val number = preferencesProvider.getPreferencesNumber(getString(keyId), getString(defaultId))
        val default = getString(defaultId)
        preference?.summary = getString(summaryFormatId, number)
        preference?.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { thisPreference, newValue ->
                val newNumber = preferencesProvider.sanitiseNumber(default, newValue.toString())
                loggerFactory.logger.debug("SettingsFragment.onPreferenceChangeListener - ${newNumber}")
                thisPreference.summary = getString(summaryFormatId, newNumber)
                true
            }
        preference?.setOnBindEditTextListener { editText ->
            editText.inputType = InputType.TYPE_CLASS_NUMBER
            editText.imeOptions = EditorInfo.IME_ACTION_DONE
            editText.setSelection(editText.text.length)
        }
    }

    override fun onPause() {
        super.onPause()
        unregisterPreferenceChangeListener()
    }

    override fun onResume() {
        super.onResume()
        registerPreferenceChangeListener()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        key?.let {
            updatePreferenceSummaryByKey(it)
        }
    }

    private fun registerPreferenceChangeListener() {
        preferenceScreen.sharedPreferences
            ?.registerOnSharedPreferenceChangeListener(this)
    }

    private fun unregisterPreferenceChangeListener() {
        preferenceScreen.sharedPreferences
            ?.unregisterOnSharedPreferenceChangeListener(this)
    }

    private fun updateLockScreenPermissionsSummary() {
        val preference: Preference? = findPreference(getString(R.string.settings_lock_screen_permission_key))
        if (preference != null) {
            if (permissionsChecker.hasDrawOverlaysPermission()) {
                preference.summary = getString(R.string.settings_lock_screen_permission_summary_ok)
            } else {
                preference.summary = getString(R.string.settings_lock_screen_permission_summary_bad, getString(R.string.app_name))
            }
        }
    }

    private fun updateLockScreenPermissionsClick() {
        val preference: Preference? = findPreference(getString(R.string.settings_lock_screen_permission_key))
        if (preference != null) {
            preference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                try {
                    loggerFactory.logger.debug("SettingsFragment.onPreferenceClickListener - lock screen permission")
                    val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                    permissionsLauncher.launch(intent)
                } catch (ex: Exception) {
                    crashReporter.logNonFatalException(ex)
                }
                true
            }
        }
    }

    private val permissionsLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            loggerFactory.logger.debug("Permissions settings Activity Result result ${it.resultCode}")
            updateLockScreenPermissionsSummary()
        }

    private fun updateVersion() {
        val versionPreference: Preference? = findPreference(getString(R.string.settings_version_key))
        if (versionPreference != null) {
            setVersionSummary(versionPreference)
            if (isDebug()) {
                versionPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
                {
                    loggerFactory.logger.debug("SettingsFragment.onPreferenceClickListener - version")
                    crashReporter.testReporting()
                    true
                }
            }
        }
    }

    private fun setVersionSummary(versionPreference: Preference) {
        var version = getString(R.string.settings_version_summary_fmt, BuildConfig.VERSION_NAME, BuildConfig.GIT_HASH)
        if (isDebug())
            version += " (debug)"

        versionPreference.summary = version
    }

    private fun isDebug(): Boolean {
        return BuildConfig.DEBUG
    }

    private fun updateOsl() {
        // we cannot do this in the XML file as our package name changes between flavours
        val oslPreference: Preference? = findPreference(getString(R.string.settings_osl_key))
        if (oslPreference != null) {
            oslPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                loggerFactory.logger.debug("SettingsFragment.onPreferenceClickListener - osl")
                val intent = Intent(activity, OpenSourceLicensesActivity::class.java)
                startActivity(intent)
                true
            }
        }
    }
    private fun updatePrivacy() {
        val privacyPreference: Preference? = findPreference(getString(R.string.settings_privacy_key))
        if (privacyPreference != null) {
            privacyPreference.onPreferenceClickListener = Preference.OnPreferenceClickListener()
            {
                loggerFactory.logger.debug("SettingsFragment.onPreferenceClickListener - privacy")
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.settings_privacy_url)))
                startActivity(browserIntent)
                true
            }
        }
    }

    private fun updateAllListPreferenceSummaries() {
        preferenceScreen.sharedPreferences?.all?.keys?.forEach {key ->
            updatePreferenceSummaryByKey(key)
        }
    }

    private fun updatePreferenceSummaryByKey(key: String) {
        updatePreferenceSummary(findPreference(key))
    }

    private fun updatePreferenceSummary(preference: Preference?) {
        (preference as? ListPreference)?.let {
            it.summary = it.entry
        }
    }
}



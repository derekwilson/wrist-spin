package net.derekwilson.wrist_spin.utility

import android.R.string
import android.content.Context
import android.graphics.drawable.Drawable
import javax.inject.Inject


interface IResourceProvider {
    fun getQuantityString(id: Int, quantity: Int): String
    fun getDrawable(id: Int): Drawable?
    fun getString(id: Int): String
}

class AndroidResourceProvider
@Inject constructor(
    internal var context: Context
) : IResourceProvider
{
    override fun getDrawable(id: Int): Drawable? {
        return context.getDrawable(id)
    }

    override fun getQuantityString(id: Int, quantity: Int): String {
        return context.resources.getQuantityString(id, quantity, quantity)
    }


    override fun getString(id: Int): String {
        return context.getString(id)
    }
}
package net.derekwilson.wrist_spin.utility

import android.content.Context
import com.mixpanel.android.mpmetrics.MixpanelAPI
import net.derekwilson.wrist_spin.BuildConfig
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import org.json.JSONObject
import javax.inject.Inject


interface IAnalyticsEngine {
    fun lifecycleLaunchEvent()
    fun lifecycleErrorEvent()
    fun lifecycleFatalErrorEvent()
    fun showLockScreenEvent(numberOfMatches: Int)
    fun startServiceEvent(isLockScreenActive: Boolean)
    fun stopServiceEvent()
    fun selectMatchEvent(id: String, title: String)
    fun getMatchStateEvent(dataSourceName: String, id: String)
}

class MixpanelAnalyticsEngine
@Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val crashReporter: ICrashReporter,
    private val applicationContext: Context,
    private val environmentProvider: IAndroidEnvironmentInformationProvider
)
    : IAnalyticsEngine {

    // to see the events go to
    // https://eu.mixpanel.com/project/3309163/view/3814235/app/home

    companion object {
        const val Event_Lifecycle_Launch = "Lifecycle_Launch"
        const val Event_Lifecycle_Error = "Lifecycle_Error"
        const val Event_Lifecycle_ErrorFatal = "Lifecycle_ErrorFatal"

        const val Event_Start_Service = "Service_Start"
        const val Event_Stop_Service = "Service_Stop"
        const val Event_Select_Match = "Match_Select"
        const val Event_Show_LockScreen = "LockScreen_Show"
        const val Event_Get_Match_State = "Match_Get_State"

        const val Property_Version = "Version"
        const val Property_IsKindle = "IsKindle";
        const val Property_IsWsa = "IsWsa";
        const val Property_IsSpeechAvailable = "IsSpeechAvailable";
        const val Property_IsDnd = "IsDnd";
        const val Property_IsRingerMuted = "IsRingerMuted";

        const val Property_Id = "Id";
        const val Property_NumberOfItems = "NoOfItems";
        const val Property_Title = "Title";
        const val Property_Name = "Name";
        const val Property_LockActive = "LockActive";
    }

    private lateinit var mixpanel: MixpanelAPI

    init {
        loggerFactory.logger.debug("MixpanelAnalyticsEngine: init - start")
        try {
            mixpanel = MixpanelAPI.getInstance(applicationContext, BuildConfig.MIXPANEL_TOKEN, true);
            loggerFactory.logger.debug("MixpanelAnalyticsEngine: init - id ${mixpanel.distinctId}")
        } catch (ex: Throwable) {
            // dont let the analytics crash the app
            loggerFactory.logger.error("MixpanelAnalyticsEngine: init: ", ex)
            crashReporter.logNonFatalException(ex)
        }
        loggerFactory.logger.debug("MixpanelAnalyticsEngine: init - end")
    }

    private fun trackEvent(event: String, properties: JSONObject?) {
        synchronized(mixpanel) {
            try {
                mixpanel.track(event, properties)
                loggerFactory.logger.debug("MixpanelAnalyticsEngine: trackEvent ${event}")
            } catch (ex: Throwable) {
                // dont let the analytics crash the app
                loggerFactory.logger.error("MixpanelAnalyticsEngine: trackEvent: ", ex)
                crashReporter.logNonFatalException(ex)
            }
        }
    }

    private fun getPropertiesForLifecycle(): JSONObject {
        val props = JSONObject()
        props.put(Property_Version, "${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})")
        props.put(Property_IsKindle, environmentProvider.isKindle().toString())
        props.put(Property_IsWsa, environmentProvider.isWsa().toString())
        props.put(Property_IsSpeechAvailable, environmentProvider.isSpeechAvailable().toString())
        props.put(Property_IsDnd, environmentProvider.isDnd().toString())
        props.put(Property_IsRingerMuted, environmentProvider.isRingerMuted().toString())
        return props
    }

    override fun lifecycleLaunchEvent() {
        trackEvent(Event_Lifecycle_Launch, getPropertiesForLifecycle())
    }

    override fun lifecycleErrorEvent() {
        trackEvent(Event_Lifecycle_Error, getPropertiesForLifecycle())
    }

    override fun lifecycleFatalErrorEvent() {
        trackEvent(Event_Lifecycle_ErrorFatal, getPropertiesForLifecycle())
    }

    override fun showLockScreenEvent(numberOfMatches: Int) {
        val props = JSONObject()
        props.put(Property_NumberOfItems, numberOfMatches)
        trackEvent(Event_Show_LockScreen, props)
    }

    override fun startServiceEvent(isLockScreenActive: Boolean) {
        val props = JSONObject()
        props.put(Property_LockActive, isLockScreenActive)
        trackEvent(Event_Start_Service, props)
    }

    override fun stopServiceEvent() {
        trackEvent(Event_Stop_Service, null)
    }

    override fun selectMatchEvent(id: String, title: String) {
        val props = JSONObject()
        props.put(Property_Id, id)
        props.put(Property_Title, title)
        trackEvent(Event_Select_Match, props)
    }

    override fun getMatchStateEvent(dataSourceName: String, id: String) {
        val props = JSONObject()
        props.put(Property_Id, id)
        props.put(Property_Name, dataSourceName)
        trackEvent(Event_Get_Match_State, props)
    }
}


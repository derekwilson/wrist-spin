package net.derekwilson.wrist_spin

import androidx.multidex.MultiDexApplication
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import net.derekwilson.wrist_spin.di.ApplicationModule
import net.derekwilson.wrist_spin.di.DaggerIApplicationComponent
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import javax.inject.Inject

open class AndroidApplication :
    MultiDexApplication(),
    HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var loggerFactory: ILoggerFactory

    @Inject
    lateinit var preferencesProvider: IPreferencesProvider

    @Inject
    lateinit var analyticsEngine: IAnalyticsEngine

    private var defaultExceptionHandler: Thread.UncaughtExceptionHandler? = null

    override fun onCreate() {
        super.onCreate()

        DaggerIApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
            .inject(this)

        val version = getString(R.string.settings_version_summary_fmt, BuildConfig.VERSION_NAME, BuildConfig.GIT_HASH)
        loggerFactory.logger.debug("app onCreate Version $version")

        // Setup handler for uncaught exceptions.
        defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { thread, e -> handleUncaughtException(thread, e) }

        preferencesProvider.setDefaultValues(R.xml.settings)

        // it looks like unless you access the app local public folders android does not create them
        // and we need them for the logging
        var dir = getExternalFilesDir(null)
        var dirs = getExternalFilesDirs(null)
        loggerFactory.logger.debug("getExternalFilesDir: ${dir?.absolutePath}")
        for (thisDir in dirs) {
            loggerFactory.logger.debug("getExternalFilesDirs: ${thisDir?.absolutePath}")
        }

        analyticsEngine.lifecycleLaunchEvent()
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    private fun handleUncaughtException(thread: Thread, e: Throwable) {
        // record it in the log file
        loggerFactory.logger.error("Unhandled exception: ", e)
        // and analytics
        analyticsEngine.lifecycleFatalErrorEvent()
        // we still want to do the OS default action
        defaultExceptionHandler?.uncaughtException(thread, e)
    }
}


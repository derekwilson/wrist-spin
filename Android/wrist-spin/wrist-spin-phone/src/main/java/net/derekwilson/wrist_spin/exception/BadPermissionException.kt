package net.derekwilson.wrist_spin.exception

class BadPermissionException(message: String?) : Throwable(message) {
}

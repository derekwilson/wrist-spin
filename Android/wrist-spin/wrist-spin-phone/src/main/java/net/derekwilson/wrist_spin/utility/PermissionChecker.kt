package net.derekwilson.wrist_spin.utility

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import androidx.core.content.ContextCompat
import javax.inject.Inject

interface IPermissionChecker {
    fun hasPushNotificationPermission(): Boolean
    fun hasDrawOverlaysPermission() : Boolean
}

class PermissionChecker
@Inject constructor(
    internal var context: Context,
) : IPermissionChecker
{
    override fun hasDrawOverlaysPermission(): Boolean {
        var canDraw = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            canDraw = Settings.canDrawOverlays(this.context)
        }
        return canDraw
    }

    override fun hasPushNotificationPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return hasPermissionBeenGranted(Manifest.permission.POST_NOTIFICATIONS)
        }
        // earlier versions didnt need to ask
        return true
    }

    private fun hasPermissionBeenGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }
}


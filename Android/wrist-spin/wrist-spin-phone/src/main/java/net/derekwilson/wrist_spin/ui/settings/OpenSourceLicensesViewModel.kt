package net.derekwilson.wrist_spin.ui.settings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.utility.IFileSystemHelper
import net.derekwilson.wrist_spin.utility.SingleLiveEvent
import javax.inject.Inject

class OpenSourceLicensesViewModel
@Inject constructor(
    app: Application,
    private val loggerFactory: ILoggerFactory,
    private val fileSystemHelper: IFileSystemHelper
)
    : AndroidViewModel(app), DefaultLifecycleObserver
{
    // observables
    data class TextBlock(
        val title: String,
        val body: String,
    )
    data class Observables(
        val displayMessage: MutableLiveData<String> = SingleLiveEvent(),
        val addTextBlocks: MutableLiveData<List<TextBlock>> = SingleLiveEvent(),
        val scrollToTopOfText: SingleLiveEvent<Void?> = SingleLiveEvent(),
    )
    val observables = Observables()
    // observables

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("OpenSourceLicensesViewModel.onCreate")
        addAllLicenseFiles()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("OpenSourceLicensesViewModel.onDestroy")
    }

    private fun addAllLicenseFiles() {
        // to add new licenses to this page just place the new licenses text file in the license folder in the assets
        // each file should just be text, the first line is treated as the title the rest as the body text
        // subfolder are supported, files and subfolders are sorted alphabetically
        // to remove a license, delete the file from the license folder in the assets

        val textBlocks: MutableList<TextBlock> = mutableListOf<TextBlock>()
        val files = fileSystemHelper.getAssetsFolderFiles("license") ?: return
        for (file in files) {
            loggerFactory.logger.debug("OpenSourceLicensesPresenter found file $file")
            textBlocks.add(getLicenseTextBlock(file))
        }
        observables.addTextBlocks.value = textBlocks

        observables.scrollToTopOfText.call()
    }

    private fun getLicenseTextBlock(filename: String): TextBlock {
        val text = fileSystemHelper.getAssetsFileContents(filename, true)
        val lineSplit = text.indexOf("\n")
        return if (lineSplit != -1) {
            TextBlock(text.substring(0, lineSplit), text.substring(lineSplit+1))
        } else {
            TextBlock("", text)
        }
    }
}

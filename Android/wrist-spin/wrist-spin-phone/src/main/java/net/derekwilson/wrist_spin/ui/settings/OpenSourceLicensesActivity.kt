package net.derekwilson.wrist_spin.ui.settings

import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.Selection
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import javax.inject.Inject

class OpenSourceLicensesActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var loggerFactory: ILoggerFactory

    private lateinit var viewModel: OpenSourceLicensesViewModel

    private lateinit var licenseScroller: ScrollView
    private lateinit var licenseText: TextView
    private fun bindControls() {
        licenseScroller = findViewById<ScrollView>(R.id.license_scroller)
        licenseText = findViewById<TextView>(R.id.license_text)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("OpenSourceLicensesActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opensourcelicenses)
        bindControls()

        viewModel = ViewModelProvider(this, viewModelFactory)[OpenSourceLicensesViewModel::class.java]
        lifecycle.addObserver(viewModel)

        setupViewModelObserver()
    }

    private fun setupViewModelObserver() {
        viewModel.observables.displayMessage.observe(this, Observer {
            showMessage(it)
        })
        viewModel.observables.scrollToTopOfText.observe(this, Observer {
            scrollToTopOfText()
        })
        viewModel.observables.addTextBlocks.observe(this, Observer {
            addTextBlocks(it)
        })
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("OpenSourceLicensesActivity.onDestroy()")
        super.onDestroy()
    }

    private fun showMessage(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun addTextBlocks(textBlocks: List<OpenSourceLicensesViewModel.TextBlock>) {
        textBlocks.forEach { block ->
            addText(licenseText, block.title, block.body)
        }
    }

    private fun addText(textView: TextView, title: String, text: String) {
        loggerFactory.logger.debug("addText: title {}", title)

        val textTitle = SpannableString(title)
        val titleLength = textTitle.length
        textTitle.setSpan(UnderlineSpan(), 0, titleLength, 0)
        textTitle.setSpan(StyleSpan(Typeface.BOLD), 0, titleLength, 0)

        textView.append(textTitle)
        textView.append("\n\n")

        textView.append(text)

        textView.append("\n\n")
    }

    private fun addTextBlock(title: String, text: String) {
        addText(licenseText, title, text)
    }

    private fun scrollToTopOfText() {
        // Appending to the textview auto scrolls the text to the bottom - force it back to the top
        Selection.setSelection(licenseText.text as Spannable, 0)
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P)
        {
            // scroll to the top of the page - for old versions
            licenseScroller.parent.requestChildFocus(licenseScroller, licenseScroller);
        }
    }
}

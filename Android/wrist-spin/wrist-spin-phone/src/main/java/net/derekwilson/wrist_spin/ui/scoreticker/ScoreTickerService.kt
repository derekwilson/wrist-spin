package net.derekwilson.wrist_spin.ui.scoreticker

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.main.MainActivity
import javax.inject.Inject

class ScoreTickerService : Service() {
    @Inject
    lateinit var scoreTicker: IScoreTicker
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var notificationManager: NotificationManager
    @Inject
    lateinit var alarmManager: AlarmManager

    override fun onBind(intent: Intent?): IBinder? {
        // we dont do communication
        return null
    }

    override fun onCreate() {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("ScoreTickerService: onCreate")
        super.onCreate()
        attachObservers()
    }

    // Unregister receiver
    override fun onDestroy() {
        loggerFactory.logger.debug("ScoreTickerService: onDestroy")
        super.onDestroy()
        scoreTicker.shutdown()
        detachObservers()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        loggerFactory.logger.debug("ScoreTickerService: onStartCommand")
        scoreTicker.startup()
        return START_STICKY
    }

    private fun attachObservers() {
        scoreTicker.observables.showServiceNotification.observeForever(showServiceNotificationObserver)
        scoreTicker.observables.registerLockScreen.observeForever(registerLockScreenObserver)
        scoreTicker.observables.unRegisterLockScreen.observeForever(unRegisterLockScreenObserver)
        scoreTicker.observables.triggerMatchUpdates.observeForever(triggerMatchUpdatesObserver)
        scoreTicker.observables.cancelAlarm.observeForever(cancelAlarmObserver)
    }

    private fun detachObservers() {
        scoreTicker.observables.showServiceNotification.removeObserver(showServiceNotificationObserver)
        scoreTicker.observables.registerLockScreen.removeObserver(registerLockScreenObserver)
        scoreTicker.observables.unRegisterLockScreen.removeObserver(unRegisterLockScreenObserver)
        scoreTicker.observables.triggerMatchUpdates.removeObserver(triggerMatchUpdatesObserver)
        scoreTicker.observables.cancelAlarm.removeObserver(cancelAlarmObserver)
    }

    private val showServiceNotificationObserver = Observer<Void?> {
        startForeground()
    }

    private val registerLockScreenObserver = Observer<Pair<BroadcastReceiver?, List<String>>> {
        loggerFactory.logger.debug("ScoreTickerService: registerLockScreenObserver")
        val (receiver, filters) = it

        val filter = IntentFilter()
        for (thisFilter in filters) {
            filter.addAction(thisFilter)
        }

        registerReceiver(receiver, filter)
    }

    private val unRegisterLockScreenObserver = Observer<BroadcastReceiver> {
        loggerFactory.logger.debug("ScoreTickerService: unRegisterLockScreenObserver")
        unregisterReceiver(it)
    }

    private val triggerMatchUpdatesObserver = Observer<Void?> {
        loggerFactory.logger.debug("ScoreTickerService: triggerMatchUpdatesObserver")
        var intent = ScoreTickerIntentReceiver.getAlarmOperation(this)
        intent.send()   // the intent receiver will setup the next alarm
    }

    private val cancelAlarmObserver = Observer<Void?> {
        loggerFactory.logger.debug("ScoreTickerService: cancelAlarmObserver")
        ScoreTickerIntentReceiver.stopAlarm(this, alarmManager)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "wrist-spin-score-ticker-service", NotificationManager.IMPORTANCE_LOW)

            // Configure the notification channel.
            notificationChannel.description = "wrist-spin-score-ticker-service"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    // Run service in foreground so it is less likely to be killed by system
    private fun startForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        builder
            .setContentTitle(resources.getString(R.string.app_name))
            .setTicker(resources.getString(R.string.app_name))
            .setContentText("wrist-spin score ticker service")
            .setSmallIcon(R.drawable.ic_app_foreground)
            .setContentIntent(getNotificationIntent())
            .setOngoing(true)

        loggerFactory.logger.debug("ScoreTickerService: startForeground")
        // lets try and tell the OS we need to stay
        startForeground(FOREGROUND_NOTIFICATION_ID, builder.build())
    }

    private fun getNotificationIntent(): PendingIntent {
        val intent = Intent(this, MainActivity::class.java)
            .setAction(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    companion object {
        // we cannot modify a channel once its created - so we need to increment this ID or reinstall
        private const val NOTIFICATION_CHANNEL_ID = "wrist-spin-score-ticker-service-channel-id-01"
        private const val FOREGROUND_NOTIFICATION_ID = 200
    }
}

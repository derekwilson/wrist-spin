package net.derekwilson.wrist_spin.utility

import android.app.ActivityManager
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.scoreticker.ScoreTickerService
import javax.inject.Inject

interface IServiceHelper {
    fun isTickerServiceRunning(): Boolean
}

class ServiceHelper @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val activityManager: ActivityManager,
) : IServiceHelper {

    @Suppress("DEPRECATION")
    override fun isTickerServiceRunning(): Boolean {
        // yes I know its deprecated - see
        // https://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android

        loggerFactory.logger.debug("MainViewModel.isMyServiceRunning ${ScoreTickerService::class.java.name}")
        for (service in activityManager.getRunningServices(Int.MAX_VALUE)) {
            loggerFactory.logger.debug("->MainViewModel.isMyServiceRunning ${service.service.className}")
            if (ScoreTickerService::class.java.name == service.service.className) {
                return true
            }
        }
        return false
    }
}
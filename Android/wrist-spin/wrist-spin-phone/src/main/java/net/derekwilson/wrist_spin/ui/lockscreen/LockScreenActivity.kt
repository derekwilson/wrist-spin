package net.derekwilson.wrist_spin.ui.lockscreen

import android.app.Activity
import android.app.KeyguardManager
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import android.widget.LinearLayout
import dagger.android.AndroidInjection
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.customviews.MatchStateDisplayView
import javax.inject.Inject

// as it stands all the logic we need to test can be held in a provider - IMatchStateDisplayTextProvider
// we dont need the overhead of a ViewModel and Observers
// if it turns out that we do need a ViewModel then we need to derive from AppCompatActivity
// and set the activity android:theme in the manifest to be @style/AppFullScreenTheme
// and then add the usual ViewModel factory and observer

class LockScreenActivity : Activity() {
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var keyGuardManager: KeyguardManager
    @Inject
    lateinit var textProvider: IMatchStateDisplayTextProvider

    private lateinit var lockscreenRoot: LinearLayout
    private lateinit var lockscreenContainer: LinearLayout
    private fun bindControls() {
        lockscreenRoot = findViewById(R.id.lockscreen_root)
        lockscreenContainer = findViewById(R.id.lockscreen_container)
    }

    private fun setupBindings() {
        lockscreenRoot.setOnClickListener {
            // the user clicked on the background as the container didnt fill the screen
            loggerFactory.logger.debug("LockScreenActivity.setOnClickListener() - root")
            finish()
        }
        lockscreenContainer.setOnClickListener {
            // the user clicked in the text
            loggerFactory.logger.debug("LockScreenActivity.setOnClickListener() - container")
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        loggerFactory.logger.debug("LockScreenActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setupWindow()
        if (Build.VERSION.SDK_INT >= 27) {
            setShowWhenLocked(true);
        }
        else {
            @Suppress("DEPRECATION")
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        }

        setContentView(R.layout.activity_lockscreen)
        bindControls()

        setupBindings()
    }

    override fun onDestroy() {
        loggerFactory.logger.debug("LockScreenActivity.onDestroy()")
        super.onDestroy()
        sendAppToBack()
    }

    override fun onResume() {
        loggerFactory.logger.debug("LockScreenActivity.onResume()")
        super.onResume()
        if (!keyGuardManager.isKeyguardLocked) {
            // user unlocked the device - they dont want a lock screen
            loggerFactory.logger.debug("LockScreenActivity.onResume() - device unlocked")
            finish()
            return
        }
        addMatchStateViews(textProvider.getSelectedMatchesDisplayTextFromStore())
    }

    override fun onPause() {
        loggerFactory.logger.debug("LockScreenActivity.onPause()")
        super.onPause()
    }

    override fun onUserLeaveHint() {
        loggerFactory.logger.debug("LockScreenActivity.onUserLeaveHint()")
        super.onUserLeaveHint()
        // whatever the reason the user leaves the screen we should kill our lock screen
        finish()
    }

    @Suppress("DEPRECATION")
    private fun setupWindow() {
        loggerFactory.logger.debug("LockScreenActivity.setupWindow()")
        this.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        loggerFactory.logger.debug("LockScreenActivity.setupWindow() - flags added")
    }

    override fun onAttachedToWindow() {
        loggerFactory.logger.debug("LockScreenActivity.onAttachedToWindow()")
        super.onAttachedToWindow()
    }

    private fun sendAppToBack() {
        loggerFactory.logger.debug("LockScreenActivity.sendAppToBack()")
        moveTaskToBack(true)
    }

    private fun addMatchStateViews(matches: List<Pair<String, String>>) {
        lockscreenContainer.removeAllViews()
        for ((title, subtitle) in matches) {
            val matchView = MatchStateDisplayView(this)
            matchView.title = title
            matchView.subtitle = subtitle
            lockscreenContainer.addView(matchView)
        }
    }
}



package net.derekwilson.wrist_spin.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.exception.BadPermissionException
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.IPermissionChecker
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin.utility.IServiceHelper
import net.derekwilson.wrist_spin.utility.ISpeechHelper
import net.derekwilson.wrist_spin.utility.SingleLiveEvent
import net.derekwilson.wrist_spin_cricket_logic.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchSelectionAndState
import java.util.Collections
import javax.inject.Inject

class MainViewModel
@Inject constructor(
    app: Application,
    private val loggerFactory: ILoggerFactory,
    private val resourceProvider: IResourceProvider,
    private val matchStore: IMatchStore,
    private val speechHelper: ISpeechHelper,
    private val crashReporter: ICrashReporter,
    private val permissionChecker: IPermissionChecker,
    private val serviceHelper: IServiceHelper,
    private val preferencesProvider: IPreferencesProvider,
)
    : AndroidViewModel(app), DefaultLifecycleObserver {

    // observables
    data class Observables(
        val displayMessage: MutableLiveData<String> = SingleLiveEvent(),
        val setScoreTickerServiceRunningState: MutableLiveData<Boolean> = SingleLiveEvent(),
        val displayScoreTickerStateUi: MutableLiveData<Pair<Boolean, String>> = SingleLiveEvent(),
        val activeMatchesLoaded: SingleLiveEvent<Void?> = SingleLiveEvent(),

        val activeMatchMoved: SingleLiveEvent<Pair<Int, Int>> = SingleLiveEvent(),
        val activeMatchRemoved: SingleLiveEvent<Int> = SingleLiveEvent(),
        val activeMatchTitle: MutableLiveData<String> = SingleLiveEvent(),

        val navigateToSettings: SingleLiveEvent<Void?> = SingleLiveEvent(),
        val requestPushNotificationPermission: SingleLiveEvent<Void?> = SingleLiveEvent(),
    )
    val observables = Observables()

    init {
        speechHelper.initialise()
    }

    override fun onCreate(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onCreate")
        restoreActiveMatchesFromPreferences()
        requestNotificationPermission()
    }

    override fun onResume(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onResume")
        updateUi()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        loggerFactory.logger.debug("MainViewModel.onDestroy")
        speechHelper.shutdown()
    }

    private fun requestNotificationPermission() {
        if (!permissionChecker.hasPushNotificationPermission()) {
            loggerFactory.logger.debug("MainViewModel.requestPushNotificationPermission")
            observables.requestPushNotificationPermission.call()
        }
    }

    fun actionSelected(itemId: Int): Boolean {
        loggerFactory.logger.debug("MainViewModel: actionSelected ${itemId}")
        when (itemId) {
            R.id.action_settings -> {
                observables.navigateToSettings.call()
                return true
            }
        }
        return false
    }

    fun updateUi() {
        setActiveMatchTitle()
        updateScoreTickerStateUi(serviceHelper.isTickerServiceRunning())
    }

    private fun updateScoreTickerStateUi(scoreTickerServiceRunning: Boolean) {
        observables.displayScoreTickerStateUi.value = Pair(
            scoreTickerServiceRunning,
            if (scoreTickerServiceRunning) resourceProvider.getString(R.string.row_title_score_ticker_on) else resourceProvider.getString(R.string.row_title_score_ticker_off)
        )
    }

    fun toggleScoreTickerService() {
        loggerFactory.logger.debug("MainViewModel: toggleScoreTickerService")
        val serviceRunning = serviceHelper.isTickerServiceRunning()
        if (!serviceRunning && !permissionChecker.hasPushNotificationPermission()) {
            // the app will work without this permission but we should alert the user that the score notification
            loggerFactory.logger.warn("MainViewModel: notifications disabled")
            observables.displayMessage.value = resourceProvider.getString(R.string.notification_permission_denied)
        }
        if (!serviceRunning && !permissionChecker.hasDrawOverlaysPermission()) {
            // the lock screen cannot work without permission
            loggerFactory.logger.warn("MainViewModel: draw overlay disabled")
            observables.displayMessage.value = resourceProvider.getString(R.string.lock_screen_bad_permissions)
            crashReporter.logNonFatalException(BadPermissionException("DrawOverlay"))
        }
        observables.setScoreTickerServiceRunningState.value = !serviceRunning
    }

    private fun setActiveMatchTitle() {
        val title = resourceProvider.getQuantityString(R.plurals.active_matches_title, itemCount)
        observables.activeMatchTitle.value = title
    }

    fun loadActiveMatches() {
        activeMatches.clear()
        val matchIds = matchStore.allIdsInStore
        for (id in matchIds) {
            loggerFactory.logger.debug("MainViewModel.loadActiveMatches ID= ${id}")
            val match = matchStore.getMatchSelection(id)
            val state = matchStore.getMatchState(id)
            match.let {
                loggerFactory.logger.debug("MainViewModel.loadActiveMatches Adding ID= ${id}")
                activeMatches.add(MatchSelectionAndState(it, state))
            }
        }
        loggerFactory.logger.debug("MainViewModel.loadActiveMatches size= ${activeMatches.size}")
        observables.activeMatchesLoaded.call()
        setActiveMatchTitle()
    }

    // the state might change if the service is currently running
    // reload the copy we have to make sure we are up to date
    private fun reloadActiveMatchesState() {
        for (thisMatch in activeMatches) {
            loggerFactory.logger.debug("MainViewModel.reloadActiveMatchesState ID= ${thisMatch.selection.id}")
            val state = matchStore.getMatchState(thisMatch.selection.id)
            thisMatch.state = state
        }
    }

    fun getMatchId(position: Int): Long {
        return activeMatches[position].selection.id.toLong()
    }

    fun getMatchLabel(position: Int): String {
        return "${activeMatches[position].selection.team1}\nv\n${activeMatches[position].selection.team2}"
    }

    fun matchMoved(fromPosition: Int, toPosition: Int) {
        loggerFactory.logger.debug("MainViewModel.matchMoved ${fromPosition} -> ${toPosition}")
        // make sure we are up to date - there is a possibility of a race condition here but lets ignore it
        // if it happens then we might throw away one update but it will self correct
        reloadActiveMatchesState()

        for (thisMatch in activeMatches) {
            loggerFactory.logger.debug("MainViewModel.matchMoved before ${thisMatch.selection.display}")
        }
        Collections.swap(activeMatches, fromPosition, toPosition)
        for (thisMatch in activeMatches) {
            loggerFactory.logger.debug("MainViewModel.matchMoved after ${thisMatch.selection.display}")
        }
        // we also need to update the store
        matchStore.replaceAllMatches(activeMatches)
        updateActiveMatchesInPreferences()

        observables.activeMatchMoved.value = Pair(fromPosition, toPosition)
    }

    fun matchOptionSelected(id: Long, position: Int) {
        loggerFactory.logger.debug("MainViewModel.matchOptionSelected ${id}, ${position}")
        // remove from the store (we look the id up in the active list
        matchStore.removeMatch(activeMatches[position].selection.id)
        updateActiveMatchesInPreferences()
        // remove from the list in the recycler
        activeMatches.removeAt(position)
        observables.activeMatchRemoved.value = position
        setActiveMatchTitle()
    }

    fun updateActiveMatchesInPreferences() {
        val key = resourceProvider.getString(R.string.settings_active_matches_key)
        val storeAsString = matchStore.getStoreMatchesAsString()
        preferencesProvider.setPreferenceString(key, storeAsString)
    }

    private fun restoreActiveMatchesFromPreferences() {
        val key = resourceProvider.getString(R.string.settings_active_matches_key)
        val storeAsString = preferencesProvider.getPreferenceString(key, "")
        matchStore.replaceStoreMatchesFromString(storeAsString)
        loadActiveMatches()
    }

    val itemCount: Int
        get() {
            return activeMatches.size
        }

    private var activeMatches: ArrayList<MatchSelectionAndState> = ArrayList()
}

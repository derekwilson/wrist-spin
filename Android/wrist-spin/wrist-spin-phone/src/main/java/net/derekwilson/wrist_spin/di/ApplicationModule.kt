package net.derekwilson.wrist_spin.di

import android.app.ActivityManager
import android.app.AlarmManager
import android.app.Application
import android.app.KeyguardManager
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.media.AudioManager
import android.os.PowerManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import dagger.Module
import dagger.Provides
import net.derekwilson.wrist_spin.AndroidApplication
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.logging.LoggingSink
import net.derekwilson.wrist_spin.logging.Slf4jLoggerFactory
import net.derekwilson.wrist_spin.utility.AndroidDefaultSharedPreferencesProvider
import net.derekwilson.wrist_spin.utility.AndroidEnvironmentInformationProvider
import net.derekwilson.wrist_spin.utility.AndroidResourceProvider
import net.derekwilson.wrist_spin.utility.CoroutineDispatcherProvider
import net.derekwilson.wrist_spin.utility.CrashlyticsReporter
import net.derekwilson.wrist_spin.utility.CricketDataSourceProvider
import net.derekwilson.wrist_spin.utility.FileSystemHelper
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IAndroidEnvironmentInformationProvider
import net.derekwilson.wrist_spin.utility.ICoroutineDispatcherProvider
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin.utility.IFileSystemHelper
import net.derekwilson.wrist_spin.utility.IPermissionChecker
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin.utility.IServiceHelper
import net.derekwilson.wrist_spin.utility.ISpeechHelper
import net.derekwilson.wrist_spin.utility.ISystemTime
import net.derekwilson.wrist_spin.utility.MixpanelAnalyticsEngine
import net.derekwilson.wrist_spin.utility.PermissionChecker
import net.derekwilson.wrist_spin.utility.ServiceHelper
import net.derekwilson.wrist_spin.utility.SpeechHelper
import net.derekwilson.wrist_spin.utility.SystemTime
import net.derekwilson.wrist_spin_cricket_logic.datasource.CricketDataSourceFactory
import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSourceFactory
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import net.derekwilson.wrist_spin_cricket_logic.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore
import javax.inject.Provider
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application.applicationContext

    /* Singleton factory that searches generated map for specific provider and uses it to get a ViewModel instance */
    @Provides
    @Singleton
    fun provideViewModelFactory(providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) =
        object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                return requireNotNull(providers[modelClass]).get() as T
            }
        }

    @Provides
    @Singleton
    fun provideLoggerFactory(loggerFactory: Slf4jLoggerFactory): ILoggerFactory {
        return loggerFactory
    }

    @Provides
    @Singleton
    fun provideCrashReporter(reporter: CrashlyticsReporter): ICrashReporter {
        return reporter
    }

    @Provides
    @Singleton
    fun provideAnalyticsEngine(engine: MixpanelAnalyticsEngine): IAnalyticsEngine {
        return engine
    }

    @Provides
    @Singleton
    fun provideEnvironmentInformationProvider(provider: AndroidEnvironmentInformationProvider): IAndroidEnvironmentInformationProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideResourceProvider(provider: AndroidResourceProvider): IResourceProvider {
        return provider
    }

    @Provides
    @Singleton
    fun providePreferencesProvider(provider: AndroidDefaultSharedPreferencesProvider): IPreferencesProvider {
        return provider
    }

    @Provides
    @Singleton
    fun providePermissionChecker(checker: PermissionChecker): IPermissionChecker {
        return checker
    }

    @Provides
    @Singleton
    fun provideContentResolver(): ContentResolver {
        return application.applicationContext.contentResolver
    }

    @Provides
    @Singleton
    fun provideNotificationManager(): NotificationManager {
        return application.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    @Provides
    @Singleton
    fun provideAudioManager(): AudioManager {
        return application.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    @Provides
    @Singleton
    fun provideAlarmManager(): AlarmManager {
        return application.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    @Provides
    @Singleton
    fun providePowerManager(): PowerManager {
        return application.getSystemService(Context.POWER_SERVICE) as PowerManager
    }

    @Provides
    @Singleton
    fun provideActivityManager(): ActivityManager {
        return application.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    }

    @Provides
    @Singleton
    fun provideKeyguardManager(): KeyguardManager {
        return application.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
    }

    @Provides
    @Singleton
    fun provideSystemTime(systemTime: SystemTime): ISystemTime {
        return systemTime
    }

    @Provides
    @Singleton
    fun provideLoggingSink(logging: LoggingSink): ILoggingSink {
        return logging
    }

    @Provides
    @Singleton
    fun provideDataSourceFactory(loggingSync: ILoggingSink): ICricketDataSourceFactory {
        return CricketDataSourceFactory.createFactory(loggingSync)
    }

    @Provides
    @Singleton
    fun provideCricketDataSourceProvider(provider: CricketDataSourceProvider): ICricketDataSourceProvider {
        return provider
    }

    @Provides
    @Singleton
    fun provideMatchStore(loggingSync: ILoggingSink): IMatchStore {
        return MatchStore.createStore(loggingSync)
    }

    @Provides
    @Singleton
    fun provideSpeechHelper(helper: SpeechHelper): ISpeechHelper {
        return helper
    }

    @Provides
    @Singleton
    fun provideFileSystemHelper(helper: FileSystemHelper): IFileSystemHelper {
        return helper
    }

    @Provides
    @Singleton
    fun provideServiceHelper(helper: ServiceHelper): IServiceHelper {
        return helper
    }

    @Provides
    fun provideCoroutineProvider(provider: CoroutineDispatcherProvider): ICoroutineDispatcherProvider {
        return provider
    }
}

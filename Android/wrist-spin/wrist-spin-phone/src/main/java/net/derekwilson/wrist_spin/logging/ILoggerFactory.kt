package net.derekwilson.wrist_spin.logging

import org.slf4j.Logger

interface ILoggerFactory {
    val logger: Logger
}
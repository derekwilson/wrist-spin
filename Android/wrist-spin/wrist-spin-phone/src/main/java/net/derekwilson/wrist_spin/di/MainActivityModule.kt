package net.derekwilson.wrist_spin.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin.ui.main.MainActivity
import net.derekwilson.wrist_spin.ui.main.MainViewModel

@Subcomponent
interface IMainActivitySubcomponent : AndroidInjector<MainActivity> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<MainActivity>
}

@Module(subcomponents = [
    IMainActivitySubcomponent::class
])
abstract class MainActivityModule {
    @Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    abstract fun bindActivityInjectorFactory(builder: IMainActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindActivityViewModel(model: MainViewModel): ViewModel
}




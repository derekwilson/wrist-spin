package net.derekwilson.wrist_spin.ui.scoreticker

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import dagger.android.AndroidInjection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.main.MainActivity
import net.derekwilson.wrist_spin.utility.ICoroutineDispatcherProvider
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin.utility.ISpeechHelper
import net.derekwilson.wrist_spin.utility.ISystemTime
import net.derekwilson.wrist_spin_cricket_logic.utility.CalendarFormatter
import javax.inject.Inject

class ScoreTickerIntentReceiver : BroadcastReceiver() {
    @Inject
    lateinit var applicationContext: Context
    @Inject
    lateinit var loggerFactory: ILoggerFactory
    @Inject
    lateinit var resourceProvider: IResourceProvider
    @Inject
    lateinit var preferencesProvider: IPreferencesProvider
    @Inject
    lateinit var coroutineDispatcherProvider: ICoroutineDispatcherProvider
    @Inject
    lateinit var alarmManager: AlarmManager
    @Inject
    lateinit var powerManager: PowerManager
    @Inject
    lateinit var crashReporter: ICrashReporter
    @Inject
    lateinit var systemTime: ISystemTime
    @Inject
    lateinit var notificationManager: NotificationManager
    @Inject
    lateinit var matchUpdater: IMatchStateUpdater
    @Inject
    lateinit var speechHelper: ISpeechHelper

    private val job = Job()
    private var scope: CoroutineScope? = null

    // Handle actions and display Lockscreen
    override fun onReceive(context: Context, intent: Intent) {
        // we only have 10 seconds if the device was asleep
        AndroidInjection.inject(this, context)
        val lock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TICKER_WAKELOCK_TAG)
        lock?.let { powerLock ->
            powerLock.acquire(TICKER_WAKELOCK_TIMEOUT)
            loggerFactory.logger.debug("ScoreTickerIntentReceiver: onReceive ${intent.action}")
            // we need to do repeating alarms ourselves
            startAlarm(context, alarmManager, systemTime, loggerFactory, preferencesProvider, resourceProvider)

            if (scope == null) {
                scope = CoroutineScope(coroutineDispatcherProvider.getCoroutineDispatcher() + job)
            }
            scope?.launch {
                try {
                    val notifications = matchUpdater.updateAllMatchStatesAndGetNotifications()
                    for (thisNotification in notifications) {
                        showTicker(context, thisNotification)
                        if (thisNotification.speech.isNotBlank()) {
                            speechHelper.speak(thisNotification.speech)
                        }
                    }
                } catch (ex: Exception) {
                    loggerFactory.logger.error("ScoreTickerIntentReceiver: ", ex)
                    crashReporter.logNonFatalException(ex)
                } finally {
                    loggerFactory.logger.debug("ScoreTickerIntentReceiver: release lock")
                    powerLock.release()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createTickerChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(TICKER_NOTIFICATION_CHANNEL_ID, "wrist-spin-score-ticker", NotificationManager.IMPORTANCE_HIGH)

            // Configure the notification channel.
            notificationChannel.description = "wrist-spin-score-ticker"
            notificationChannel.enableLights(false)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun showTicker(context: Context, params: NotificationParams) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createTickerChannel()
        }
        val builder = NotificationCompat.Builder(context, TICKER_NOTIFICATION_CHANNEL_ID)

        // icon from - https://freesvg.org/cricket-bat
        builder
            .setTicker(params.title)
            .setSubText(params.subTitle)
            .setContentTitle(params.title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(params.content))
            .setContentText(params.content)
            .setSmallIcon(R.drawable.ic_cricket_bat)
            .setColor(ContextCompat.getColor(context, R.color.colour_red))
            .setColorized(true)
            .setContentIntent(getNotificationIntent(context))
            .setOngoing(false)
            .setSilent(params.silent)

        loggerFactory.logger.debug("ScoreTickerIntentReceiver:showTicker ${params.id}, ${CalendarFormatter.convertCalendarToTimeString(systemTime.getCurrentTime())}, silent = ${params.silent}")
        notificationManager.notify(params.id, builder.build())
    }

    private fun getNotificationIntent(context: Context): PendingIntent {
        val intent = Intent(context, MainActivity::class.java)
            .setAction(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
    }

    companion object {
        private const val TICKER_WAKELOCK_TAG = "wrist-spin::wakelock"
        private const val TICKER_WAKELOCK_TIMEOUT = 10 * 60 * 1000L         // we have 10 mins to do our stuff

        private const val TICKER_NOTIFICATION_CHANNEL_ID = "wrist-spin-score-ticker-channel-id-01"
        private const val TICKER_NOTIFICATION_ID = 1000

        private const val MILLISECONDS_IN_A_MINUTE = 1 * 60 * 1000L

        public fun startAlarm(
            context: Context,
            alarmManager: AlarmManager,
            systemTime: ISystemTime,
            loggerFactory: ILoggerFactory,
            preferencesProvider: IPreferencesProvider,
            resourceProvider: IResourceProvider
        ) {
            val intervalInMins = preferencesProvider.getPreferencesNumber(
                resourceProvider.getString(R.string.settings_ticker_update_mins_key),
                resourceProvider.getString(R.string.settings_ticker_update_mins_default)
            )
            val interval = intervalInMins * MILLISECONDS_IN_A_MINUTE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                loggerFactory.logger.debug("ScoreTickerIntentReceiver: setAndAllowWhileIdle, interval = ${intervalInMins}, ${interval}")
                alarmManager.setAndAllowWhileIdle(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    systemTime.getBootElapsedMillis() + interval,
                    getAlarmOperation(context)
                )
            } else {
                loggerFactory.logger.debug("ScoreTickerIntentReceiver: set, interval = ${intervalInMins}, ${interval}")
                alarmManager.set(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    systemTime.getBootElapsedMillis() + interval,
                    getAlarmOperation(context)
                )
            }
        }

        public fun stopAlarm(context: Context, alarmManager: AlarmManager) {
            alarmManager.cancel(getAlarmOperation(context))
        }

        public fun getAlarmOperation(context: Context): PendingIntent {
            val intent = Intent(context, ScoreTickerIntentReceiver::class.java)
            return PendingIntent.getBroadcast(
                context,
                TICKER_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        }
    }
}

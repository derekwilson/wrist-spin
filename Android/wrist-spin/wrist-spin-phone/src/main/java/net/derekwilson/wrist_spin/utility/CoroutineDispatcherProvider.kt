package net.derekwilson.wrist_spin.utility

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

interface ICoroutineDispatcherProvider {
    fun getCoroutineDispatcher(): CoroutineDispatcher
    fun getMainContext(): CoroutineContext
}

class CoroutineDispatcherProvider @Inject constructor():  ICoroutineDispatcherProvider{
    override fun getCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    override fun getMainContext(): CoroutineContext {
        return Dispatchers.Main
    }
}

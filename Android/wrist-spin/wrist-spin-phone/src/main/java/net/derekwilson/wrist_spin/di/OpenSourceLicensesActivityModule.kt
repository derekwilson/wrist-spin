package net.derekwilson.wrist_spin.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import net.derekwilson.wrist_spin.ui.settings.OpenSourceLicensesActivity
import net.derekwilson.wrist_spin.ui.settings.OpenSourceLicensesViewModel


@Subcomponent
interface IOpenSourceLicensesActivitySubcomponent  : AndroidInjector<OpenSourceLicensesActivity> {
    @Subcomponent.Factory
    public interface Factory : AndroidInjector.Factory<OpenSourceLicensesActivity>
}

@Module(subcomponents = [
    IOpenSourceLicensesActivitySubcomponent::class
])
abstract class OpenSourceLicensesActivityModule {
    @Binds
    @IntoMap
    @ClassKey(OpenSourceLicensesActivity::class)
    abstract fun bindActivityInjectorFactory(builder: IOpenSourceLicensesActivitySubcomponent.Factory): AndroidInjector.Factory<*>

    @Binds
    @IntoMap
    @ViewModelKey(OpenSourceLicensesViewModel::class)
    abstract fun bindCompareViewModel(model: OpenSourceLicensesViewModel): ViewModel
}



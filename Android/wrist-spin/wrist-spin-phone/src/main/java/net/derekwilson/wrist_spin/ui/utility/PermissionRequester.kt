package net.derekwilson.wrist_spin.ui.utility

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import net.derekwilson.wrist_spin.R

object PermissionRequester {

    const val MY_PERMISSIONS_REQUEST_NOTIFICATION = 96

    fun requestPushNotificationPermission(activity: Activity) {
        requestPermission(
            activity,
            android.Manifest.permission.POST_NOTIFICATIONS,
            R.string.notification_permission_request_title,
            R.string.notification_permission_request_body,
            MY_PERMISSIONS_REQUEST_NOTIFICATION
        )
    }

    private fun requestPermission(activity: Activity, permission: String, messageTitle: Int, messageBody: Int, requestCode: Int) {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            AlertDialog.Builder(activity)
                .setTitle(messageTitle)
                .setMessage(messageBody)
                .setPositiveButton(R.string.ok_button) {
                        _, _ ->
                    run {
                        //Prompt the user once explanation has been shown
                        ActivityCompat.requestPermissions(activity, arrayOf<String>(permission), requestCode)
                    }
                }
                .create()
                .show()
        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity, arrayOf<String>(permission), requestCode)
        }
    }

}

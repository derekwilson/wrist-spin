package net.derekwilson.wrist_spin.ui.scoreticker

import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.scoreticker.MatchStateUpdater.Companion.TICKER_NOTIFICATION_ID
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IAndroidEnvironmentInformationProvider
import net.derekwilson.wrist_spin.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin_cricket_logic.matchstore.IMatchStore
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState
import javax.inject.Inject

interface IMatchStateUpdater {
    fun updateAllMatchStatesAndGetNotifications(): List<NotificationParams>
}

data class NotificationParams(
    val id: Int = TICKER_NOTIFICATION_ID,
    val title: String = "",
    val subTitle: String = "",
    val content: String = "",
    val silent: Boolean = false,
    val speech: String = "",
)

class MatchStateUpdater @Inject constructor(
    private val loggerFactory: ILoggerFactory,
    private val preferencesProvider: IPreferencesProvider,
    private val resourceProvider: IResourceProvider,
    private val matchStore: IMatchStore,
    private val cricketDataSourceProvider: ICricketDataSourceProvider,
    private val androidEnvironmentInformationProvider: IAndroidEnvironmentInformationProvider,
    private val analyticsEngine: IAnalyticsEngine,
) : IMatchStateUpdater {

    override fun updateAllMatchStatesAndGetNotifications(): List<NotificationParams> {
        loggerFactory.logger.debug("MatchStateUpdater: updateAllMatchStatesAndGetNotifications")
        val notifications = ArrayList<NotificationParams>()

        val dataSource = cricketDataSourceProvider.getDataSource()
        val ids = matchStore.allIdsInStore
        val multipleMatches = ids.size > 1
        for (id in ids) {
            analyticsEngine.getMatchStateEvent(dataSource.name, id)
            val state = dataSource.getMatchState(id)
            state?.let {
                matchStore.updateMatchState(id, it)
                val params = NotificationParams(
                    id = getNotificationId(it),
                    title = getNotificationTitle(it),
                    subTitle = getNotificationSubTitle(it),
                    content = getNotificationContentText(it),
                    silent = !isSoundNeeded(it),
                    speech = if (isSpeechNeeded(it)) getSpeech(it, multipleMatches) else ""
                )
                if (!shouldSuppress(it)) {
                    notifications.add(params)
                }
                // we only notify once - if the caller does not do it then its lost
                it.resetAlerts()
            } ?: run {
                loggerFactory.logger.debug("MatchStateUpdater:updateAllMatchStatesAndGetNotifications - null state")
            }
        }
        return notifications
    }

    private fun shouldSuppress(state: MatchState): Boolean {
        if (!preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_suppress_ticker_update_key
                ), false
            )
        ) {
            return false
        }
        if (state.alerts.isIndividual50 || state.alerts.isIndividual100 ||
            state.alerts.isTeam50 || state.alerts.isTeam100 ||
            state.alerts.isWicket) {
            // there was a chimable event, dont suppress
            return false
        }
        loggerFactory.logger.debug("MatchStateUpdater:shouldSuppress - suppressing alert")
        return true
    }

    private fun isSoundNeeded(state: MatchState): Boolean {
        if (state.alerts.isWicket && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_wicket_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isTeam50 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_team50_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isTeam100 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_team100_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isIndividual50 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_individual50_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isIndividual100 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_alert_individual100_key
                ), false
            )
        ) {
            return true
        }
        return false
    }

    private fun isSpeechNeeded(state: MatchState): Boolean {
        if (doesStateNeedSpeech(state)) {
            if (preferencesProvider.getPreferenceBoolean(resourceProvider.getString(R.string.settings_speak_override_key), false)) {
                // we need speech no matter what the phone state
                loggerFactory.logger.debug("MatchStateUpdater:isSpeechNeeded - speech needed - override")
                return true
            }
            if (androidEnvironmentInformationProvider.isDnd()) {
                loggerFactory.logger.debug("MatchStateUpdater:isSpeechNeeded - speech needed - DND")
                return false
            }
            if (androidEnvironmentInformationProvider.isRingerMuted()) {
                loggerFactory.logger.debug("MatchStateUpdater:isSpeechNeeded - speech needed - phone silent")
                return false
            }
            return true
        }
        return false
    }

    private fun doesStateNeedSpeech(state: MatchState): Boolean {
        if (state.alerts.isWicket && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_speak_wicket_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isTeam50 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_speak_team50_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isTeam100 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_speak_team100_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isIndividual50 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_speak_individual50_key
                ), false
            )
        ) {
            return true
        }
        if (state.alerts.isIndividual100 && preferencesProvider.getPreferenceBoolean(
                resourceProvider.getString(
                    R.string.settings_speak_individual100_key
                ), false
            )
        ) {
            return true
        }
        return false
    }

    private fun getSpeech(state: MatchState, multipleMatches: Boolean): String {
        val scoreSpeech = state.getScoreSpeech(multipleMatches)
        if ((state.alerts.isIndividual50 || state.alerts.isIndividual100) && state.alerts.individualSpeech.isNotEmpty()) {
            // there is additional speech in the alter
            return "${scoreSpeech}, ${state.alerts.individualSpeech}"
        }
        return scoreSpeech
    }

    private fun getNotificationId(state: MatchState): Int {
        val id = state.id.toIntOrNull()
        id?.let {
            return it
        } ?: run {
            return TICKER_NOTIFICATION_ID
        }
    }

    private fun getNotificationSubTitle(state: MatchState): String {
        return "${state.matchTitle}";
    }

    private fun getNotificationTitle(state: MatchState): String {
        return "${state.scoreDisplayWithOvers}";
    }

    private fun getNotificationContentText(state: MatchState): String {
        val stringBuilder = StringBuilder()
        if (state.strikerName.isNotBlank()) {
            stringBuilder.append(state.strikerName)
            if (state.strikerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.strikerStats)
            }
        }
        if (state.nonStrikerName.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append(", ")
                stringBuilder.append(state.nonStrikerName)
            }
            if (state.nonStrikerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.nonStrikerStats)
            }
        }
        if (state.bowlerName.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            stringBuilder.append(state.bowlerName)
            if (state.bowlerStats.isNotBlank()) {
                stringBuilder.append(" ")
                stringBuilder.append(state.bowlerStats)
            }
        }
        if (state.lead.isNotBlank() || state.fact.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            if (state.lead.isNotBlank()) {
                stringBuilder.append(state.lead)
                if (state.fact.isNotBlank()) {
                    if (state.lead.isNotBlank()) {
                        stringBuilder.append(", ")
                    }
                    stringBuilder.append(state.fact)
                }
            }
        }
        if (state.staleDisplayWithGeneratedTime.isNotBlank()) {
            if (stringBuilder.isNotEmpty()) {
                stringBuilder.append("\n")
            }
            stringBuilder.append(state.staleDisplayWithGeneratedTime)
        }
        return stringBuilder.toString()
    }

    companion object {
        public const val TICKER_NOTIFICATION_ID = 1000
   }
}
package net.derekwilson.wrist_spin.ui.matchpicker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.logging.ILoggerFactory

class MatchPickerRecyclerItemAdapter(
    private val context: Context,
    private val viewModel: MatchPickerViewModel,
    private val loggerFactory: ILoggerFactory,
) : RecyclerView.Adapter<MatchPickerRecyclerItemAdapter.RecyclerViewHolder>()
{
    init {
        setHasStableIds(true)
        viewModel.loadMatches()
    }

    override fun getItemCount(): Int {
        return viewModel.itemCount
    }

    override fun getItemId(position: Int): Long {
        return viewModel.getMatchId(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_match, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val id = viewModel.getMatchId(position)
        val label = viewModel.getMatchLabel(position)
        val subLabel = viewModel.getMatchSubLabel(position)
        holder.bind(label, subLabel)
        holder.rowLabelContainer.setOnClickListener {
            viewModel.matchSelected(id, position)
        }
    }

    inner class RecyclerViewHolder(private var view: View) : RecyclerView.ViewHolder(view) {

        internal var rowLabelContainer: View = view.findViewById(R.id.match_row_label_container)
        private var txtLabel: TextView = view.findViewById(R.id.match_row_label)
        private var txtSubLabel: TextView = view.findViewById(R.id.match_row_sub_label)

        fun bind(label: String, subLabel: String) {
            txtLabel.text = label
            if (subLabel == "") {
                txtSubLabel.visibility = View.GONE
            } else {
                txtSubLabel.text = subLabel
                txtSubLabel.visibility = View.VISIBLE
            }
        }
    }
}

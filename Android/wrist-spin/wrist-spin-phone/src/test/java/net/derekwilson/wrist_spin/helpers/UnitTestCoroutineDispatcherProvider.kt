package net.derekwilson.wrist_spin.helpers

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import net.derekwilson.wrist_spin.utility.ICoroutineDispatcherProvider
import kotlin.coroutines.CoroutineContext

class UnitTestCoroutineDispatcherProvider : ICoroutineDispatcherProvider {
    override fun getCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.Unconfined
    }

    override fun getMainContext(): CoroutineContext {
        return Dispatchers.Unconfined
    }
}

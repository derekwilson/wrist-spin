package net.derekwilson.wrist_spin.helpers

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchSelectionAndState
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore

object MatchStoreHelper {
    private fun createMatchSelectionAndState(id: String, runs: Int, wickets: Int, overs: Int, balls: Int): MatchSelectionAndState {
        val selection =
            MatchSelection(
                id,
                "team1_${id}",
                "team2_${id}"
            )
        val state = MatchStateHelper.setupMatchState(id, runs, wickets, overs, balls)
        return MatchSelectionAndState(selection, state)
    }

    fun addMatchToStore(store: MatchStore, id: String, runs: Int, wickets: Int, overs: Int, balls: Int) {
        val match = createMatchSelectionAndState(id, runs, wickets, overs, balls)
        store.addMatch(match)
    }

    fun updateMatchStateInStore(store: MatchStore, id: String, runs: Int, wickets: Int, overs: Int, balls: Int) {
        val state = MatchStateHelper.setupMatchState(id, runs, wickets, overs, balls)
        store.updateMatchState(id,state)
    }

    fun setupStoreWithOneMatch(store: MatchStore, id: String, runs: Int, wickets: Int, overs: Int, balls: Int) {
        val match = createMatchSelectionAndState(id, runs, wickets, overs, balls)
        setupMatchStore(store, listOf(match))
    }

    fun setupMatchStore(store: MatchStore, matches: List<MatchSelectionAndState>) {
        store.reset()
        store.replaceAllMatches(matches)
    }
}
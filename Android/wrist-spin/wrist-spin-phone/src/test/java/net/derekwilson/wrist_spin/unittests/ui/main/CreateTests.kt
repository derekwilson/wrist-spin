package net.derekwilson.wrist_spin.unittests.ui.main

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class CreateTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun create_requests_notification_permission() {
        // arrange
        `when`(mockPermissionChecker.hasPushNotificationPermission()).thenReturn(false)

        // act
        viewModel.onCreate(mockLifecycleOwner)

        // assert
        verify(mockRequestPushNotificationPermissionObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockRequestPushNotificationPermissionObserver)
    }

    @Test
    fun create_does_not_requests_notification_permission_if_granted() {
        // arrange
        `when`(mockPermissionChecker.hasPushNotificationPermission()).thenReturn(true)

        // act
        viewModel.onCreate(mockLifecycleOwner)

        // assert
        verify(mockRequestPushNotificationPermissionObserver, never()).onChanged(null)
    }

    @Test
    fun create_restores_active_matches() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn("1:Team_1_1:Team_2_1:")

        // act
        viewModel.onCreate(mockLifecycleOwner)

        // assert
        val idPos0 = viewModel.getMatchId(0)
        val labelPos0 = viewModel.getMatchLabel(0)
        assertThat(idPos0, `is`(1))
        assertThat(labelPos0, `is`("Team_1_1\nv\nTeam_2_1"))

        verify(mockActiveMatchesLoadedObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockActiveMatchesLoadedObserver)
    }
}





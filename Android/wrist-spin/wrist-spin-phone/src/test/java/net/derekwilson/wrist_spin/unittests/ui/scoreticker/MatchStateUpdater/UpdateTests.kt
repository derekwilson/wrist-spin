package net.derekwilson.wrist_spin.unittests.ui.scoreticker.MatchStateUpdater

import net.derekwilson.wrist_spin.helpers.MatchStoreHelper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class UpdateTests : MatchStateUpdaterSetup() {

    @Before
    fun before_each_test() {
        setupMatchStateUpdater()
    }

    @Test
    fun update_with_no_matches_returns_empty() {
        // arrange
        MatchStoreHelper.setupMatchStore(matchStore, listOf())

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(0))

        verify(mockAnalytics, never()).getMatchStateEvent(anyString(), anyString())
        verifyNoMoreInteractions(mockAnalytics)
    }

    @Test
    fun update_with_one_match() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 0, 10, 1)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/0 10.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))

        verify(mockAnalytics, times(1)).getMatchStateEvent(DATASOURCE_NAME, "1")
        verifyNoMoreInteractions(mockAnalytics)
    }

    @Test
    fun update_with_two_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 10, 1, 20, 0)
        setupNewStateFromDataSource("1", 2, 0, 10, 1)
        setupNewStateFromDataSource("2", 51, 1, 21, 0)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(2))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/0 10.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))

        assertThat(results.get(1).id, `is`(2))
        assertThat(results.get(1).title, `is`("51/1 21.0"))
        assertThat(results.get(1).subTitle, `is`("TEAM1_2 v TEAM2_2"))
        assertThat(results.get(1).content, startsWith("striker_2 s_stats_2, nonstriker_2 n_stats_2\nbowler_2 b_stats_2\n"))
        assertThat(results.get(1).silent, `is`(true))
        assertThat(results.get(1).speech, `is`(""))

        verify(mockAnalytics, times(1)).getMatchStateEvent(DATASOURCE_NAME, "1")
        verify(mockAnalytics, times(1)).getMatchStateEvent(DATASOURCE_NAME, "2")
        verifyNoMoreInteractions(mockAnalytics)
    }

    ///
    /// suppress notifications
    ///

    @Test
    fun update_suppressed_for_score_only() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 0, 10, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_SUPPRESS_TICKER_UPDATE_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(0))
    }

    @Test
    fun update_not_suppressed_for_chime_event() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_WICKET_KEY, false)).thenReturn(true)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_SUPPRESS_TICKER_UPDATE_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_not_suppressed_for_chime_event_even_if_not_configured() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_SUPPRESS_TICKER_UPDATE_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))
    }

    ///
    /// Chimes
    ///

    @Test
    fun update_alerts_wicket() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_WICKET_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_alerts_50() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 52, 0, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_TEAM50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("52/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_alerts_100() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 102, 0, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_TEAM50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("102/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_alerts_individual_50() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 110, 0, 10, 0)
        setupNewStateFromDataSource("1", 116, 0, 11, 1, "51", "4")
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_INDIVIDUAL50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("116/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_alerts_individual_100() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 110, 0, 10, 0)
        setupNewStateFromDataSource("1", 116, 0, 11, 1, "102", "4")
        `when`(mockPreferencesProvider.getPreferenceBoolean(ALERT_INDIVIDUAL100_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("116/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(false))
        assertThat(results.get(0).speech, `is`(""))
    }


    ///
    /// Speech
    ///

    @Test
    fun update_speaks_wicket() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("2 for 1"))
    }

    @Test
    fun update_speaks_50() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 52, 0, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_TEAM50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("52/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("52 without loss"))
    }

    @Test
    fun update_speaks_individual_50() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 110, 0, 10, 0)
        setupNewStateFromDataSource("1", 116, 0, 11, 1, "51", "4")
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_INDIVIDUAL50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("116/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("116 without loss, striker_1, 51"))
    }


    @Test
    fun update_speaks_individual_100() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 110, 0, 10, 0)
        setupNewStateFromDataSource("1", 116, 0, 11, 1, "102", "4")
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_INDIVIDUAL100_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("116/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("116 without loss, striker_1, 102"))
    }

    @Test
    fun update_speaks_100() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 102, 0, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_TEAM100_KEY, false)).thenReturn(true)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_TEAM50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("102/0 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("102 without loss"))
    }

    @Test
    fun update_speech_with_two_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 10, 1, 20, 0)
        setupNewStateFromDataSource("1", 2, 0, 10, 1)
        setupNewStateFromDataSource("2", 51, 1, 21, 0)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_TEAM50_KEY, false)).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(2))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/0 10.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))

        assertThat(results.get(1).id, `is`(2))
        assertThat(results.get(1).title, `is`("51/1 21.0"))
        assertThat(results.get(1).subTitle, `is`("TEAM1_2 v TEAM2_2"))
        assertThat(results.get(1).content, startsWith("striker_2 s_stats_2, nonstriker_2 n_stats_2\nbowler_2 b_stats_2\n"))
        assertThat(results.get(1).silent, `is`(true))
        // the team name is included as there are multiple matches
        assertThat(results.get(1).speech, `is`("batting_2, 51 for 1"))
    }

    @Test
    fun update_speaks_suppress_for_dnd() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isDnd()).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_speaks_suppress_for_ringer_off() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isRingerMuted()).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_speaks_suppress_for_ringer_off_and_dnd() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isRingerMuted()).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isDnd()).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`(""))
    }

    @Test
    fun update_speaks_override_for_ringer_off_and_dnd() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        setupNewStateFromDataSource("1", 2, 1, 11, 1)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_WICKET_KEY, false)).thenReturn(true)
        `when`(mockPreferencesProvider.getPreferenceBoolean(SPEAK_OVERRIDE_KEY, false)).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isRingerMuted()).thenReturn(true)
        `when`(mockAndroidEnvironmentInformationProvider.isDnd()).thenReturn(true)

        // act
        val results = matchStateUpdater.updateAllMatchStatesAndGetNotifications()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).id, `is`(1))
        assertThat(results.get(0).title, `is`("2/1 11.1"))
        assertThat(results.get(0).subTitle, `is`("TEAM1_1 v TEAM2_1"))
        assertThat(results.get(0).content, startsWith("striker_1 s_stats_1, nonstriker_1 n_stats_1\nbowler_1 b_stats_1\n"))
        assertThat(results.get(0).silent, `is`(true))
        assertThat(results.get(0).speech, `is`("2 for 1"))
    }

}

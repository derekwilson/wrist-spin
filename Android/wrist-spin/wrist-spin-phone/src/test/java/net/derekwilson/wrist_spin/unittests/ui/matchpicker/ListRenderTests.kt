package net.derekwilson.wrist_spin.unittests.ui.matchpicker

import net.derekwilson.wrist_spin.helpers.MatchSelectionHelper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

class ListRenderTests : MatchPickerViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun render_id() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getSingleMatchSelection())
        viewModel.loadMatches()

        // act
        val result = viewModel.getMatchId(0)

        // assert
        assertThat(result, `is`(1))
    }

    @Test
    fun render_label() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getSingleMatchSelection())
        viewModel.loadMatches()

        // act
        val result = viewModel.getMatchLabel(0)

        // assert
        assertThat(result, `is`("Team1\nv\nTeam2"))
    }

    @Test
    fun render_sublabel() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getSingleMatchSelection())
        viewModel.loadMatches()

        // act
        val result = viewModel.getMatchSubLabel(0)

        // assert
        assertThat(result, `is`("status1"))
    }

}

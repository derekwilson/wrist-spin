package net.derekwilson.wrist_spin.unittests.ui.main

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class DestroyTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun create_requests_notification_permission() {
        // arrange
        viewModel.onCreate(mockLifecycleOwner)

        // act
        viewModel.onDestroy(mockLifecycleOwner)

        // assert
        verify(mockSpeechHelper, times(1)).shutdown()
    }
}


package net.derekwilson.wrist_spin.unittests.ui.main

import net.derekwilson.wrist_spin.R
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class ActionSelectedTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun action_settings() {
        // arrange
        viewModel.onCreate(mockLifecycleOwner)

        // act
        viewModel.actionSelected(R.id.action_settings)

        // assert
        verify(mockNavigateToSettingsObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockNavigateToSettingsObserver)
    }
}



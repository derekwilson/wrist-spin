package net.derekwilson.wrist_spin.unittests.ui.scoreticker.ScoreTicker

import net.derekwilson.wrist_spin.helpers.MockitoHelper.anyObject
import net.derekwilson.wrist_spin.helpers.MockitoHelper.eq
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class StartupTests : ScoreTickerSetup() {

    @Before
    fun before_each_test() {
        setupScoreTicker()
    }

    @Test
    fun start_shows_the_notification() {
        // arrange

        // act
        scoreTicker.startup()

        // assert
        verify(mockShowServiceNotificationObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockShowServiceNotificationObserver)
    }

    @Test
    fun start_triggers_an_update() {
        // arrange

        // act
        scoreTicker.startup()

        // assert
        verify(mockTriggerMatchUpdatesObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockTriggerMatchUpdatesObserver)
    }

    @Test
    fun start_does_not_register_the_lock_screen_when_not_selected() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(false)

        // act
        scoreTicker.startup()

        // assert
        verify(mockRegisterLockScreenObserver, never()).onChanged(anyObject())
    }

    @Test
    fun start_registers_the_lock_screen_when_not_selected() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(true)

        // act
        scoreTicker.startup()

        // assert
        verify(mockRegisterLockScreenObserver, times(1)).onChanged(anyObject())
        verifyNoMoreInteractions(mockRegisterLockScreenObserver)
    }

    @Test
    fun start_calls_analytics() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(true)

        // act
        scoreTicker.startup()

        // assert
        verify(mockAnalytics, times(1)).startServiceEvent(true)
        verifyNoMoreInteractions(mockAnalytics)
    }

}
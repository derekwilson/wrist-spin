package net.derekwilson.wrist_spin.helpers

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection

object MatchSelectionHelper {
    fun getSingleMatchSelection(): List<MatchSelection> {
        val matches: List<MatchSelection> = listOf(
            MatchSelection(
                "1",
                "Team1",
                "Team2"
            )
        )
        matches[0].status = "status1"
        return matches
    }

    fun getMultipleMatchSelection(): List<MatchSelection> {
        val matches: List<MatchSelection> = listOf(
            MatchSelection(
                "1",
                "Team1",
                "Team2"
            ),
            MatchSelection(
                "2",
                "Team3",
                "Team4"
            ),
            MatchSelection(
                "3",
                "Team5",
                "Team6"
            ),
            MatchSelection(
                "4",
                "Team7",
                "Team8"
            ),
        )
        return matches
    }
}
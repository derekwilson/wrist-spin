package net.derekwilson.wrist_spin.unittests.ui.scoreticker.ScoreTicker

import net.derekwilson.wrist_spin.helpers.MockitoHelper.anyObject
import net.derekwilson.wrist_spin.helpers.MockitoHelper.eq
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ShutdownTests : ScoreTickerSetup() {

    @Before
    fun before_each_test() {
        setupScoreTicker()
    }

    @Test
    fun shutdown_kills_the_alarm() {
        // arrange
        scoreTicker.startup()

        // act
        scoreTicker.shutdown()

        // assert
        verify(mockCancelAlarmObserver, times(1)).onChanged(anyObject())
        verifyNoMoreInteractions(mockCancelAlarmObserver)
    }

    @Test
    fun shutdown_unregisters_the_lock_screen() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(true)
        scoreTicker.startup()

        // act
        scoreTicker.shutdown()

        // assert
        verify(mockUnregisterLockScreenObserver, times(1)).onChanged(anyObject())
        verifyNoMoreInteractions(mockUnregisterLockScreenObserver)
    }

    @Test
    fun shutdown_does_not_unregisters_the_lock_screen_when_not_configured() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(false)
        scoreTicker.startup()

        // act
        scoreTicker.shutdown()

        // assert
        verify(mockUnregisterLockScreenObserver, never()).onChanged(anyObject())
        verifyNoMoreInteractions(mockUnregisterLockScreenObserver)
    }

    @Test
    fun shutdown_calls_analytics() {
        // arrange
        `when`(mockPreferencesProvider.getPreferenceBoolean(eq(LOCK_SCREEN_KEY), anyBoolean())).thenReturn(true)
        scoreTicker.startup()

        // act
        scoreTicker.shutdown()

        // assert
        verify(mockAnalytics, times(1)).startServiceEvent(true)
        verify(mockAnalytics, times(1)).stopServiceEvent()
        verifyNoMoreInteractions(mockAnalytics)
    }

}

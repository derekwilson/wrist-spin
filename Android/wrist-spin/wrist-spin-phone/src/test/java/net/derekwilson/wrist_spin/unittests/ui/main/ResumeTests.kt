package net.derekwilson.wrist_spin.unittests.ui.main

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ResumeTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun resume_updates_service_switch_off() {
        // arrange
        `when`(mockServiceHelper.isTickerServiceRunning()).thenReturn(false)
        viewModel.onCreate(mockLifecycleOwner)

        // act
        viewModel.onResume(mockLifecycleOwner)

        // assert
        verify(mockDisplayScoreTickerStateUiObserver, times(1)).onChanged(Pair(false, TICKER_OFF))
        verifyNoMoreInteractions(mockDisplayScoreTickerStateUiObserver)
    }

    @Test
    fun resume_updates_service_switch_on() {
        // arrange
        `when`(mockServiceHelper.isTickerServiceRunning()).thenReturn(true)
        viewModel.onCreate(mockLifecycleOwner)

        // act
        viewModel.onResume(mockLifecycleOwner)

        // assert
        verify(mockDisplayScoreTickerStateUiObserver, times(1)).onChanged(Pair(true, TICKER_ON))
        verifyNoMoreInteractions(mockDisplayScoreTickerStateUiObserver)
    }

    @Test
    fun resume_updates_active_match_title_no_matches() {
        // arrange
        viewModel.onCreate(mockLifecycleOwner)

        // act
        viewModel.onResume(mockLifecycleOwner)

        // assert
        // once for the restore and once for the adapter init
        verify(mockActiveMatchTitleObserver, times(2)).onChanged("active matches title 0")
        verifyNoMoreInteractions(mockActiveMatchTitleObserver)
    }
}



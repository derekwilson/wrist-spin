package net.derekwilson.wrist_spin.unittests.ui.main

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.helpers.LoggingHelper
import net.derekwilson.wrist_spin.helpers.MockitoHelper.eq
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.main.MainViewModel
import net.derekwilson.wrist_spin.unittests.MockingAnnotationSetup
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.IPermissionChecker
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin.utility.IServiceHelper
import net.derekwilson.wrist_spin.utility.ISpeechHelper
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.`when`

open class MainViewModelSetup : MockingAnnotationSetup() {
    protected lateinit var viewModel: MainViewModel
    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var matchStore: MatchStore

    @Mock protected lateinit var mockApplication: Application
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockloggingSink: ILoggingSink
    @Mock protected lateinit var mockSpeechHelper: ISpeechHelper
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockPermissionChecker: IPermissionChecker
    @Mock protected lateinit var mockServiceHelper: IServiceHelper
    @Mock protected lateinit var mockPreferencesProvider: IPreferencesProvider

    // Rule for help testing livedata
    @Rule @JvmField var rule = InstantTaskExecutorRule()
    @Mock protected lateinit var mockMessageObserver: Observer<String>
    @Mock protected lateinit var mockRequestPushNotificationPermissionObserver: Observer<Void?>
    @Mock protected lateinit var mockNavigateToSettingsObserver: Observer<Void?>
    @Mock protected lateinit var mockDisplayScoreTickerStateUiObserver: Observer<Pair<Boolean, String>>
    @Mock protected lateinit var mockActiveMatchTitleObserver: Observer<String>
    @Mock protected lateinit var mockActiveMatchesLoadedObserver: Observer<Void?>

    @Mock protected lateinit var mockLifecycleOwner: LifecycleOwner

    protected fun setupViewModel() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        matchStore = MatchStore(mockloggingSink)

        setupResources()

        viewModel = MainViewModel(
            mockApplication,
            mockLoggerFactory,
            mockResourceProvider,
            matchStore,
            mockSpeechHelper,
            mockCrashReporter,
            mockPermissionChecker,
            mockServiceHelper,
            mockPreferencesProvider,
        )

        viewModel.observables.displayMessage.observeForever(mockMessageObserver)
        viewModel.observables.requestPushNotificationPermission.observeForever(mockRequestPushNotificationPermissionObserver)
        viewModel.observables.navigateToSettings.observeForever(mockNavigateToSettingsObserver)
        viewModel.observables.displayScoreTickerStateUi.observeForever(mockDisplayScoreTickerStateUiObserver)
        viewModel.observables.activeMatchTitle.observeForever(mockActiveMatchTitleObserver)
        viewModel.observables.activeMatchesLoaded.observeForever(mockActiveMatchesLoadedObserver)
    }

    private fun setupResources() {
        `when`(mockResourceProvider.getString(R.string.row_title_score_ticker_on)).thenReturn(TICKER_ON)
        `when`(mockResourceProvider.getString(R.string.row_title_score_ticker_off)).thenReturn(TICKER_OFF)
        `when`(mockResourceProvider.getQuantityString(eq(R.plurals.active_matches_title), anyInt())).thenAnswer {
            "active matches title ${it.getArgument<Int>(1)}"
        }
        `when`(mockResourceProvider.getString(R.string.settings_active_matches_key)).thenReturn(ACTIVE_MATCHES_KEY)
    }

    companion object {
        const val TICKER_ON = "TICKER_ON"
        const val TICKER_OFF = "TICKER_OFF"
        const val ACTIVE_MATCH_TITLE = "ACTIVE_MATCH_TITLE %d"
        const val ACTIVE_MATCHES_KEY = "ACTIVE_MATCHES_KEY"
    }
}
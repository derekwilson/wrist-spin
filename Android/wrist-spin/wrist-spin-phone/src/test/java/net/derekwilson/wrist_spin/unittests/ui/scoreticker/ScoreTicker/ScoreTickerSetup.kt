package net.derekwilson.wrist_spin.unittests.ui.scoreticker.ScoreTicker

import android.content.BroadcastReceiver
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.helpers.LoggingHelper
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.scoreticker.ScoreTicker
import net.derekwilson.wrist_spin.unittests.MockingAnnotationSetup
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class ScoreTickerSetup : MockingAnnotationSetup() {
    protected lateinit var scoreTicker: ScoreTicker
    private lateinit var mockLoggerFactory: ILoggerFactory

    @Mock protected lateinit var mockPreferencesProvider: IPreferencesProvider
    @Mock protected lateinit var mockResourceProvider: IResourceProvider

    // Rule for help testing livedata
    @Rule @JvmField public var rule = InstantTaskExecutorRule()
    @Mock protected lateinit var mockShowServiceNotificationObserver: Observer<Void?>
    @Mock protected lateinit var mockRegisterLockScreenObserver: Observer<Pair<BroadcastReceiver?, List<String>>>
    @Mock protected lateinit var mockUnregisterLockScreenObserver: Observer<BroadcastReceiver>
    @Mock protected lateinit var mockTriggerMatchUpdatesObserver: Observer<Void?>
    @Mock protected lateinit var mockCancelAlarmObserver: Observer<Void?>
    @Mock protected lateinit var mockAnalytics: IAnalyticsEngine

    protected fun setupScoreTicker() {
        setupResources()
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()

        scoreTicker = ScoreTicker(
            mockLoggerFactory,
            mockPreferencesProvider,
            mockResourceProvider,
            mockAnalytics
        )

        scoreTicker.observables.showServiceNotification.observeForever(mockShowServiceNotificationObserver)
        scoreTicker.observables.registerLockScreen.observeForever(mockRegisterLockScreenObserver)
        scoreTicker.observables.unRegisterLockScreen.observeForever(mockUnregisterLockScreenObserver)
        scoreTicker.observables.triggerMatchUpdates.observeForever(mockTriggerMatchUpdatesObserver)
        scoreTicker.observables.cancelAlarm.observeForever(mockCancelAlarmObserver)
    }

    private fun setupResources() {
        `when`(mockResourceProvider.getString(R.string.settings_lock_screen_key)).thenReturn(LOCK_SCREEN_KEY)
    }

    companion object {
        const val LOCK_SCREEN_KEY = "LOCK_SCREEN_KEY"
    }
}
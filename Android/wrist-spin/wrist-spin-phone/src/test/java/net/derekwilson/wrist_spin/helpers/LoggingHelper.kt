package net.derekwilson.wrist_spin.helpers

import net.derekwilson.wrist_spin.logging.ILoggerFactory
import org.mockito.Mockito
import org.slf4j.Logger

object LoggingHelper {
    fun setupMockLoggingFactory(): ILoggerFactory {
        var mockLoggerFactory = Mockito.mock(ILoggerFactory::class.java)
        val logger = Mockito.mock(Logger::class.java)
        Mockito.`when`(mockLoggerFactory.logger).thenReturn(logger)
        return mockLoggerFactory
    }
}
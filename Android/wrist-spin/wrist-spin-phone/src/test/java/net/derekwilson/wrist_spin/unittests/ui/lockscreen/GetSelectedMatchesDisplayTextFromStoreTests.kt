package net.derekwilson.wrist_spin.unittests.ui.lockscreen

import net.derekwilson.wrist_spin.helpers.MatchStoreHelper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class GetSelectedMatchesDisplayTextFromStoreTests : MatchStateDisplayTextProviderSetup() {

    @Before
    fun before_each_test() {
        setupMatchStateTextProvider()
    }

    @Test
    fun get_with_no_matches_returns_empty_message() {
        // arrange
        MatchStoreHelper.setupMatchStore(matchStore, listOf())

        // act
        val results = matchStateDisplayTextProvider.getSelectedMatchesDisplayTextFromStore()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).first, `is`("NO_DATA"))
        assertThat(results.get(0).second, `is`("APP_NAME"))

        verify(mockAnalytics, times(1)).showLockScreenEvent(0)
        Mockito.verifyNoMoreInteractions(mockAnalytics)
    }

    @Test
    fun get_with_one_match() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)

        // act
        val results = matchStateDisplayTextProvider.getSelectedMatchesDisplayTextFromStore()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(1))

        assertThat(results.get(0).first, `is`("1/0"))
        assertThat(results.get(0).second, `is`("batting_1\n10.0\nstriker_1 s_stats_1\nnonstriker_1 n_stats_1\n"))

        verify(mockAnalytics, times(1)).showLockScreenEvent(1)
        Mockito.verifyNoMoreInteractions(mockAnalytics)
    }

    @Test
    fun get_with_two_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1, 0, 10, 0)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 10, 1, 20, 0)

        // act
        val results = matchStateDisplayTextProvider.getSelectedMatchesDisplayTextFromStore()

        // assert
        assertNotNull(results)
        assertThat(results.size, `is`(2))

        assertThat(results.get(0).first, `is`("1/0"))
        assertThat(results.get(0).second, `is`("batting_1\n10.0\nstriker_1 s_stats_1\nnonstriker_1 n_stats_1\n"))

        assertThat(results.get(1).first, `is`("10/1"))
        assertThat(results.get(1).second, `is`("batting_2\n20.0\nstriker_2 s_stats_2\nnonstriker_2 n_stats_2\n"))

        verify(mockAnalytics, times(1)).showLockScreenEvent(2)
        Mockito.verifyNoMoreInteractions(mockAnalytics)
    }
}
package net.derekwilson.wrist_spin.unittests.ui.main

import net.derekwilson.wrist_spin.helpers.MatchStoreHelper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class ActiveMatchListTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }


    @Test
    fun load_updates_active_match_title_no_matches() {
        // arrange
        MatchStoreHelper.setupMatchStore(matchStore, listOf())
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        reset(mockActiveMatchTitleObserver)

        // act
        viewModel.loadActiveMatches()

        // assert
        verify(mockActiveMatchTitleObserver, times(1)).onChanged("active matches title 0")
        verifyNoMoreInteractions(mockActiveMatchTitleObserver)
    }

    @Test
    fun load_updates_active_match_title_1_match() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        reset(mockActiveMatchTitleObserver)

        // act
        viewModel.loadActiveMatches()

        // assert
        verify(mockActiveMatchTitleObserver, times(1)).onChanged("active matches title 1")
        verifyNoMoreInteractions(mockActiveMatchTitleObserver)
    }

    @Test
    fun load_updates_active_match_title_2_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        reset(mockActiveMatchTitleObserver)

        // act
        viewModel.loadActiveMatches()

        // assert
        verify(mockActiveMatchTitleObserver, times(1)).onChanged("active matches title 2")
        verifyNoMoreInteractions(mockActiveMatchTitleObserver)
    }

    @Test
    fun load_renders_1_match() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)

        // act
        val id = viewModel.getMatchId(0)
        val label = viewModel.getMatchLabel(0)

        // assert
        assertThat(id, `is`(1))
        assertThat(label, `is`("team1_1\nv\nteam2_1"))
    }

    @Test
    fun load_renders_2_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)

        // act
        val idPos0 = viewModel.getMatchId(0)
        val labelPos0 = viewModel.getMatchLabel(0)
        val idPos1 = viewModel.getMatchId(1)
        val labelPos1 = viewModel.getMatchLabel(1)

        // assert
        assertThat(idPos0, `is`(1))
        assertThat(labelPos0, `is`("team1_1\nv\nteam2_1"))
        assertThat(idPos1, `is`(2))
        assertThat(labelPos1, `is`("team1_2\nv\nteam2_2"))
    }

    @Test
    fun load_renders_3_matches() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)

        // act
        val idPos0 = viewModel.getMatchId(0)
        val labelPos0 = viewModel.getMatchLabel(0)
        val idPos1 = viewModel.getMatchId(1)
        val labelPos1 = viewModel.getMatchLabel(1)
        val idPos2 = viewModel.getMatchId(2)
        val labelPos2 = viewModel.getMatchLabel(2)

        // assert
        assertThat(idPos0, `is`(1))
        assertThat(labelPos0, `is`("team1_1\nv\nteam2_1"))
        assertThat(idPos1, `is`(2))
        assertThat(labelPos1, `is`("team1_2\nv\nteam2_2"))
        assertThat(idPos2, `is`(3))
        assertThat(labelPos2, `is`("team1_3\nv\nteam2_3"))
    }

    @Test
    fun move_match_updates_render() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)

        // act
        viewModel.matchMoved(0,1)
        val idPos0 = viewModel.getMatchId(0)
        val labelPos0 = viewModel.getMatchLabel(0)
        val idPos1 = viewModel.getMatchId(1)
        val labelPos1 = viewModel.getMatchLabel(1)
        val idPos2 = viewModel.getMatchId(2)
        val labelPos2 = viewModel.getMatchLabel(2)

        // assert
        assertThat(idPos0, `is`(2))
        assertThat(labelPos0, `is`("team1_2\nv\nteam2_2"))
        assertThat(idPos1, `is`(1))
        assertThat(labelPos1, `is`("team1_1\nv\nteam2_1"))
        assertThat(idPos2, `is`(3))
        assertThat(labelPos2, `is`("team1_3\nv\nteam2_3"))
    }

    @Test
    fun move_match_updates_preferences() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)

        // act
        viewModel.matchMoved(0,1)

        // assert
        verify(mockPreferencesProvider, times(1)).setPreferenceString(ACTIVE_MATCHES_KEY,"2:team1_2:team2_2:1:team1_1:team2_1:3:team1_3:team2_3:")
    }

    @Test
    fun move_match_updates_store() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "3", 21,22, 23, 24)

        // act
        viewModel.matchMoved(0,1)

        // assert
        assertThat(matchStore.allIdsInStore.toString(), `is`("[2, 1, 3]"))
        assertThat(matchStore.getMatchSelection("1")!!.id, `is`("1"))
        assertThat(matchStore.getMatchState("1")!!.id, `is`("1"))
        assertThat(matchStore.getMatchSelection("2")!!.id, `is`("2"))
        assertThat(matchStore.getMatchState("2")!!.id, `is`("2"))
        assertThat(matchStore.getMatchSelection("3")!!.id, `is`("3"))
        assertThat(matchStore.getMatchState("3")!!.id, `is`("3"))
    }

    @Test
    fun delete_match_updates_render() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "3", 21,22, 23, 24)

        // act
        viewModel.matchOptionSelected(2, 1)
        val idPos0 = viewModel.getMatchId(0)
        val labelPos0 = viewModel.getMatchLabel(0)
        val idPos1 = viewModel.getMatchId(1)
        val labelPos1 = viewModel.getMatchLabel(1)

        // assert
        assertThat(idPos0, `is`(1))
        assertThat(labelPos0, `is`("team1_1\nv\nteam2_1"))
        assertThat(idPos1, `is`(3))
        assertThat(labelPos1, `is`("team1_3\nv\nteam2_3"))
    }

    @Test
    fun delete_match_updates_preferences() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "3", 21,22, 23, 24)

        // act
        viewModel.matchOptionSelected(2, 1)

        // assert
        verify(mockPreferencesProvider, times(1)).setPreferenceString(ACTIVE_MATCHES_KEY,"1:team1_1:team2_1:3:team1_3:team2_3:")
    }

    @Test
    fun delete_match_updates_store() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.addMatchToStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.addMatchToStore(matchStore, "3", 21,22, 23, 24)
        val storeAsString = matchStore.storeMatchesAsString
        `when`(mockPreferencesProvider.getPreferenceString(ACTIVE_MATCHES_KEY, "")).thenReturn(storeAsString)
        viewModel.onCreate(mockLifecycleOwner)
        viewModel.onResume(mockLifecycleOwner)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "1", 1,2, 3, 4)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "2", 11,12, 13, 14)
        MatchStoreHelper.updateMatchStateInStore(matchStore, "3", 21,22, 23, 24)

        // act
        viewModel.matchOptionSelected(2, 1)

        // assert
        assertThat(matchStore.allIdsInStore.toString(), `is`("[1, 3]"))
        assertThat(matchStore.getMatchSelection("1")!!.id, `is`("1"))
        assertThat(matchStore.getMatchState("1")!!.id, `is`("1"))
        assertThat(matchStore.getMatchSelection("3")!!.id, `is`("3"))
        assertThat(matchStore.getMatchState("3")!!.id, `is`("3"))
    }
}




package net.derekwilson.wrist_spin.unittests.ui.lockscreen

import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.helpers.LoggingHelper
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.lockscreen.MatchStateDisplayTextProvider
import net.derekwilson.wrist_spin.unittests.MockingAnnotationSetup
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class MatchStateDisplayTextProviderSetup : MockingAnnotationSetup() {
    protected lateinit var matchStateDisplayTextProvider: MatchStateDisplayTextProvider
    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var matchStore: MatchStore

    @Mock protected lateinit var mockloggingSink: ILoggingSink
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockAnalytics: IAnalyticsEngine

    protected fun setupMatchStateTextProvider() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        matchStore = MatchStore(mockloggingSink)

        setupResources()

        matchStateDisplayTextProvider = MatchStateDisplayTextProvider(
            mockLoggerFactory,
            mockResourceProvider,
            matchStore,
            mockAnalytics
        )
    }

    private fun setupResources() {
        `when`(mockResourceProvider.getString(R.string.match_display_no_data)).thenReturn(NO_DATA)
        `when`(mockResourceProvider.getString(R.string.app_name)).thenReturn(APP_NAME)
    }

    companion object {
        const val NO_DATA = "NO_DATA"
        const val APP_NAME = "APP_NAME"
    }
}

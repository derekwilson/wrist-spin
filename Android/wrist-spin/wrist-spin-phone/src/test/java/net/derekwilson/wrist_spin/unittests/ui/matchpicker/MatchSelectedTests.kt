package net.derekwilson.wrist_spin.unittests.ui.matchpicker

import net.derekwilson.wrist_spin.helpers.MatchSelectionHelper
import net.derekwilson.wrist_spin.helpers.MatchStoreHelper
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class MatchSelectedTests : MatchPickerViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun new_selection() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore,"1", 100, 0, 15, 0)
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getMultipleMatchSelection())
        viewModel.loadMatches()

        // act
        viewModel.matchSelected(2, 1)

        // assert
        verify(mockMessageObserver, never()).onChanged(anyString())
        verify(mockExitObserver, times(1)).onChanged("2")
        verifyNoMoreInteractions(mockExitObserver)
        verify(mockAnalytics, times(1)).selectMatchEvent("2", "Team3\nv\nTeam4")
        verifyNoMoreInteractions(mockAnalytics)
    }

    @Test
    fun duplicate_selection() {
        // arrange
        MatchStoreHelper.setupStoreWithOneMatch(matchStore,"2", 100, 0, 15, 0)
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getMultipleMatchSelection())
        viewModel.loadMatches()

        // act
        viewModel.matchSelected(2, 1)

        // assert
        verify(mockExitObserver, never()).onChanged(anyString())
        verify(mockMessageObserver, times(1)).onChanged("DUPLICATE_MATCH")
        verifyNoMoreInteractions(mockMessageObserver)
        verify(mockAnalytics, never()).selectMatchEvent(anyString(), anyString())
        verifyNoMoreInteractions(mockAnalytics)
    }

}


package net.derekwilson.wrist_spin.unittests

import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.slf4j.Logger

class EnvironmentTests : MockingAnnotationSetup() {
    @Mock
    protected lateinit var mockLogger: Logger

    @Test
    fun junit_can_assert() {
        Assert.assertEquals(4, 2 + 2)
    }

    @Test
    fun mockito_can_verify() {
        // arrange

        // act
        mockLogger.debug("TEST")

        // assert
        verify(mockLogger).debug("TEST")
    }
}


package net.derekwilson.wrist_spin.unittests.ui.matchpicker

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.helpers.LoggingHelper
import net.derekwilson.wrist_spin.helpers.UnitTestCoroutineDispatcherProvider
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.matchpicker.MatchPickerViewModel
import net.derekwilson.wrist_spin.unittests.MockingAnnotationSetup
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.ICrashReporter
import net.derekwilson.wrist_spin.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSource
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore
import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class MatchPickerViewModelSetup : MockingAnnotationSetup() {
    protected lateinit var viewModel: MatchPickerViewModel
    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var matchStore: MatchStore

    @Mock protected lateinit var mockApplication: Application
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockloggingSink: ILoggingSink
    @Mock protected lateinit var mockCricketDataSourceProvider: ICricketDataSourceProvider
    @Mock protected lateinit var mockCricketDataSource: ICricketDataSource
    @Mock protected lateinit var mockCrashReporter: ICrashReporter
    @Mock protected lateinit var mockAnalytics: IAnalyticsEngine

    // Rule for help testing livedata
    @Rule @JvmField var rule = InstantTaskExecutorRule()
    @Mock protected lateinit var mockMessageObserver: Observer<String>
    @Mock protected lateinit var mockStartProgressObserver: Observer<Void?>
    @Mock protected lateinit var mockCompleteProgressObserver: Observer<Void?>
    @Mock protected lateinit var mockMatchesLoadedObserver: Observer<Void?>
    @Mock protected lateinit var mockExitObserver: Observer<String>

    protected fun setupViewModel() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        matchStore = MatchStore(mockloggingSink)
        `when`(mockCricketDataSourceProvider.getDataSource()).thenReturn(mockCricketDataSource)

        setupResources()

        viewModel = MatchPickerViewModel(
            mockApplication,
            mockLoggerFactory,
            mockResourceProvider,
            matchStore,
            mockCricketDataSourceProvider,
            UnitTestCoroutineDispatcherProvider(),
            mockCrashReporter,
            mockAnalytics
        )

        viewModel.observables.displayMessage.observeForever(mockMessageObserver)
        viewModel.observables.startProgress.observeForever(mockStartProgressObserver)
        viewModel.observables.completeProgress.observeForever(mockCompleteProgressObserver)
        viewModel.observables.matchesLoaded.observeForever(mockMatchesLoadedObserver)
        viewModel.observables.exit.observeForever(mockExitObserver)
    }

    private fun setupResources() {
        `when`(mockResourceProvider.getString(R.string.duplicate_match)).thenReturn(DUPLICATE_MATCH)
    }

    companion object {
        const val DUPLICATE_MATCH = "DUPLICATE_MATCH"
    }
}
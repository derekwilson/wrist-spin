package net.derekwilson.wrist_spin.helpers

import net.derekwilson.wrist_spin_cricket_logic.model.MatchState
import net.derekwilson.wrist_spin_cricket_logic.model.Overs
import net.derekwilson.wrist_spin_cricket_logic.model.Score

object MatchStateHelper {
    fun setupMatchState(id: String, runs: Int, wickets: Int, overs: Int, balls: Int): MatchState {
        val state = MatchState(
            Score(
                runs,
                wickets
            ), Overs(overs, balls)
        )
        state.id = id
        state.currentState = MatchState.CurrentState.Current
        state.team1.abbreviation = "team1_${id}"
        state.team2.abbreviation = "team2_${id}"
        state.strikerName = "striker_${id}"
        state.strikerScore = "s_score_${id}"
        state.strikerStats = "s_stats_${id}"
        state.nonStrikerName = "nonstriker_${id}"
        state.nonStrikerScore = "n_score_${id}"
        state.nonStrikerStats = "n_stats_${id}"
        state.bowlerName = "bowler_${id}"
        state.bowlerStats = "b_stats_${id}"
        state.battingTeamAbbreviation = "batting_${id}"
        return state
    }
}
package net.derekwilson.wrist_spin.unittests.ui.matchpicker

import net.derekwilson.wrist_spin.helpers.MatchSelectionHelper
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`

class LoadMatchesTests : MatchPickerViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun load_empty_updates_progress() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(listOf())

        // act
        viewModel.loadMatches()

        // assert
        assertThat(viewModel.itemCount, `is`(0))
        verify(mockStartProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockStartProgressObserver)
        verify(mockCompleteProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockCompleteProgressObserver)
        verify(mockMatchesLoadedObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockMatchesLoadedObserver)
    }

    @Test
    fun load_not_empty_updates_progress() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getSingleMatchSelection())

        // act
        viewModel.loadMatches()

        // assert
        assertThat(viewModel.itemCount, `is`(1))
        verify(mockStartProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockStartProgressObserver)
        verify(mockCompleteProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockCompleteProgressObserver)
        verify(mockMatchesLoadedObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockMatchesLoadedObserver)
    }

    @Test
    fun load_multiple_updates_progress() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenReturn(MatchSelectionHelper.getMultipleMatchSelection())

        // act
        viewModel.loadMatches()

        // assert
        assertThat(viewModel.itemCount, `is`(4))
        verify(mockStartProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockStartProgressObserver)
        verify(mockCompleteProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockCompleteProgressObserver)
        verify(mockMatchesLoadedObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockMatchesLoadedObserver)
    }

    @Test
    fun load_with_error_updates_progress() {
        // arrange
        `when`(mockCricketDataSource.matchSelection).thenAnswer {
            throw Exception("TEST EXCEPTION")
        }

        // act
        viewModel.loadMatches()

        // assert
        assertThat(viewModel.itemCount, `is`(0))
        verify(mockStartProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockStartProgressObserver)
        verify(mockCompleteProgressObserver, times(1)).onChanged(null)
        verifyNoMoreInteractions(mockCompleteProgressObserver)
        verify(mockMatchesLoadedObserver, never()).onChanged(null)
    }
}
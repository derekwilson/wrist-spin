package net.derekwilson.wrist_spin.unittests

import org.junit.After
import org.junit.Before
import org.mockito.MockitoAnnotations

// we only need to derive from this class if we want to use annotations such as @Mock
open class MockingAnnotationSetup {
    private var closeable: AutoCloseable? = null

    @Before
    fun openMocks() {
        closeable = MockitoAnnotations.openMocks(this)
    }

    @After
    @Throws(Exception::class)
    fun releaseMocks() {
        closeable!!.close()
    }

}


package net.derekwilson.wrist_spin.unittests.ui.main

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class InitTests : MainViewModelSetup() {
    @Before
    fun before_each_test() {
        setupViewModel()
    }

    @After
    fun after_each_test() {
    }

    @Test
    fun init_initialises_speech() {
        // arrange

        // act

        // assert
        verify(mockSpeechHelper, times(1)).initialise()
    }
}
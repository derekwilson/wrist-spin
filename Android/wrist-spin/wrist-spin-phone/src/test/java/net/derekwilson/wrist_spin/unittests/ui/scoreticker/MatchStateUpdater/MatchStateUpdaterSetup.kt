package net.derekwilson.wrist_spin.unittests.ui.scoreticker.MatchStateUpdater

import net.derekwilson.wrist_spin.R
import net.derekwilson.wrist_spin.helpers.LoggingHelper
import net.derekwilson.wrist_spin.helpers.MatchStateHelper
import net.derekwilson.wrist_spin.logging.ILoggerFactory
import net.derekwilson.wrist_spin.ui.scoreticker.MatchStateUpdater
import net.derekwilson.wrist_spin.unittests.MockingAnnotationSetup
import net.derekwilson.wrist_spin.utility.IAnalyticsEngine
import net.derekwilson.wrist_spin.utility.IAndroidEnvironmentInformationProvider
import net.derekwilson.wrist_spin.utility.ICricketDataSourceProvider
import net.derekwilson.wrist_spin.utility.IPreferencesProvider
import net.derekwilson.wrist_spin.utility.IResourceProvider
import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSource
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink
import net.derekwilson.wrist_spin_cricket_logic.matchstore.MatchStore
import org.mockito.Mock
import org.mockito.Mockito.`when`

open class MatchStateUpdaterSetup : MockingAnnotationSetup() {
    protected lateinit var matchStateUpdater: MatchStateUpdater
    private lateinit var mockLoggerFactory: ILoggerFactory
    protected lateinit var matchStore: MatchStore

    @Mock protected lateinit var mockloggingSink: ILoggingSink
    @Mock protected lateinit var mockPreferencesProvider: IPreferencesProvider
    @Mock protected lateinit var mockResourceProvider: IResourceProvider
    @Mock protected lateinit var mockCricketDataSourceProvider: ICricketDataSourceProvider
    @Mock protected lateinit var mockCricketDataSource: ICricketDataSource
    @Mock protected lateinit var mockAndroidEnvironmentInformationProvider: IAndroidEnvironmentInformationProvider
    @Mock protected lateinit var mockAnalytics: IAnalyticsEngine

    protected fun setupMatchStateUpdater() {
        mockLoggerFactory = LoggingHelper.setupMockLoggingFactory()
        matchStore = MatchStore(mockloggingSink)

        setupResources()
        `when`(mockCricketDataSourceProvider.getDataSource()).thenReturn(mockCricketDataSource)
        `when`(mockCricketDataSource.name).thenReturn(DATASOURCE_NAME)

        matchStateUpdater = MatchStateUpdater(
            mockLoggerFactory,
            mockPreferencesProvider,
            mockResourceProvider,
            matchStore,
            mockCricketDataSourceProvider,
            mockAndroidEnvironmentInformationProvider,
            mockAnalytics
        )
    }

    protected fun setupNewStateFromDataSource(id: String, runs: Int, wickets: Int, overs: Int, balls: Int) {
        val state = MatchStateHelper.setupMatchState(id, runs, wickets, overs, balls)
        `when`(mockCricketDataSource.getMatchState(id)).thenReturn(state)
    }

    protected fun setupNewStateFromDataSource(id: String, runs: Int, wickets: Int, overs: Int, balls: Int, strikerScore: String, nonstrikerScore: String) {
        val state = MatchStateHelper.setupMatchState(id, runs, wickets, overs, balls)
        state.strikerScore = strikerScore
        state.nonStrikerScore = nonstrikerScore
        `when`(mockCricketDataSource.getMatchState(id)).thenReturn(state)
    }

    private fun setupResources() {
        `when`(mockResourceProvider.getString(R.string.settings_alert_suppress_ticker_update_key)).thenReturn(ALERT_SUPPRESS_TICKER_UPDATE_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_alert_wicket_key)).thenReturn(ALERT_WICKET_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_alert_team50_key)).thenReturn(ALERT_TEAM50_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_alert_team100_key)).thenReturn(ALERT_TEAM100_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_alert_individual50_key)).thenReturn(ALERT_INDIVIDUAL50_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_alert_individual100_key)).thenReturn(ALERT_INDIVIDUAL100_KEY)

        `when`(mockResourceProvider.getString(R.string.settings_speak_override_key)).thenReturn(SPEAK_OVERRIDE_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_speak_wicket_key)).thenReturn(SPEAK_WICKET_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_speak_team50_key)).thenReturn(SPEAK_TEAM50_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_speak_team100_key)).thenReturn(SPEAK_TEAM100_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_speak_individual50_key)).thenReturn(SPEAK_INDIVIDUAL50_KEY)
        `when`(mockResourceProvider.getString(R.string.settings_speak_individual100_key)).thenReturn(SPEAK_INDIVIDUAL100_KEY)
    }

    companion object {
        const val DATASOURCE_NAME = "MOCK_DATASOURCE"

        const val ALERT_SUPPRESS_TICKER_UPDATE_KEY = "ALERT_SUPPRESS_TICKER_UPDATE_KEY"
        const val ALERT_WICKET_KEY = "ALERT_WICKET_KEY"
        const val ALERT_TEAM50_KEY = "ALERT_TEAM50_KEY"
        const val ALERT_TEAM100_KEY = "ALERT_TEAM100_KEY"
        const val ALERT_INDIVIDUAL50_KEY = "ALERT_INDIVIDUAL50_KEY"
        const val ALERT_INDIVIDUAL100_KEY = "ALERT_INDIVIDUAL100_KEY"

        const val SPEAK_OVERRIDE_KEY = "SPEAK_OVERRIDE_KEY"
        const val SPEAK_WICKET_KEY = "SPEAK_WICKET_KEY"
        const val SPEAK_TEAM50_KEY = "SPEAK_TEAM50_KEY"
        const val SPEAK_TEAM100_KEY = "SPEAK_TEAM100_KEY"
        const val SPEAK_INDIVIDUAL50_KEY = "SPEAK_INDIVIDUAL50_KEY"
        const val SPEAK_INDIVIDUAL100_KEY = "SPEAK_INDIVIDUAL100_KEY"
    }
}
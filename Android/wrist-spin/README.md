# wrist-spin #

This is the wrist-spin Android production app.

### wrist-spin-phone

The app for android phones, using kotlin to deliver scores on notifications, lock screens and speech

### wrist-spin-cricket-logic

The logic required for decoding match selections and states from different data sources. This is a pure Java module for cross platform use.


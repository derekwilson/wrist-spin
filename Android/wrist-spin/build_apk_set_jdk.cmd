del .\wrist-spin-phone\build\outputs\apk\release\wrist-spin-phone-*.apk
echo %JAVA_HOME%
rem  override the machine specified JAVA_HOME as we need to use the embedded JDK
rem  this will only work if the embedded JDK is 17+
call gradlew clean testDebugUnitTest assembleRelease "-Dorg.gradle.java.home=C:\Program Files\Android\Android Studio\jbr"
copy .\wrist-spin-phone\build\outputs\apk\release\wrist-spin-phone-*.apk ..\Support\Releases
pause

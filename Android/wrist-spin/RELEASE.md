# PassTheParcel Release Process #

This is the process for building, testing and deploying wrist-spin to the app store.

## Prerequisite

1. **LocalOnly folder**. This folder contains the files needed to sign a release APK.
1. **A build machine**. Either a CI build server or your local developer machine can be used.

### What is the folder "LocalOnly"?

This folder contains the files needed to sign a release APK. The folder is present on developer machines as well as any build servers however it is not checked into GIT, hence the name. This folder is not needed to use Android Studio or Gradle to produce debug build of the app, it is only used with the release build type.

#### What files need to be in the folder?

The folder needs to contain.

1. wrist-spin.keystore - this is the upload signing key
1. keystore.proerties - this contains the gradle build configurations for release signing

#### What happens if I dont have "LocalOnly"?

You will be able to build the debug variant as well as run all the tests, the only thing you will not be able to do is build the release APK, if you try you will see this error

```
FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':app:validateSigningRelease'.
> Keystore file not set for signing config release
```

### Command line tools

If you want to build and work with the code form the command line you will need to add the following locations to your path

Windows

1. Android SDK =  `%localappdata%\Android\sdk\platform-tools`
1. Java JDK (Embedded) = `"%ProgramFiles%\Android\Android Studio\jre\bin"`
or
1. Java JDK (Stand Alone) = `"%ProgramFiles%\Java\jdk1.8.0_92\bin"`

Mac

1. Android SDK = `~/Library/Android/sdk/platform-tools`
1. Java JDK (Embedded) = `/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home`
or
1. Java JDK (Stand Alone) = `/Library/Java/JavaVirtualMachines/jdk1.8.0_172.jdk/Contents/Home`

### Building from the command line

Note that if you use `gradlew assembleDebug` or `build_apk.cmd` then it will use the environment variable `JAVA_HOME` to find the JDK. If that is set incorrectly you will see an error like this

```
FAILURE: Build failed with an exception.

* Where:
Build file 'D:\Data\Code\PassTheParcel\PassTheParcelAndroid\app\build.gradle' line: 2

* What went wrong:
A problem occurred evaluating project ':app'.
> Failed to apply plugin 'com.android.internal.application'.
   > Android Gradle plugin requires Java 17 to run. You are currently using Java 11.
      Your current JDK is located in C:\Program Files\Microsoft\jdk-11.0.14.9-hotspot
      You can try some of the following options:
       - changing the IDE settings.
       - changing the JAVA_HOME environment variable.
       - changing `org.gradle.java.home` in `gradle.properties`.
```

As you can see this error means that the configured JDK is incompatible with AGP.

You may want or need to override that variable, if you do then you can set it via the command line `gradlew assembleRelease "-Dorg.gradle.java.home=C:\Program Files\Android\Android Studio\jbr"`. The `build_apk_set_jdk.cmd` and `build_aab_set_jdk.cmd` scripts are examples of how to set the JDK used to be the one embedded in Android Studio JDK. This will only work if you have installed Android Studio in the default location and it contains a JDK that is compatibly with AGP.


### Installing the release

The build script will copy the APK or AAB to `Support\Releases` then CD to that folder and then install the APK like this `adb install -r wrist-spin-phone-1.1.0-2-e690b1f-release.apk`

### Running the unit tests

```gradlew testDebugUnitTest```

The test results HTML is here
app\build\reports\tests\debug\index.html

The test XML is here
app\build\test-results\debug\*.xml


## Deployment Process

The process to deploy a new app is as follows

1. Ensure all code to be deployed is in the `master` branch
1. Create a new branch called `release/1.0.1` from `master` locally (where 1.0.1 is the version name we are releasing)
1. Edit the file `app/build.gradle`. In the section `android -> defaultConfig` we need to edit the `versionName` and `versionCode` values
1. Add the release notes for the store listing to `Support/Releases/README.md`
1. Ensure that all files have been committed before building the release APK.
1. Build the release APK and AAB, essentially this means running `gradlew assembleRelease`, there are `build_aab.cmd` and `build_apk.cmd` scripts to do this and copy the APK to the `Support/Releases` folder
1. Install the APK on a test device using this command `adb install -r wrist-spin-phone-1.0.0-1-3f760fe-release.apk`, the APK name will reflect the version and the commit.
1. Run the smoke tests on the device. If the test fails check the existing production app if the problem already exists then log the issue and continue, if it is a new problem then you will need to fix it or abandon the release.
1. Copy the APK to a `vX.Y.Z` folder to match the `versionName`
1. Upload the AAB to the [play store console][play-console-url] 
1. Upload the APK to the [amazon app store console][amazon-console-url] - replace the APK in the upcoming version to keep the same device support
1. Refresh the release notes
1. Release to Internal Test, check the correct app is delivered to a handset (goto Settings page and look at the version)
1. Tag the commit with the version number, for example `1.0.1(2)`, which is `versionName(versionCode)`
1. Merge into `master` locally and push to the repo, ensure the tag is pushed by using `git push origin "1.0.1(2)"`
1. Promote to Production

## Smoke Tests

Check the following, estimated execution time should be less than 5 minutes

1. Goto Settings, does the Version match this build?
1. Goto Open-source Licenses
1. Goto Privacy statement
1. Switch on all the alerts / speech
1. Switch on the lock screen
1. Set the lock screen permission - does the status text change?
1. Adjust the refresh rate to be 1 - does the status text change?
1. Add a match - does the list appear? is it added in the active match list?
1. Add another match - is it added?
1. Try to add the same match again - is an error displayed?
1. Turn the service on - are the notifications displayed?
1. Exit the app and run another app - eg.contacts
1. Turn the device off and on - is the custom lock screen displayed?
1. Unlock the device - is the app you ran eg. contacts displayed?
1. Tap on a notification - do you go to wrist-spin?
1. Reorder the list
1. Turn the device off and on - new order displayed on the lock screen?
1. Turn the device off and then unlock using finger print, you should skip the lock screen
1. If possible check chimes and speech
1. Delete a match, is it removed?
1. Stop the service, clear the notifications - they should not reappear
1. Turn the device off and on - the custom lock screen should not be displayed
1. Kill and restart the app, are the matches still in the list

[play-console-url]:		https://play.google.com/apps/publish
[amazon-console-url]:	https://developer.amazon.com/apps-and-games/console/apps/list.html



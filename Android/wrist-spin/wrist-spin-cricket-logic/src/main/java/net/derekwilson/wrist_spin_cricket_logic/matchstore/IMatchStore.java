package net.derekwilson.wrist_spin_cricket_logic.matchstore;

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IMatchStore {
    /**
     * @return true if there are no matches in the store
     */
    boolean isEmpty();

    /**
     * remove all matches from the store
     */
    void reset();

    boolean addMatch(MatchSelectionAndState selectionAndState);

    boolean addMatch(MatchSelection selection);

    void addMatches(List<MatchSelection> matches);

    void replaceAllMatches(@NotNull List<MatchSelectionAndState> matches);

    void removeMatch(String id);

    List<String> getAllIdsInStore();

    @Nullable MatchSelection getMatchSelection(String id);

    @Nullable MatchState getMatchState(String id);

    /**
     * @param id the ID to update, because the state might be null
     * @param state the state of the game, null if we cannot get the state
     * this method will also set the alarm state based on the change in state
     */
    void updateMatchState(String id, @Nullable MatchState state);

    /**
     * @return the contents of the store as a string (only the selections not the state)
     * this is part of the store serialisation
     */
    String getStoreMatchesAsString();

    /**
     * @param str string to use to inflate the store, usually obtained from getStoreMatchesAsString()
     * @return number of items now in the store
     * this is part of the store serialisation
     */
    int replaceStoreMatchesFromString(String str);
}


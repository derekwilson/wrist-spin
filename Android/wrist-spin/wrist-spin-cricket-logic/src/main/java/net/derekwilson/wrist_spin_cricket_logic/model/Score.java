package net.derekwilson.wrist_spin_cricket_logic.model;

public class Score {
    private int runs;
    private int wickets;
    private boolean declared;

    public Score(int runs, int wickets) {
        this.runs = runs;
        this.wickets = wickets;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public String getDisplay() {
        StringBuilder str = new StringBuilder();
        str.append(runs);
        if (wickets < 10) {
            str.append("/");
            str.append(wickets);
        }
        if (declared) {
            str.append("D");
        }
        return str.toString();
    }

    public boolean isDeclared() {
        return declared;
    }

    public void setDeclared(boolean declared) {
        this.declared = declared;
    }
}



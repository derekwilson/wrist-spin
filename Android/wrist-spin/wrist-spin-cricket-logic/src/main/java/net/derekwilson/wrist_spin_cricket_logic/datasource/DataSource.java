package net.derekwilson.wrist_spin_cricket_logic.datasource;
public enum DataSource {
    /**
     * cricinfo with RSS feed for match selection
     */
    Cricinfo,
    /**
     * crciinfo with HTML page for match selection
     */
    Cricinfo2,
    /**
     * canned in data for testing
     */
    Test,
}




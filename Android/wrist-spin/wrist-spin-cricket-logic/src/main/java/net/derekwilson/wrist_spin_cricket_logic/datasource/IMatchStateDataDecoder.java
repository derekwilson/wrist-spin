package net.derekwilson.wrist_spin_cricket_logic.datasource;

import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

public interface IMatchStateDataDecoder {
    MatchState decodeMatchState(String id, String data);
}

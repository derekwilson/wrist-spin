package net.derekwilson.wrist_spin_cricket_logic.matchstore;

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class MatchStore implements IMatchStore {
    private static final int NO_INDEX = -1;
    private static final String SEPARATOR = ":";

    private final ILoggingSink loggingSink;
    private final ArrayList<MatchSelectionAndState> matchStore = new ArrayList<>();

    public MatchStore(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    public static @NotNull IMatchStore createStore(ILoggingSink loggingSink) {
        // we do it like this so we can add any module private dependencies here
        return new MatchStore(loggingSink);
    }

    @Override
    public synchronized boolean isEmpty() {
        return matchStore.isEmpty();
    }

    @Override
    public synchronized void reset() {
        matchStore.clear();
    }

    @Override
    public boolean addMatch(MatchSelectionAndState selectionAndState) {
        if (selectionAndState == null || selectionAndState.getSelection() == null) {
            return false;
        }
        int index = findIdInStore(selectionAndState.getSelection().getId());
        if (index != NO_INDEX) {
            loggingSink.debug(String.format("MatchStore.addMatch %s, duplicate match suppressed", selectionAndState.getSelection().getId()));
            return false;
        }
        loggingSink.debug(String.format("MatchStore.addMatch %s", selectionAndState.getSelection().getId()));
        matchStore.add(selectionAndState);
        return true;
    }

    @Override
    public synchronized boolean addMatch(MatchSelection selection) {
        return addMatch(new MatchSelectionAndState(selection, null));
    }

    @Override
    public void addMatches(List<MatchSelection> matches) {
        if (matches == null) {
            return;
        }
        for (MatchSelection match : matches) {
            addMatch(match);
        }
    }

    @Override
    public void replaceAllMatches(@NotNull List<MatchSelectionAndState> matches) {
        reset();
        loggingSink.debug(String.format("MatchStore.replaceAllMatches %s", matches.size()));
        matchStore.addAll(matches);
    }

    @Override
    public synchronized void removeMatch(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            loggingSink.debug(String.format("MatchStore.removeMatch %s", id));
            matchStore.remove(index);
        } else {
            loggingSink.debug(String.format("MatchStore.removeMatch %s, not in store", id));
        }
    }

    @Override
    public synchronized List<String> getAllIdsInStore() {
        List<String> ids = new ArrayList<>();
        for (MatchSelectionAndState thisMatch : matchStore) {
            if (thisMatch.getSelection() != null) {
                ids.add(thisMatch.getSelection().getId());
            }
        }
        return ids;
    }

    @Override
    public synchronized @Nullable MatchSelection getMatchSelection(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            return matchStore.get(index).getSelection();
        }
        loggingSink.debug(String.format("MatchStore.getMatchSelection %s, not in store", id));
        return null;
    }

    @Override
    public @Nullable MatchState getMatchState(String id) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            return matchStore.get(index).getState();
        }
        loggingSink.debug(String.format("MatchStore.getMatchState %s, not in store", id));
        return null;
    }

    @Override
    public void updateMatchState(String id, @Nullable MatchState newState) {
        int index = findIdInStore(id);
        if (index != NO_INDEX) {
            loggingSink.debug(String.format("MatchStore.UpdateMatchState %s", id));
            MatchSelectionAndState matchInStore = matchStore.get(index);
            if (newState != null) {
                newState.setAlertsFromPreviousState(matchInStore.getState(), loggingSink);
            }
            matchInStore.setState(newState);
        } else {
            loggingSink.debug(String.format("MatchStore.UpdateMatchState %s not found", id));
        }
    }

    /**
     * @return the contents of the store as a string (only the selections not the state)
     * this is part of the store serialisation
     */
    @Override
    public String getStoreMatchesAsString() {
        loggingSink.debug(String.format("MatchStore.getStoreMatchesAsString %s", matchStore.size()));
        // cannot use StringJoiner as our android API is too low
        StringBuilder retval = new StringBuilder();
        for (MatchSelectionAndState thisMatch : matchStore) {
            if (thisMatch.getSelection() != null && thisMatch.getSelection().getId() != null) {
                retval.append(sanitizeString(thisMatch.getSelection().getId()));
                retval.append(SEPARATOR);
                retval.append(sanitizeString(thisMatch.getSelection().getTeam1()));
                retval.append(SEPARATOR);
                retval.append(sanitizeString(thisMatch.getSelection().getTeam2()));
                retval.append(SEPARATOR);
            }
        }
        return retval.toString();
    }

    private String sanitizeString(String str) {
        if (str == null) {
            return null;
        }
        // we cannot have the separator in the strings
        return str.replace(SEPARATOR, "");
    }

    /**
     * @param str string to use to inflate the store, usually obtained from getStoreMatchesAsString()
     * @return number of items now in the store
     * this is part of the store serialisation
     */
    @Override
    public int replaceStoreMatchesFromString(String str) {
        reset();
        if (str == null) {
            loggingSink.debug("MatchStore.replaceStoreMatchesFromString - null");
            return matchStore.size();
        }
        String tokens[] = str.split(SEPARATOR);
        for (int index = 0; index < tokens.length; index+=3) {
            if (index+2 < tokens.length) {
                // we have at least three items left
                MatchSelection matchSelection = new MatchSelection(
                        tokens[index],
                        tokens[index+1],
                        tokens[index+2]
                );
                addMatch(matchSelection);
            }
        }
        loggingSink.debug(String.format("MatchStore.replaceStoreMatchesFromString %s", matchStore.size()));
        return matchStore.size();
    }

    private int findIdInStore(String id) {
        if (id == null) {
            return NO_INDEX;
        }
        for (int index = 0, size = matchStore.size(); index < size; index++) {
            MatchSelection selection = matchStore.get(index).getSelection();
            if (selection != null && id.equals(selection.getId())) {
                return index;
            }
        }
        return NO_INDEX;
    }
}



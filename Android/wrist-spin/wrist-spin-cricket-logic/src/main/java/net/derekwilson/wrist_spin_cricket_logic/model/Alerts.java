package net.derekwilson.wrist_spin_cricket_logic.model;

import java.util.Calendar;

public class Alerts {
    private static final long MINUTES_FOR_VERY_STALE = 60;
    private static final long MINUTES_FOR_STALE = 6;

    private boolean wicket = false;
    private boolean team50 = false;
    private boolean team100 = false;
    private boolean individual50 = false;
    private boolean individual100 = false;
    private String individualSpeech = "";
    private Calendar lastBallBowled = Calendar.getInstance();

    public boolean isWicket() {
        return wicket;
    }

    public void setWicket(boolean wicket) {
        this.wicket = wicket;
    }

    public boolean isTeam50() {
        return team50;
    }

    public void setTeam50(boolean team50) {
        this.team50 = team50;
    }

    public boolean isTeam100() {
        return team100;
    }

    public void setTeam100(boolean team100) {
        this.team100 = team100;
    }

    public boolean isIndividual50() {
        return individual50;
    }

    public void setIndividual50(boolean individual50) {
        this.individual50 = individual50;
    }

    public boolean isIndividual100() {
        return individual100;
    }

    public void setIndividual100(boolean individual100) {
        this.individual100 = individual100;
    }

    public String getIndividualSpeech() {
        return individualSpeech;
    }

    public void setIndividualSpeech(String individualSpeech) {
        this.individualSpeech = individualSpeech;
    }

    public Calendar getLastBallBowled() {
        return lastBallBowled;
    }

    public void setLastBallBowled(Calendar lastBallBowled) {
        this.lastBallBowled = lastBallBowled;
    }

    public long getMinutesSinceLastBallBowled(Calendar now) {
        if (now == null || lastBallBowled == null) {
            return 0;
        }
        long diff = now.getTimeInMillis() - lastBallBowled.getTimeInMillis();
        if (diff < 0) {
            return 0;
        }
        return diff / (60 * 1000);
    }

    public boolean isStale() {
        return getMinutesSinceLastBallBowled(Calendar.getInstance()) >= MINUTES_FOR_STALE;
    }

    public String getDisplayStale() {
        long mins = getMinutesSinceLastBallBowled(Calendar.getInstance());
        if (mins >= MINUTES_FOR_VERY_STALE) {
            return "> 60 mins old";
        }
        if (mins >= MINUTES_FOR_STALE) {
            return String.format("%d mins old", mins);
        }
        return "";
    }
}


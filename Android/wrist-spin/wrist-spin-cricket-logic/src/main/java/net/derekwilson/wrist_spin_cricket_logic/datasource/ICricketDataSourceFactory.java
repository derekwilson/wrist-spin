package net.derekwilson.wrist_spin_cricket_logic.datasource;

import org.jetbrains.annotations.NotNull;

public interface ICricketDataSourceFactory {
    @NotNull ICricketDataSource getDataSource(DataSource source);
}

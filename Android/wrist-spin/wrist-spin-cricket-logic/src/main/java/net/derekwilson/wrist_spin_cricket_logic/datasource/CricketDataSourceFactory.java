package net.derekwilson.wrist_spin_cricket_logic.datasource;

import net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo.Cricinfo2DataSource;
import net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo.CricinfoDataSource;
import net.derekwilson.wrist_spin_cricket_logic.datasource.test.TestDataSource;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.jetbrains.annotations.NotNull;

public class CricketDataSourceFactory implements ICricketDataSourceFactory {
    private ILoggingSink loggingSink;

    private ICricketDataSource testDataSource;
    private ICricketDataSource cricinfoDataSource;
    private ICricketDataSource cricinfo2DataSource;

    private CricketDataSourceFactory(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;

        this.testDataSource = new TestDataSource(loggingSink);
        this.cricinfoDataSource = CricinfoDataSource.createDataSource(loggingSink);
        this.cricinfo2DataSource = Cricinfo2DataSource.createDataSource(loggingSink);
    }

    // we do this so that we dont need to use @Inject annotation in the library
    public static @NotNull ICricketDataSourceFactory createFactory(ILoggingSink loggingSink) {
        return new CricketDataSourceFactory(loggingSink);
    }

    @Override
    public @NotNull ICricketDataSource getDataSource(DataSource source) {
        switch (source) {
            case Cricinfo:
                return cricinfoDataSource;
            case Cricinfo2:
                return cricinfo2DataSource;
            case Test:
                return testDataSource;
            default:
                loggingSink.debug(String.format("CricketDataSourceFactory.getDataSource unknown source %s", source));
                return testDataSource;
        }
    }
}

package net.derekwilson.wrist_spin_cricket_logic.utility;

import net.derekwilson.wrist_spin_cricket_logic.exception.DataSourceException;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonHelper {

    public static JSONObject getSubObjectFromKey(ILoggingSink logger, JSONObject json, String key) {
        try {
            if (json != null && json.getJSONObject(key) != null) {
                return json.getJSONObject(key);
            }
        } catch (JSONException ex) {
            logger.error(String.format("missing key %s", key), new DataSourceException(String.format("missing key %s", key), "", ex));
        }
        return null;
    }

    public static int getNumberFromKeyAndSubkey(ILoggingSink logger, JSONObject json, String key, String subkey, int defaultValue) {
        try {
            if (json != null && json.getJSONObject(key) != null) {
                return json.getJSONObject(key).getInt(subkey);
            }
        } catch (JSONException ex) {
            logger.error(String.format("missing key %s/%s", key, subkey), new DataSourceException(String.format("missing key %s/%s", key, subkey), "", ex));
        }
        return defaultValue;
    }

    public static String getFromKeyAndSubkey(ILoggingSink logger, JSONObject json, String key, String subkey, String defaultValue) {
        try {
            if (json != null && json.getJSONObject(key) != null) {
                return json.getJSONObject(key).optString(subkey, defaultValue);
            }
        } catch (JSONException ex) {
            logger.error(String.format("missing key %s/%s", key, subkey), new DataSourceException(String.format("missing key %s/%s", key, subkey), "", ex));
        }
        return defaultValue;
    }

    public static String getFromKeyAndSubkey(ILoggingSink logger, JSONObject json, String key, String subkey, String subsubkey, String defaultValue) {
        try {
            if (json != null && json.getJSONObject(key) != null && json.getJSONObject(key).getJSONObject(subkey) != null) {
                return json.getJSONObject(key).getJSONObject(subkey).optString(subsubkey, defaultValue);
            }
        } catch (JSONException ex) {
            logger.error(String.format("missing key %s/%s/%s", key, subkey, subsubkey), new DataSourceException(String.format("missing key %s/%s/%s", key, subkey, subsubkey), "", ex));
        }
        return defaultValue;
    }

}




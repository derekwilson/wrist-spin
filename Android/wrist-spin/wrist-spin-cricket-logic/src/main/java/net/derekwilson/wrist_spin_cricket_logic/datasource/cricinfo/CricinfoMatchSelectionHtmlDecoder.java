package net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo;

import net.derekwilson.wrist_spin_cricket_logic.datasource.IMatchSelectionDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic.utility.HtmlHelper;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class CricinfoMatchSelectionHtmlDecoder implements IMatchSelectionDataDecoder {
    private ILoggingSink loggingSink;

    public CricinfoMatchSelectionHtmlDecoder(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    @Override
    public List<MatchSelection> decodeMatchSelectionList(String data, String baseUrl) {
        Document doc = HtmlHelper.loadHtmlFromString(data, baseUrl);
        Elements elements = doc.select("section[data-matchstatus]");
        if (elements != null) {
            loggingSink.debug(String.format("CricinfoMatchSelectionHtmlDecoder.decodeMatchSelectionList nodes= %s", elements.size()));
            List<MatchSelection> returnValue = new ArrayList<>();
            for (int i = 0; i < elements.size(); i++) {
                Element section = elements.get(i);
                String id = getIdFromSection(section);
                if (id != null && !"".equals(id)) {
                    MatchSelection thisMatch = new MatchSelection(id);
                    thisMatch.setTeam1(getTeamFromSection(section, "div.innings-info-1"));
                    thisMatch.setScore1(getScoreFromSection(section, "div.innings-info-1"));
                    thisMatch.setTeam2(getTeamFromSection(section, "div.innings-info-2"));
                    thisMatch.setScore2(getScoreFromSection(section, "div.innings-info-2"));
                    thisMatch.setStatus(getStatusFromSection(section, "div.match-status"));
                    returnValue.add(thisMatch);
                }
            }
            return returnValue;
        }
        return null;
    }

    private String getStatusFromSection(Element section, String selector) {
        Element statusDiv = section.selectFirst(selector);
        if (statusDiv == null) {
            return "";
        }
        return statusDiv.text().trim();
    }

    private String getTeamFromSection(Element section, String selector) {
        Element teamDiv = section.selectFirst(selector);
        if (teamDiv == null) {
            return "";
        }
        List<TextNode> textNodes = teamDiv.textNodes();
        if (textNodes.isEmpty()) {
            return "";
        }
        return textNodes.get(0).text().trim();
    }

    private String getScoreFromSection(Element section, String selector) {
        Element teamDiv = section.selectFirst(selector);
        if (teamDiv == null) {
            return "";
        }
        Element scoreSpan = teamDiv.selectFirst("span");
        if (scoreSpan != null) {
            String score = scoreSpan.text().trim();
            int index = score.indexOf("(");
            if (index >= 0) {
                // get the bit before the first "("
                score = score.substring(0,index).trim();
            }
            if (score.length() > 0) {
                // surround with brackets so it can be discarded later
                return score;
            }
        }
        return "";
    }

    private String getIdFromSection(Element section) {
        Element titleSpan = section.selectFirst("span.match-no");
        if (titleSpan == null) {
            return null;
        }
        Element link = titleSpan.selectFirst("a[href]");
        if (link == null) {
            return null;
        }
        return convertUrlToMatchId(link.attr("href"));
    }

    private String convertUrlToMatchId(String url) {
        if (url == null) {
            return null;
        }
        String id = findIdAfterToken(url, "/game/");
        if (id != null) {
            return id;
        }
        return findIdAfterToken(url, "/scorecard/");
    }

    private String findIdAfterToken(String url, String token) {
        if (url.contains(token)) {
            int index = url.lastIndexOf(token);
            if (index >= 0) {
                // get the bit after the token
                String tail = url.substring(index + token.length());
                int index2 = tail.indexOf("/");
                if (index2 >= 0) {
                    // get the bit before the first "/"
                    return tail.substring(0,index2);
                }
            }
        }
        return null;
    }
}



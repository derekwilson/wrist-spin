package net.derekwilson.wrist_spin_cricket_logic.datasource;


import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;

import java.util.List;

public interface IMatchSelectionDataDecoder {
    List<MatchSelection> decodeMatchSelectionList(String data, String baseUrl) throws Exception;
}


package net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo;

import net.derekwilson.wrist_spin_cricket_logic.datasource.ICricketDataSource;
import net.derekwilson.wrist_spin_cricket_logic.datasource.IMatchSelectionDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic.datasource.IMatchStateDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;
import net.derekwilson.wrist_spin_cricket_logic.exception.DataSourceException;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic.utility.DataDownloader;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class CricinfoDataSource implements ICricketDataSource {
    private static final String MATCH_SELECTION = "https://static.espncricinfo.com/rss/livescores.xml";
    private static final String MATCH_STATE_TEMPLATE = "https://www.espncricinfo.com/ci/engine/match/%s.json";

    private IMatchSelectionDataDecoder selectionDecoder;
    private IMatchStateDataDecoder stateDecoder;
    private ILoggingSink loggingSink;

    private CricinfoDataSource(ILoggingSink loggingSink, IMatchSelectionDataDecoder selectionDecoder, IMatchStateDataDecoder decoder) {
        this.loggingSink = loggingSink;
        this.stateDecoder = decoder;
        this.selectionDecoder = selectionDecoder;
    }

    public static @NotNull ICricketDataSource createDataSource(ILoggingSink loggingSink) {
        return new CricinfoDataSource(
                loggingSink,
                new CricinfoMatchSelectionXmlDecoder(loggingSink),
                new CricinfoMatchStateDecoder(loggingSink)
        );
    }

    @Override
    public @NotNull String getName() {
        return "CricInfo";
    }

    @Override
    public List<MatchSelection> getMatchSelection() {
        try {
            loggingSink.debug("CricinfoDataSource.getMatchSelection");
            String rawXml = getRawMatchSelectionData();
            return selectionDecoder.decodeMatchSelectionList(rawXml, MATCH_SELECTION);
        } catch (Throwable ex) {
            loggingSink.error("error unpacking match selection ", new DataSourceException(MATCH_SELECTION, ex));
        }
        return null;
    }

    private String getRawMatchSelectionData() throws IOException {
        loggingSink.debug(String.format("CricinfoDataSource.getRawMatchSelectionData %s", MATCH_SELECTION));
        return DataDownloader.getStringFromUrl(new URL(MATCH_SELECTION));
    }

    @Override
    public MatchState getMatchState(String id) {
        try {
            loggingSink.debug(String.format("CricinfoDataSource.getMatchState %s", id));
            String rawJson = getRawMatchStateData(id);
            return stateDecoder.decodeMatchState(id, rawJson);
        } catch (Throwable ex) {
            loggingSink.error("error unpacking selection state ", new DataSourceException(id, ex));
        }
        return null;
    }

    private String getRawMatchStateData(String id) throws IOException {
        String urlStr = String.format(MATCH_STATE_TEMPLATE, id);
        loggingSink.debug(String.format("CricinfoDataSource.getRawMatchStateData %s", urlStr));
        return DataDownloader.getStringFromUrl(new URL(urlStr));
    }
}



package net.derekwilson.wrist_spin_cricket_logic.exception;

public class DataSourceException extends Exception {
    public DataSourceException(String id, Throwable throwable) {
        super("ID: " + id, throwable);
    }
    public DataSourceException(String reason, String id, Throwable throwable) {
        super("Reason: " + reason + " ID: " + id, throwable);
    }
}



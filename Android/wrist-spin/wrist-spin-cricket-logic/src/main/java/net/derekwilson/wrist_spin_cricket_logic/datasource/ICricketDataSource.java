package net.derekwilson.wrist_spin_cricket_logic.datasource;

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ICricketDataSource {
    @NotNull String getName();

    @Nullable List<MatchSelection> getMatchSelection();

    @Nullable MatchState getMatchState(String id);
}

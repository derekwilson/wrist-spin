package net.derekwilson.wrist_spin_cricket_logic.matchstore;

import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.jetbrains.annotations.Nullable;

public class MatchSelectionAndState {
    private MatchSelection selection;
    private MatchState state;

    public MatchSelectionAndState(MatchSelection selection, @Nullable MatchState state) {
        this.selection = selection;
        this.state = state;
    }

    public MatchSelection getSelection() {
        return selection;
    }

    public void setSelection(MatchSelection selection) {
        this.selection = selection;
    }

    public @Nullable MatchState getState() {
        return state;
    }

    public void setState(@Nullable MatchState state) {
        this.state = state;
    }
}


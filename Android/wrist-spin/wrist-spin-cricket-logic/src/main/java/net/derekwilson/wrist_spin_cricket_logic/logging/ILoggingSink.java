package net.derekwilson.wrist_spin_cricket_logic.logging;

import org.jetbrains.annotations.NotNull;

public interface ILoggingSink {
    void debug(String message);
    void error(String message, @NotNull Throwable t);
}

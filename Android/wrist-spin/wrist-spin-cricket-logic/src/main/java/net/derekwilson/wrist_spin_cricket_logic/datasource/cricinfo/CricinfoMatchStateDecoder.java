package net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo;

import net.derekwilson.wrist_spin_cricket_logic.datasource.IMatchStateDataDecoder;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;
import net.derekwilson.wrist_spin_cricket_logic.model.Overs;
import net.derekwilson.wrist_spin_cricket_logic.model.Player;
import net.derekwilson.wrist_spin_cricket_logic.model.Team;
import net.derekwilson.wrist_spin_cricket_logic.utility.JsonHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class CricinfoMatchStateDecoder implements IMatchStateDataDecoder {

    private ILoggingSink loggingSink;

    public CricinfoMatchStateDecoder(ILoggingSink loggingSink) {
        this.loggingSink = loggingSink;
    }

    @Override
    public MatchState decodeMatchState(String id, String data) {
        JSONObject json = new JSONObject(data);

        // build the team and player maps
        HashMap<String, Player> playersMap = new HashMap<>();
        HashMap<String, Team> teamsMap = decodeTeams(json, playersMap);

        // is the match currently on, complete ot dormant?
        MatchState returnValue = MatchState.emptyState();
        returnValue.setId(id);
        String match_status = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "match_status", "complete");
        String match_result = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "result", "0");
        if ((match_status == null || "current".equals(match_status)) && !"0".equals(match_result)) {
            match_status = "complete";
        }
        String batting_team_id = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "live", "innings", "batting_team_id", "0");
        if ("current".equals(match_status) && "0".equals(batting_team_id)) {
            match_status = "dormant";
        }
        loggingSink.debug(String.format("CricinfoDecoder.decodeMatchState %s, %s", id, match_status));

        // setup the teams
        returnValue.getTeam1().setId(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team1_id", ""));
        returnValue.getTeam1().setAbbreviation(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team1_abbreviation", ""));
        returnValue.getTeam1().setShortName(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team1_short_name", ""));
        returnValue.getTeam1().setName(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team1_name", ""));

        returnValue.getTeam2().setId(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team2_id", ""));
        returnValue.getTeam2().setAbbreviation(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team2_abbreviation", ""));
        returnValue.getTeam2().setShortName(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team2_short_name", ""));
        returnValue.getTeam2().setName(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "team2_name", ""));

        // process the specifics for the state of the game
        switch (match_status) {
            case "dormant":
                returnValue.setCurrentState(MatchState.CurrentState.Dormant);
                decodeDormantMatch(json, returnValue, teamsMap);
                break;
            case "complete":
                returnValue.setCurrentState(MatchState.CurrentState.Complete);
                decodeCompleteMatch(json, returnValue, teamsMap);
                break;
            case "current":
                returnValue.setCurrentState(MatchState.CurrentState.Current);
                decodeCurrentMatch(json, returnValue, teamsMap, playersMap);
                break;
        }

        return returnValue;
    }

    private HashMap<String, Team> decodeTeams(JSONObject json, HashMap<String, Player> playersMap) {
        HashMap<String, Team> teamsMap = new HashMap<>();
        JSONArray teams = json.optJSONArray("team");
        if (teams != null) {
            for (int index = 0, size = teams.length(); index < size; index++) {
                JSONObject team = teams.getJSONObject(index);
                Team thisTeam = new Team();
                thisTeam.setId(team.optString("team_id", ""));
                thisTeam.setAbbreviation(team.optString("team_abbreviation", ""));
                thisTeam.setShortName(team.optString("team_short_name", ""));
                thisTeam.setName(team.optString("team_name", ""));
                teamsMap.put(thisTeam.getId(), thisTeam);

                // now add the player into the supplied map
                decodePlayers(team, "player", playersMap);
                decodePlayers(team, "squad", playersMap);
            }
        }
        return teamsMap;
    }

    private Team findTeamByIdInJsonKey(HashMap<String, Team> teamsMap, JSONObject json, String key, String subKey) {
        if (teamsMap == null || json == null || key == null || subKey == null) {
            return null;
        }

        String id = JsonHelper.getFromKeyAndSubkey(loggingSink, json, key, subKey, "");
        return findTeamById(teamsMap, id);
    }

    private Team findTeamById(HashMap<String, Team> teamsMap, String id) {
        if (id == null || "".equals(id)) {
            return null;
        }
        if (teamsMap.containsKey(id)) {
            return teamsMap.get(id);
        }
        return null;
    }

    private void decodePlayers(JSONObject team, String playerKey, HashMap<String, Player> playersMap) {
        if (team == null || playerKey == null || playersMap == null) {
            return;
        }

        JSONArray players = team.optJSONArray(playerKey);
        if (players != null) {
            for (int index = 0, size = players.length(); index < size; index++) {
                JSONObject player = players.getJSONObject(index);
                Player thisPlayer = new Player();
                thisPlayer.setId(player.optString("player_id", ""));
                thisPlayer.setCardShortName(player.optString("card_short", ""));
                thisPlayer.setPopularName(player.optString("popular_name", ""));
                thisPlayer.setName(player.optString("name", ""));
                playersMap.put(thisPlayer.getId(), thisPlayer);
            }
        }
    }

    private JSONObject findPlayerObjectByTypeInJsonArray(
            HashMap<String, Player> playersMap,
            JSONObject json,
            String arrayKey,
            String searchNameKey,
            String searchNameValue
    ) {
        if (playersMap == null || json == null || arrayKey == null || searchNameKey == null || searchNameValue == null) {
            return null;
        }

        JSONArray players = json.optJSONArray(arrayKey);
        if (players != null) {
            for (int index = 0, size = players.length(); index < size; index++) {
                JSONObject player = players.getJSONObject(index);
                String searchProperty = player.optString(searchNameKey, "");
                if (searchNameValue.equals(searchProperty)) {
                    return player;
                }
            }
        }
        return null;
    }

    private Player findPlayerById(HashMap<String, Player> playersMap, JSONObject playerObject, String idKey) {
        if (playersMap == null || playerObject == null || idKey == null) {
            return null;
        }

        return findPlayerById(playersMap, playerObject.optString(idKey, ""));
    }

    private Player findPlayerById(HashMap<String, Player> playersMap, String id) {
        if (id == null || "".equals(id)) {
            return null;
        }
        if (playersMap.containsKey(id)) {
            return playersMap.get(id);
        }
        return null;
    }

    private void decodeDormantMatch(JSONObject json, MatchState returnValue, HashMap<String, Team> teamsMap) {
        String fact = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "live", "break", "");
        String lead = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "match_clock", "");

        returnValue.setFact(fact);
        if (!"".equals(lead)) {
            returnValue.setLead(String.format("Match starts in %s", lead));
        } else if (!"".equals(fact)) {
            returnValue.setLead(fact);
            returnValue.setFact("");
        }

        String toss = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "toss_decision", "");
        if (!("0".equals(toss) || "".equals(toss))) {
            // toss decided
            Team winner = findTeamByIdInJsonKey(teamsMap, json, "match", "toss_winner_team_id");
            if (winner != null) {
                returnValue.setStrikerName(String.format("%s won toss, ", winner.getShortName()));
                returnValue.setNonStrikerName(String.format("will %s",
                        JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "toss_decision_name", "")
                ));
            }
        }
    }

    private void decodeCompleteMatch(JSONObject json, MatchState returnValue, HashMap<String, Team> teamsMap) {
        String winnerId = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "winner_team_id", "");
        if (!"".equals(winnerId)) {
            if ("0".equals(winnerId)) {
                returnValue.setStrikerName("Match drawn");
            } else {
                Team winner = findTeamById(teamsMap, winnerId);
                if (winner != null) {
                    returnValue.setStrikerName(String.format("%s won by", winner.getShortName()));
                    String amountName = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "amount_name", "");
                    String amount = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "amount", "");
                    int amountBalls = JsonHelper.getNumberFromKeyAndSubkey(loggingSink, json, "match", "amount_balls", 0);
                    if ("innings".equals(amountName)) {
                        returnValue.setNonStrikerName(String.format("innings and %s runs", amount));
                    } else {
                        returnValue.setNonStrikerName(String.format("%s %s", amount, amountName));
                        if (amountBalls > 0) {
                            returnValue.setBowlerName(String.format("(%s balls remaining)", amountBalls));
                        }
                    }
                }
            }
        }
    }

    private void decodeCurrentMatch(JSONObject json, MatchState returnValue, HashMap<String, Team> teamsMap, HashMap<String, Player> playersMap) {
        JSONObject live = JsonHelper.getSubObjectFromKey(loggingSink, json, "live");
        if (live == null) {
            loggingSink.debug("CricinfoDecoder.decodeCurrentMatch: cannot find live key in current match");
            return;
        }
        Team batting = findTeamByIdInJsonKey(teamsMap, live, "innings", "batting_team_id");
        if (batting != null) {
            returnValue.setBattingTeamAbbreviation(batting.getAbbreviation());
        }

        returnValue.getScore().setRuns(JsonHelper.getNumberFromKeyAndSubkey(loggingSink, live, "innings", "runs", 0));
        returnValue.getScore().setWickets(JsonHelper.getNumberFromKeyAndSubkey(loggingSink, live, "innings", "wickets", 0));
        String event = JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "event", "");
        returnValue.getScore().setDeclared("declared".equals(event));

        returnValue.getOvers().parse(JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "overs", ""));

        String scheduledOvers = JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "scheduled_overs", "");
        String inningsNumber = JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "innings_number", "");
        int lead = JsonHelper.getNumberFromKeyAndSubkey(loggingSink, live, "innings", "lead", 0);
        String runRate = JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "run_rate", "");
        if ("".equals(scheduledOvers) || "0".equals(scheduledOvers)) {
            // not a limited overs game
            switch (inningsNumber) {
                case "1":
                    returnValue.setLead("First innings");
                    break;
                case "4":
                    returnValue.setLead(String.format("Need %s, target %s",
                            1-lead,
                            JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "target", "")
                    ));
                    break;
                default:
                    if (lead < 0) {
                        returnValue.setLead(String.format("Trail by %s", -lead));
                    } else if (lead > 0) {
                        returnValue.setLead(String.format("Lead by %s", lead));
                    } else {
                        returnValue.setLead("Scores level");
                    }
                    break;
            }
        } else {
            // limited overs
            if ("1".equals(inningsNumber)) {
                returnValue.setLead(String.format("Run rate: %s", runRate));
            } else {
                Overs remaining = new Overs(0,0);
                remaining.parse(JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "remaining_overs", ""));
                String remainingDisplay = (remaining.getComplete() < 20
                        ? JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "remaining_balls", "")
                        : JsonHelper.getFromKeyAndSubkey(loggingSink, live, "innings", "remaining_overs", "") + " ov"
                );
                returnValue.setLead(String.format("Need %s from %s", 1-lead, remainingDisplay));
            }
        }

        JSONObject striker = findPlayerObjectByTypeInJsonArray(playersMap, live, "batting", "live_current_name", "striker");
        Player strikerPlayer = findPlayerById(playersMap, striker, "player_id");
        if (striker != null && strikerPlayer != null) {
            returnValue.setStrikerName(strikerPlayer.getDisplay());
            returnValue.setStrikerScore(striker.optString("runs"));
            returnValue.setStrikerStats(
                    String.format("%s (%s)",
                            returnValue.getStrikerScore(),
                            striker.optString("balls_faced")
                    )
            );
        }

        JSONObject nonStriker = findPlayerObjectByTypeInJsonArray(playersMap, live, "batting", "live_current_name", "non-striker");
        Player nonStrikerPlayer = findPlayerById(playersMap, nonStriker, "player_id");
        if (nonStriker != null && nonStrikerPlayer != null) {
            returnValue.setNonStrikerName(nonStrikerPlayer.getDisplay());
            returnValue.setNonStrikerScore(nonStriker.optString("runs"));
            returnValue.setNonStrikerStats(
                    String.format("%s (%s)",
                            returnValue.getNonStrikerScore(),
                            nonStriker.optString("balls_faced")
                    )
            );
        }

        JSONObject bowler = findPlayerObjectByTypeInJsonArray(playersMap, live, "bowling", "live_current_name", "current bowler");
        Player bowlerPlayer = findPlayerById(playersMap, bowler, "player_id");
        if (bowler != null && bowlerPlayer != null) {
            returnValue.setBowlerName(bowlerPlayer.getDisplay());
            returnValue.setBowlerStats(
                    String.format("%s-%s-%s-%s",
                            bowler.optString("overs"),
                            bowler.optString("maidens"),
                            bowler.optString("conceded"),
                            bowler.optString("wickets")
                    )
            );
        }

        // we might be at a break in play
        returnValue.setFact(JsonHelper.getFromKeyAndSubkey(loggingSink, json, "match", "live_state", ""));
        if ("".equals(returnValue.getFact())) {
            // we dont have a fact - lets look back thru the commentary
            returnValue.setFact(decodeCommentaryFact(json));
        }
    }

    private String decodeCommentaryFact(JSONObject json) {
        JSONArray comms = json.optJSONArray("comms");
        if (comms != null) {
            for (int over_index = 0, over_size = comms.length(); over_index < over_size; over_index++) {
                JSONObject over = comms.getJSONObject(over_index);
                JSONArray balls = over.optJSONArray("ball");
                if (balls != null) {
                    for (int ball_index = 0, ball_size = balls.length(); ball_index < ball_size; ball_index++) {
                        JSONObject ball = balls.getJSONObject(ball_index);
                        String event = ball.optString("event");
                        String dismissal = ball.optString("dismissal");
                        String oversActual = ball.optString("overs_actual");

                        if (!"".equals(dismissal)) {
                            event = "OUT";
                        }
                        String fact = "";
                        String[] commsLine = ball.optString("players").split(" to ");
                        switch (event) {
                            case "OUT":
                                // TODO abbreviate this
                                fact = dismissal.replace("&dagger;", "").replace("&amp;","&");
                                break;
                            case "FOUR":
                                if (commsLine.length > 1) {
                                    fact = "FOUR " + commsLine[1];
                                } else {
                                    fact = "FOUR";
                                }
                                break;
                            case "SIX":
                                if (commsLine.length > 1) {
                                    fact = "SIX " + commsLine[1];
                                } else {
                                    fact = "SIX";
                                }
                                break;
                        }
                        if (!"".equals(fact)) {
                            return String.format("%s %s", oversActual, fact);
                        }
                    }
                }
            }
        }
        return "";
    }
}



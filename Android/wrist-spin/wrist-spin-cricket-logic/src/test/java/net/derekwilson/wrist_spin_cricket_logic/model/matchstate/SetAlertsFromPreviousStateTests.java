package net.derekwilson.wrist_spin_cricket_logic.model.matchstate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.model.BaseModelTest;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.junit.Before;
import org.junit.Test;

public class SetAlertsFromPreviousStateTests extends BaseModelTest {
    private MatchState matchState;
    private MatchState newMatchState;

    @Before
    public void setUp() {
    }

    @Test
    public void setAlertsFromPreviousState_null_no_alerts() {
        // arrange
        newMatchState = getMatchState(MatchState.CurrentState.Current, 12, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(null, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_carry_alerts() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
        matchState.getAlerts().setWicket(true);
        matchState.getAlerts().setTeam50(true);
        matchState.getAlerts().setTeam100(true);
        matchState.getAlerts().setIndividual50(true);
        matchState.getAlerts().setIndividual100(true);
        matchState.getAlerts().setIndividualSpeech("TEST");
        newMatchState = getMatchState(MatchState.CurrentState.Current, 12, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isWicket(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(true));
        assertThat(newMatchState.getAlerts().isTeam50(), is(true));
        assertThat(newMatchState.getAlerts().isTeam100(), is(true));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is("TEST"));
    }

    @Test
    public void setAlertsFromPreviousState_no_alerts() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 12, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_wicket_alert() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 12, 2, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_unwicket_alert() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 12, 0, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_team100_alert() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 97, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 100, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(true));
        assertThat(newMatchState.getAlerts().isTeam100(), is(true));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_team100_alert_low() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 97, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 99, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_team100_alert_high() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 100, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 101, 1, 6, 0);

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(false));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(""));
    }

    @Test
    public void setAlertsFromPreviousState_individual100_alert() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 110, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 111, 1, 6, 0);
        matchState.setStrikerScore("99");
        matchState.setNonStrikerScore("5");
        newMatchState.setStrikerScore("101");
        newMatchState.setNonStrikerScore("5");

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(true));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(BATTER_1 + ", 101"));
    }

    @Test
    public void setAlertsFromPreviousState_individual100_alert_crossing() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 110, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 111, 1, 6, 0);

        matchState.setStrikerScore("99");
        matchState.setNonStrikerScore("5");

        newMatchState.setStrikerName(BATTER_2);
        newMatchState.setStrikerScore("5");
        newMatchState.setNonStrikerName(BATTER_1);
        newMatchState.setNonStrikerScore("100");

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(true));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(BATTER_1 + ", 100"));
    }

    @Test
    public void setAlertsFromPreviousState_individual100_alert_both() {
        // arrange
        matchState = getMatchState(MatchState.CurrentState.Current, 110, 1, 5, 1);
        newMatchState = getMatchState(MatchState.CurrentState.Current, 121, 1, 6, 0);

        matchState.setStrikerScore("99");
        matchState.setNonStrikerScore("49");

        newMatchState.setStrikerName(BATTER_2);
        newMatchState.setStrikerScore("50");
        newMatchState.setNonStrikerName(BATTER_1);
        newMatchState.setNonStrikerScore("101");

        // act
        newMatchState.setAlertsFromPreviousState(matchState, mockLogger);

        // assert
        assertThat(newMatchState.getAlerts().isStale(), is(false));
        assertThat(newMatchState.getAlerts().isWicket(), is(false));
        assertThat(newMatchState.getAlerts().isIndividual50(), is(true));
        assertThat(newMatchState.getAlerts().isIndividual100(), is(true));
        assertThat(newMatchState.getAlerts().isTeam50(), is(false));
        assertThat(newMatchState.getAlerts().isTeam100(), is(false));
        // really this should show both batters
        assertThat(newMatchState.getAlerts().getIndividualSpeech(), is(BATTER_1 + ", 101"));
    }
}

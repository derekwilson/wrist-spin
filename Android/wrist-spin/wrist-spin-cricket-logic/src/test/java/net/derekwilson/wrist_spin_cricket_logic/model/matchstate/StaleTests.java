package net.derekwilson.wrist_spin_cricket_logic.model.matchstate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.model.BaseModelTest;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

public class StaleTests extends BaseModelTest {
    private MatchState matchState;

    @Before
    public void setUp() {
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -7);
        matchState.getAlerts().setLastBallBowled(now);
    }

    @Test
    public void getStaleDisplay_dormant() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Dormant);

        // act
        String result = matchState.getStaleDisplay();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getStaleDisplay_complete() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Complete);

        // act
        String result = matchState.getStaleDisplay();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getStaleDisplay_current() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Current);

        // act
        String result = matchState.getStaleDisplay();

        // assert
        assertThat(result, is("7 mins old"));
    }

    @Test
    public void getStaleDisplayWithGeneratedTime_dormant() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Dormant);

        // act
        String result = matchState.getStaleDisplayWithGeneratedTime();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getStaleDisplayWithGeneratedTime_complete() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Complete);

        // act
        String result = matchState.getStaleDisplayWithGeneratedTime();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getStaleDisplayWithGeneratedTime_current() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Current);

        // act
        String result = matchState.getStaleDisplayWithGeneratedTime();

        // assert
        assertThat(result, containsString("7 mins old"));
    }

}

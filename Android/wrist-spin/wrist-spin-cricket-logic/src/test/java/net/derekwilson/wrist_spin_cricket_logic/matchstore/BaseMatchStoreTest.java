package net.derekwilson.wrist_spin_cricket_logic.matchstore;

import net.derekwilson.wrist_spin_cricket_logic.MockingAnnotationSetup;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.mockito.Mock;

public class BaseMatchStoreTest  extends MockingAnnotationSetup {
    @Mock protected ILoggingSink mockLogger;

    protected MatchStore matchStore;

    protected void setupMatchStore(int numberOfMatches) {
        matchStore = new MatchStore(mockLogger);
        addMatchesToStore(numberOfMatches);
    }

    protected void addMatchesToStore(int numberOfMatches) {
        for (int index=0; index<numberOfMatches; index++) {
            MatchSelection selection = new MatchSelection(
                    String.valueOf(index+1),
                    "Team1_" + (index+1),
                    "Team2_" + (index+1)
            );
            matchStore.addMatch(selection);
        }
    }
}

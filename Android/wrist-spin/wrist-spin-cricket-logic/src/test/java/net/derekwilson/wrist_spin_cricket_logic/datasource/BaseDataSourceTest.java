package net.derekwilson.wrist_spin_cricket_logic.datasource;

import net.derekwilson.wrist_spin_cricket_logic.MockingAnnotationSetup;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.apache.commons.io.IOUtils;
import org.mockito.Mock;

import java.io.IOException;

public class BaseDataSourceTest  extends MockingAnnotationSetup {
    @Mock protected ILoggingSink mockLogger;

    protected String getResourcesFile(String pathname) throws IOException {
        return IOUtils.toString(this.getClass().getResource(pathname),"UTF-8");
    }
}


package net.derekwilson.wrist_spin_cricket_logic.matchstore;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class SerialisationTests extends BaseMatchStoreTest {

    @Before
    public void setUp() {
        setupMatchStore(0);
    }

    @Test
    public void getStoreMatchesAsString_empty_store() {
        // arrange

        // act
        String result = matchStore.getStoreMatchesAsString();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getStoreMatchesAsString_1match_store() {
        // arrange
        addMatchesToStore(1);

        // act
        String result = matchStore.getStoreMatchesAsString();

        // assert
        assertThat(result, is("1:Team1_1:Team2_1:"));
    }

    @Test
    public void getStoreMatchesAsString_2match_store() {
        // arrange
        addMatchesToStore(2);

        // act
        String result = matchStore.getStoreMatchesAsString();

        // assert
        assertThat(result, is("1:Team1_1:Team2_1:2:Team1_2:Team2_2:"));
    }

    @Test
    public void getStoreMatchesAsString_1match_store_illegal_char() {
        // arrange
        addMatchesToStore(1);
        matchStore.getMatchSelection("1").setTeam1("Illegal:_:chars");

        // act
        String result = matchStore.getStoreMatchesAsString();

        // assert
        assertThat(result, is("1:Illegal_chars:Team2_1:"));
    }

    @Test
    public void replaceStoreMatchesFromString_2match_string() {
        // arrange
        String str = "1:Team1_1:Team2_1:2:Team1_2:Team2_2:";

        // act
        int result = matchStore.replaceStoreMatchesFromString(str);

        // assert
        assertThat(result, is(2));
        assertThat(matchStore.getAllIdsInStore().toString(), is("[1, 2]"));
        assertThat(matchStore.getMatchSelection("1").getDisplay(), is("Team1_1 v Team2_1"));
        assertThat(matchStore.getMatchSelection("2").getDisplay(), is("Team1_2 v Team2_2"));
    }

    @Test
    public void replaceStoreMatchesFromString_2match_string_no_last_sep() {
        // arrange
        String str = "1:Team1_1:Team2_1:2:Team1_2:Team2_2";

        // act
        int result = matchStore.replaceStoreMatchesFromString(str);

        // assert
        assertThat(result, is(2));
        assertThat(matchStore.getAllIdsInStore().toString(), is("[1, 2]"));
        assertThat(matchStore.getMatchSelection("1").getDisplay(), is("Team1_1 v Team2_1"));
        assertThat(matchStore.getMatchSelection("2").getDisplay(), is("Team1_2 v Team2_2"));
    }

    @Test
    public void replaceStoreMatchesFromString_1match_and_a_bit_1_string() {
        // arrange
        String str = "1:Team1_1:Team2_1:2:Team1_2:";

        // act
        int result = matchStore.replaceStoreMatchesFromString(str);

        // assert
        assertThat(result, is(1));
        assertThat(matchStore.getAllIdsInStore().toString(), is("[1]"));
        assertThat(matchStore.getMatchSelection("1").getDisplay(), is("Team1_1 v Team2_1"));
    }

    @Test
    public void replaceStoreMatchesFromString_1match_and_a_bit_2_string() {
        // arrange
        String str = "1:Team1_1:Team2_1:2:";

        // act
        int result = matchStore.replaceStoreMatchesFromString(str);

        // assert
        assertThat(result, is(1));
        assertThat(matchStore.getAllIdsInStore().toString(), is("[1]"));
        assertThat(matchStore.getMatchSelection("1").getDisplay(), is("Team1_1 v Team2_1"));
    }


}

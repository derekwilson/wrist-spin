package net.derekwilson.wrist_spin_cricket_logic;

import org.junit.After;
import org.junit.Before;
import org.mockito.MockitoAnnotations;

public class MockingAnnotationSetup {
    private AutoCloseable closeable = null;

    @Before
    public void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @After
    public void releaseMocks() throws Exception {
        if (closeable != null) {
            closeable.close();
        }
    }
}



package net.derekwilson.wrist_spin_cricket_logic.datasource.cricinfo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.datasource.BaseDataSourceTest;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchSelection;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CricinfoMatchSelectionHtmlDecoderTests extends BaseDataSourceTest {
    private static final String MATCH_SELECTION = "https://www.espncricinfo.com/ci/engine/match/index/live.html";

    private CricinfoMatchSelectionHtmlDecoder decoder;

    @Before
    public void setUp() {
        decoder = new CricinfoMatchSelectionHtmlDecoder(mockLogger);
    }

    @Test
    public void decodeMatchSelection_gets() throws Exception {
        // arrange
        String rawHtml = getResourcesFile("/testdata/cricinfo/match_selection.html");

        // act
        List<MatchSelection> matches = decoder.decodeMatchSelectionList(rawHtml, MATCH_SELECTION);

        // assert
        assertThat(matches.size(), is(18));
        assertThat(matches.get(0).getId(), is("1388205"));
        assertThat(matches.get(0).getTeam1(), is("New Zealand Women"));
        assertThat(matches.get(0).getScore1(), is(""));
        assertThat(matches.get(0).getTeam2(), is("England Women"));
        assertThat(matches.get(0).getScore2(), is(""));
        assertThat(matches.get(0).getStatus(), is("Match scheduled to begin in 1 hour 45 mins (13:00 local | 00:00 GMT)"));

        assertThat(matches.get(1).getId(), is("1398261"));
        assertThat(matches.get(1).getTeam1(), is("South Africa Women"));
        assertThat(matches.get(1).getScore1(), is(""));
        assertThat(matches.get(1).getTeam2(), is("Sri Lanka Women"));
        assertThat(matches.get(1).getScore2(), is(""));
        assertThat(matches.get(1).getStatus(), is("Match scheduled to begin at 18:00 local time (16:00 GMT | 05:00 NZDT 1d)"));

        assertThat(matches.get(2).getId(), is("1426046"));
        assertThat(matches.get(2).getTeam1(), is("Papua New Guinea Women"));
        assertThat(matches.get(2).getScore1(), is("125"));
        assertThat(matches.get(2).getTeam2(), is("Zimbabwe Women"));
        assertThat(matches.get(2).getScore2(), is("126/8"));
        assertThat(matches.get(2).getStatus(), is("Zimbabwe Women won by 2 wickets (with 45 balls remaining)"));

        assertThat(matches.get(3).getId(), is("1425063"));
        assertThat(matches.get(3).getTeam1(), is("Bangladesh Women"));
        assertThat(matches.get(3).getScore1(), is(""));
        assertThat(matches.get(3).getTeam2(), is("Australia Women"));
        assertThat(matches.get(3).getScore2(), is(""));
        assertThat(matches.get(3).getStatus(), is("Match scheduled to begin at 09:30 local time (03:30 GMT | 16:30 NZDT)"));

        assertThat(matches.get(5).getId(), is("1391925"));
        assertThat(matches.get(5).getTeam1(), is("Wellington"));
        assertThat(matches.get(5).getScore1(), is("323 & 127/4"));
        assertThat(matches.get(5).getTeam2(), is("Northern Districts"));
        assertThat(matches.get(5).getScore2(), is("362"));
        assertThat(matches.get(5).getStatus(), is("Stumps - Wellington lead by 88 runs with 6 wickets remaining"));

        assertThat(matches.get(16).getId(), is("1422126"));
        assertThat(matches.get(16).getTeam1(), is("Sunrisers Hyderabad"));
        assertThat(matches.get(16).getScore1(), is(""));
        assertThat(matches.get(16).getTeam2(), is("Mumbai Indians"));
        assertThat(matches.get(16).getScore2(), is(""));
        assertThat(matches.get(16).getStatus(), is("Match scheduled to begin at 19:30 local time (14:00 GMT | 03:00 NZDT 1d)"));

        assertThat(matches.get(17).getId(), is("1426057"));
        assertThat(matches.get(17).getTeam1(), is("Nepal"));
        assertThat(matches.get(17).getScore1(), is("198/7"));
        assertThat(matches.get(17).getTeam2(), is("Ireland A"));
        assertThat(matches.get(17).getScore2(), is("127"));
        assertThat(matches.get(17).getStatus(), is("Nepal won by 71 runs"));

    }
}



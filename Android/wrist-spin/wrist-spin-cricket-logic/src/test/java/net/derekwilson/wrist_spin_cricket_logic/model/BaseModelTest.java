package net.derekwilson.wrist_spin_cricket_logic.model;

import net.derekwilson.wrist_spin_cricket_logic.MockingAnnotationSetup;
import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.mockito.Mock;

import java.util.Calendar;

public class BaseModelTest extends MockingAnnotationSetup {
    protected static final String BATTER_1 = "BATTER-1";
    protected static final String BATTER_2 = "BATTER-2";

    @Mock protected ILoggingSink mockLogger;

    protected Calendar getTestNow() {
        var now = Calendar.getInstance();
        now.set(2017, 11, 30, 15, 37, 22);
        now.set(Calendar.MILLISECOND, 0);
        return now;
    }

    protected MatchState getMatchState(MatchState.CurrentState state, int runs, int wickets, int completeOvers, int balls) {
        Score score = new Score(runs,wickets);
        Overs overs = new Overs(completeOvers, balls);
        MatchState matchState = new MatchState(score, overs);
        matchState.setCurrentState(state);
        matchState.getTeam1().setAbbreviation("TEAM1 ABBREV");
        matchState.getTeam2().setAbbreviation("TEAM2 ABBREV");
        matchState.setBattingTeamAbbreviation("BATTING TEAM");
        matchState.setStrikerName(BATTER_1);
        matchState.setNonStrikerName(BATTER_2);
        return matchState;
    }
}

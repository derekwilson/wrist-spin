package net.derekwilson.wrist_spin_cricket_logic.model.matchstate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.model.BaseModelTest;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.junit.Before;
import org.junit.Test;

public class ScoreTests extends BaseModelTest {
    private MatchState matchState;

    @Before
    public void setUp() {
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
    }

    @Test
    public void getScoreDisplay_dormant() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Dormant);

        // act
        String result = matchState.getScoreDisplay();

        // assert
        assertThat(result, is("TEAM1 ABBREV v TEAM2 ABBREV"));
    }

    @Test
    public void getScoreDisplay_complete() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Complete);

        // act
        String result = matchState.getScoreDisplay();

        // assert
        assertThat(result, is("TEAM1 ABBREV v TEAM2 ABBREV"));
    }

    @Test
    public void getScoreDisplay_current() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Current);

        // act
        String result = matchState.getScoreDisplay();

        // assert
        assertThat(result, is("10/1"));
    }

    @Test
    public void getScoreDisplayWithOvers_dormant() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Dormant);

        // act
        String result = matchState.getScoreDisplayWithOvers();

        // assert
        assertThat(result, is("TEAM1 ABBREV v TEAM2 ABBREV"));
    }

    @Test
    public void getScoreDisplayWithOvers_complete() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Complete);

        // act
        String result = matchState.getScoreDisplayWithOvers();

        // assert
        assertThat(result, is("TEAM1 ABBREV v TEAM2 ABBREV"));
    }

    @Test
    public void getScoreDisplayWithOvers_current() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Current);

        // act
        String result = matchState.getScoreDisplayWithOvers();

        // assert
        assertThat(result, is("10/1 5.1"));
    }
}

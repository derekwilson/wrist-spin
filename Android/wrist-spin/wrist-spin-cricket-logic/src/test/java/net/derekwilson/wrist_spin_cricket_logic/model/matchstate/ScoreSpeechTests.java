package net.derekwilson.wrist_spin_cricket_logic.model.matchstate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.model.BaseModelTest;
import net.derekwilson.wrist_spin_cricket_logic.model.MatchState;

import org.junit.Before;
import org.junit.Test;

public class ScoreSpeechTests extends BaseModelTest {
    private MatchState matchState;

    @Before
    public void setUp() {
        matchState = getMatchState(MatchState.CurrentState.Current, 10, 1, 5, 1);
    }

    @Test
    public void getScoreSpeech_dormant() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Dormant);

        // act
        String result = matchState.getScoreSpeech(false);

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getScoreSpeech_complete() {
        // arrange
        matchState.setCurrentState(MatchState.CurrentState.Complete);

        // act
        String result = matchState.getScoreSpeech(false);

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getScoreSpeech_current() {
        // arrange

        // act
        String result = matchState.getScoreSpeech(false);

        // assert
        assertThat(result, is("10 for 1"));
    }

    @Test
    public void getScoreSpeech_current_0_wickets() {
        // arrange
        matchState.getScore().setWickets(0);

        // act
        String result = matchState.getScoreSpeech(false);

        // assert
        assertThat(result, is("10 without loss"));
    }

    @Test
    public void getScoreSpeech_current_10_wickets() {
        // arrange
        matchState.getScore().setWickets(10);

        // act
        String result = matchState.getScoreSpeech(false);

        // assert
        assertThat(result, is("10 all out"));
    }

    @Test
    public void getScoreSpeech_current_with_team() {
        // arrange

        // act
        String result = matchState.getScoreSpeech(true);

        // assert
        assertThat(result, is("BATTING TEAM, 10 for 1"));
    }

}

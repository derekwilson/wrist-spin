package net.derekwilson.wrist_spin_cricket_logic.model.alerts;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import net.derekwilson.wrist_spin_cricket_logic.model.Alerts;
import net.derekwilson.wrist_spin_cricket_logic.model.BaseModelTest;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

public class AlertsTests extends BaseModelTest {
    private Alerts alerts;

    @Before
    public void setUp() {
        alerts = new Alerts();
    }

    @Test
    public void getMinutesSinceLastBallBowled_nulls() {
        // arrange

        // act
        long result = alerts.getMinutesSinceLastBallBowled(getTestNow());

        // assert
        assertThat(result, is(0L));
    }

    @Test
    public void getMinutesSinceLastBallBowled_earlier() {
        // arrange
        alerts.setLastBallBowled(getTestNow());
        Calendar ballTime = getTestNow();
        ballTime.add(Calendar.SECOND, -60);

        // act
        long result = alerts.getMinutesSinceLastBallBowled(ballTime);

        // assert
        assertThat(result, is(0L));
    }

    @Test
    public void getMinutesSinceLastBallBowled_live() {
        // arrange
        alerts.setLastBallBowled(getTestNow());
        Calendar ballTime = getTestNow();
        ballTime.add(Calendar.SECOND, 120);

        // act
        long result = alerts.getMinutesSinceLastBallBowled(ballTime);

        // assert
        assertThat(result, is(2L));
    }

    @Test
    public void getDisplayStale_live() {
        // arrange
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -2);
        alerts.setLastBallBowled(now);

        // act
        String result = alerts.getDisplayStale();

        // assert
        assertThat(result, is(""));
    }

    @Test
    public void getDisplayStale_stale() {
        // arrange
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -7);
        alerts.setLastBallBowled(now);

        // act
        String result = alerts.getDisplayStale();

        // assert
        assertThat(result, is("7 mins old"));
    }

    @Test
    public void getDisplayStale_very_stale() {
        // arrange
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -70);
        alerts.setLastBallBowled(now);

        // act
        String result = alerts.getDisplayStale();

        // assert
        assertThat(result, is("> 60 mins old"));
    }

}

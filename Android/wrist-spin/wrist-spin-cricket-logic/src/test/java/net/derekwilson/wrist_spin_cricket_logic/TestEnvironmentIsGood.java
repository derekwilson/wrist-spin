package net.derekwilson.wrist_spin_cricket_logic;

import static org.mockito.Mockito.verify;

import net.derekwilson.wrist_spin_cricket_logic.logging.ILoggingSink;

import org.junit.Test;
import org.mockito.Mock;

public class TestEnvironmentIsGood extends MockingAnnotationSetup {

    @Mock
    ILoggingSink mockLogger;

    @Test
    public void mockito_can_verify() {
        // arrange

        // act
        mockLogger.debug("TEST");

        // assert
        verify(mockLogger).debug("TEST");
    }
}



# wrist-spin Release Archive
This is the release archive for wrist-spin. If you cannot get an APK from a play store you can side-load it from here

## APKs

| Version | Date        | MinSDK          | TargetSDK
| ------- | ----------- | --------------- | ---------------
| 1.4.0   | 26 Aug 2024 | 21 (Android 5)  | 34 (Android 14)
| 1.3.0   | 24 May 2024 | 21 (Android 5)  | 34 (Android 14)
| 1.2.0   |  8 May 2024 | 21 (Android 5)  | 34 (Android 14)
| 1.1.0   | 30 Apr 2024 | 21 (Android 5)  | 34 (Android 14)
| 1.0.0   | 30 Apr 2024 | 21 (Android 5)  | 34 (Android 14)

## Notes

Major changes for each version

### v1.4.0 (5) - 26 Aug 2024
- Added ability to suppress notification updates unless a chime type event occurs
- Fixed chiming for individual scores of 50/100

### v1.3.0 (4) - 24 May 2024
- Added clock to lock screen

### v1.2.0 (3) - 8 May 2024
- Added chimes and speech alerts for individual 50 and 100's
- Added ability to follow or override phone "do not disturb" and ringer muted for speech alerts
- Added runs needed to text display for fourth innings
- Fixed an issue with setting lock screen permission on Android 5

### v1.1.0 (2) - 30 Apr 2024

- Fixed issue when clicking on the blank area of the lock screen
- Fixed issue on launch when using Kindle devices running Android 5

### v1.0.0 (1) - 30 Apr 2024

- Initial version


# wrist-spin

A live cricket score notifier for Fitbit Versa

This app displays the score for a selected cricket match. A variety of sizes and detail are available. This is a port of the original Pebble App

## Requirement
### Local
- Node.js 8.+

(I used Node v10.16.0)

### Fitbit Studio
- None

## Build
### Local

```sh
$ git clone https://bitbucket.org/derekwilson/wrist-spin.git
$ cd wrist-spin/Fitbit-os4
wrist-spin/Fitbit$ npm install
wrist-spin/Fitbit$ npx fitbit
fitbit$ build
fitbit$ conncet device
fitbit$ connect phone
fitbit$ install
```

### Fitbit Studio
```sh
$ git clone https://bitbucket.org/derekwilson/wrist-spin.git
```
import * as literals from "./literals.js";
import * as emptyObjects from "./objects.js";

/////////////////////////////////////////////
// split point for test harness
/////////////////////////////////////////////


// init from browser

export function readyEventListener(e) {
  console.log("wrist-spin ready");
  log_watch_info();
  perform_action(literals.ACTION_READY);
}

let running_on_fitbit = false;
let last_fact_ball = 0;

// init from fitbit

export function initialize() {
  console.log("wrist-spin companion running on fitbit");
  running_on_fitbit = true;
//  perform_action("SELECT-REFRESH", "0", "CRICINFO");
//  perform_action("SELECT-REFRESH", "0", "CRICINFO2");
//  perform_action("SELECT-REFRESH", "0", "CRICSCORE");
//  perform_action("REFRESH", "1192569", "CRICINFO2");
//  send_test_request();
}

function create_network_request() {
  if (running_on_fitbit == true) {
    var req = new XMLHttpRequest();
    return req;
  }
  return new XMLHttpRequest();
}

function create_dom_parser() {
  if (running_on_fitbit == true) {
    return new DOMParser();
  }
  if (window.DOMParser) {
    return new DOMParser();
  }
  return null;
}

/////////////
// cricscore
/////////////

var strContains = function (str, token) {
  var index = str.indexOf(token);
  if (index == -1) {
    return false;
  }
  return true;
}

var strBetween = function (str,start,end) {
  var start_index = str.indexOf(start);
  if (start_index == -1) {
    return "";
  }
  var trimmed_str = str.substr(start_index + start.length);
  var end_index = trimmed_str.indexOf(end);
  if (end_index == -1) {
    // we cannot find the end - just return the rest of the string
    return trimmed_str;
  }
  return trimmed_str.substr(0,end_index);
};

var strBefore = function (str,seperator) {
  var index = str.indexOf(seperator);
  if (index == -1) {
    return str;
  }
  return str.substr(0,index);
}

var strBeforeLast = function (str,seperator) {
  var index = str.lastIndexOf(seperator);
  if (index == -1) {
    return str;
  }
  return str.substr(0,index);
}

var strAfter = function (str,seperator) {
  var index = str.indexOf(seperator);
  if (index == -1) {
    return str;
  }
  return str.substr(index+seperator.length);
}

var strAfterLast = function (str,seperator) {
  var index = str.lastIndexOf(seperator);
  if (index == -1) {
    return str;
  }
  return str.substr(index+seperator.length);
}

var batsmanScore = function (str) {
  var score = strAfterLast(str, " ");
  if (score.length > 0 && score[score.length-1] == '*') {
    return score.substr(0,score.length-1);
  }
  return score;
}

var playerName = function (str) {
  var name = strBeforeLast(str, " ");
  name = strAfterLast(name, " ");
  return name;
}

function unpack_cricscore_match_data (data, out) {
  var scoreDataString = data[0].de;
  console.log("DE:"+scoreDataString);
  var bracketPart = strBetween(scoreDataString,"(",")");
  if (!bracketPart || bracketPart.length < 1) {
    // there doesnt seem to be a score here
    out.striker_name = scoreDataString;
    return;
  }
  if (scoreDataString.indexOf(")") != -1) {
    // the is an end bracket
    out.lead = strAfterLast(scoreDataString,")");
  }

  var mainScore = strBefore(scoreDataString, "(");
  out.team_batting = strBefore(mainScore, " ").trim();
  out.score = strAfter(mainScore, " ").trim();

  var bracketBits = bracketPart.split(',');
  var charIndex = -1;
  for (var index = 0; index < bracketBits.length; index++) {
    switch (index) {
      case 0:
        out.overs = strBefore(bracketBits[index], " ");
        break;
      case 1:
        out.striker_name = playerName(bracketBits[index]);
        out.striker_stats = batsmanScore(bracketBits[index]);
        out.striker_score = batsmanScore(bracketBits[index]);
        break;
      case 2:
        out.nonstriker_name = playerName(bracketBits[index]);
        out.nonstriker_stats = batsmanScore(bracketBits[index]);
        out.nonstriker_score = batsmanScore(bracketBits[index]);
        break;
      case 3:
        out.bowler_name = playerName(bracketBits[index]);
        out.bowler_stats = strAfterLast(bracketBits[index], " ");
        break;
    }
  }
};

function output_cricscore_match(data, index, out) {
  console.log("outputting match index = ", index);
  out.selection_game_id = "" + data[index].id;  // make sure it gets treated as a string
  out.team_1 = data[index].t1;
  out.team_2 = data[index].t2;
}

function unpack_cricscore_match_list_data (action, matchId, data, out) {
  console.log("current match ID = ", matchId);
  out.action = action;
  // exclude there being no data
  if (data.length < 1) {
    out = emptyObjects.new_out_error("No games in progress");
    return out;
  }

  // exclude getting the whole list
  if (action == literals.ACTION_SELECT_REFRESH_LIST) {
    out = emptyObjects.new_out_match_select_list();
    out.action = action;
    for (var index = 0; index<data.length; index++) {
      var out_item = emptyObjects.new_out_match_select_item();
      output_cricscore_match(data, index, out_item);
      out.matches.push(out_item);
    }
    return out;
  }

  // find the one match to output
  if (!matchId || matchId=="0") {
    // there is no current match just get the first one
    output_cricscore_match(data, 0, out);
    return out;
  }

  var matchIndex = 0;
  for (var index = 0; index<data.length; index++) {
    if (data[index].id == matchId) {
      // we have found the current match
      switch (action) {
        case literals.ACTION_SELECT_REFRESH:
          matchIndex = index;
          break;
        case literals.ACTION_SELECT_PREVIOUS:
          matchIndex = index;
          if (index > 0) {
            matchIndex = index - 1;
          }
          break;
        case literals.ACTION_SELECT_NEXT:
          matchIndex = index;
          if (index < data.length-1) {
            matchIndex = index + 1;
          }
      }
    }
  }
  output_cricscore_match(data, matchIndex, out);
  return out;
}

////////////
// cricapi
////////////

function unpack_cricapi_match_data (data, out) {
  var scoreDataString = data.score;
  console.log("score:"+scoreDataString);
  var bracketPart = strBetween(scoreDataString,"(",")");
  if (!bracketPart || bracketPart.length < 1) {
    // there doesnt seem to be a score here
    out.striker_name = scoreDataString;
    return;
  }
  if (scoreDataString.indexOf(")") != -1) {
    // the is an end bracket
    out.lead = strAfterLast(scoreDataString,")");
  }

  var mainScore = strBefore(scoreDataString, "(");
  out.team_batting = strBefore(mainScore, " ").trim();
  out.score = strAfter(mainScore, " ").trim();

  var bracketBits = bracketPart.split(',');
  var charIndex = -1;
  for (var index = 0; index < bracketBits.length; index++) {
    switch (index) {
      case 0:
        out.overs = strBefore(bracketBits[index], " ");
        break;
      case 1:
        out.striker_name = playerName(bracketBits[index]);
        out.striker_stats = batsmanScore(bracketBits[index]);
        out.striker_score = batsmanScore(bracketBits[index]);
        break;
      case 2:
        out.nonstriker_name = playerName(bracketBits[index]);
        out.nonstriker_stats = batsmanScore(bracketBits[index]);
        out.nonstriker_score = batsmanScore(bracketBits[index]);
        break;
      case 3:
        out.bowler_name = playerName(bracketBits[index]);
        out.bowler_stats = strAfterLast(bracketBits[index], " ");
        break;
    }
  }
};

function output_cricapi_match(data, index, out) {
  console.log("outputting match index = ", index);
  out.selection_game_id = "" + data[index].unique_id;  // make sure it gets treated as a string
  var title = data[index].title;
  out.team_1 = strBefore(title," v ").replace(" \&amp; ",",");
  out.team_2 = strAfter(title, " v ").replace(" \&amp; ",",");
}

function unpack_cricapi_match_list_data (action, matchId, jsonObject, out) {
  var data = jsonObject.data;
  console.log("current match ID = ", matchId);
  out.action = action;
  if (data.length < 1) {
    out.team_1 = "No games";
    out.team_2 = "in progress";
    return;
  }
  if (!matchId || matchId=="0") {
    // there is no current match just get the first one
    output_cricapi_match(data, 0, out);
    return;
  }

  var matchIndex = 0;
  for (var index = 0; index<data.length; index++) {
    if (data[index].unique_id == matchId) {
      // we have found the current match
      switch (action) {
        case literals.ACTION_SELECT_REFRESH:
          matchIndex = index;
          break;
        case literals.ACTION_SELECT_PREVIOUS:
          matchIndex = index;
          if (index > 0) {
            matchIndex = index - 1;
          }
          break;
        case literals.ACTION_SELECT_NEXT:
          matchIndex = index;
          if (index < data.length-1) {
            matchIndex = index + 1;
          }
      }
    }
  }
  output_cricapi_match(data, matchIndex, out);
}

////////////
// cricinfo
////////////

// match list RSS

function idFromItem(items, index) {
  var guid = items[index].getElementsByTagName('guid')[0].textContent;
  var id = strBefore(strAfterLast(guid, "/match/"),".");
  return id;
}

function output_cricinfo_match(items, index, out) {
  console.log("outputting match index = ", index);
  var title = items[index].getElementsByTagName('title')[0].textContent;
  out.selection_game_id = "" + idFromItem(items, index);  // make sure it gets treated as a string
  out.team_1 = strBefore(title," v ");
  out.team_2 = strAfter(title, " v ");
}

function unpack_cricinfo_match_list_data(action, matchId, xmlDoc, out) {
  var items =  xmlDoc.getElementsByTagName("item");
  console.log("games found = " + items.length);
  out.action = action;

  // exclude there being no data
  if (items.length < 1) {
    out = emptyObjects.new_out_error("No games in progress");
    return out;
  }

  // exclude getting the whole list
  if (action == literals.ACTION_SELECT_REFRESH_LIST) {
    out = emptyObjects.new_out_match_select_list();
    out.action = action;
    for (var index = 0; index<items.length; index++) {
      var out_item = emptyObjects.new_out_match_select_item();
      output_cricinfo_match(items, index, out_item);
      out.matches.push(out_item);
    }
    return out;
  }

  // find the one match to output
  if (!matchId || matchId=="0") {
    // there is no current match just get the first one
    output_cricinfo_match(items, 0, out);
    return out;
  }
  var matchIndex = 0;
  for (var index = 0; index<items.length; index++) {
    if (idFromItem(items, index) == matchId) {
      // we have found the current match
      switch (action) {
        case literals.ACTION_SELECT_REFRESH:
          matchIndex = index;
          break;
        case literals.ACTION_SELECT_PREVIOUS:
          matchIndex = index;
          if (index > 0) {
            matchIndex = index - 1;
          }
          break;
        case literals.ACTION_SELECT_NEXT:
          matchIndex = index;
          if (index < items.length-1) {
            matchIndex = index + 1;
          }
      }
    }
  }
  output_cricinfo_match(items, matchIndex, out);
  return out;
}

// match list HTML

function idFromSection(items, index) {
  var titleSpan = items[index].querySelectorAll('span.match-no')
  var title = titleSpan[0].getElementsByTagName('a')[0].href;
  console.log("title = ", title);
  var id = "";
  if (strContains(title, "/game/")) {
    id = strBefore(strAfterLast(title, "/game/"),"/");
    return id;
  }
  if (strContains(title, "/scorecard/")) {
    id = strBefore(strAfterLast(title, "/scorecard/"),"/");
    return id;
  }
  return "";
}

function teamFromSection(items, index, teamNo) {
  var teamDiv = items[index].querySelectorAll('div.innings-info-' + teamNo)
  var team = strBefore(teamDiv[0].textContent, "(");
  return team.replace(" \&amp; ",",").replace("\n","").replace("\n","").trim();
}

//div.innings-info-1

function output_cricinfo_2_match(items, index, out) {
  console.log("outputting match index = ", index);
  out.selection_game_id = "" + idFromSection(items, index);  // make sure it gets treated as a string
  out.team_1 = "" + teamFromSection(items, index, "1");
  out.team_2 = "" + teamFromSection(items, index, "2");
}

function unpack_cricinfo_2_match_list_data(action, matchId, htmlDoc, out) {
  var items =  htmlDoc.querySelectorAll('section[data-matchstatus]');
  console.log("games found = " + items.length);
  out.action = action;

  // exclude there being no data
  if (items.length < 1) {
    out = emptyObjects.new_out_error("No games in progress");
    return out;
  }

  // exclude getting the whole list
  if (action == literals.ACTION_SELECT_REFRESH_LIST) {
    out = emptyObjects.new_out_match_select_list();
    out.action = action;
    for (var index = 0; index<items.length; index++) {
      var out_item = emptyObjects.new_out_match_select_item();
      output_cricinfo_2_match(items, index, out_item);
      out.matches.push(out_item);
    }
    return out;
  }

  // find the one match to output
  if (!matchId || matchId=="0") {
    // there is no current match just get the first one
    output_cricinfo_2_match(items, 0, out);
    return out;
  }
  var matchIndex = 0;
  for (var index = 0; index<items.length; index++) {
    if (idFromSection(items, index) == matchId) {
      // we have found the current match
      switch (action) {
        case literals.ACTION_SELECT_REFRESH:
          matchIndex = index;
          break;
        case literals.ACTION_SELECT_PREVIOUS:
          matchIndex = index;
          if (index > 0) {
            matchIndex = index - 1;
          }
          break;
        case literals.ACTION_SELECT_NEXT:
          matchIndex = index;
          if (index < items.length-1) {
            matchIndex = index + 1;
          }
      }
    }
  }
  output_cricinfo_2_match(items, matchIndex, out);
  return out;
}


// cricinfo json decoder code was here
// https://github.com/robn/sixfour

var measure = function (n) {
    var len = n.length;
    var nspaces = (n.match(/ /g) || []).length;
    var nwide   = (n.match(/[WwMm]/g) || []).length;
    var nnarrow = (n.match(/[Iil]/g) || []).length;
    var nnormal = n.length - nspaces - nwide - nnarrow;
    return nnormal + nspaces*0.8 + nnarrow*0.5 + nwide*1.5;
};

var shorten = function (n) {
    var bits = n.split(' ');
    for (var i = 0; i < bits.length-1; i++)
        bits[i] = bits[i].substr(0,1);
    return bits.join('.');
};

var player_pretty_name = function (p) {
    var name = p.card_short;
    if (measure(name) > 10 && p.popular_name.length > 0)
        name = p.popular_name;
    if (measure(name) > 10)
        name = shorten(name);
    return name;
};

function unpack_cricinfo_match_data (data, out) {
    var teams = {};
    var players = {};
    var playerByShortName = {};
    var playerByLongName = {};
    data.team.forEach(function (team) {
        teams[team.team_id] = team;
        var playerList = team.player || team.squad;
        playerList.forEach(function (player) {
            players[player.player_id] = player;
            playerByShortName[player.card_short] = player;
            playerByLongName[player.card_long] = player;
        });
    });

    var match_status = data.match.match_status;
    if ((!match_status || match_status === "current") && data.match.result !== "0")
        match_status = "complete";

    if (match_status === "current" && !data.live.innings.batting_team_id) {
        match_status = "dormant";
    }

    switch (match_status) {
        case "dormant":
            out.score = data.match.team1_abbreviation.toUpperCase()+" v "+data.match.team2_abbreviation.toUpperCase();

            if (data.live["break"]) {
                out.fact = data.live["break"];
            }

            if (data.match.match_clock && data.match.match_clock !== "") {
                out.lead = "Match starts in "+data.match.match_clock;
            }
            else if (out.fact !== "") {
                out.lead = out.fact;
                out.fact = "";
            }

            if (data.match.toss_decision && data.match.toss_decision !== "" && data.match.toss_decision !== "0") {
                out.striker_name = teams[data.match.toss_winner_team_id].team_short_name+" won toss,";
                out.nonstriker_name = "will "+data.match.toss_decision_name;
            }
            break;

        case "complete": {
            out.score = data.match.team1_abbreviation.toUpperCase()+" v "+data.match.team2_abbreviation.toUpperCase();

            if (data.match.winner_team_id == "0") {
                out.striker_name = "Match drawn";
            }
            else {
                out.striker_name = teams[data.match.winner_team_id].team_short_name+" won by";
                if (data.match.amount_name === "innings") {
                    out.nonstriker_name = "innings and "+data.match.amount+" runs";
                }
                else {
                    out.nonstriker_name = data.match.amount+" "+data.match.amount_name;
                    if (data.match.amount_balls && data.match.amount_balls > 0) {
                        out.bowler_name = "(" + data.match.amount_balls + " balls remaining)";
                    }
                }
            }
            break;
        }

        case "current": {
            var innings = data.live.innings;

            out.team_batting = teams[innings.batting_team_id].team_abbreviation.toUpperCase();
            out.score = [
                innings.runs,
                (innings.wickets < 10) ? "/"+innings.wickets : "",
                ((innings.event && innings.event == "declared") ? "D" : "")
            ].join('');

            out.overs = innings.overs;

            if (+data.match.scheduled_overs > 0) {
                switch (+innings.innings_number) {
                    case 1:
                        out.lead = "Run rate: "+innings.run_rate;
                        break;
                    default:
                        out.lead =
                            "Need "+(1-innings.lead)+" from "+
                            (innings.remaining_overs <= 10.0 ? innings.remaining_balls : innings.remaining_overs+" ov");
                        break;
                }
            }
            else {
                switch (+innings.innings_number) {
                    case 1:
                        out.lead = "First innings";
                        break;
                    case 4:
                        out.lead = "Target "+(innings.target);
                        break;
                    default:
                        out.lead =
                            innings.lead < 0 ? "Trail by "+(-innings.lead) :
                            innings.lead > 0 ? "Lead by "+innings.lead :
                                            "Scores level";
                }
            }

            var striker = data.live.batting.filter(function (player) { return player.live_current_name == "striker"; })[0];
            var nonstriker = data.live.batting.filter(function (player) { return player.live_current_name == "non-striker"; })[0];

            if (!striker) {
                striker = nonstriker;
                nonstriker = null;
            }

            if (striker) {
                out.striker_name = player_pretty_name(players[striker.player_id]);
                out.striker_stats = striker.runs+" ("+striker.balls_faced+")";
                out.striker_score = striker.runs;
            }

            if (nonstriker) {
                out.nonstriker_name = player_pretty_name(players[nonstriker.player_id]);
                out.nonstriker_stats = nonstriker.runs+" ("+nonstriker.balls_faced+")";
                out.nonstriker_score = nonstriker.runs;
            }

            var bowler = data.live.bowling.filter(function (player) { return player.live_current_name == "current bowler"; })[0];
            if (bowler) {
                out.bowler_name = player_pretty_name(players[bowler.player_id]);
                out.bowler_stats = bowler.overs+"-"+bowler.maidens+"-"+bowler.conceded+"-"+bowler.wickets;
            }

            var facts = [];

            if (data.match.live_state) {
                facts.push(data.match.live_state);
            }

            var factBall = last_fact_ball || 0;

            console.log("lastFactBall: "+factBall);

            var newLastFactBall = factBall;
            data.comms.forEach(function (over) {
                over.ball.forEach(function (ball) {
                    if (ball.overs_unique > newLastFactBall) {
                        newLastFactBall = ball.overs_unique;
                    }

                    if (ball.event) {
                        var fact;

                        var ev = ball.event.match(/OUT|SIX|FOUR/);
                        if (!ev && ball.dismissal) {
                            ev = ["OUT"];
                        }

                        if (ev) switch (ev[0]) {

                            case "OUT":
                                var dismissal =
                                    ball.dismissal
                                    .replace(/\s+/g, " ")
                                    .replace("&dagger;", "\u2020")
                                    .replace("&amp;", "&")
                                    .match(/(.+?) (lbw b|hit wicket b|run out|retired hurt|c \& b|c|b|st) ((?:.(?! (?:b|\d+)))+.)/);

                                fact = player_pretty_name(playerByShortName[dismissal[1]] || playerByLongName[dismissal[1]]) +
                                        " " + dismissal[2] +
                                        (dismissal[2].match(/run out|retired hurt/) ? "" : " " + dismissal[3]);
                                break;

                            case "SIX":
                            case "FOUR":
                                var name = ball.players.match(/to (.+)$/)[1];
                                fact = ev[0] + " " + player_pretty_name(playerByShortName[name] || playerByLongName[name]);
                                break;
                        }

                        if (fact) {
                            facts.push([ball.overs_actual,fact].join(' '));
                        }
                    }
                });
            });

            facts.forEach(function (fact) { console.log("FACT: "+fact); });

            console.log("newLastFactBall: "+newLastFactBall);
            if (newLastFactBall > factBall) {
                last_fact_ball = newLastFactBall;
            }

            // XXX send the lot to watch and have it cycle
            if (facts.length > 0) {
                out.fact = facts[0];
            }

            break;
        }
    }
}

function update_match (action, matchId, dataProvider) {
    var out = emptyObjects.new_out_match();
    out.action = action;
    console.log("match ID = ", matchId);
    if (!matchId || matchId=="0") {
        out.striker_name = "No match";
        out.nonstriker_name = "selected";
        send_message_to_watch(out);
        return;
    }
    var req = create_network_request();
    switch (dataProvider) {
      case literals.PROVIDER_CRICINFO:
      case literals.PROVIDER_CRICINFO_2:
        req.open('GET', "https://www.espncricinfo.com/ci/engine/match/"+matchId+".json");
        //req.open('GET', "https://derekapi.azurewebsites.net/cricinfo-match/"+matchId);
        break;
      case literals.PROVIDER_CRICSCORE:
        req.open('GET', "https://cricscore-api.appspot.com/csa?id="+matchId);
        break;
      case literals.PROVIDER_CRICAPI:
        req.open("POST", "http://www.cricapi.com/api/cricketScore/");
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        break;
      default:
        out.striker_name = "Unknown";
        out.nonstriker_name = "Provider";
        send_message_to_watch(out);
        return;
    }
    req.onerror = function(e) {
      console.log("update_match - error in request");
      out.striker_name = "Error in";
      out.nonstriker_name = "Request";
      send_message_to_watch(out);
    };
    req.onload = function(e) {
        if (req.readyState == 4) {
            if(req.status == 200) {
                console.log("got new match data, updating");
                //console.log(req.responseText);

                var data = JSON.parse(req.responseText);
                if (data) {
                  switch (dataProvider) {
                      case literals.PROVIDER_CRICINFO:
                      case literals.PROVIDER_CRICINFO_2:
                        unpack_cricinfo_match_data(data, out);
                        break;
                      case literals.PROVIDER_CRICSCORE:
                        console.log(req.responseText);
                        unpack_cricscore_match_data(data, out);
                        break;
                      case literals.PROVIDER_CRICAPI:
                        console.log(req.responseText);
                        unpack_cricapi_match_data(data, out);
                        break;
                    }
                    console.log(JSON.stringify(out));
                    send_message_to_watch(out);
                }

            } else {
                console.log("Update error " + req.status);
                out.lead = "Error: " + req.status;
                send_message_to_watch(out);
            }
        }
    };
    switch (dataProvider) {
      case literals.PROVIDER_CRICINFO:
      case literals.PROVIDER_CRICINFO_2:
      case literals.PROVIDER_CRICSCORE:
        req.send(null);
        break;
      case literals.PROVIDER_CRICAPI:
        var req_obj = {};
        req_obj.unique_id = "" + matchId;
        req_obj.apikey = "" + literals.CRICAPI_KEY;
        req.send(JSON.stringify(req_obj));
        break;
    }
}

function update_match_selection (action, matchId, dataProvider) {
    var out = emptyObjects.new_out_match_select();
    out.action = action;
    var req = create_network_request();
    switch (dataProvider) {
      case literals.PROVIDER_CRICINFO:
        req.open('GET', "https://static.espncricinfo.com/rss/livescores.xml");
//        req.open('GET', "https://derekapi.azurewebsites.net/cricinfo-rss");
        break;
      case literals.PROVIDER_CRICINFO_2:
        req.open('GET', "https://www.espncricinfo.com/ci/engine/match/index/live.html");
//        req.open('GET',"https://derekapi.azurewebsites.net/cricinfo-matches-html")
        break;  
      case literals.PROVIDER_CRICAPI:
        req.open('POST', "http://www.cricapi.com/api/cricket/");
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        break;
      case literals.PROVIDER_CRICSCORE:
        req.open('GET', "https://cricscore-api.appspot.com/csa");
        break;
      default:
        out = emptyObjects.new_out_error("Unknown Provider");
        send_message_to_watch(out);
        return;
    };
    req.onerror = function(e) {
      console.log("update_match_selection - error in request");
      out = emptyObjects.new_out_error("error in request");
      send_message_to_watch(out);
    };
    req.onload = function(e) {
        if (req.readyState == 4) {
            if(req.status == 200) {
                console.log("got new match selection data, updating");
//                console.log(req.responseText);

                if (req.responseText) {
                  switch (dataProvider) {
                      case literals.PROVIDER_CRICINFO:
                        var parser = create_dom_parser();
                        if (parser) {
                          var xmlDoc = parser.parseFromString(req.responseText,"text/xml");
                          out = unpack_cricinfo_match_list_data(action, matchId, xmlDoc, out);
                        } else {
                          console.log("No DOMParser");
                          out = emptyObjects.new_out_error("No DOMParser");
                        }
                        break;

                      case literals.PROVIDER_CRICINFO_2:
                        var parser = create_dom_parser();
                        if (parser) {
                          var htmlDoc = parser.parseFromString(req.responseText, "text/html");
                          out = unpack_cricinfo_2_match_list_data(action, matchId, htmlDoc, out);
                        } else {
                          console.log("No DOMParser");
                          out = emptyObjects.new_out_error("No DOMParser");
                        }
                        break;

                      case literals.PROVIDER_CRICAPI:
                        var data = JSON.parse(req.responseText);
                        if (data) {
                          unpack_cricapi_match_list_data(action, matchId, data, out);
                        }
                        break;

                      case literals.PROVIDER_CRICSCORE:
                        var data = JSON.parse(req.responseText);
                        if (data) {
                          out = unpack_cricscore_match_list_data(action, matchId, data, out);
                        }
                        break;

                    }
                    console.log(JSON.stringify(out));
                    send_message_to_watch(out);
                }
            } else {
                console.log("Match Selection Error " + req.status);
                out = emptyObjects.new_out_error("Error getting" + req.status);
                send_message_to_watch(out);
            }
        }
    };
    switch (dataProvider) {
      case literals.PROVIDER_CRICINFO:
      case literals.PROVIDER_CRICINFO_2:
      case literals.PROVIDER_CRICSCORE:
        console.log("sending request");
        req.send(null);
        break;
      case literals.PROVIDER_CRICAPI:
        console.log("sending request");
        req.send(JSON.stringify({"apikey": "" + literals.CRICAPI_KEY}));
        break;
    }
    console.log("request sent");
}

function send_action_to_watch(action) {
  var out = {
    "action":action
  };
  send_message_to_watch(out);
}

export function perform_action(action, matchId, dataProvider) {
  console.log("wrist-spin action " + action + " ID " + matchId + " PROV " + dataProvider);
  switch (action) {
    case literals.ACTION_READY:
      send_action_to_watch(action);
      break;
    case literals.ACTION_REFRESH:
    case literals.ACTION_SELECT_MATCH:
      update_match(action, matchId, dataProvider);
      break;
    case literals.ACTION_SELECT_PREVIOUS:
    case literals.ACTION_SELECT_NEXT:
    case literals.ACTION_SELECT_REFRESH:
    case literals.ACTION_SELECT_REFRESH_LIST:  
      update_match_selection(action, matchId, dataProvider);
      break;
    default:
      console.log("unknown action");
  }
}

function log_watch_info() {
  if (typeof Pebble != "undefined" && Pebble.getActiveWatchInfo) {
    console.log("wrist-spin Platform: " + Pebble.getActiveWatchInfo().platform);
    console.log("wrist-spin Model: " + Pebble.getActiveWatchInfo().model);
    console.log("wrist-spin Language: " + Pebble.getActiveWatchInfo().language);
    console.log("wrist-spin Firmware: v"
      + Pebble.getActiveWatchInfo().firmware.major
      + "."
      + Pebble.getActiveWatchInfo().firmware.minor
      + "."
      + Pebble.getActiveWatchInfo().firmware.patch
      + "."
      + Pebble.getActiveWatchInfo().firmware.suffix
      );
  }
}

function send_test_request() {
//  var url = "https://static.espncricinfo.com/rss/livescores.xml";
//  var url = "https://bit.ly/cricinforss";
//  var url = "https://jan-v.nl/post/enable-ssl-for-your-azure-functions";
//  var url = "https://derekapi.azurewebsites.net/cricinfo-rss";
//  var url = "https://derekapi.azurewebsites.net/cricinfo-matches-html";
  var url = "https://derekapi.azurewebsites.net/cricinfo-match/1152839";

  var myHeaders = new Headers();
  var myInit = { 
    method: 'GET',
    headers: myHeaders,
    cache: 'default' };
  var myRequest = new Request(url, myInit);

  console.log("sending request " + url);
  fetch(myRequest).then(function(response) {
    console.log("response status " + response.status + " " + response.statusText);
    var contentType = response.headers.get('content-type');
    console.log("response content " + contentType);
    return response.text();
  })
  .then(function(text) {
    console.log("response text " + text);
  })
  .catch(function(error) {
    console.log("response error " + error);
  })
}

////////////////////////////////
// bridge code to watch or HTML
////////////////////////////////

// send data to output device

function sendData(data) {
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(data);
  } else {
    console.log("No peerSocket connection");
  }
}

function send_message_to_watch(out) {
  if (running_on_fitbit == true) {
    // we are running on the watch
    sendData(out)
  }
  else {
    // we are running in the test harness html
    document.getElementById('test-harness-results').innerHTML = JSON.stringify(out);
  }
}


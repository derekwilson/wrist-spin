// action sent from JS to phone when the JS is ready
export const ACTION_READY = "READY";
// refresh the scores
export const ACTION_REFRESH = "REFRESH";
// select the given match id and get the scores
export const ACTION_SELECT_MATCH = "SELECT-MATCH";
// get the previous active match
export const ACTION_SELECT_PREVIOUS = "SELECT-PREVIOUS";
// get the next active match
export const ACTION_SELECT_NEXT = "SELECT-NEXT";
// get the current match teams
export const ACTION_SELECT_REFRESH = "SELECT-REFRESH";
// get all the current active matches
export const ACTION_SELECT_REFRESH_LIST = "SELECT-REFRESH-LIST";


export const PROVIDER_CRICINFO = "CRICINFO";
export const PROVIDER_CRICINFO_2 = "CRICINFO2";
export const PROVIDER_CRICSCORE = "CRICSCORE";
export const PROVIDER_CRICAPI = "CRICAPI2";
export const CRICAPI_KEY = "oaxpTMPlXKSYrv9G9x7LM4gjxut1";




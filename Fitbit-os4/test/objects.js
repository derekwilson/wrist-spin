export function new_out_error (message) {
    var out = {
        "isError":true,
        "errorMessage":message
    };
    return out;
}

export function new_out_match () {
    var out = {
        "team_batting":"",
        "score":"",
        "overs":"",
        "lead":"",
        "striker_name":"",
        "striker_stats":"",
        "striker_score":"",
        "nonstriker_name":"",
        "nonstriker_stats":"",
        "nonstriker_score":"",
        "bowler_name":"",
        "bowler_stats":"",
        "fact":"",
        "action":""
    };
    return out;
}

export function new_out_match_select () {
    var out = {
        "selection_game_id":"",
        "team_1":"",
        "team_2":"",
        "action":""
    };
    return out;
}

export function new_out_match_select_list () {
    var out = {
        "matches": [],
        "action":""
    };
    return out;
}

export function new_out_match_select_item () {
    var out = {
        "selection_game_id":"",
        "team_1":"",
        "team_2":"",
    };
    return out;
}

export function new_out_dev () {
    var out = {
        "isError":false,
        "errorMessage":"",
        "game_id":"123",
        "selection_game_id":"456",
        "team_1":"TEAM1",
        "team_2":"TEAM2",
        "team_batting":"BATTING",
        "score":"SCORE",
        "overs":"OVERS",
        "lead":"LEAD STRING",
        "striker_name":"SNAME",
        "striker_stats":"SSTATS",
        "striker_score":"SCORE",
        "nonstriker_name":"NSNAME",
        "nonstriker_stats":"NSTATS",
        "nonstriker_score":"NSCORE",
        "bowler_name":"BNAME",
        "bowler_stats":"BSTATS",
        "fact":"FACT LINE",
        "action":"ACTION"
    };
    return out;
}


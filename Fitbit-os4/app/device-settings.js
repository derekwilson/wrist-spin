/*
  Responsible for loading, applying and saving settings.
*/
import { me } from "appbit";
import * as fs from "fs";
import * as emptyObjects from "../common/objects";
import * as logging from "../common/logging";
import * as alert from "./wrist-spin/alert";

const SETTINGS_TYPE = "cbor";
const SETTINGS_FILE = "settings.cbor";

export let settingsObject;

export function initialize() {
  settingsObject = loadSettings();
  logging.debug("settings: " + JSON.stringify(settingsObject));
}

// Load settings from filesystem
function loadSettings() {
  try {
    return fs.readFileSync(SETTINGS_FILE, SETTINGS_TYPE);
  } catch (ex) {
    logging.error("loadSettings error " + JSON.stringify(ex));
    return emptyObjects.new_settings();
  }
}

// Register for the unload event
me.addEventListener("unload", saveSettings);

// Save settings to the filesystem
function saveSettings() {
  logging.debug("saveSettings: " + JSON.stringify(settingsObject));
  fs.writeFileSync(SETTINGS_FILE, settingsObject, SETTINGS_TYPE);
  if (settingsObject.alertExit) {
    alert.alert_app_exit();
  }
}

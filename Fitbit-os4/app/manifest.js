import * as fs from "fs";
import * as logging from "../common/logging";

const MANIFEST_TYPE = "json";
const MANIFEST_FILE = "/mnt/assets/manifest.json";

let manifestObject = null;

export function getBundleDate() {
  let bundleDateStr = getBundleDateString();
  logging.debug(`bundle date string ${bundleDateStr}`);
  let bundleDate = new Date(bundleDateStr);
  logging.debug(`bundle date ${bundleDate}`);
  return bundleDate;
}

function getBundleDateString() {
  getManifest();
  return manifestObject.bundleDate;
}

function getManifest() {
  if (manifestObject == null) {
    manifestObject = loadManifest();
  }
}

function loadManifest() {
  try {
    return fs.readFileSync(MANIFEST_FILE, MANIFEST_TYPE);
  } catch (ex) {
    logging.error("loadManifest error: " + JSON.stringify(ex));
    return {};
  }
}


import * as messaging from "messaging";
import * as literals from "../../common/literals";
import * as emptyObjects from "../../common/objects";
import * as logging from "../../common/logging";

let clientCallback;
let currentProvider = literals.PROVIDER_CRICINFO;

export function initialize(callback, provider) {
  clientCallback = callback;
  set_provider(provider);
}

export function set_provider(provider) {
  logging.debug(`provider set to be ${provider}`);
  currentProvider = provider;
}

export function is_companion_attached() {
  const readyState = messaging.peerSocket.readyState;
  const connected = (readyState === OPEN);
  logging.debug(`App running - Connectivity status=${readyState} Connected? ${connected}`);
  return connected;
}

export function refresh_match_selection(id) {
  send(
    {
      "action": literals.ACTION_SELECT_REFRESH,
      "id" : id,
      "provider" : currentProvider
    }
  );
}

export function next_match_selection(id) {
  send(
    {
      "action": literals.ACTION_SELECT_NEXT,
      "id" : id,
      "provider" : currentProvider
    }
  );
}

export function previous_match_selection(id) {
  send(
    {
      "action": literals.ACTION_SELECT_PREVIOUS,
      "id" : id,
      "provider" : currentProvider
    }
  );
}

export function refresh_match_score(id) {
  send(
    {
      "action": literals.ACTION_REFRESH,
      "id" : id,
      "provider" : currentProvider
    }
  );
}

// Received message containing cricket data
messaging.peerSocket.addEventListener("message", function(evt) {
  logging.debug("listener fired: " + JSON.stringify(evt));
  clientCallback(evt.data);
})

// Listen for the onerror event
messaging.peerSocket.onerror = function(err) {
  // Handle any errors
  logging.debug("Connection error: " + err.code + " - " + err.message);
}

const OPEN = messaging.peerSocket.OPEN;

// Helpful to check whether we are connected or not.
// setInterval(() => {
//   const readyState = messaging.peerSocket.readyState;
//   console.log(`App running - Connectivity status=${readyState} 
//     Connected? ${readyState === OPEN ? 'YES' : 'no'}`);
// }, 3000);

function send(data) {
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(data);
  } else {
    logging.warning("trying to send when connection is not open");
    var errorObject = emptyObjects.new_out_error("no phone");
    errorObject.noPhone = true;
    clientCallback(errorObject);
  }
}

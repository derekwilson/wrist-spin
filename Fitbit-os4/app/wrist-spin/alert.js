import { vibration } from "haptics";
import * as logging from "../../common/logging";

const MINUTES_FOR_STALE = 5;

let lastScore = null;
let lastOvers = null;
let lastUpdateTime = Date.now();
let noPhoneHasBeenAlerted = true;       // dont start alerting until we have started up

export function alert_app_exit() {
    logging.debug(`alert_app_exit`);
    vibration.stop();
    vibration.start("nudge-max");
}

export function alert_no_phone() {
    logging.debug(`alert_no_phone`);
    if (noPhoneHasBeenAlerted) {
        // we have already done the alert
        logging.debug(`suppressed`);
        return;
    }
    noPhoneHasBeenAlerted = true;
    vibration.stop();
    vibration.start("nudge-max");
}

export function reset_alert_no_phone() {
    logging.debug(`reset_alert_no_phone`);
    noPhoneHasBeenAlerted = false;
}

function alert_wicket_fallen() {
    logging.debug(`alert_wicket_fallen`);
    vibration.stop();
    vibration.start("nudge-max");
}

export function is_stale() {
    var howOld = minutes_since_last_update();
    logging.debug(`is_stale mins ${howOld}`);
    if (howOld >= MINUTES_FOR_STALE) {
        return true;
    }
    return false;
}

function reset_last_update_time() {
    logging.debug("reset last update time");
    lastUpdateTime = Date.now();
}

export function minutes_since_last_update() {
    if (lastUpdateTime == null) {
        return 0;
    }
    var millis = Date.now() - lastUpdateTime;
    if (millis < 1) {
        return 0;
    }
    var seconds = Math.floor(millis/1000);
    if (seconds < 1) {
        return 0;
    }
    return Math.floor(seconds / 60);
}

function get_wickets(score) {
    var numbers = score.split('/');
    if (numbers.length < 2) {
        return null;
    }
    return numbers[1];
}

function has_a_wicket_fallen(newScore) {
    if (lastScore == null) {
        logging.debug("has_a_wicket_fallen: initial state");
        return false;
    }
    if (newScore == null) {
        logging.debug("has_a_wicket_fallen: no new score");
        return false;
    }
    if (lastScore !== newScore) {
        reset_last_update_time();
    }
    var lastWickets = get_wickets(lastScore);
    var newWickets = get_wickets(newScore);
    if (lastWickets == null) {
        logging.debug("has_a_wicket_fallen: no last wickets");
        return false;
    }
    if (newWickets == null) {
        if (lastWickets != null) {
            logging.debug("has_a_wicket_fallen: last wicket or end of innings");
            return true;
        }
        logging.debug("has_a_wicket_fallen: no new wickets");
        return false;
    }
    logging.debug(`has_a_wicket_fallen: ${lastWickets}, ${newWickets}`);
    if (lastWickets !== newWickets) {
        return true;
    }
    return false;
}

function has_overs_changed(newOvers) {
    if (lastOvers == null) {
        logging.debug("has_overs_changed: initial state");
        return false;
    }
    if (newOvers == null) {
        logging.debug("has_overs_changed: no new overs");
        return false;
    }
    logging.debug(`has_overs_changed: ${lastOvers}, ${newOvers}`);
    if (lastOvers !== newOvers) {
        return true;
    }
    return false;
}

export function check_alert_wicket_fall_and_stale_data(data, alertOnWicket) {
    if (has_a_wicket_fallen(data.score)) {
        if (alertOnWicket) {
            alert_wicket_fallen();
        }
    }
    if (has_overs_changed(data.overs)) {
        reset_last_update_time();
    }
    lastScore = data.score;
    lastOvers = data.overs;
}
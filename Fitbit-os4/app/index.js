import { me } from "appbit";
import * as logging from "../common/logging";

logging.set_logging_level(logging.LOGGING_LEVEL_DEBUG);

// this does not appear to work in the simulator - lets hope its OK in a device
// see https://community.fitbit.com/t5/SDK-Development/me-appTimeoutEnabled-will-not-be-set/td-p/3079978
me.appTimeoutEnabled = false;
logging.debug(`Timeout = ${me.appTimeoutEnabled}`);

import document from "document";

import * as manifest from "./manifest";
import * as simpleClock from "./simple/clock";
import * as cricket from "./wrist-spin/cricket";
import * as alert from "./wrist-spin/alert";
import * as literals from "../common/literals";
import * as settings from "./device-settings";

// variables
// these are lost after each run so dont put state that needs to persist here
// instead use the settingsObject

// this is the match to display on the select game screen
// it could be different from the match being displayed on the main screen
let currentMatchSelection = 0;

let buildTimestamp = manifest.getBundleDate();
settings.initialize();
currentMatchSelection = settings.settingsObject.currentMatch;

/////////////////////////
// main screen
/////////////////////////
let mainScreen = document.getElementById("main-screen");
let mainContainer = document.getElementById("main-container");

// menu panel
let list = document.getElementById("menu-list");
let items = list.getElementsByClassName("menu-list-item");
items.forEach((element, index) => {
  let touch = element.getElementById("touch-me");
  touch.onclick = (evt) => {
    logging.debug(`menu item: ${index}`);
    switch (index) {
        case 0:     // select game
            showSelectGameScreen();
            break;
        case 1:     // provider
            showSelectProviderScreen();
            break;
        case 2:     // settings
            showSettingsScreen();
            break;
        case 3:     // about
            showAboutScreen();
            break;
        case 4:     // licenses
            showLicenseScreen();
            break;
    }
  }
});

// watch panel
let txtWatchTime = document.getElementById("txtWatchTime");
let txtWatchScore = document.getElementById("txtWatchScore");
let txtWatchStatus = document.getElementById("txtWatchStatus");

// simple panel
let txtSimpleScore = document.getElementById("txtSimpleScore");
let txtSimpleStrikerLeft = document.getElementById("txtSimpleStrikerLeft");
let backgroundSimpleStrikerRight = document.getElementById("backgroundSimpleStrikerRight");
let txtSimpleStrikerRight = document.getElementById("txtSimpleStrikerRight");
let txtSimpleNonStrikerLeft = document.getElementById("txtSimpleNonStrikerLeft");
let backgroundSimpleNonStrikerRight = document.getElementById("backgroundSimpleNonStrikerRight");
let txtSimpleNonStrikerRight = document.getElementById("txtSimpleNonStrikerRight");
let txtSimpleMessage = document.getElementById("txtSimpleMessage");
let txtSimpleOvers = document.getElementById("txtSimpleOvers");
let txtSimpleTime = document.getElementById("txtSimpleTime");

// detail panel
let txtDetailTeam = document.getElementById("txtDetailTeam");
let backgroundDetailScore = document.getElementById("backgroundDetailScore");
let txtDetailScore = document.getElementById("txtDetailScore");
let txtDetailOvers = document.getElementById("txtDetailOvers");
let txtDetailStrikerLeft = document.getElementById("txtDetailStrikerLeft");
let backgroundDetailStrikerRight = document.getElementById("backgroundDetailStrikerRight");
let txtDetailStrikerRight = document.getElementById("txtDetailStrikerRight");
let txtDetailNonStrikerLeft = document.getElementById("txtDetailNonStrikerLeft");
let backgroundDetailNonStrikerRight = document.getElementById("backgroundDetailNonStrikerRight");
let txtDetailNonStrikerRight = document.getElementById("txtDetailNonStrikerRight");
let txtDetailBowlerLeft = document.getElementById("txtDetailBowlerLeft");
let backgroundDetailBowlerRight = document.getElementById("backgroundDetailBowlerRight");
let txtDetailBowlerRight = document.getElementById("txtDetailBowlerRight");
let txtDetailMessage = document.getElementById("txtDetailMessage");
let txtDetailFact = document.getElementById("txtDetailFact");
let txtDetailStatus = document.getElementById("txtDetailStatus");
let txtDetailTime = document.getElementById("txtDetailTime");

function moveBackgroundToTextDisplay(txt, background) {
    logging.verbose(`moveBackgroundToTextDisplay txt x ${txt.getBBox().x} y ${txt.getBBox().y} w ${txt.getBBox().width} h ${txt.getBBox().height}`);
    logging.verbose(`moveBackgroundToTextDisplay txt2 x ${txt.x} y ${txt.y} w ${txt.width} h ${txt.height}`);

    if (txt.getBBox().width > 0) {
        background.x = txt.getBBox().x - 6;
        //background.y = txt.getBBox().y;
        background.width = txt.getBBox().width + 6;
        //background.height = txt.getBBox().height;
    } else {
        background.x = txt.getBBox().x;
        background.width = txt.getBBox().width;
    }

    logging.verbose(`moveBackgroundToTextDisplay background x ${background.x} y ${background.y} w ${background.width} h ${background.height}`);
}

function alignBackgrounds() {
    moveBackgroundToTextDisplay(txtSimpleStrikerRight, backgroundSimpleStrikerRight);
    moveBackgroundToTextDisplay(txtSimpleNonStrikerRight, backgroundSimpleNonStrikerRight);
    moveBackgroundToTextDisplay(txtDetailScore, backgroundDetailScore);
    moveBackgroundToTextDisplay(txtDetailStrikerRight, backgroundDetailStrikerRight);
    moveBackgroundToTextDisplay(txtDetailNonStrikerRight, backgroundDetailNonStrikerRight);
    moveBackgroundToTextDisplay(txtDetailBowlerRight, backgroundDetailBowlerRight);
}

function setupGameDisplay(data) {
    txtWatchScore.text = data.score;

    txtSimpleScore.text = data.score;
    txtSimpleOvers.text = data.overs;
    txtSimpleStrikerLeft.text = data.striker_name;
    txtSimpleStrikerRight.text = data.striker_score;
    txtSimpleNonStrikerLeft.text = data.nonstriker_name;
    txtSimpleNonStrikerRight.text = data.nonstriker_score;
    txtSimpleMessage.text = data.lead;

    if (data.team_batting.length > 3) {
        txtDetailTeam.text = data.team_batting.substring(0,3);
    } else {
        txtDetailTeam.text = data.team_batting;
    }
    txtDetailScore.text = data.score;
    txtDetailOvers.text = data.overs;
    txtDetailStrikerLeft.text = data.striker_name;
    txtDetailStrikerRight.text = data.striker_stats;
    txtDetailNonStrikerLeft.text = data.nonstriker_name;
    txtDetailNonStrikerRight.text = data.nonstriker_stats;
    txtDetailBowlerLeft.text = data.bowler_name;
    txtDetailBowlerRight.text = data.bowler_stats;
    txtDetailMessage.text = data.lead;
    txtDetailFact.text = data.fact;

    alignBackgrounds();
}

function renderGameDisplayError(message) {
    txtWatchStatus.text = message;
    txtSimpleMessage.text = message;
    txtDetailStatus.text = message;
}

mainContainer.onclick = (evt) => {
    logging.debug(`container click ${JSON.stringify(evt)}`);
}

mainContainer.onselect = (evt) => {
    logging.debug(`container select ${JSON.stringify(evt)}`);
    alignBackgrounds();
}

/////////////////////////
// select provider screen
/////////////////////////
let selectProviderScreen = document.getElementById("select-provider-screen");
let providerTileList = document.getElementById("provider-list");
let chkProviderCricinfo = providerTileList.getElementById("provider-cricinfo");
let chkProviderCricinfo2 = providerTileList.getElementById("provider-cricinfo2");
let chkProviderCricScore = providerTileList.getElementById("provider-cricscore");
let chkProviderTest = providerTileList.getElementById("provider-test");
let btnSelectProviderBack = document.getElementById("btn-select-provider-back");

chkProviderCricinfo.firstChild.value = settings.settingsObject.provider == literals.PROVIDER_CRICINFO ? 1 : 0;
chkProviderCricinfo2.firstChild.value = settings.settingsObject.provider == literals.PROVIDER_CRICINFO_2 ? 1 : 0;
chkProviderCricScore.firstChild.value = settings.settingsObject.provider == literals.PROVIDER_CRICSCORE ? 1 : 0;
chkProviderTest.firstChild.value = settings.settingsObject.provider == literals.PROVIDER_TEST ? 1 : 0;
function resetProviderCheckboxStates() {
    chkProviderCricinfo.firstChild.value = 0;
    chkProviderCricinfo2.firstChild.value = 0;
    chkProviderCricScore.firstChild.value = 0;
    chkProviderTest.firstChild.value = 0;
}
chkProviderCricinfo.firstChild.onclick = (evt) => {
    logging.debug(`provider cricinfo :: ${JSON.stringify(evt)}`);
    // the settings module has an unload callback to save this state
    settings.settingsObject.provider = literals.PROVIDER_CRICINFO;
    resetProviderCheckboxStates();
};
chkProviderCricinfo2.firstChild.onclick = (evt) => {
    logging.debug(`provider cricinfo2 :: ${JSON.stringify(evt)}`);
    // the settings module has an unload callback to save this state
    settings.settingsObject.provider = literals.PROVIDER_CRICINFO_2;
    resetProviderCheckboxStates();
};
chkProviderCricScore.firstChild.onclick = (evt) => {
    logging.debug(`provider cricscore :: ${JSON.stringify(evt)}`);
    // the settings module has an unload callback to save this state
    settings.settingsObject.provider = literals.PROVIDER_CRICSCORE;
    resetProviderCheckboxStates();
};
chkProviderTest.firstChild.onclick = (evt) => {
    logging.debug(`provider test :: ${JSON.stringify(evt)}`);
    // the settings module has an unload callback to save this state
    settings.settingsObject.provider = literals.PROVIDER_TEST;
    resetProviderCheckboxStates();
};

btnSelectProviderBack.onclick = function(evt) {
    logging.debug("onclick select provider back");
    cricket.set_provider(settings.settingsObject.provider);
    cricket.refresh_match_selection(currentMatchSelection);
    if (settings.settingsObject.currentMatch > 0) {
        cricket.refresh_match_score(settings.settingsObject.currentMatch);
    }
    showMainScreen(0);
}

/////////////////////////
// select game screen
/////////////////////////
let selectGameScreen = document.getElementById("select-game-screen");
let selectGameMatch = document.getElementById("select-game-match");
let btnSelectGameBack = document.getElementById("btn-select-game-back");
let btnSelectGameForward = document.getElementById("btn-select-game-forward");
let btnSelectGameRefresh = document.getElementById("btn-select-game-refresh");

// it turns out that the combo buttons have a bug that make onActiviate not work very well
// https://community.fitbit.com/t5/SDK-Development/Combo-Button-and-physical-button/td-p/2334229
// so I am using onclick and handling the physical buttons myself
btnSelectGameBack.onclick = function(evt) {
    logging.debug("onclick Select game back");
    previousGame();
}

btnSelectGameBack.onkeydown  = function(evt) {
    logging.debug("onkeydown Select game back");
    previousGame();
}

btnSelectGameForward.onclick = function(evt) {
    logging.debug("onclick Select game forward");
    nextGame();
}

btnSelectGameForward.onkeydown  = function(evt) {
    logging.debug("onkeydown Select game forward");
    nextGame();
}

btnSelectGameRefresh.onclick = function(evt) {
    logging.debug("onclick Select game refresh");
    selectGameMatch.text = "Loading...";
    // go back to the beginning and reload the match
    currentMatchSelection = 0;
    cricket.refresh_match_selection(currentMatchSelection);
}

selectGameMatch.onclick = function(evt) {
    logging.debug("onclick Select game");
    settings.settingsObject.currentMatch = currentMatchSelection;
    cricket.refresh_match_score(settings.settingsObject.currentMatch);
    showMainScreen(1);
}

function setupSelectGame(data) {
    currentMatchSelection = data.selection_game_id;
    logging.debug(`current game: ${currentMatchSelection}`);
    selectGameMatch.text = data.team_1 + "\nv\n" + data.team_2;
}

function previousGame() {
    selectGameMatch.text = "Loading...";
    cricket.previous_match_selection(currentMatchSelection);
}

function nextGame() {
    selectGameMatch.text = "Loading...";
    cricket.next_match_selection(currentMatchSelection);
}


/////////////////////////
// settings screen
/////////////////////////
let settingsScreen = document.getElementById("settings-screen");
let settingsTileList = document.getElementById("settings-list");
let chkSettingsWicketAlert = settingsTileList.getElementById("settings-wicket-alert");
let chkSettingsNoPhoneAlert = settingsTileList.getElementById("settings-no-phone-alert");
let chkSettingsExitAlert = settingsTileList.getElementById("settings-exit-alert");
let btnSettingsBack = document.getElementById("btn-settings-back");

chkSettingsWicketAlert.firstChild.value = settings.settingsObject.alertWicket ? 1 : 0;
chkSettingsNoPhoneAlert.firstChild.value = settings.settingsObject.alertNoPhone ? 1 : 0;
chkSettingsExitAlert.firstChild.value = settings.settingsObject.alertExit ? 1 : 0;
chkSettingsWicketAlert.firstChild.onclick = (evt) => {
    // the settings module has an unload callback to save this state
    settings.settingsObject.alertWicket = !settings.settingsObject.alertWicket;
    logging.debug(`wicketAlert :: ${settings.settingsObject.alertWicket ? "checked" : "unchecked"}`)
};
chkSettingsNoPhoneAlert.firstChild.onclick = (evt) => {
    // the settings module has an unload callback to save this state
    settings.settingsObject.alertNoPhone = !settings.settingsObject.alertNoPhone;
    logging.debug(`noPhoneAlert :: ${settings.settingsObject.alertNoPhone ? "checked" : "unchecked"}`)
};
chkSettingsExitAlert.firstChild.onclick = (evt) => {
    // the settings module has an unload callback to save this state
    settings.settingsObject.alertExit = !settings.settingsObject.alertExit;
    logging.debug(`exitAlert :: ${settings.settingsObject.alertExit ? "checked" : "unchecked"}`)
};

btnSettingsBack.onclick = function(evt) {
    logging.debug("onclick settings back");
    showMainScreen(0);
}


/////////////////////////
// about screen
/////////////////////////
let aboutScreen = document.getElementById("about-screen");
let aboutText = document.getElementById("about-text");
aboutText.text = `wrist-spin\nv${literals.APP_VERSION}\n${buildTimestamp}\n${logging.getCurrentLogLevelLabel()}`;
aboutText.onclick = function(evt) {
    logging.debug("onclick about");
    showMainScreen(0);
}


/////////////////////////
// license screen
/////////////////////////
let licenseScreen = document.getElementById("licenses-screen");


/////////////////////////
// screen navigation
/////////////////////////

let allScreens = [];
allScreens.push(mainScreen);
allScreens.push(selectGameScreen);
allScreens.push(selectProviderScreen);
allScreens.push(settingsScreen);
allScreens.push(aboutScreen);
allScreens.push(licenseScreen);

function hideAllScreensExcept(screenToShow) {
    for (var index = 0; index<allScreens.length; index++) {
        if (allScreens[index] == null) {
            console.error(`hideAllScreensExcept NULL screen at index ${index}`);
        }
        allScreens[index].style.display = (allScreens[index] === screenToShow ? "inline" : "none");
    }
}

function showMainScreen(panelIndex) {
    logging.debug("Show main screen");
    hideAllScreensExcept(mainScreen);
    // Set the selected panel in the panorama
    mainContainer.value = panelIndex;
}
  
function showSelectGameScreen() {
    logging.debug("Show select game screen");
    hideAllScreensExcept(selectGameScreen);
}

function showSelectProviderScreen() {
    logging.debug("Show select provider screen");
    hideAllScreensExcept(selectProviderScreen);
}

function showSettingsScreen() {
    logging.debug("Show about screen");
    hideAllScreensExcept(settingsScreen);
}

function showAboutScreen() {
    logging.debug("Show about screen");
    hideAllScreensExcept(aboutScreen);
}

function showLicenseScreen() {
    logging.debug("Show license screen");
    hideAllScreensExcept(licenseScreen);
}

// physical buttons

document.onkeypress = function(evt) {
    logging.debug(`onkeypress: ${JSON.stringify(evt)}`);
    if (evt.key === "back") {
        if (mainScreen.style.display === "inline") {
            // Default behaviour to exit the app
        } else {
            if (selectProviderScreen.style.display == "inline") {
                cricket.set_provider(settings.settingsObject.provider);
                cricket.refresh_match_selection(currentMatchSelection);
                if (settings.settingsObject.currentMatch > 0) {
                    cricket.refresh_match_score(settings.settingsObject.currentMatch);
                }
            }
            // Go to main screen
            showMainScreen(0);
            evt.preventDefault();
        }
    }

    /* this only get called in the simulator - the real device calls btn.onkeydown */
    if (evt.key === "up") {
        if (selectGameScreen.style.display === "inline") {
            previousGame();
            evt.preventDefault();
        }
    }

    if (evt.key === "down") {
        if (selectGameScreen.style.display === "inline") {
            nextGame();
            evt.preventDefault();
        }
    }

}

// callbacks

/* -------- CRICKET --------- */

function cricketCallback(data) {
    if (data.isError) {
        renderGameDisplayError(data.errorMessage);
        if (data.noPhone && settings.settingsObject.alertNoPhone) {
            alert.alert_no_phone();
        }
        return;
    }
    alert.reset_alert_no_phone();
    logging.debug(`action: ${data.action}`);
    switch (data.action) {
        case literals.ACTION_READY:
            if (settings.settingsObject.currentMatch > 0) {
                cricket.refresh_match_score(settings.settingsObject.currentMatch);
                //cricket.refresh_match_score(1234);
                if (settings.settingsObject.currentDisplayPanel > 0) {
                    showMainScreen(settings.settingsObject.currentDisplayPanel);
                } else {
                    showMainScreen(1);
                }
            }
            cricket.refresh_match_selection(currentMatchSelection);
            break;
        case literals.ACTION_SELECT_REFRESH:
        case literals.ACTION_SELECT_PREVIOUS:
        case literals.ACTION_SELECT_NEXT:
            setupSelectGame(data);
            break;
        case literals.ACTION_REFRESH:
            // when we get a non error return - clear the error
            renderGameDisplayError("");
            setupGameDisplay(data);
            alert.check_alert_wicket_fall_and_stale_data(data, settings.settingsObject.alertWicket);
            if (alert.is_stale()) {
                renderGameDisplayError(`${alert.minutes_since_last_update()} mins old`);
            }
            // update the current panel - it will be persisted when we exit
            settings.settingsObject.currentDisplayPanel = mainContainer.value;
            break;
    }
}
cricket.initialize(cricketCallback, settings.settingsObject.provider);

/* --------- CLOCK ---------- */
function clockCallback(data) {
    txtWatchTime.text = data.time;
    txtSimpleTime.text = data.time;
    txtDetailTime.text = data.time;
}
simpleClock.initialize("minutes", "longDate", clockCallback);

/* --------- TIMER ---------- */
// this may be bad for battery life we might want to consider a more nuanced solution like this
// https://community.fitbit.com/t5/SDK-Development/Reliable-way-of-invoking-a-function-every-second/td-p/2608715
setTimeout(function tick() {
    logging.debug("*** TICK ***");
    if (settings.settingsObject.currentMatch > 0) {
        cricket.refresh_match_score(settings.settingsObject.currentMatch);
    }
    setTimeout(tick, literals.DEFAULT_TIMER_TICK_INTERVAL);
}, literals.DEFAULT_TIMER_TICK_INTERVAL);

console.log(`wrist-spin v${literals.APP_VERSION} started, ${me.buildId}, ${me.launchArguments}`);
   

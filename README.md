# wrist-spin

A live cricket score notifier for wearable devices

This app displays the score for a selected cricket match. 

Its available for Pebble, Fitbit OS4, Amazfit/ZeppOS and Android devices



# wrist-spin

A live cricket score notifier for Pebble watches

This app displays the score for a selected cricket match. A variety of sizes and detail are available.

Thanks to [SixFour](https://github.com/robn/sixfour) for the javascript code.


## Usage

Use the middle button to goto the match selection screen, Up and down move between the current matches. The middle button selects a match as the current match.

From the main screen the up button displays less information but should be easier to glance.


## How To Compile


Compiled using Ubuntu 14.04 and Pebble Tool v4.2 (active SDK: v3.10)


## Testing the Javascript


There is a test runner in ~/pebble-dev/code/wrist-spin/test/test.html

If you see the following error in the test runner
Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote resource at http://cricscore-api.appspot.com/csa. (Reason: CORS header 'Access-Control-Allow-Origin' missing).

You will need to supress cors errors [this addon](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/) seems to work fine for Firefox


[License]: LICENSE.md
[OFL]: OFL.md
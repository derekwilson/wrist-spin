#include <pebble.h>

#include "globals.h"

static void handle_bluetooth_event(bool connected) {
  if (connected) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Event: Phone is connected!");
  } else {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Event: Phone is not connected!");
    if (current_vibrate_on_phone_disconnect) {
      if (quiet_time_is_active ()) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "quiet time - supress");
      } else {
        vibes_cancel();
        vibes_long_pulse();
      }
    }
  }
  update_app_status_text();
}

void init_bluetooth_event() {
  connection_service_subscribe((ConnectionHandlers) {
    .pebble_app_connection_handler = handle_bluetooth_event
  });
}

void deinit_bluetooth_event() {
  connection_service_unsubscribe();
}

#include <pebble.h>

#include "globals.h"

static void init(void) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "wrist-spin v %s", get_version());

  init_persistent_data();
  switch (current_display) {
    case DISPLAY_WATCH:
      showWatchWindow();
      break;
    case DISPLAY_DETAIL:
      showDetailWindow();
      break;
    case DISPLAY_MAIN:
    default:
      showMainWindow();
      break;
  }
  init_app_sync();
  open_app_sync();
  init_timer_tick();
  init_bluetooth_event();
}

static void deinit(void) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Exiting...");

  deinit_app_sync();
  if (current_vibrate_on_exit) {
      if (quiet_time_is_active ()) {
        APP_LOG(APP_LOG_LEVEL_DEBUG, "quiet time - supress vibrate");
      } else {
        vibes_short_pulse();
      }
  }
  deinit_persistent_data();
  deinit_timer_tick();
  deinit_bluetooth_event();

  // these will only close if they are open
  closeMainWindow();
  closeWatchWindow();
  closeDetailWindow();
}

int main(void) {
  init_version();
#ifdef SUPRESS_LOGGING
  app_log(APP_LOG_LEVEL_INFO, __FILE_NAME__, __LINE__, "wrist-spin v%s starting - Logging is supressed Provider %s", get_version(), DATA_PROVIDER);
#endif

  init();
  app_event_loop();
  deinit();
}

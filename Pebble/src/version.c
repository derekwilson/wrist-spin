#include <pebble.h>

#include "globals.h"

static char versionBuffer[10];

/* - using the generated appinfo file
#include "appinfo.h"

void init_version() {
    snprintf(versionBuffer,sizeof(versionBuffer),"%s",VERSION_LABEL);
}
*/

/* 
 * using the hacky method described here
 * https://forums.getpebble.com/discussion/10405/how-can-i-get-my-app-version-in-c-code
 */ 
#include "pebble_process_info.h"
extern const PebbleProcessInfo __pbl_app_info;
void init_version() {
    snprintf(versionBuffer,sizeof(versionBuffer),"%d.%d%s",
        __pbl_app_info.process_version.major, 
        __pbl_app_info.process_version.minor,
        "" 
    );
}

char* get_version() {
    return versionBuffer;
}

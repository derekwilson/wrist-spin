#include <pebble.h>

#include "globals.h"

bool send_to_phone(char* current_id, char* action) {
  if ((current_id == NULL) || (action == NULL)) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "no data to send");
    // well, the "nothing" that was sent to us was queued, anyway ...
    return true;
  }

  DictionaryIterator *iter = NULL;
  AppMessageResult begin_result = app_message_outbox_begin(&iter);
  if (iter == NULL) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "app_message_outbox_begin returned null - result %d", (int) begin_result);
    open_app_sync();    // try to reset
    return false;
  }

  DictionaryResult result;
  Tuplet id_tuple = TupletCString(GAME_ID_KEY, current_id);
  result = dict_write_tuplet(iter, &id_tuple);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "add ID result (non zero is bad) %d", (int) result);
  Tuplet action_tuple = TupletCString(ACTION_KEY, action);
  result = dict_write_tuplet(iter, &action_tuple);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "add ACTION result (non zero is bad) %d", (int) result);
  Tuplet data_provider_tuple = TupletCString(DATA_PROVIDER_KEY, &current_data_provider[0]);
  result = dict_write_tuplet(iter, &data_provider_tuple);
  if (result != 0) {
    app_log(APP_LOG_LEVEL_ERROR, __FILE_NAME__, __LINE__, "wrist-spin error sending to phone %d", result);
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "add PROVIDER result (non zero is bad) %d", (int) result);
  dict_write_end(iter);

  AppMessageResult send_result = app_message_outbox_send();
  APP_LOG(APP_LOG_LEVEL_DEBUG, "send result %d", (int) send_result);
  
  // only used in debug builds
  UNUSED(begin_result);
  UNUSED(send_result);
  
  return true;
}

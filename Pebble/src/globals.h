
// uncover for release build
#define RELEASE_BUILD 1

// backend data providers
#define DATA_PROVIDER_CRICINFO "CRICINFO"
#define DATA_PROVIDER_CRICINFO_2 "CRICINFO2"
#define DATA_PROVIDER_CRICSCORE "CRICSCORE"
#define DATA_PROVIDER_CRICAPI "CRICAPI2"
#define DATA_PROVIDER_CRICAPI_OLD "CRICAPI"

#ifdef RELEASE_BUILD

// uncover to supress logging
#define SUPRESS_LOGGING 1
#define DATA_PROVIDER DATA_PROVIDER_CRICINFO

#else // debug build

#define DATA_PROVIDER DATA_PROVIDER_CRICINFO

#endif

// uncover to stop the display from updating
//#define SUPRESS_TIMER_TICK 1


#define UNUSED(x) (void)(x)

#ifdef SUPRESS_LOGGING
#undef APP_LOG
#define APP_LOG(...)
#endif

// refresh the match scores
#define ACTION_READY "READY"
// refresh the match scores
#define ACTION_REFRESH "REFRESH"
// get the match info for the previous match, given the ID of the current match
#define ACTION_SELECT_PREVIOUS "SELECT-PREVIOUS"
// get the match info for the next match, given the ID of the current match
#define ACTION_SELECT_NEXT "SELECT-NEXT"
// set this ID as current and get the match scores
#define ACTION_SELECT_MATCH "SELECT-MATCH"
// refresh the teams
#define ACTION_SELECT_REFRESH "SELECT-REFRESH"

// key used to keep the current game id in persistent storage
#define PERSIST_KEY_GAME_ID 1
// key used to keep the current game id in persistent storage
#define PERSIST_KEY_DATA_PROVIDER 2
// key used to keep vibrate on wicket switch
#define PERSIST_KEY_VIBRATE_ON_WICKET 3
// key used to keep vibrate on wicket switch
#define PERSIST_KEY_VIBRATE_ON_PHONE_DISCONNECT 4
// key used to keep vibrate on wicket switch
#define PERSIST_KEY_VIBRATE_ON_EXIT 5
// key used to keep vibrate on wicket switch
#define PERSIST_KEY_CURRENT_DISPLAY 6

// the different displays
#define DISPLAY_MAIN 0
#define DISPLAY_WATCH 1
#define DISPLAY_DETAIL 2

// max length of the game id in chars
#define MAX_MATCH_ID_LENGTH 20
// max length of the data provider in chars
#define MAX_DATA_PROVIDER_LENGTH 20

// need to match up with appinfo.json
enum SyncKey {
  GAME_ID_KEY = 0x1,
  SELECTION_GAME_ID_KEY,
  TEAM_1_KEY,
  TEAM_2_KEY,
  TEAM_BATTING_KEY,
  SCORE_KEY,
  OVERS_KEY,
  LEAD_KEY,
  STRIKER_NAME_KEY,
  STRIKER_STATS_KEY,
  STRIKER_SCORE_KEY,
  NONSTRIKER_NAME_KEY,
  NONSTRIKER_STATS_KEY,
  NONSTRIKER_SCORE_KEY,
  BOWLER_NAME_KEY,
  BOWLER_STATS_KEY,
  FACT_KEY,
  ACTION_KEY,
  DATA_PROVIDER_KEY
};

extern char current_match_id[];
extern char current_data_provider[];
extern bool current_vibrate_on_wicket;
extern bool current_vibrate_on_phone_disconnect;
extern bool current_vibrate_on_exit;
extern int current_display;

// main display
extern TextLayer* main_score_text_layer;
extern TextLayer* main_overs_text_layer;
extern TextLayer* main_lead_text_layer;
extern TextLayer* main_striker_name_text_layer;
extern TextLayer* main_striker_stats_text_layer;
extern TextLayer* main_nonstriker_name_text_layer;
extern TextLayer* main_nonstriker_stats_text_layer;
extern TextLayer* main_time_text_layer;
extern TextLayer* main_app_status_text_layer;

// watch veiw
extern TextLayer* watch_score_text_layer;
extern TextLayer* watch_time_text_layer;
extern TextLayer* watch_app_status_text_layer;

// details view
extern TextLayer* detail_team_text_layer;
extern TextLayer* detail_score_text_layer;
extern TextLayer* detail_overs_text_layer;
extern TextLayer* detail_lead_text_layer;
extern TextLayer* detail_striker_name_text_layer;
extern TextLayer* detail_striker_stats_text_layer;
extern TextLayer* detail_nonstriker_name_text_layer;
extern TextLayer* detail_nonstriker_stats_text_layer;
extern TextLayer* detail_bowler_name_text_layer;
extern TextLayer* detail_bowler_stats_text_layer;
extern TextLayer* detail_time_text_layer;
extern TextLayer* detail_fact_text_layer;
extern TextLayer* detail_app_status_text_layer;

// match selection
extern TextLayer* select_title_text;
extern TextLayer* select_team1_text;
extern TextLayer* select_vs_text;
extern TextLayer* select_team2_text;

// appSync.c
void init_app_sync();
void deinit_app_sync();
void open_app_sync();
void update_app_status_text();

// sendToPhone.c
bool send_to_phone(char* current_id, char* action);

// timerTick.c
void init_timer_tick();
void deinit_timer_tick();
void kick_timer_tick();

// bluetooth.c
void init_bluetooth_event();
void deinit_bluetooth_event();

// persist.c
void init_persistent_data();
void deinit_persistent_data();

// version.h
void init_version();
char* get_version();

// mainWindow.c
void showMainWindow();
void closeMainWindow();

// detailWindow.c
void showDetailWindow();
void closeDetailWindow();

// watchWindow.c
void showWatchWindow();
void closeWatchWindow();

// selectGameWindow.c
void set_current_selection_match_id(const char* id);
void showGameSelectWindow();

// aboutWindow.c
void showAboutWindow();

// licenseWindow.c
void showLicenseWindow();

// dataProviderWindow.c
void showDataProviderWindow();

// settingsWindow.c
void showSettingsWindow();

// menuWindow.c
void showMenuWindow();

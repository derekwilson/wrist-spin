#include <pebble.h>

#include "globals.h"

static void handle_minute_tick(struct tm* tick_time, TimeUnits units_changed) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "tick");

  if (!tick_time) {
    time_t now = time(NULL);
    tick_time = localtime(&now);
  }

  static char currentTimeBuffer[10];
  char* currentTimeFormatted = &currentTimeBuffer[0];
  if (clock_is_24h_style()) {
    strftime(currentTimeBuffer, sizeof(currentTimeBuffer), "%R", tick_time);
  }
  else {
    strftime(currentTimeBuffer, sizeof(currentTimeBuffer), "%l:%M", tick_time);
    if (' ' == currentTimeBuffer[0]) {
      // space padded
      currentTimeFormatted = &currentTimeBuffer[1];
    }
  }
  currentTimeBuffer[sizeof(currentTimeBuffer)-1] = '\0';
  if (main_time_text_layer != NULL) {
    text_layer_set_text(main_time_text_layer, currentTimeFormatted);
  }
  if (detail_time_text_layer != NULL) {
    text_layer_set_text(detail_time_text_layer, currentTimeFormatted);
  }
  if (watch_time_text_layer != NULL) {
    text_layer_set_text(watch_time_text_layer, currentTimeFormatted);
  }

  APP_LOG(APP_LOG_LEVEL_DEBUG, "time |%s|", currentTimeFormatted);
  // get updated scores
#ifndef SUPRESS_TIMER_TICK
  send_to_phone(current_match_id, ACTION_REFRESH);
#endif
  update_app_status_text();
}

void kick_timer_tick() {
#ifdef SUPRESS_TIMER_TICK
  if (watch_score_text_layer != NULL) {
    text_layer_set_text(watch_score_text_layer, "888/8");
  }
  if (watch_time_text_layer != NULL) {
    text_layer_set_text(watch_time_text_layer, "12:00");
  }
  return;
#endif
  APP_LOG(APP_LOG_LEVEL_DEBUG, "kicking timer tick");
  handle_minute_tick(NULL,0);
}

void init_timer_tick() {
  tick_timer_service_subscribe(MINUTE_UNIT, &handle_minute_tick);
}

void deinit_timer_tick() {
  tick_timer_service_unsubscribe();
}

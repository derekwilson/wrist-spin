#include <pebble.h>

#include "globals.h"

static void get_persistent_string(int key, char* buffer, int length) {
  if (persist_exists(key)) {
    persist_read_string(key, buffer, length);
  }
}

static bool get_persistent_bool(int key, bool defaultValue) {
  if (persist_exists(key)) {
    return persist_read_bool(key);
  }
  return defaultValue;
}

static int get_persistent_int(int key, int defaultValue) {
  if (persist_exists(key)) {
    return persist_read_int(key);
  }
  return defaultValue;
}

void init_persistent_data() {
  // setup hard coded defaults
  strncpy(current_match_id, "0", MAX_MATCH_ID_LENGTH);
  strncpy(current_data_provider, DATA_PROVIDER, MAX_DATA_PROVIDER_LENGTH);

  // read from presistant storage
  get_persistent_string(PERSIST_KEY_GAME_ID, current_match_id, MAX_MATCH_ID_LENGTH);
  get_persistent_string(PERSIST_KEY_DATA_PROVIDER, current_data_provider, MAX_DATA_PROVIDER_LENGTH);
  current_vibrate_on_wicket = get_persistent_bool(PERSIST_KEY_VIBRATE_ON_WICKET, true);
  current_vibrate_on_phone_disconnect = get_persistent_bool(PERSIST_KEY_VIBRATE_ON_PHONE_DISCONNECT, true);
  current_vibrate_on_exit = get_persistent_bool(PERSIST_KEY_VIBRATE_ON_EXIT, false);
  current_display = get_persistent_int(PERSIST_KEY_CURRENT_DISPLAY, DISPLAY_MAIN);

  if (strcmp(current_data_provider, DATA_PROVIDER_CRICAPI_OLD) == 0) {
    // if we were using the old cricapi them shift to cricinfo
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Init - shifting from old CRICAPI to CRICINFO");
    strncpy(current_data_provider, DATA_PROVIDER_CRICINFO, MAX_DATA_PROVIDER_LENGTH);
  }

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Init - Match ID: %s", current_match_id);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Init - Data Provider: %s", current_data_provider);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Init - Display: %d", current_display);
}

void deinit_persistent_data() {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Persisting match ID: %s", current_match_id);
  persist_write_string(PERSIST_KEY_GAME_ID, current_match_id);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Persisting data provider: %s", current_data_provider);
  persist_write_string(PERSIST_KEY_DATA_PROVIDER, current_data_provider);
  
  persist_write_bool(PERSIST_KEY_VIBRATE_ON_WICKET, current_vibrate_on_wicket);
  persist_write_bool(PERSIST_KEY_VIBRATE_ON_PHONE_DISCONNECT, current_vibrate_on_phone_disconnect);
  persist_write_bool(PERSIST_KEY_VIBRATE_ON_EXIT, current_vibrate_on_exit);
  persist_write_int(PERSIST_KEY_CURRENT_DISPLAY, current_display);
}

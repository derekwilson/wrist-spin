#include <pebble.h>

#include "globals.h"

static AppSync sync;
static uint8_t sync_buffer[512];

#define MAX_STATUS_BUFFER 100
static char app_status[MAX_STATUS_BUFFER];
static time_t last_update_time;

#define MINUTES_FOR_STALE 5
#define MAX_LAST_BUFFER 100
static char last_score[MAX_LAST_BUFFER];
static char last_overs[MAX_LAST_BUFFER];

static void sync_error_callback(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Sync Error: %d", app_message_error);
}

static void app_message_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Error: %d", (int) reason);
}

static void log_time(char* message,  time_t time) {
  #ifndef RELEASE_BUILD
    tm* time_to_log = localtime(&time);
    char *time_format = clock_is_24h_style() ? "%R" : "%l:%M";
    static char buf[10];
    strftime(buf, sizeof(buf), time_format, time_to_log);
    buf[sizeof(buf)-1] = '\0';
    APP_LOG(APP_LOG_LEVEL_DEBUG, "%s: %s", message, buf);
  #endif
}

static void reset_last_update() {
  last_update_time = time(NULL);
  log_time("RESET update time", last_update_time);
}

static int minutes_since_last_update() {
  double seconds = difftime(time(NULL), last_update_time);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "elapsed seconds: %d", (int) seconds);
  if (seconds > 0) {
    return ((int) (seconds) / 60);
  }
  return 0;
}

static bool is_stale() {
  log_time("last update time", last_update_time);
  log_time("now", time(NULL));
  if (minutes_since_last_update() >= MINUTES_FOR_STALE) {
    return true;
  }
  return false;
}

static void set_text_and_show(TextLayer* layer, const char* text) {
  if (layer != NULL) {
    text_layer_set_text(layer, text);
    layer_set_hidden((Layer*) layer, strlen(text) < 1);
  }
}

static void set_tuple_text(TextLayer* layer, const Tuple* tuple) {
  if (layer != NULL) {
    text_layer_set_text(layer, tuple->value->cstring);
  }
}

static void set_tuple_text_if_present(TextLayer* layer, const Tuple* tuple) {
  if (layer != NULL) {
    if (tuple->value->cstring != NULL && strlen(tuple->value->cstring) > 0) {
      text_layer_set_text(layer, tuple->value->cstring);
    }
  }
}

static char* get_wickets(const char *score) {
  return strchr(score, '/');
}

static bool has_a_wicket_fallen(const char* newScore) {
  if (last_score == NULL || strlen(last_score) < 1) {
    // excude initial state
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_a_wicket_fallen: Initial state");
    return false;
  }
  if (newScore == NULL) {
    // excude bum data
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_a_wicket_fallen: No new score");
    return false;
  }
  if (strcmp(last_score, newScore) != 0) {
    // the score has changed
    reset_last_update();
  }
  char *oldWickets = get_wickets(last_score);
  char *newWickets = get_wickets(newScore);
  if (oldWickets == NULL) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_a_wicket_fallen: No old wickets");
    return false;
  }
  if (newWickets == NULL) {
    if (oldWickets != NULL) {
      APP_LOG(APP_LOG_LEVEL_DEBUG, "has_a_wicket_fallen: Last wicket or end of innings");
      return true;
    }
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_a_wicket_fallen: No new wickets");
    return false;
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "old score %s, new score %s, old wickets %s, new wickets %s", last_score, newScore, oldWickets, newWickets);
  if (strcmp(oldWickets, newWickets) != 0) {
    return true;
  }
  return false;
}

static bool has_overs_changed(const char* newOvers) {
  if (last_overs == NULL || strlen(last_overs) < 1) {
    // excude initial state
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_overs_changed: Initial state");
    return false;
  }
  if (newOvers == NULL || strlen(newOvers) < 1) {
    // excude bum data
    APP_LOG(APP_LOG_LEVEL_DEBUG, "has_overs_changed: No new overs");
    return false;
  }
  APP_LOG(APP_LOG_LEVEL_DEBUG, "old overs %s, new overs %s", last_overs, newOvers);
  if (strcmp(last_overs, newOvers) != 0) {
    return true;
  }
  return false;
}

static void trigger_wicket_alert() {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "trigger wicket alert");
  if (quiet_time_is_active ()) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "trigger wicket alert - quiet time - supress");
    return;    
  }
  if (current_vibrate_on_wicket) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "trigger wicket alert - vibrate");
    vibes_cancel();
    vibes_double_pulse();
  }
}

static void sync_tuple_changed_callback(const uint32_t key, const Tuple* new_tuple, const Tuple* old_tuple, void* context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "change key %d val %s", (int)key, new_tuple->value->cstring);
    switch (key) {
        case ACTION_KEY:
            if (strcmp(new_tuple->value->cstring, ACTION_READY) == 0) {
              APP_LOG(APP_LOG_LEVEL_DEBUG, "JS is ready");
              kick_timer_tick();
            }
            break;
        case SELECTION_GAME_ID_KEY:
            if (new_tuple->value->cstring != NULL && strlen(new_tuple->value->cstring) > 0) {
              set_current_selection_match_id(new_tuple->value->cstring);
            }
            break;
        case TEAM_1_KEY:
            set_tuple_text_if_present(select_team1_text, new_tuple);
            break;
        case TEAM_2_KEY:
            set_tuple_text_if_present(select_team2_text, new_tuple);
            break;
        case TEAM_BATTING_KEY:
            set_tuple_text(detail_team_text_layer, new_tuple);
            break;
        case SCORE_KEY:
            // must check *before* updating the last score
            if (has_a_wicket_fallen(new_tuple->value->cstring)) {
              trigger_wicket_alert();
            }
            set_tuple_text(main_score_text_layer, new_tuple);
            set_tuple_text(detail_score_text_layer, new_tuple);
            set_tuple_text(watch_score_text_layer, new_tuple);
            strncpy(last_score, new_tuple->value->cstring, MAX_LAST_BUFFER);
            break;
        case OVERS_KEY:
            if (has_overs_changed(new_tuple->value->cstring)) {
              reset_last_update();
            }
            set_tuple_text(main_overs_text_layer, new_tuple);
            set_tuple_text(detail_overs_text_layer, new_tuple);
            strncpy(last_overs, new_tuple->value->cstring, MAX_LAST_BUFFER);
            break;
        case LEAD_KEY:
            set_tuple_text(main_lead_text_layer, new_tuple);
            set_tuple_text(detail_lead_text_layer, new_tuple);
            break;
        case STRIKER_NAME_KEY:
            set_tuple_text(main_striker_name_text_layer, new_tuple);
            set_tuple_text(detail_striker_name_text_layer, new_tuple);
            break;
        case STRIKER_SCORE_KEY:
            set_tuple_text(main_striker_stats_text_layer, new_tuple);
            break;
        case STRIKER_STATS_KEY:
            set_tuple_text(detail_striker_stats_text_layer, new_tuple);
            break;
        case NONSTRIKER_NAME_KEY:
            set_tuple_text(main_nonstriker_name_text_layer, new_tuple);
            set_tuple_text(detail_nonstriker_name_text_layer, new_tuple);
            break;
        case NONSTRIKER_SCORE_KEY:
            set_tuple_text(main_nonstriker_stats_text_layer, new_tuple);
            break;
        case NONSTRIKER_STATS_KEY:
            set_tuple_text(detail_nonstriker_stats_text_layer, new_tuple);
            break;
        case BOWLER_NAME_KEY:
            set_tuple_text(detail_bowler_name_text_layer, new_tuple);
            break;
        case BOWLER_STATS_KEY:
            set_tuple_text(detail_bowler_stats_text_layer, new_tuple);
            break;
        case FACT_KEY:
            set_tuple_text(detail_fact_text_layer, new_tuple);
            break;
    }
}

void init_app_sync() {
  Tuplet initial_values[] = {
    TupletCString(GAME_ID_KEY, "0"),
    TupletCString(SELECTION_GAME_ID_KEY, ""),
    TupletCString(TEAM_1_KEY, ""),
    TupletCString(TEAM_2_KEY, ""),
    TupletCString(TEAM_BATTING_KEY, ""),
    TupletCString(SCORE_KEY, ""),
    TupletCString(OVERS_KEY, ""),
    TupletCString(LEAD_KEY, "Loading..."),
    TupletCString(STRIKER_NAME_KEY, ""),
    TupletCString(STRIKER_STATS_KEY, ""),
    TupletCString(STRIKER_SCORE_KEY, ""),
    TupletCString(NONSTRIKER_NAME_KEY, ""),
    TupletCString(NONSTRIKER_STATS_KEY, ""),
    TupletCString(NONSTRIKER_SCORE_KEY, ""),
    TupletCString(BOWLER_NAME_KEY, ""),
    TupletCString(BOWLER_STATS_KEY, ""),
    TupletCString(FACT_KEY, ""),
    TupletCString(ACTION_KEY, ""),
    TupletCString(DATA_PROVIDER_KEY, ""),
  };
  //APP_LOG(APP_LOG_LEVEL_DEBUG, "want dict buffer: %d", (int) dict_calc_buffer_size_from_tuplets(initial_values, MAX_KEY));
  app_sync_init(&sync, sync_buffer, sizeof(sync_buffer), initial_values, ARRAY_LENGTH(initial_values),
          sync_tuple_changed_callback, sync_error_callback, NULL);

}

void deinit_app_sync() {
  app_sync_deinit(&sync);
}

void open_app_sync() {
  strcpy(last_overs,"");
  strcpy(last_score,"");
  reset_last_update();
  app_message_register_outbox_failed(app_message_failed_callback);
  app_message_open(sizeof(sync_buffer), 64);
  APP_LOG(APP_LOG_LEVEL_DEBUG, "inbox max size: %d", (int) app_message_inbox_size_maximum());
  APP_LOG(APP_LOG_LEVEL_DEBUG, "outbox max size: %d", (int) app_message_outbox_size_maximum());
}

void update_app_status_text() {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "update app status");

  strcpy(app_status, "");
  if (!connection_service_peek_pebble_app_connection()) {
    strcpy(app_status, "no phone");
  }
  else if (is_stale()) {
    snprintf(app_status, MAX_STATUS_BUFFER, "%d mins old", minutes_since_last_update());
  }

#ifdef SUPRESS_TIMER_TICK
  strcpy(app_status, "999 mins old");
#endif

  APP_LOG(APP_LOG_LEVEL_DEBUG, "app status %s", app_status);
  set_text_and_show(main_app_status_text_layer, app_status);
  set_text_and_show(detail_app_status_text_layer, app_status);
  set_text_and_show(watch_app_status_text_layer, app_status);
}

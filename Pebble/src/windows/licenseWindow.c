#include <pebble.h>

#include "globals.h"

static Window *licenseWindow;

// This is a scroll layer
static ScrollLayer *license_scroll_layer;

// We also use a text layer to scroll in the scroll layer
static TextLayer *license_text_layer;

static char licenseText[] = "\
Font: Anonymous Pro\n\
Copyright (c) 2009, Mark Simonson (http://www.ms-studio.com, mark@marksimonson.com)\n\
This Font Software is licensed under the SIL Open Font License, Version 1.1.\n\
This license is available at: http://scripts.sil.org/OFL\n\
\n\
Font: OSP DIN\n\
Copyright (C) 2011 OSP http://ospublish.constantvzw.org.\n\
This Font Software is licensed under the SIL Open Font License, Version 1.1.\n\
This license is available at: http://scripts.sil.org/OFL\n\
\n\
Javascript decoder\n\
Copyright (c) 2014 Robert Norris. MIT license.\n\
https://github.com/robn/sixfour/blob/master/LICENSE.md\n\
";

static GColor textColour;
static GColor backgroundColour;

static void license_window_load(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Load license Window. %p", window);
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  GRect max_text_bounds = GRect(0, 0, bounds.size.w, 2000);

  // Initialize the scroll layer
  license_scroll_layer = scroll_layer_create(bounds);

  // This binds the scroll layer to the window so that up and down map to scrolling
  // You may use scroll_layer_set_callbacks to add or override interactivity
  scroll_layer_set_click_config_onto_window(license_scroll_layer, window);

  // Scrolls through text on a round screen by page (scroll offset is changed by frame's height)
#ifdef PBL_ROUND
  scroll_layer_set_paging(license_scroll_layer, true);
#endif

  // Initialize the text layer
  license_text_layer = text_layer_create(max_text_bounds);
  text_layer_set_text(license_text_layer, licenseText);

  // Change the font to a nice readable one
  // This is system font; you can inspect pebble_fonts.h for all system fonts
  // or you can take a look at feature_custom_font to add your own font
  text_layer_set_font(license_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_color(license_text_layer, textColour);
  text_layer_set_background_color(license_text_layer, GColorClear);

  // Add the layers for display
  scroll_layer_add_child(license_scroll_layer, text_layer_get_layer(license_text_layer));

  layer_add_child(window_layer, scroll_layer_get_layer(license_scroll_layer));

  // Fit the text to the round screen with inset around edge of screen
#ifdef PBL_ROUND
  text_layer_set_text_alignment(license_text_layer, GTextAlignmentCenter);
  uint8_t inset = 4;
  text_layer_enable_screen_text_flow_and_paging(license_text_layer, inset);
#endif

  // Trim text layer and scroll content to fit text box
  GSize max_size = text_layer_get_content_size(license_text_layer);
  text_layer_set_size(license_text_layer, max_size);
  scroll_layer_set_content_size(license_scroll_layer, GSize(bounds.size.w, max_size.h + 4));
}

static void license_window_unload(Window *window) {
  text_layer_destroy(license_text_layer);
  scroll_layer_destroy(license_scroll_layer);
  license_text_layer = NULL;
  license_scroll_layer = NULL;
}

void showLicenseWindow() {
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing license Window.");
  textColour = GColorGreen;
  backgroundColour = GColorBlack;
  licenseWindow = window_create();
  window_set_background_color(licenseWindow, backgroundColour);
  window_set_window_handlers(licenseWindow, (WindowHandlers) {
    .load = license_window_load,
    .unload = license_window_unload,
  });

  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing license Window Window. %p", licenseWindow);
  const bool animated = true;
  window_stack_push(licenseWindow, animated);
}

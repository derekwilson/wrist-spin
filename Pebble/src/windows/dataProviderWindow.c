#include <pebble.h>

#include "globals.h"

static Window *dataProviderWindow;
static MenuLayer *s_menu_layer;

#define RADIO_BUTTON_WINDOW_NUM_ROWS     4
#define RADIO_BUTTON_WINDOW_CELL_HEIGHT  44
#define RADIO_BUTTON_WINDOW_RADIO_RADIUS 6

static int s_current_selection = 0;

static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *context) {
  return RADIO_BUTTON_WINDOW_NUM_ROWS;
}

static char* convertIndexToProvider(int index) {
  switch (index) {
    case 0:
      return DATA_PROVIDER_CRICAPI;
    case 1:
      return DATA_PROVIDER_CRICSCORE;
    case 2:
      return DATA_PROVIDER_CRICINFO;
    case 3:
      return DATA_PROVIDER_CRICINFO_2;
  }
  return "UNKNOWN";
}

static int convertProviderToIndex(char *provider) {
  if (strcmp(provider, DATA_PROVIDER_CRICAPI) == 0) {
    return 0;
  }
  if (strcmp(provider, DATA_PROVIDER_CRICSCORE) == 0) {
    return 1;
  }
  if (strcmp(provider, DATA_PROVIDER_CRICINFO) == 0) {
    return 2;
  }
  if (strcmp(provider, DATA_PROVIDER_CRICINFO_2) == 0) {
    return 3;
  }
  return 0;
}

static void draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *context) {
  static char s_buff[16];
  snprintf(s_buff, sizeof(s_buff), convertIndexToProvider(cell_index->row));
  menu_cell_basic_draw(ctx, cell_layer, s_buff, NULL, NULL);

  GRect bounds = layer_get_bounds(cell_layer);
  GPoint p = GPoint(bounds.size.w - (3 * RADIO_BUTTON_WINDOW_RADIO_RADIUS), (bounds.size.h / 2));

  // Selected?
#if defined(PBL_COLOR)
  if(menu_cell_layer_is_highlighted(cell_layer)) {
    graphics_context_set_stroke_color(ctx, GColorBlack);
    graphics_context_set_fill_color(ctx, GColorBlack);
  } else {
    graphics_context_set_stroke_color(ctx, GColorWhite);
    graphics_context_set_fill_color(ctx, GColorWhite);
  }
#else
  if(menu_cell_layer_is_highlighted(cell_layer)) {
    graphics_context_set_stroke_color(ctx, GColorWhite);
    graphics_context_set_fill_color(ctx, GColorWhite);
  } else {
    graphics_context_set_stroke_color(ctx, GColorBlack);
    graphics_context_set_fill_color(ctx, GColorBlack);
  }
#endif

  // Draw radio filled/empty
  graphics_draw_circle(ctx, p, RADIO_BUTTON_WINDOW_RADIO_RADIUS);
  if(cell_index->row == s_current_selection) {
    // This is the selection
    graphics_fill_circle(ctx, p, RADIO_BUTTON_WINDOW_RADIO_RADIUS - 3);
  }
}

static int16_t get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *context) {
  return PBL_IF_ROUND_ELSE(
    menu_layer_is_index_selected(menu_layer, cell_index) ?
      MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT : MENU_CELL_ROUND_UNFOCUSED_TALL_CELL_HEIGHT,
      RADIO_BUTTON_WINDOW_CELL_HEIGHT);
}

static void select_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  // Change selection
  s_current_selection = cell_index->row;
  menu_layer_reload_data(menu_layer);
  strncpy(current_data_provider,convertIndexToProvider(s_current_selection),MAX_DATA_PROVIDER_LENGTH);
  window_stack_pop(true);

  // get the scores refreshed - with the new provider
  send_to_phone(current_match_id, ACTION_SELECT_MATCH);
}

static void data_provider_window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_menu_layer = menu_layer_create(bounds);
  menu_layer_set_click_config_onto_window(s_menu_layer, window);
  #if defined(PBL_COLOR)
    menu_layer_set_normal_colors(s_menu_layer, GColorBlack, GColorWhite);
    menu_layer_set_highlight_colors(s_menu_layer, GColorGreen, GColorBlack);
  #endif
  menu_layer_set_callbacks(s_menu_layer, NULL, (MenuLayerCallbacks) {
      .get_num_rows = get_num_rows_callback,
      .draw_row = draw_row_callback,
      .get_cell_height = get_cell_height_callback,
      .select_click = select_callback,
  });
  layer_add_child(window_layer, menu_layer_get_layer(s_menu_layer));
}

static void data_provider_window_unload(Window *window) {
  menu_layer_destroy(s_menu_layer);

  window_destroy(window);
  dataProviderWindow = NULL;
}

void showDataProviderWindow() {
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing Data Provider Window.");
  s_current_selection = convertProviderToIndex(current_data_provider);
  if(!dataProviderWindow) {
    dataProviderWindow = window_create();
    window_set_window_handlers(dataProviderWindow, (WindowHandlers) {
        .load = data_provider_window_load,
        .unload = data_provider_window_unload,
    });
  }
  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing Data Provider Window. %p", dataProviderWindow);
  window_stack_push(dataProviderWindow, true);
}

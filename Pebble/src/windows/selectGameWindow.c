#include <pebble.h>

#include "globals.h"

static Window* selectWindow;

static GColor textColour;
static GColor backgroundColour;

// the current selection, not the current match being displayed
static char matchId[MAX_MATCH_ID_LENGTH];

static void showLoading() {
  text_layer_set_text(select_team1_text, "loading...");
  text_layer_set_text(select_vs_text, "V");
  text_layer_set_text(select_team2_text, "loading...");
}

void set_current_selection_match_id(const char* id) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Current selection id %s", id);
  strncpy(matchId, id, MAX_MATCH_ID_LENGTH);
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Select - select");
  // update current selected id
  strncpy(current_match_id, matchId, MAX_MATCH_ID_LENGTH);
  send_to_phone(current_match_id, ACTION_SELECT_MATCH);
  window_stack_pop(true);   // remove this window
  window_stack_pop(true);   // remove the menu window when we have selected
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Up - select");
  showLoading();
  send_to_phone(matchId, ACTION_SELECT_PREVIOUS);
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Down - select");
  showLoading();
  send_to_phone(matchId, ACTION_SELECT_NEXT);
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

void gameSelectUnload(Window* win)
{
	APP_LOG(APP_LOG_LEVEL_INFO, "Destroying Game Select Window.");
  text_layer_destroy(select_title_text);
  text_layer_destroy(select_team1_text);
  text_layer_destroy(select_vs_text);
  text_layer_destroy(select_team2_text);
  select_title_text = NULL;
  select_team1_text = NULL;
  select_vs_text = NULL;
  select_team2_text = NULL;
	window_destroy(selectWindow);
}

void showGameSelectWindow()
{
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing Game Select Window.");

  textColour = GColorGreen;
  backgroundColour = GColorBlack;

  // get the current selected id
  strncpy(matchId, current_match_id, MAX_MATCH_ID_LENGTH);

	WindowHandlers wh = { .unload = &gameSelectUnload };
	selectWindow = window_create();
  Layer *window_layer = window_get_root_layer(selectWindow);
  GRect bounds = layer_get_bounds(window_layer);

  window_set_background_color(selectWindow, backgroundColour);
  window_set_click_config_provider(selectWindow, click_config_provider);
	window_set_window_handlers(selectWindow, wh);

  select_title_text = text_layer_create((GRect) {
     .origin = { 0, 0 },
     .size = { bounds.size.w, 40 }
  });
  text_layer_set_text_color(select_title_text, textColour);
  text_layer_set_background_color(select_title_text, GColorClear);
	text_layer_set_text_alignment(select_title_text, GTextAlignmentCenter); // Center the text.
	text_layer_set_font(select_title_text, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
  text_layer_set_text(select_title_text, "Select Game");

  select_team1_text = text_layer_create((GRect) {
     .origin = { 0, 20 },
     .size = { bounds.size.w, 60 }
  });
  text_layer_set_text_color(select_team1_text, textColour);
  text_layer_set_background_color(select_team1_text, GColorClear);
	text_layer_set_text_alignment(select_team1_text, GTextAlignmentCenter); // Center the text.
	text_layer_set_font(select_team1_text, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));

  select_vs_text = text_layer_create((GRect) {
     .origin = { 0, 80 },
     .size = { bounds.size.w, 60 }
  });
  text_layer_set_text_color(select_vs_text, textColour);
  text_layer_set_background_color(select_vs_text, GColorClear);
	text_layer_set_text_alignment(select_vs_text, GTextAlignmentCenter); // Center the text.
	text_layer_set_font(select_vs_text, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));

  select_team2_text = text_layer_create((GRect) {
     .origin = { 0, 100 },
     .size = { bounds.size.w, 60 }
  });
  text_layer_set_text_color(select_team2_text, textColour);
  text_layer_set_background_color(select_team2_text, GColorClear);
	text_layer_set_text_alignment(select_team2_text, GTextAlignmentCenter); // Center the text.
	text_layer_set_font(select_team2_text, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));

  showLoading();

  layer_add_child(window_layer, text_layer_get_layer(select_title_text));
  layer_add_child(window_layer, text_layer_get_layer(select_team1_text));
  layer_add_child(window_layer, text_layer_get_layer(select_vs_text));
  layer_add_child(window_layer, text_layer_get_layer(select_team2_text));

  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing Game Select Window. %p", selectWindow);
	window_stack_push(selectWindow, true); // The back button will dismiss the current window, not close the app.  So just press back to go back to the master view.

  send_to_phone(matchId, ACTION_SELECT_REFRESH);
}

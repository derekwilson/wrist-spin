#include <pebble.h>

#include "globals.h"

static Window *aboutWindow;

static char aboutText[200];

static TextLayer* about_text_layer;
static GColor textColour;
static GColor backgroundColour;

static void about_window_load(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Load About Window. %p", window);
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  about_text_layer = text_layer_create((GRect) {
    .origin = { 0, 20 },
    .size = { bounds.size.w, bounds.size.h - 20 }
  });
  text_layer_set_text_color(about_text_layer, textColour);
  text_layer_set_background_color(about_text_layer, GColorClear);
  text_layer_set_font(about_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
  text_layer_set_text_alignment(about_text_layer, GTextAlignmentCenter);

  APP_LOG(APP_LOG_LEVEL_INFO, "version %s", get_version());
  strcpy(aboutText, "wrist-spin\nv");
  strcat(aboutText, get_version());
#ifdef RELEASE_BUILD
  strcat(aboutText, "\n(Prod)");
#else
  strcat(aboutText, "\n(Dev)");
#endif
  strcat(aboutText, "\n");
  strcat(aboutText, current_data_provider);
  APP_LOG(APP_LOG_LEVEL_INFO, "about %s", aboutText);
  text_layer_set_text(about_text_layer, aboutText);

  layer_add_child(window_layer, text_layer_get_layer(about_text_layer));
}

static void about_window_unload(Window *window) {
  text_layer_destroy(about_text_layer);
  about_text_layer = NULL;
}

void showAboutWindow() {
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing About Window.");
  textColour = GColorGreen;
  backgroundColour = GColorBlack;
  aboutWindow = window_create();
  window_set_background_color(aboutWindow, backgroundColour);
  window_set_window_handlers(aboutWindow, (WindowHandlers) {
    .load = about_window_load,
    .unload = about_window_unload,
  });

  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing About Window. %p", aboutWindow);
  const bool animated = true;
  window_stack_push(aboutWindow, animated);
}

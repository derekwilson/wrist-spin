#include <pebble.h>

#include "globals.h"

static Window *detailWindow = NULL;

static GColor textColour;
static GColor backgroundColour;

static void showLoading() {
  if (detail_score_text_layer != NULL) {
    text_layer_set_text(detail_score_text_layer, "loading...");
  }
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Detail - Select");
  showMenuWindow();
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Detail - Up");
  window_stack_pop(true);   // remove this window
  showMainWindow();
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Detail - Down");
  window_stack_pop(true);   // remove this window
  showWatchWindow();
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void detail_window_load(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Load Details Window. %p", window);
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  #if defined(PBL_ROUND)
    int top_margin = 20;
    int left_margin = 15;
    int double_left_margin = 45;
    int right_margin = 15;
    int double_right_margin = 45;
    int bottom_margin = 15;
  #else // square
    int top_margin = 0;
    int left_margin = 0;
    int double_left_margin = 0;
    int right_margin = 0;
    int double_right_margin = 0;
    int bottom_margin = 0;
  #endif
  // 144x168

  detail_team_text_layer = text_layer_create((GRect) {
    .origin = { 0, -6 },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_team_text_layer, textColour);
  text_layer_set_background_color(detail_team_text_layer, GColorClear);
  text_layer_set_font(detail_team_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
#if defined(PBL_ROUND)
  text_layer_set_text_alignment(detail_team_text_layer, GTextAlignmentCenter);
#else
  text_layer_set_text_alignment(detail_team_text_layer, GTextAlignmentLeft);
#endif
  layer_add_child(window_layer, text_layer_get_layer(detail_team_text_layer));

  detail_score_text_layer = text_layer_create((GRect) {
    .origin = { 0, -6 + top_margin },
    .size = { bounds.size.w, 32 }
  });
  text_layer_set_text_color(detail_score_text_layer, textColour);
  text_layer_set_background_color(detail_score_text_layer, GColorClear);
  text_layer_set_font(detail_score_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  text_layer_set_text_alignment(detail_score_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(detail_score_text_layer));

  // overs go top right for a square display and bottom left on a round display
#if defined(PBL_ROUND)
  detail_overs_text_layer = text_layer_create((GRect) {
    .origin = { 0 + double_left_margin, bounds.size.h - (20+bottom_margin) },
    .size = { bounds.size.w - double_right_margin, 20 }
  });
  text_layer_set_font(detail_overs_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(detail_overs_text_layer, GTextAlignmentLeft);
#else // square
  detail_overs_text_layer = text_layer_create((GRect) {
    .origin = { 0, -6 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_font(detail_overs_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_overs_text_layer, GTextAlignmentRight);
#endif
  text_layer_set_text_color(detail_overs_text_layer, textColour);
  text_layer_set_background_color(detail_overs_text_layer, GColorClear);
  layer_add_child(window_layer, text_layer_get_layer(detail_overs_text_layer));

  detail_lead_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 22 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_lead_text_layer, textColour);
  text_layer_set_background_color(detail_lead_text_layer, GColorClear);
  text_layer_set_font(detail_lead_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(detail_lead_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(detail_lead_text_layer));

  detail_striker_name_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 44 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_striker_name_text_layer, textColour);
  text_layer_set_background_color(detail_striker_name_text_layer, GColorClear);
  text_layer_set_font(detail_striker_name_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_striker_name_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(detail_striker_name_text_layer));

  detail_striker_stats_text_layer = text_layer_create((GRect) {
    .origin = { 0, 44 + top_margin },
    .size = { bounds.size.w - right_margin, 28 }
  });
  text_layer_set_text_color(detail_striker_stats_text_layer, textColour);
  text_layer_set_background_color(detail_striker_stats_text_layer, GColorClear);
  text_layer_set_font(detail_striker_stats_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_striker_stats_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(detail_striker_stats_text_layer));

  detail_nonstriker_name_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 68 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_nonstriker_name_text_layer, textColour);
  text_layer_set_background_color(detail_nonstriker_name_text_layer, GColorClear);
  text_layer_set_font(detail_nonstriker_name_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_nonstriker_name_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(detail_nonstriker_name_text_layer));

  detail_nonstriker_stats_text_layer = text_layer_create((GRect) {
    .origin = { 0, 68 + top_margin },
    .size = { bounds.size.w - right_margin, 28 }
  });
  text_layer_set_text_color(detail_nonstriker_stats_text_layer, textColour);
  text_layer_set_background_color(detail_nonstriker_stats_text_layer, GColorClear);
  text_layer_set_font(detail_nonstriker_stats_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_nonstriker_stats_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(detail_nonstriker_stats_text_layer));

  detail_bowler_name_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 96 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_bowler_name_text_layer, textColour);
  text_layer_set_background_color(detail_bowler_name_text_layer, GColorClear);
  text_layer_set_font(detail_bowler_name_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_bowler_name_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(detail_bowler_name_text_layer));

  detail_bowler_stats_text_layer = text_layer_create((GRect) {
    .origin = { 0, 96 + top_margin },
    .size = { bounds.size.w - right_margin, 28 }
  });
  text_layer_set_text_color(detail_bowler_stats_text_layer, textColour);
  text_layer_set_background_color(detail_bowler_stats_text_layer, GColorClear);
  text_layer_set_font(detail_bowler_stats_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(detail_bowler_stats_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(detail_bowler_stats_text_layer));

  detail_time_text_layer = text_layer_create((GRect) {
    .origin = { 0, bounds.size.h - (20+bottom_margin) },
    .size = { bounds.size.w - double_right_margin, 20 }
  });
  text_layer_set_text_color(detail_time_text_layer, textColour);
  text_layer_set_background_color(detail_time_text_layer, GColorClear);
  text_layer_set_font(detail_time_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(detail_time_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(detail_time_text_layer));

  detail_app_status_text_layer = text_layer_create((GRect) {
    .origin = { double_left_margin, bounds.size.h - (20+bottom_margin) },
    .size = { bounds.size.w - double_left_margin, 20 }
  });
  text_layer_set_text_color(detail_app_status_text_layer, textColour);
  text_layer_set_background_color(detail_app_status_text_layer, GColorClear);
  text_layer_set_font(detail_app_status_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(detail_app_status_text_layer, GTextAlignmentLeft);
  // there is not enough room on the round
#if ! defined(PBL_ROUND)
  layer_add_child(window_layer, text_layer_get_layer(detail_app_status_text_layer));
#endif

  detail_fact_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 122 + top_margin },
    .size = { bounds.size.w, 28 }
  });
  text_layer_set_text_color(detail_fact_text_layer, textColour);
  text_layer_set_background_color(detail_fact_text_layer, GColorClear);
  text_layer_set_font(detail_fact_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(detail_fact_text_layer, GTextAlignmentLeft);
  // there is not enough room on the round
#if ! defined(PBL_ROUND)
  layer_add_child(window_layer, text_layer_get_layer(detail_fact_text_layer));
#endif
  showLoading();
}

static void detail_window_unload(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Unload Details Window. %p", window);

  text_layer_destroy(detail_score_text_layer);
  text_layer_destroy(detail_lead_text_layer);
  text_layer_destroy(detail_striker_name_text_layer);
  text_layer_destroy(detail_striker_stats_text_layer);
  text_layer_destroy(detail_nonstriker_name_text_layer);
  text_layer_destroy(detail_nonstriker_stats_text_layer);
  text_layer_destroy(detail_bowler_name_text_layer);
  text_layer_destroy(detail_bowler_stats_text_layer);
  text_layer_destroy(detail_time_text_layer);
  text_layer_destroy(detail_fact_text_layer);
  text_layer_destroy(detail_app_status_text_layer);

  detail_score_text_layer = NULL;
  detail_overs_text_layer = NULL;
  detail_lead_text_layer = NULL;
  detail_striker_name_text_layer = NULL;
  detail_striker_stats_text_layer = NULL;
  detail_nonstriker_name_text_layer = NULL;
  detail_nonstriker_stats_text_layer = NULL;
  detail_bowler_name_text_layer = NULL;
  detail_bowler_stats_text_layer = NULL;
  detail_time_text_layer = NULL;
  detail_fact_text_layer = NULL;
  detail_app_status_text_layer = NULL;
}

void showDetailWindow() {
  closeDetailWindow();    // if it was open
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing Details Window.");
  textColour = GColorCyan;
  backgroundColour = GColorBlack;
  detailWindow = window_create();
  window_set_background_color(detailWindow, backgroundColour);
  window_set_click_config_provider(detailWindow, click_config_provider);
  window_set_window_handlers(detailWindow, (WindowHandlers) {
    .load = detail_window_load,
    .unload = detail_window_unload,
  });

  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing Details Window. %p", detailWindow);
  const bool animated = true;
  window_stack_push(detailWindow, animated);

  current_display = DISPLAY_DETAIL;

  // get the scores refreshed
  kick_timer_tick();
}

void closeDetailWindow(void) {
  if (detailWindow != NULL) {
    window_destroy(detailWindow);
  }
  detailWindow = NULL;
}

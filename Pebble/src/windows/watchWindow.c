#include <pebble.h>

#include "globals.h"

static Window *watchWindow = NULL;

static GFont watchFont;
static GColor textColour;
static GColor backgroundColour;

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Watch - Select");
  showMenuWindow();
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Watch - Up");
  window_stack_pop(true);   // remove this window
  showDetailWindow();
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Watch - Down");
  window_stack_pop(true);   // remove this window
  showMainWindow();
}

static void watch_window_click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void watch_window_load(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Load Watch Window. %p", window);
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  #if defined(PBL_ROUND)
    int bottom_margin = 15;
  #else // square
    int bottom_margin = 0;
  #endif

  watchFont = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_OSP_DIN_65));

  watch_score_text_layer = text_layer_create((GRect) {
    .origin = { 0, (bounds.size.h / 2) - 80},
    .size = { bounds.size.w, 70 }
  });
  text_layer_set_text_color(watch_score_text_layer, textColour);
  text_layer_set_background_color(watch_score_text_layer, GColorClear);
  text_layer_set_font(watch_score_text_layer, watchFont);
  text_layer_set_text_alignment(watch_score_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(watch_score_text_layer));

  watch_time_text_layer = text_layer_create((GRect) {
     .origin = { 0, (bounds.size.h / 2) - 10 },
     .size = { bounds.size.w, 70 }
  });
  text_layer_set_text_color(watch_time_text_layer, textColour);
  text_layer_set_background_color(watch_time_text_layer, GColorClear);
  text_layer_set_font(watch_time_text_layer, watchFont);
  text_layer_set_text_alignment(watch_time_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(watch_time_text_layer));

  watch_app_status_text_layer = text_layer_create((GRect) {
    .origin = { 0, bounds.size.h - (20+bottom_margin) },
    .size = { bounds.size.w, 20 }
  });
  text_layer_set_text_color(watch_app_status_text_layer, textColour);
  text_layer_set_background_color(watch_app_status_text_layer, backgroundColour);
  text_layer_set_font(watch_app_status_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_text_alignment(watch_app_status_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(watch_app_status_text_layer));
}

static void watch_window_unload(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Unload Watch Window. %p", window);

  text_layer_destroy(watch_score_text_layer);
  text_layer_destroy(watch_time_text_layer);
  text_layer_destroy(watch_app_status_text_layer);

  watch_score_text_layer = NULL;
  watch_time_text_layer = NULL;
  watch_app_status_text_layer = NULL;

  // Unload custom font
  fonts_unload_custom_font(watchFont);
  watchFont = NULL;
}

void showWatchWindow() {
  closeWatchWindow();
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing Watch Window.");
  textColour = GColorCyan;
  backgroundColour = GColorBlack;
  watchWindow = window_create();
  window_set_background_color(watchWindow, backgroundColour);
  window_set_click_config_provider(watchWindow, watch_window_click_config_provider);
  window_set_window_handlers(watchWindow, (WindowHandlers) {
    .load = watch_window_load,
    .unload = watch_window_unload,
  });

  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing Watch Window. %p", watchWindow);
  const bool animated = true;
  window_stack_push(watchWindow, animated);

  current_display = DISPLAY_WATCH;

  // get the scores refreshed
  kick_timer_tick();
}

void closeWatchWindow(void) {
  if (watchWindow != NULL) {
    window_destroy(watchWindow);
  }
  watchWindow = NULL;
}

#include <pebble.h>

#include "globals.h"

static Window *window = NULL;

static GFont mainScoreFont;
static GColor textColour;
static GColor backgroundColour;

static void showLoading() {
  if (main_lead_text_layer != NULL) {
    text_layer_set_text(main_lead_text_layer, "loading...");
  }
}

static void select_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Select");
  showMenuWindow();
}

static void up_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Up");
  window_stack_pop(true);   // remove this window
  showWatchWindow();
}

static void down_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Down");
  window_stack_pop(true);   // remove this window
  showDetailWindow();
}

static void main_window_click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);
}

static void main_window_load(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Load Main Window. %p", window);
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  mainScoreFont = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_ANONYMOUS_PRO_BOLD_50));

#if defined(PBL_ROUND)
  int top_margin = 10;
  int left_margin = 12;
  int double_left_margin = 22;
  int right_margin = 12;
#else // square
  int top_margin = 0;
  int left_margin = 0;
  int double_left_margin = 0;
  int right_margin = 0;
#endif
  // 144x168

  main_score_text_layer = text_layer_create((GRect) {
    .origin = { 0, -6 + top_margin },
    .size = { bounds.size.w, 56 }
  });
  text_layer_set_text_color(main_score_text_layer, textColour);
  text_layer_set_background_color(main_score_text_layer, GColorClear);
  text_layer_set_font(main_score_text_layer, mainScoreFont);
  text_layer_set_text_alignment(main_score_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(main_score_text_layer));

  main_striker_name_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 50 + top_margin },
    .size = { bounds.size.w-(35+right_margin), 32 }
  });
  text_layer_set_text_color(main_striker_name_text_layer, textColour);
  text_layer_set_background_color(main_striker_name_text_layer, GColorClear);
  text_layer_set_font(main_striker_name_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  text_layer_set_text_alignment(main_striker_name_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(main_striker_name_text_layer));

  main_striker_stats_text_layer = text_layer_create((GRect) {
    .origin = { 0, 50 + top_margin },
    .size = { bounds.size.w - right_margin, 32 }
  });
  text_layer_set_text_color(main_striker_stats_text_layer, textColour);
  text_layer_set_background_color(main_striker_stats_text_layer, GColorClear);
  text_layer_set_font(main_striker_stats_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  text_layer_set_text_alignment(main_striker_stats_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(main_striker_stats_text_layer));

  main_nonstriker_name_text_layer = text_layer_create((GRect) {
    .origin = { 0 + left_margin, 82 + top_margin },
    .size = { bounds.size.w-(35+right_margin), 32 }
  });
  text_layer_set_text_color(main_nonstriker_name_text_layer, textColour);
  text_layer_set_background_color(main_nonstriker_name_text_layer, GColorClear);
  text_layer_set_font(main_nonstriker_name_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  text_layer_set_text_alignment(main_nonstriker_name_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(main_nonstriker_name_text_layer));

  main_nonstriker_stats_text_layer = text_layer_create((GRect) {
    .origin = { 0, 82 + top_margin },
    .size = { bounds.size.w - right_margin, 32 }
  });
  text_layer_set_text_color(main_nonstriker_stats_text_layer, textColour);
  text_layer_set_background_color(main_nonstriker_stats_text_layer, GColorClear);
  text_layer_set_font(main_nonstriker_stats_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  text_layer_set_text_alignment(main_nonstriker_stats_text_layer, GTextAlignmentRight);
  layer_add_child(window_layer, text_layer_get_layer(main_nonstriker_stats_text_layer));

  main_lead_text_layer = text_layer_create((GRect) {
    .origin = { 0 + double_left_margin, 115 + top_margin },
    .size = { bounds.size.w-right_margin, 28 }
  });
  text_layer_set_text_color(main_lead_text_layer, textColour);
  text_layer_set_background_color(main_lead_text_layer, GColorClear);
  text_layer_set_font(main_lead_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(main_lead_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(main_lead_text_layer));

  main_app_status_text_layer = text_layer_create((GRect) {
    .origin = { 0 + double_left_margin, 115 + top_margin },
    .size = { bounds.size.w-right_margin, 28 }
  });
  text_layer_set_text_color(main_app_status_text_layer, textColour);
  text_layer_set_background_color(main_app_status_text_layer, backgroundColour);
  text_layer_set_font(main_app_status_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(main_app_status_text_layer, GTextAlignmentLeft);
  layer_add_child(window_layer, text_layer_get_layer(main_app_status_text_layer));

  #if defined(PBL_ROUND)
    // there isnt emough room on a round device
    main_overs_text_layer = text_layer_create(GRect(0, 0, 0, 0));
    text_layer_set_font(main_overs_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
    text_layer_set_text_alignment(main_overs_text_layer, GTextAlignmentLeft);
    //layer_add_child(window_layer, text_layer_get_layer(main_overs_text_layer));

    main_time_text_layer = text_layer_create((GRect) {
      .origin = { 0, bounds.size.h -35 },
      .size = { bounds.size.w, 28 }
    });
    text_layer_set_font(main_time_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
    text_layer_set_text_alignment(main_time_text_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, text_layer_get_layer(main_time_text_layer));
  #else // square
    main_overs_text_layer = text_layer_create((GRect) {
      .origin = { 4, bounds.size.h - 28 },
      .size = { bounds.size.w - 4, 28 }
    });
    main_overs_text_layer = text_layer_create(GRect(4, 140, 140, 28));
    text_layer_set_font(main_overs_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
    text_layer_set_text_alignment(main_overs_text_layer, GTextAlignmentLeft);
    layer_add_child(window_layer, text_layer_get_layer(main_overs_text_layer));

    main_time_text_layer = text_layer_create((GRect) {
      .origin = { 0, bounds.size.h - 28 },
      .size = { bounds.size.w - 4, 28 }
    });
    text_layer_set_font(main_time_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
    text_layer_set_text_alignment(main_time_text_layer, GTextAlignmentRight);
    layer_add_child(window_layer, text_layer_get_layer(main_time_text_layer));
  #endif

  text_layer_set_text_color(main_overs_text_layer, textColour);
  text_layer_set_background_color(main_overs_text_layer, GColorClear);

  text_layer_set_text_color(main_time_text_layer, textColour);
  text_layer_set_background_color(main_time_text_layer, GColorClear);

  showLoading();
}

static void main_window_unload(Window *window) {
  APP_LOG(APP_LOG_LEVEL_INFO, "Unload Main Window. %p", window);

  text_layer_destroy(main_score_text_layer);
  text_layer_destroy(main_lead_text_layer);
  text_layer_destroy(main_striker_name_text_layer);
  text_layer_destroy(main_striker_stats_text_layer);
  text_layer_destroy(main_nonstriker_name_text_layer);
  text_layer_destroy(main_nonstriker_stats_text_layer);
  text_layer_destroy(main_time_text_layer);
  text_layer_destroy(main_app_status_text_layer);

  main_score_text_layer = NULL;
  main_lead_text_layer = NULL;
  main_striker_name_text_layer = NULL;
  main_striker_stats_text_layer = NULL;
  main_nonstriker_name_text_layer = NULL;
  main_nonstriker_stats_text_layer = NULL;
  main_time_text_layer = NULL;
  main_app_status_text_layer = NULL;

  // Unload custom font
  fonts_unload_custom_font(mainScoreFont);
  mainScoreFont = NULL;
}

void showMainWindow(void) {
  closeMainWindow();    // if it was open
  textColour = GColorCyan;
  backgroundColour = GColorBlack;
  window = window_create();
  window_set_background_color(window, backgroundColour);
  window_set_click_config_provider(window, main_window_click_config_provider);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = main_window_load,
    .unload = main_window_unload,
  });

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Pushed main window: %p", window);
  const bool animated = true;
  window_stack_push(window, animated);

  current_display = DISPLAY_MAIN;

  // get the scores refreshed
  kick_timer_tick();
}

void closeMainWindow(void) {
  if (window != NULL) {
    window_destroy(window);
  }
  window = NULL;
}

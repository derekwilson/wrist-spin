#include <pebble.h>

#include "globals.h"

static Window *settingsWindow;
static MenuLayer *s_menu_layer;

#define CHECKBOX_WINDOW_NUM_ROWS    3
#define CHECKBOX_WINDOW_BOX_SIZE    12
#define CHECKBOX_WINDOW_CELL_HEIGHT 44

static GBitmap *s_tick_black_bitmap, *s_tick_white_bitmap;

static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *context) {
  return CHECKBOX_WINDOW_NUM_ROWS;
}

static char* convertIndexToSetting(int index) {
  switch (index) {
    case 0:
      return "Wicket vibrate";
    case 1:
      return "BT lost vibrate";
    case 2:
      return "Vibrate on exit";
  }
  return "UNKNOWN";
}

static bool getIndexSwitch(int index) {
  switch (index) {
    case 0:
      return current_vibrate_on_wicket;
    case 1:
      return current_vibrate_on_phone_disconnect;
    case 2:
      return current_vibrate_on_exit;
  }
  return false;
}

static void setIndexSwitch(int index, bool settingValue) {
  switch (index) {
    case 0:
      current_vibrate_on_wicket = settingValue;
      break;
    case 1:
      current_vibrate_on_phone_disconnect = settingValue;
      break;
    case 2:
      current_vibrate_on_exit = settingValue;
      break;
  }
}

static void draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *context) {
  // Choice item
  static char s_buff[16];
  snprintf(s_buff, sizeof(s_buff), convertIndexToSetting(cell_index->row));
  menu_cell_basic_draw(ctx, cell_layer, s_buff, NULL, NULL);

  // Selected?
  GBitmap *ptr = s_tick_white_bitmap;
#if defined(PBL_COLOR)
  if (menu_cell_layer_is_highlighted(cell_layer)) {
    graphics_context_set_stroke_color(ctx, GColorBlack);
    ptr = s_tick_black_bitmap;
  }
  else {
    graphics_context_set_stroke_color(ctx, GColorWhite);
    ptr = s_tick_white_bitmap;
  }
#else
  if(menu_cell_layer_is_highlighted(cell_layer)) {
    graphics_context_set_stroke_color(ctx, GColorWhite);
    ptr = s_tick_white_bitmap;
  }
  else {
    graphics_context_set_stroke_color(ctx, GColorBlack);
    ptr = s_tick_black_bitmap;
  }
#endif


  GRect bounds = layer_get_bounds(cell_layer);
  GRect bitmap_bounds = gbitmap_get_bounds(ptr);
  GRect r = GRect(
    bounds.size.w - (2 * CHECKBOX_WINDOW_BOX_SIZE),
    (bounds.size.h / 2) - (CHECKBOX_WINDOW_BOX_SIZE / 2),
    CHECKBOX_WINDOW_BOX_SIZE, CHECKBOX_WINDOW_BOX_SIZE);
  graphics_draw_rect(ctx, r);
  if (getIndexSwitch(cell_index->row)) {
    graphics_context_set_compositing_mode(ctx, GCompOpSet);
    graphics_draw_bitmap_in_rect(ctx, ptr, GRect(r.origin.x, r.origin.y - 3, bitmap_bounds.size.w, bitmap_bounds.size.h));
  }
}

static int16_t get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *context) {
  return PBL_IF_ROUND_ELSE(
    menu_layer_is_index_selected(menu_layer, cell_index) ?
      MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT : MENU_CELL_ROUND_UNFOCUSED_TALL_CELL_HEIGHT,
      CHECKBOX_WINDOW_CELL_HEIGHT);
}

static void select_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {
  // Check/uncheck
  int row = cell_index->row;
  setIndexSwitch(row, !getIndexSwitch(row));
  menu_layer_reload_data(menu_layer);
}

static void settings_window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_tick_black_bitmap = gbitmap_create_with_resource(RESOURCE_ID_TICK_BLACK);
  s_tick_white_bitmap = gbitmap_create_with_resource(RESOURCE_ID_TICK_WHITE);

  s_menu_layer = menu_layer_create(bounds);
  menu_layer_set_click_config_onto_window(s_menu_layer, window);
  #if defined(PBL_COLOR)
    menu_layer_set_normal_colors(s_menu_layer, GColorBlack, GColorWhite);
    menu_layer_set_highlight_colors(s_menu_layer, GColorGreen, GColorBlack);
  #endif
  menu_layer_set_callbacks(s_menu_layer, NULL, (MenuLayerCallbacks) {
      .get_num_rows = get_num_rows_callback,
      .draw_row = draw_row_callback,
      .get_cell_height = get_cell_height_callback,
      .select_click = select_callback,
  });
  layer_add_child(window_layer, menu_layer_get_layer(s_menu_layer));
}

static void settings_window_unload(Window *window) {
  menu_layer_destroy(s_menu_layer);
  
  gbitmap_destroy(s_tick_black_bitmap);
  gbitmap_destroy(s_tick_white_bitmap);

  window_destroy(window);
  settingsWindow = NULL;
}

void showSettingsWindow() {
  APP_LOG(APP_LOG_LEVEL_INFO, "Showing Settings Window.");
  if (!settingsWindow) {
    settingsWindow = window_create();
    window_set_window_handlers(settingsWindow, (WindowHandlers) {
        .load = settings_window_load,
        .unload = settings_window_unload,
    });
  }
  APP_LOG(APP_LOG_LEVEL_INFO, "Pushing Settings Window. %p", settingsWindow);
  window_stack_push(settingsWindow, true);
}

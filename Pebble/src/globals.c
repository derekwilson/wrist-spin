#include <pebble.h>

#include "globals.h"

// persistent application settings
char current_match_id[MAX_MATCH_ID_LENGTH];
char current_data_provider[MAX_DATA_PROVIDER_LENGTH];
bool current_vibrate_on_wicket;
bool current_vibrate_on_phone_disconnect;
bool current_vibrate_on_exit;
int current_display;

// main display
TextLayer* main_score_text_layer;
TextLayer* main_overs_text_layer;
TextLayer* main_lead_text_layer;
TextLayer* main_striker_name_text_layer;
TextLayer* main_striker_stats_text_layer;
TextLayer* main_nonstriker_name_text_layer;
TextLayer* main_nonstriker_stats_text_layer;
TextLayer* main_time_text_layer;
TextLayer* main_app_status_text_layer;

// watch veiw
TextLayer* watch_score_text_layer;
TextLayer* watch_time_text_layer;
TextLayer* watch_app_status_text_layer;

// details windows
TextLayer* detail_team_text_layer;
TextLayer* detail_score_text_layer;
TextLayer* detail_overs_text_layer;
TextLayer* detail_lead_text_layer;
TextLayer* detail_striker_name_text_layer;
TextLayer* detail_striker_stats_text_layer;
TextLayer* detail_nonstriker_name_text_layer;
TextLayer* detail_nonstriker_stats_text_layer;
TextLayer* detail_bowler_name_text_layer;
TextLayer* detail_bowler_stats_text_layer;
TextLayer* detail_time_text_layer;
TextLayer* detail_fact_text_layer;
TextLayer* detail_app_status_text_layer;

// match selection
TextLayer* select_title_text;
TextLayer* select_team1_text;
TextLayer* select_vs_text;
TextLayer* select_team2_text;
